FROM node:10
COPY dist/ftetrack dist/ftetrack/
WORKDIR dist/ftetrack/
EXPOSE 3000
CMD npm start -D FOREGROUND --server.port=3000

