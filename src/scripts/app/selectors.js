import { createSelector } from "reselect";
import * as selectors from "../redux/selectors";

export const getLoginData = () =>
  createSelector(selectors.appState, (appState) =>
    appState.get("userData").toJS()
  );

export const getFullScreenDialog = () =>
  createSelector(selectors.appState, (appState) =>
    appState.get("fullScreenDialog")
  );

export const getProgressingValue = () =>
  createSelector(selectors.appState, (appState) => appState.get("progressing"));

export const getSearchProgressingValue = () =>
  createSelector(selectors.appState, (appState) =>
    appState.get("searchProgressing")
  );

export const getSearchList = () =>
  createSelector(selectors.appState, (appState) =>
    appState.get("searchList").toJS()
  );

export const getSchoolList = () =>
  createSelector(selectors.appState, (appState) =>
    appState.get("schoolList").toJS()
  );

export const getSummaryList = () =>
  createSelector(selectors.appState, (appState) =>
    appState.get("summaryList").toJS()
  );

export const getStudentDetails = () =>
  createSelector(selectors.appState, (appState) =>
    appState.get("studentDetails").toJS()
  );
