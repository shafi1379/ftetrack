import { fromJS } from "immutable";
import {
  SET_USER_LOGIN,
  LOGOUT_USER,
  TOGGLE_FULLSCREEN_DIALOG,
  SET_PROGRESSING_VALUE,
  SET_SEARCH_LIST,
  SET_SEARCH_PROGRESSING_VALUE,
  SET_STUDENTS_DETAILS,
  SET_SCHOOL_LIST,
  SET_SUMMARY_LIST,
} from "./actions";

const initialState = fromJS({
  userData: {},
  schoolList: [],
  summaryList: {},
  fullScreenDialog: false,
  progressing: false,
  searchProgressing: false,
  searchList: [],
  studentDetails: {},
});

const login = (state = initialState, action) => {
  switch (action.type) {
    case SET_USER_LOGIN:
      return state.set("userData", fromJS({ ...action.userData }));
    case LOGOUT_USER:
      return state.set("userData", fromJS({}));
    case TOGGLE_FULLSCREEN_DIALOG:
      return state.set("fullScreenDialog", action.open);
    case SET_PROGRESSING_VALUE:
      return state.set("progressing", action.progressing);
    case SET_SEARCH_PROGRESSING_VALUE:
      return state.set("searchProgressing", action.progressing);
    case SET_SEARCH_LIST:
      return state.set("searchList", fromJS([...action.searchList]));
    case SET_SCHOOL_LIST:
      return state.set("schoolList", fromJS([...action.schoolList]));
    case SET_SUMMARY_LIST:
      return state.set("summaryList", fromJS({ ...action.summaryList }));
    case SET_STUDENTS_DETAILS:
      return state.set("studentDetails", fromJS({ ...action.studentDetails }));
    default:
      return state;
  }
};

export default login;
