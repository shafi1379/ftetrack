import { all, call, fork, put, takeLatest } from "redux-saga/effects";
import {
  LOAD_USER_LOGIN,
  setUserLoginData,
  LOAD_SEARCH_LIST,
  setSearchList,
  setSearchProgressingValue,
  LOAD_STUDENT_DETAILS,
  setProgressingValue,
  setStudentDetails,
  setSchoolList,
  setSummaryList,
  LOAD_SUMMARY_DETAILS,
} from "./actions";
import * as API from "../services/api";
import { API_URLS } from "../utilities/constants";

function* userLoginRequested({ request }) {
  try {
    const response = yield call(API.doPostRequest, request.url, request.params);
    if (response) {
      if (response.loginData == null) {
        window.location.href = "../index.php";
      }
      yield put(setUserLoginData(response.loginData));
      const schoolList = response.schools.map((res) => ({
        ...res,
        label: res.schoolname,
        value: res.school,
      }));
      yield put(setSchoolList(schoolList));
      yield put(setSummaryList(response.summary));
    }
  } catch (error) {
    console.error(error);
  }
}

function* userLoginListener() {
  yield takeLatest(LOAD_USER_LOGIN, userLoginRequested);
}

function* searchRequested(params) {
  try {
    const response = yield call(API.doPostRequest, API_URLS.search, {
      school: params.data.school,
      name: params.data.name,
    });
    if (response) {
      yield put(setSearchList(response));
      yield put(setSearchProgressingValue(false));
    }
  } catch (error) {
    console.error(error);
  } finally {
    yield put(setSearchProgressingValue(false));
  }
}

function* searchListListener() {
  yield takeLatest(LOAD_SEARCH_LIST, searchRequested);
}

function* loadStudentDetails({ request }) {
  try {
    yield put(setProgressingValue(true));
    const response = yield call(
      API.doPostRequest,
      API_URLS.studentsDetails,
      request
    );
    if (response) {
      yield put(setProgressingValue(false));
      yield put(setStudentDetails(response));
    }
  } catch (error) {
    console.error(error);
  }
}

function* studentDetailsListener() {
  yield takeLatest(LOAD_STUDENT_DETAILS, loadStudentDetails);
}

function* summaryDetailsRequested({ request }) {
  try {
    const response = yield call(API.doPostRequest, request.url, request.params);
    if (response) {
      yield put(setSummaryList(response));
      yield put(setProgressingValue(false));
    }
  } catch (error) {
    console.error(error);
    yield put(setProgressingValue(false));
  }
}

function* summaryDetailsListener() {
  yield takeLatest(LOAD_SUMMARY_DETAILS, summaryDetailsRequested);
}

export default function* root() {
  yield all([
    fork(userLoginListener),
    fork(searchListListener),
    fork(studentDetailsListener),
    fork(summaryDetailsListener),
  ]);
}
