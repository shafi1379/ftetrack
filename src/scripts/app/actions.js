export const LOAD_USER_LOGIN = "LOAD_USER_LOGIN";
export const loadUserLoginData = (request) => ({
  type: LOAD_USER_LOGIN,
  request,
});

export const SET_USER_LOGIN = "SET_USER_LOGIN";
export const setUserLoginData = (userData) => ({
  type: SET_USER_LOGIN,
  userData,
});

export const LOGOUT_USER = "LOGOUT_USER";
export const logoutUser = () => ({
  type: LOGOUT_USER,
});

export const TOGGLE_FULLSCREEN_DIALOG = "TOGGLE_FULLSCREEN_DIALOG";
export const toggleFullScreenDialog = (open) => ({
  type: TOGGLE_FULLSCREEN_DIALOG,
  open,
});

export const SET_PROGRESSING_VALUE = "SET_PROGRESSING_VALUE";
export const setProgressingValue = (progressing) => ({
  type: SET_PROGRESSING_VALUE,
  progressing,
});

export const LOAD_SEARCH_LIST = "LOAD_SEARCH_LIST";
export const loadSearchList = (data) => ({
  type: LOAD_SEARCH_LIST,
  data,
});

export const SET_SEARCH_LIST = "SET_SEARCH_LIST";
export const setSearchList = (searchList) => ({
  type: SET_SEARCH_LIST,
  searchList,
});

export const SET_SCHOOL_LIST = "SET_SCHOOL_LIST";
export const setSchoolList = (schoolList) => ({
  type: SET_SCHOOL_LIST,
  schoolList,
});

export const SET_SUMMARY_LIST = "SET_SUMMARY_LIST";
export const setSummaryList = (summaryList) => ({
  type: SET_SUMMARY_LIST,
  summaryList,
});

export const SET_SEARCH_PROGRESSING_VALUE = "SET_SEARCH_PROGRESSING_VALUE";
export const setSearchProgressingValue = (progressing) => ({
  type: SET_SEARCH_PROGRESSING_VALUE,
  progressing,
});

export const LOAD_STUDENT_DETAILS = "LOAD_STUDENT_DETAILS";
export const loadStudentDetails = (request) => ({
  type: LOAD_STUDENT_DETAILS,
  request,
});

export const SET_STUDENTS_DETAILS = "SET_STUDENTS_DETAILS";
export const setStudentDetails = (studentDetails) => ({
  type: SET_STUDENTS_DETAILS,
  studentDetails,
});

export const LOAD_SUMMARY_DETAILS = "LOAD_SUMMARY_DETAILS";
export const loadSummaryDetailsData = (request) => ({
  type: LOAD_SUMMARY_DETAILS,
  request,
});
