import React, { Component, lazy, Suspense } from "react";
import CssBaseline from "@material-ui/core/CssBaseline";
import ReactNotifications from "react-notifications-component";
import { ThemeProvider } from "@material-ui/core";
import fteTrackTheme from "./theme";
import { API_URLS } from "../utilities/constants";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import * as selectors from "./selectors";
import * as actions from "./actions";
import LoadingIndicator from "../components/generic/loadingIndicator";

const Content = lazy(() => import("../components/content"));
const Header = lazy(() => import("../components/header"));
const Footer = lazy(() => import("../components/footer"));

class App extends Component {
  componentWillMount() {
    this.props.loadUserLoginData({
      url: API_URLS.login,
      params: { userid: "fteadmin" },
    });
  }

  render() {
    const { userData } = this.props;
    return (
      <>
        <ReactNotifications />
        <ThemeProvider theme={fteTrackTheme}>
          <CssBaseline />
          <Suspense fallback={<LoadingIndicator />}>
            <Header />
          </Suspense>
          {userData && userData.school && (
            <div style={{ marginTop: 40 }}>
              <Suspense fallback={<LoadingIndicator />}>
                <Content />
              </Suspense>
              <Suspense fallback={<LoadingIndicator />}>
                <Footer />
              </Suspense>
            </div>
          )}
        </ThemeProvider>
      </>
    );
  }
}

const mapStateToProps = () =>
  createStructuredSelector({
    userData: selectors.getLoginData(),
  });

const mapDispatchToProps = (dispatch) => ({
  loadUserLoginData: (params) => dispatch(actions.loadUserLoginData(params)),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
