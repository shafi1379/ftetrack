import { cancel, fork, take } from "redux-saga/effects";
import Login from "../app/sagas";
import Attendance from "../components/content/attendance/sagas";
import CohortGraduation from "../components/content/cohort_graduation/sagas";
import Grades from "../components/content/grades/sagas";
import Behaviour from "../components/content/behaviour/sagas";
import EWS from "../components/content/ews/sagas";
import Enrollment from "../components/content/enrollment/sagas";
import ProgressGrade from "../components/content/progress_grade/sagas";
import HonorRoll from "../components/content/honor_roll/sagas";
import GradeDistribution from "../components/content/grade_distribution/sagas";
import Pbis from "../components/content/pbis/sagas";
import Ctae from "../components/content/ctae/sagas";
const sagas = [
  Login,
  Attendance,
  CohortGraduation,
  Grades,
  Behaviour,
  EWS,
  Enrollment,
  ProgressGrade,
  HonorRoll,
  GradeDistribution,
  Pbis,
  Ctae,
];

export const CANCEL_SAGAS_HMR = "CANCEL_SAGAS_HMR";

const createAbortableSaga = (saga) => {
  if (process.env.NODE_ENV === "development") {
    return function* main() {
      const sagaTask = yield fork(saga);

      yield take(CANCEL_SAGAS_HMR);
      yield cancel(sagaTask);
    };
  }
  return saga;
};

const SagaManager = {
  cancelSagas: (store) => {
    store.dispatch({
      type: CANCEL_SAGAS_HMR,
    });
  },
  startSagas: (sagaMiddleware) => {
    sagas.map(createAbortableSaga).forEach((saga) => sagaMiddleware.run(saga));
  },
};

export default SagaManager;
