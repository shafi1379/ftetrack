import { combineReducers } from "redux-immutable";
import { connectRouter } from "connected-react-router/immutable";
import login from "../app/reducers";
import attendance from "../components/content/attendance/reducers";
import cohort_graduation from "../components/content/cohort_graduation/reducers";
import grades from "../components/content/grades/reducers";
import behaviour from "../components/content/behaviour/reducers";
import ews from "../components/content/ews/reducers";
import enrollment from "../components/content/enrollment/reducers";
import progress_grade from "../components/content/progress_grade/reducers";
import honor_roll from "../components/content/honor_roll/reducers";
import grade_distribution from "../components/content/grade_distribution/reducers";
import pbis from "../components/content/pbis/reducers";
import ctae from "../components/content/ctae/reducers";
import summary from "../components/content/summary/reducers";

const rootReducer = (history) =>
  combineReducers({
    router: connectRouter(history),
    login,
    attendance,
    cohort_graduation,
    grades,
    behaviour,
    ews,
    progress_grade,
    enrollment,
    honor_roll,
    grade_distribution,
    pbis,
    ctae,
    summary,
  });

export default rootReducer;
