export const appState = (state) => state.get("login");
export const summaryState = (state) => state.get("summary");
export const attendanceState = (state) => state.get("attendance");
export const behaviourState = (state) => state.get("behaviour");
export const cohort_graduationState = (state) => state.get("cohort_graduation");
export const gradesState = (state) => state.get("grades");
export const enrollmentState = (state) => state.get("enrollment");
export const ewsState = (state) => state.get("ews");
export const progress_gradeState = (state) => state.get("progress_grade");
export const honor_rollState = (state) => state.get("honor_roll");
export const grade_distributionState = (state) =>
  state.get("grade_distribution");
export const pbisState = (state) => state.get("pbis");
export const ctaeState = (state) => state.get("ctae");
