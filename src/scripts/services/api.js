import axios from "axios";
import { BASE_URL } from "../utilities/constants";

export async function doPostRequest(urls, params) {
  const response = await axios.post(BASE_URL + urls, params);
  return response.data;
}
