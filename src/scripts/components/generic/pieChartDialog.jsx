import React from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Box from "@material-ui/core/Box";
import Dialog from "@material-ui/core/Dialog";
import MuiDialogContent from "@material-ui/core/DialogContent";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Plot from "react-plotly.js";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import ToggleButton from "@material-ui/lab/ToggleButton";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";

const pieChartLabels = {
  A: "Asian",
  B: "Black",
  H: "Hispanic",
  I: "Indian",
  P: "Pacific Islander",
  M: "Multi-Racial",
  W: "White",
};

const useStyles = makeStyles((theme) => ({
  appBar: {
    position: "static",
  },
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(0.5),
    color: "#f9f9f9",
    padding: 10,
  },
}));

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const initialState = {
  values: [],
  labels: [],
  type: "pie",
  marker: {
    colors: [
      "#5DA5DA",
      "#FAA43A",
      "#60BD68",
      "#F17CB0",
      "#B276B2",
      "#B2912F",
      "#F15854",
    ],
  },
};

function reducer(state, action) {
  switch (action.type) {
    case "AddLabels":
      return { ...state, labels: [...state.labels, action.payload] };
    case "AddValues":
      return { ...state, values: [...state.values, action.payload] };
    case "ClearLabelsAndValues":
      return { ...state, labels: [], values: [] };
    default:
      return { ...state };
  }
}

export default function AttendanceHelpDialog(props) {
  const classes = useStyles();
  const [toggle, setToggle] = React.useState("on");
  const handleClose = () => props.onClose();
  const [state, dispatch] = React.useReducer(reducer, initialState);

  React.useEffect(() => {
    if (props.data && props.data.length) {
      Object.keys(props.data[0]).forEach((key) => {
        dispatch({ type: "AddLabels", payload: pieChartLabels[key] });
        dispatch({ type: "AddValues", payload: props.data[0][key] });
      });
    }
    // eslint-disable-next-line
  }, []);

  const handleToggle = () => {
    dispatch({ type: "ClearLabelsAndValues" });
    if (toggle === "on") {
      if (props.data && props.data.length) {
        Object.keys(props.data[1]).forEach((key) => {
          dispatch({ type: "AddLabels", payload: pieChartLabels[key] });
          dispatch({ type: "AddValues", payload: props.data[1][key] });
        });
      }
      setToggle("off");
    } else {
      if (props.data && props.data.length) {
        Object.keys(props.data[0]).forEach((key) => {
          dispatch({ type: "AddLabels", payload: pieChartLabels[key] });
          dispatch({ type: "AddValues", payload: props.data[0][key] });
        });
      }
      setToggle("on");
    }
  };

  return (
    <>
      <Dialog
        open={props.open}
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        aria-describedby="customized-dialog-description"
      >
        <AppBar className={classes.appBar}>
          <Toolbar style={{ minHeight: 45 }}>
            <Typography className="capitalize" variant="button">
              {props.title} By Race
            </Typography>
            <IconButton
              aria-label="close"
              className={classes.closeButton}
              onClick={handleClose}
            >
              <CloseIcon />
            </IconButton>
          </Toolbar>
        </AppBar>
        <DialogContent dividers>
          {props.data && props.data.length && (
            <Box component="div" style={{ textAlign: "center" }}>
              <ToggleButtonGroup
                value={toggle}
                onChange={handleToggle}
                aria-label="device"
              >
                <ToggleButton value="on" aria-label="laptop">
                  Students
                </ToggleButton>
                <ToggleButton value="off" aria-label="tv">
                  Enrollment
                </ToggleButton>
              </ToggleButtonGroup>
            </Box>
          )}
          <Plot
            data={[{ ...state }]}
            layout={{
              width: 500,
              margin: { l: 40, r: 40, b: 40, t: 40, pad: 15 },
            }}
          />
        </DialogContent>
      </Dialog>
    </>
  );
}
