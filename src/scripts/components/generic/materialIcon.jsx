import classnames from "classnames";
import React, { Component } from "react";

export default class MaterialIcon extends Component {
  render() {
    const { icon, ...otherProps } = this.props;
    return <MaterialIconDefault icon={icon} {...otherProps} />;
  }
}

const MaterialIconDefault = (props) => {
  const { className, icon, ...otherProps } = props;
  const classes = classnames("material-icons", className);
  return (
    <i className={classes} {...otherProps}>
      {" "}
      {icon}{" "}
    </i>
  );
};
