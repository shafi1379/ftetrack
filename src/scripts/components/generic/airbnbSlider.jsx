import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import Slider from "@material-ui/core/Slider";
import Tooltip from "@material-ui/core/Tooltip";
import * as Utilities from "../../utilities/utilities";
import { connect } from "react-redux";
import * as attendanceAction from "../../components/content/attendance/actions";

const AirbnbSlider = withStyles({
  root: {
    color: "#3a8589",
    padding: "13px 0",
    width: "90%",
    marginLeft: "10%",
  },
  thumb: {
    height: 27,
    width: 27,
    backgroundColor: "#fff",
    border: "2px solid green",
    marginTop: -12,
    marginLeft: -13,
    "& .bar": {
      height: 9,
      width: 1,
      backgroundColor: "currentColor",
      marginLeft: 1,
      marginRight: 1,
    },
  },
  active: {},
  valueLabel: {
    left: "calc(-50% + 4px)",
  },
  track: {
    height: 3,
  },
  rail: {
    color: "#d8d8d8",
    opacity: 1,
    height: 3,
  },
})(Slider);

function AirbnbThumbComponent(props) {
  return (
    <Tooltip title={props["aria-valuetext"]}>
      <span {...props}>
        <span className="bar" />
        <span className="bar" />
        <span className="bar" />
      </span>
    </Tooltip>
  );
}

function valueText(value) {
  return Utilities.formatDate(new Date(value));
}

class CustomizedSlider extends Component {
  handleChange = (event, newValue) => {
    this.props.setDateRangePeriod(newValue);
  };
  render() {
    const { data } = this.props;
    const len = data.length;
    let startDate = null,
      endDate = null;
    if (data[0]) {
      startDate = Utilities.formatDateHyphen(data[0].school_date);
    }
    if (data[len ? len - 1 : 0]) {
      endDate = Utilities.formatDateHyphen(data[len ? len - 1 : 0].school_date);
    }
    const step = 86400000;
    const start = data[0] && new Date(startDate).getTime();
    const end = data[len ? len - 1 : 0] && new Date(endDate).getTime();
    return (
      <div>
        {start && end && (
          <AirbnbSlider
            ThumbComponent={AirbnbThumbComponent}
            getAriaValueText={valueText}
            defaultValue={[start, end]}
            min={start}
            max={end}
            step={step}
            onChange={this.handleChange}
          />
        )}
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  setDateRangePeriod: (range) =>
    dispatch(attendanceAction.setAttDateRangePeriod(range)),
});

export default connect(null, mapDispatchToProps)(CustomizedSlider);
