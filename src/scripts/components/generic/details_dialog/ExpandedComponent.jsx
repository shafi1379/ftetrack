import React from "react";
import MaterialIcon from "../materialIcon";

// eslint-disable-next-line react/prop-types
export default ({ data }) => (
  <>
    {data.children.map((obj, index) => (
      <div key={index} className="expanded-style">
        <div className="ex-course">
          <MaterialIcon icon="insert_drive_file_outlined" />
          {obj.course}
        </div>
        <div className="ex-courseName">{obj.courseName}</div>
        <div className="ex-score">{obj.score}</div>
        <div className="ex-date">{obj.date}</div>
      </div>
    ))}
  </>
);
