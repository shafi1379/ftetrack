import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Box from "@material-ui/core/Box";
import Toolbar from "@material-ui/core/Toolbar";
import CloseIcon from "@material-ui/icons/Close";
import DataTable from "react-data-table-component";
import Dialog from "@material-ui/core/Dialog";
import IconButton from "@material-ui/core/IconButton";
import Paper from "@material-ui/core/Paper";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import * as TableConfig from "../../../utilities/tableConfig";
import MaterialIcon from "../materialIcon";
import ExpandedComponent from "./ExpandedComponent";

const useStyles = makeStyles((theme) => ({
  appBar: {
    position: "static",
  },
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(1),
    color: "#f9f9f9",
    padding: 10,
  },
}));

function TabPanel(props) {
  const { children, value, index, ...other } = props;
  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-prevent-tabpanel-${index}`}
      aria-labelledby={`scrollable-prevent-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

export default function MaxWidthDialog(props) {
  const classes = useStyles();
  const [value, setValue] = React.useState(props.value ? props.value : 0);
  const data = { ...props.data };
  const handleClose = () => props.openStudentsDetailsInfo();
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  let gradesObj = {};
  data["grades"].map((obj) => {
    if (Object.keys(gradesObj).includes("" + obj.taskID)) {
      gradesObj[obj.taskID].children.push({ ...obj });
    } else {
      let courseObj = {
        course: "Term " + obj.termName + " ,Grading Task : " + obj.taskName,
        termName: obj.termName,
        taskID: obj.taskID,
        children: [{ ...obj }],
      };
      gradesObj[obj.taskID] = courseObj;
    }
    return null;
  });

  const gradesArr = Object.keys(gradesObj).map((key) => {
    return gradesObj[key];
  });

  return (
    <>
      <Dialog
        fullWidth={true}
        maxWidth={"md"}
        open={props.open}
        onClose={handleClose}
        aria-labelledby="max-width-dialog-title"
      >
        <AppBar className={classes.appBar}>
          <Toolbar style={{ minHeight: 45 }}>
            <Typography className="capitalize" variant="button">
              {(props.row && props.row.studentName) +
                " (" +
                (props.row && props.row.studentNumber) +
                ")"}
            </Typography>
            <IconButton
              aria-label="close"
              className={classes.closeButton}
              onClick={handleClose}
            >
              <CloseIcon />
            </IconButton>
          </Toolbar>
        </AppBar>
        <div style={{ height: "800px" }}>
          <Paper square>
            <Tabs
              value={value}
              indicatorColor="primary"
              textColor="primary"
              onChange={handleChange}
              centered
            >
              <Tab label="Attendance"></Tab>
              <Tab label="Behavior" />
              <Tab label="Course Grade (Current)" />
              <Tab label="EOG Milestones" />
              <Tab label="Grades (Posted)" />
            </Tabs>
          </Paper>
          <TabPanel value={value} index={0}>
            <DataTable
              noHeader={true}
              columns={[...TableConfig.ewsStudentsDetailsColumns.attendance]}
              data={data["attendance"]}
              customStyles={TableConfig.customStyles}
            />
          </TabPanel>
          <TabPanel value={value} index={1}>
            <DataTable
              noHeader={true}
              columns={TableConfig.ewsStudentsDetailsColumns.behavior}
              data={data["behavior"]}
              customStyles={TableConfig.customStyles}
            />
          </TabPanel>
          <TabPanel value={value} index={2}>
            <DataTable
              noHeader={true}
              columns={TableConfig.ewsStudentsDetailsColumns.course}
              data={data["coure"]}
              customStyles={TableConfig.customStyles}
            />
          </TabPanel>
          <TabPanel value={value} index={3}>
            <DataTable
              className="dashboard-table"
              noHeader={true}
              pagination={false}
              columns={TableConfig.ewsStudentsDetailsColumns.test}
              data={data["tests"]}
              customStyles={TableConfig.customStyles}
            />
          </TabPanel>
          <TabPanel value={value} index={4}>
            <DataTable
              className="dashboard-table"
              noHeader={true}
              expandableRows={true}
              expandableRowExpanded={(row) => true}
              expandableIcon={{
                collapsed: (
                  <MaterialIcon icon="folder" style={{ color: "#3f51b5" }} />
                ),
                expanded: (
                  <MaterialIcon
                    icon="folder_open"
                    style={{ color: "#3f51b5" }}
                  />
                ),
              }}
              expandableRowsComponent={<ExpandedComponent />}
              columns={TableConfig.ewsStudentsDetailsColumns.grades}
              data={gradesArr}
              customStyles={TableConfig.customStyles}
            />
          </TabPanel>
        </div>
      </Dialog>
    </>
  );
}
