import React, { Component } from "react";
import CircularProgress from "@material-ui/core/CircularProgress";
import createStyles from "@material-ui/styles/createStyles";
import WithStyles from "@material-ui/styles/WithStyles";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import Grid from "@material-ui/core/Grid";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import CloseIcon from "@material-ui/icons/Close";
import Paper from "@material-ui/core/Paper";
import Slide from "@material-ui/core/Slide";
import Tooltip from "@material-ui/core/Tooltip";
import Select from "react-select";
import MaterialIcon from "./materialIcon";
import DataTable from "react-data-table-component";
import { selectCustomStyles } from "./reactSelectCustomization";
import { CSVLink } from "react-csv";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";
import * as selectors from "../../app/selectors";
import * as actions from "../../app/actions";
import * as ewsSelectors from "../content/ews/selectors";
import * as ewsActions from "../content/ews/actions";
import * as TableConfig from "../../utilities/tableConfig";
import * as Utilities from "../../utilities/utilities";
import * as attendanceAction from "../../components/content/attendance/actions";
import * as attendanceSelectors from "../../components/content/attendance/selectors";
import * as loginSelectors from "../../app/selectors";
import StudentDetailsDialog from "./details_dialog";

const useStyles = (theme) =>
  createStyles({
    appBar: {
      position: "fixed",
      flexGrow: 1,
    },
  });

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

class FullScreenDialog extends Component {
  csvLink = React.createRef();
  state = {
    dashboardStudentsListColumns: TableConfig.dashboardStudentsListColumns.map(
      (dash, index) => {
        if (index === 0) {
          dash.cell = (row) => (
            <span
              style={{
                color: "#683598",
                textDecoration: "underline",
                cursor: "pointer",
              }}
              onClick={this.handleAction.bind(this, row)}
            >
              {row.studentNumber}
            </span>
          );
        }
        return dash;
      },
    ),
    csvExport: {
      data: [],
      fileName: "attendance_dashboard.csv",
    },
    progressing: false,
    openStudentDetailsDialog: false,
    rowContent: null,
  };

  handleClose = () => this.props.toggleFullScreenDialog(false);

  handleAction = (row) => {
    this.props.loadStudentsDetails({
      personID: "" + row.personID,
    });
    this.setState({
      rowContent: row,
      openStudentDetailsDialog: true,
    });
  };

  handleStudentDetailsDialog = () => {
    this.props.setStudentsDetails([]);
    this.setState({ openStudentDetailsDialog: false });
  };

  handleDropdownChange = (state, option) => this.props[`set${state}`](option);

  runAdhocQuery = () => {
    const reqObj = {
      school: this.props.selectedSchoolAdhoc.value,
      cutoff: this.props.selectedADAPercent.value,
    };
    if (this.props.selectedSchoolAdhoc.value !== -1) {
      this.setState({ progressing: true });
      this.props.runAdhocQuery(reqObj);
    }
  };

  downloadCSV = () => {
    const currentTime = `_${new Date().getTime()}`;
    const currentScreen = this.props.currentFullScreenDialog;
    const columns = {
      "ada-adhoc": {
        School: "name",
        Date: "school_date",
        Enrollments: "enrollments",
        Absences: "absences",
        "ADA %": "ada",
      },
      "ada-dashboard": {
        "Students#": "studentNumber",
        Name: "studentName",
        Grade: "grade",
        Race: "race",
        Gender: "gender",
        "Birth Date": "birthdate",
        SPED: "sped",
        Homeless: "homeless",
        "Days Enrolled": "DaysEnrolled",
        "Days Absent": "TotalAbsence",
        "% Absent": "PercentAbsence",
      },
    };
    if (currentScreen === "ada-students-list") {
      columns[currentScreen] = [];
      TableConfig.studentAncentsColumns.forEach((obj) => {
        columns[currentScreen][obj["name"]] = obj["selector"];
      });
    }
    let exportList = [];
    if (currentScreen === "ada-adhoc") {
      exportList = this.props.adhocQueryResult;
    } else if (currentScreen === "ada-dashboard") {
      exportList = this.props.dashboardStudentsList;
    } else if (currentScreen === "ada-students-list") {
      exportList = this.props.studentAbcentsList;
    }
    const data = Utilities.generateExcelData(
      exportList || [],
      columns[currentScreen] || {},
    );
    let excelFileName = currentScreen + currentTime;
    if (currentScreen === "ada-dashboard") {
      excelFileName = "Attd_Dashboard_Students" + currentTime;
      data.unshift([
        `School: ${
          this.props.selectedSchool && this.props.selectedSchool.label
        }`,
      ]);
      data.unshift([`${this.props.title}`]);
    } else if (currentScreen === "ada-adhoc") {
      excelFileName = "ADA_Adhoc_Query" + currentTime;
      data.unshift([
        `ADA Percent: ${
          this.props.selectedADAPercent && this.props.selectedADAPercent.label
        }`,
      ]);
      data.unshift([
        `School: ${
          this.props.selectedSchoolAdhoc && this.props.selectedSchoolAdhoc.label
        }`,
      ]);
      data.unshift([`${this.props.title}`]);
    } else if (currentScreen === "ada-students-list") {
      excelFileName = "Student_Absence" + currentTime;
      data.unshift([
        `School: ${
          this.props.selectedSchool && this.props.selectedSchool.label
        }`,
      ]);
      data.unshift([`${this.props.title}`]);
    }
    this.setState({ csvExport: { data, fileName: `${excelFileName}.csv` } });
    setTimeout(() => {
      this.csvLink.current.link.click();
    });
  };

  render() {
    const {
      dashboardStudentsListColumns,
      csvExport,
      openStudentDetailsDialog,
      rowContent,
      progressing,
    } = this.state;
    const {
      classes,
      open,
      title,
      currentFullScreenDialog,
      dashboardStudentsList,
      adhocQueryResult,
      schoolList,
      ADAPercentList,
      selectedSchoolAdhoc,
      selectedADAPercent,
      studentAbcentsList,
    } = this.props;

    const adhocQueryResultList = adhocQueryResult.map((res) => {
      res.school_date = Utilities.formatDateSlash(
        res.school_date,
        "mm/dd/yyyy",
      );
      return res;
    });

    return (
      <Dialog
        className="full-screen-dialog"
        fullScreen
        open={open}
        onClose={this.handleClose}
        TransitionComponent={Transition}
      >
        <AppBar className={classes.appBar}>
          <Toolbar style={{ minHeight: 45 }}>
            <Typography
              className="capitalize"
              variant="button"
              style={{ flexGrow: 1 }}
            >
              {title}
            </Typography>
            <Typography>
              <Tooltip
                title="Export to Excel"
                aria-label="excel-export-tooltip"
              >
                <IconButton
                  className="tools-btn"
                  aria-label="excel-export"
                  style={{
                    marginRight: 15,
                    color: "#ffffff",
                    display:
                      currentFullScreenDialog === "ada-adhoc"
                        ? "none"
                        : "inherit",
                  }}
                  onClick={this.downloadCSV}
                >
                  <MaterialIcon icon="cloud_download" />
                </IconButton>
              </Tooltip>
            </Typography>
            <Typography>
              <IconButton
                edge="start"
                color="inherit"
                onClick={this.handleClose}
                aria-label="close"
              >
                <CloseIcon />
              </IconButton>
            </Typography>
          </Toolbar>
        </AppBar>
        <div className="fullScreen-grid-container">
          {currentFullScreenDialog === "ada-adhoc" && (
            <>
              <Paper
                className="sub-toolbar sub-toolbar-fullscreen"
                elevation={3}
              >
                <Grid
                  item
                  md={12}
                  xs={12}
                  container
                  className="vertical-align"
                  spacing={2}
                >
                  <Grid
                    item
                    md={4}
                    sm={6}
                    xs={12}
                    container
                    className="vertical-align"
                  >
                    <Grid
                      style={{ textAlign: "right" }}
                      item
                      md={3}
                      xs={3}
                      className="vertical-align"
                    >
                      <Typography className="mr-rt10" variant="caption">
                        {" "}
                        School:{" "}
                      </Typography>
                    </Grid>
                    <Grid item md={9} xs={9} className="vertical-align">
                      <Select
                        styles={selectCustomStyles}
                        value={selectedSchoolAdhoc}
                        onChange={this.handleDropdownChange.bind(
                          this,
                          "SelectedSchoolAdhoc",
                        )}
                        options={schoolList}
                      />
                    </Grid>
                  </Grid>
                  <Grid
                    item
                    md={3}
                    sm={6}
                    xs={12}
                    container
                    className="vertical-align"
                  >
                    <Grid
                      style={{ textAlign: "right" }}
                      item
                      md={8}
                      xs={3}
                      className="vertical-align"
                    >
                      <Typography className="mr-rt10" variant="caption">
                        {" "}
                        ADA Less than:{" "}
                      </Typography>
                    </Grid>
                    <Grid item md={4} xs={9} className="vertical-align">
                      <Select
                        styles={selectCustomStyles}
                        value={selectedADAPercent}
                        onChange={this.handleDropdownChange.bind(
                          this,
                          "SelectedADAPercent",
                        )}
                        options={ADAPercentList}
                      />
                    </Grid>
                  </Grid>
                  <Grid
                    item
                    md={5}
                    sm={6}
                    xs={6}
                    container
                    className="other-tools vertical-align"
                  >
                    <Grid item md={2} xs={3} className="vertical-align"></Grid>
                    <Grid item md={4} xs={3} className="vertical-align">
                      <Button
                        onClick={this.runAdhocQuery}
                        size="small"
                        variant="outlined"
                        color="primary"
                      >
                        Run Query
                      </Button>
                    </Grid>
                    <Grid
                      item
                      md={4}
                      xs={3}
                      className="other-tools-container vertical-align"
                    >
                      <Tooltip
                        title="Export to Excel"
                        aria-label="excel-export-tooltip"
                      >
                        <IconButton
                          onClick={this.downloadCSV}
                          className="tools-btn"
                          aria-label="excel-export"
                        >
                          <MaterialIcon icon="cloud_download" />
                        </IconButton>
                      </Tooltip>
                    </Grid>
                  </Grid>
                </Grid>
              </Paper>
              <DataTable
                className="ada-table"
                noHeader={true}
                fixedHeader={true}
                fixedHeaderScrollHeight={"75vh"}
                columns={TableConfig.adhocQueryResultColumns}
                progressPending={
                  !(adhocQueryResult && adhocQueryResult.length) && progressing
                }
                progressComponent={<CircularProgress />}
                data={adhocQueryResultList || [{}]}
                customStyles={TableConfig.customStyles}
              />
            </>
          )}
          {currentFullScreenDialog === "ada-dashboard" && (
            <DataTable
              className="dashboard-table"
              noHeader={true}
              fixedHeader={true}
              fixedHeaderScrollHeight={"82vh"}
              pagination={false}
              columns={dashboardStudentsListColumns}
              progressPending={
                !(dashboardStudentsList && dashboardStudentsList.length)
              }
              progressComponent={<CircularProgress />}
              data={dashboardStudentsList || [{}]}
              customStyles={TableConfig.customStyles}
            />
          )}
          {currentFullScreenDialog === "ada-students-list" && (
            <DataTable
              className="student-abcent-table"
              noHeader={true}
              fixedHeader={true}
              fixedHeaderScrollHeight={"82vh"}
              pagination={false}
              columns={TableConfig.studentAncentsColumns}
              progressPending={
                !(studentAbcentsList && studentAbcentsList.length)
              }
              progressComponent={<CircularProgress />}
              data={studentAbcentsList || [{}]}
              customStyles={TableConfig.customStyles}
            />
          )}
        </div>
        <CSVLink
          ref={this.csvLink}
          data={csvExport.data}
          className="hidden"
          filename={csvExport.fileName}
          target="_self"
        />
        {Object.keys(this.props.studentsDetailsList).length > 0 && (
          <StudentDetailsDialog
            open={openStudentDetailsDialog}
            openStudentsDetailsInfo={this.handleStudentDetailsDialog}
            row={rowContent}
            data={this.props.studentsDetailsList}
          />
        )}
      </Dialog>
    );
  }
}

const mapStateToProps = () =>
  createStructuredSelector({
    open: selectors.getFullScreenDialog(),
    selectedSchool: attendanceSelectors.getAttSelectedSchool(),
    selectedSchoolAdhoc: attendanceSelectors.getAttSelectedSchoolAdhoc(),
    selectedADAPercent: attendanceSelectors.getAttSelectedADAPercent(),
    ADAPercentList: attendanceSelectors.getAttADAPercentList(),
    schoolList: loginSelectors.getSchoolList(),
    //schoolList: attendanceSelectors.getAttSchoolList(),
    studentsDetailsList: ewsSelectors.getEwsStudentsDetails(),
    currentFullScreenDialog:
      attendanceSelectors.getAttCurrentFullScreenDialog(),
    dashboardStudentsList: attendanceSelectors.getAttDashboardStudentsList(),
    adhocQueryResult: attendanceSelectors.getAttAdhocQueryResult(),
    studentAbcentsList: attendanceSelectors.getStudentAbcentsList(),
  });
const mapDispatchToProps = (dispatch) => ({
  toggleFullScreenDialog: (open) =>
    dispatch(actions.toggleFullScreenDialog(open)),
  runAdhocQuery: (query) => dispatch(attendanceAction.runAttAdhocQuery(query)),
  setSelectedADAPercent: (option) =>
    dispatch(attendanceAction.setAttSelectedADAPercent(option)),
  setSelectedSchoolAdhoc: (option) =>
    dispatch(attendanceAction.setAttSelectedSchoolAdhoc(option)),
  loadStudentsDetails: (request) =>
    dispatch(ewsActions.loadEwsStudentsDetails(request)),
  setStudentsDetails: (val) => dispatch(ewsActions.setEwsStudentsDetails(val)),
});
const styleFullScreenDialog = WithStyles(useStyles)(FullScreenDialog);
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(styleFullScreenDialog);
