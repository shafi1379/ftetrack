import {
  SET_PG_EL_ONLY,
  SET_PG_ACTUAL_DATA,
  SET_PG_FILTER_DATA,
  SET_PG_GRADING_TASK,
  SET_PG_SCHOOL_LIST,
  SET_PG_TERM,
  SET_PG_SPED_ONLY,
  SET_PG_SELECTED_TASK,
  SET_PG_SELECTED_SCHOOL,
  SET_PG_SELECTED_TERM,
  SET_PG_STUDENT_LIST,
  SET_PG_PROGRESS_GRADE,
  SET_PG_FAILING_GRADE,
  SET_SEARCH_TEXT,
} from "./actions";
import { fromJS } from "immutable";

const initialState = fromJS({
  elOnly: false,
  spedOnly: false,
  pgSchoolList: [],
  pgFailingGradeList: [],
  pgSelectedSchool: { label: "Select a School ...", value: -1 },
  pgSelectedTerm: {},
  pgSelectedTask: {},
  pgActualDataList: [],
  pgFilterDataList: [],
  pgGradingTaskList: [],
  pgProgressGradeList: [],
  pgTermList: [],
  pgStudentList: [],
  searchText: null,
});

const progress_grade = (state = initialState, action) => {
  switch (action.type) {
    case SET_PG_EL_ONLY:
      return state.set("elOnly", action.elOnly);
    case SET_PG_SPED_ONLY:
      return state.set("spedOnly", action.spedOnly);
    case SET_PG_SCHOOL_LIST:
      return state.set("pgSchoolList", fromJS([...action.pgSchoolList]));
    case SET_PG_STUDENT_LIST:
      return state.set("pgStudentList", fromJS([...action.pgStudentList]));
    case SET_PG_TERM:
      return state.set("pgTermList", fromJS([...action.pgTermList]));
    case SET_PG_ACTUAL_DATA:
      return state.set(
        "pgActualDataList",
        fromJS([...action.pgActualDataList]),
      );
    case SET_PG_FILTER_DATA:
      return state.set(
        "pgFilterDataList",
        fromJS([...action.pgFilterDataList]),
      );
    case SET_PG_GRADING_TASK:
      return state.set(
        "pgGradingTaskList",
        fromJS([...action.pgGradingTaskList]),
      );
    case SET_PG_SELECTED_TASK:
      return state.set("pgSelectedTask", fromJS({ ...action.pgSelectedTask }));
    case SET_PG_SELECTED_TERM:
      return state.set("pgSelectedTerm", fromJS({ ...action.pgSelectedTerm }));
    case SET_PG_SELECTED_SCHOOL:
      return state.set(
        "pgSelectedSchool",
        fromJS({ ...action.pgSelectedSchool }),
      );
    case SET_PG_PROGRESS_GRADE:
      return state.set(
        "pgProgressGradeList",
        fromJS([...action.pgProgressGradeList]),
      );
    case SET_PG_FAILING_GRADE:
      return state.set(
        "pgFailingGradeList",
        fromJS([...action.pgFailingGradeList]),
      );
    case SET_SEARCH_TEXT:
      return state.set("searchText", action.text);
    default:
      return state;
  }
};

export default progress_grade;
