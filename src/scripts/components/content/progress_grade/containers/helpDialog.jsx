import React from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Dialog from "@material-ui/core/Dialog";
import MuiDialogContent from "@material-ui/core/DialogContent";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles((theme) => ({
  appBar: {
    position: "static",
  },
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(0.5),
    color: "#f9f9f9",
    padding: 10,
  },
}));

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

export default function AttendanceHelpDialog(props) {
  const classes = useStyles();
  const handleClose = () => props.handleClose();

  return (
    <>
      <Dialog
        open={props.open}
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        aria-describedby="customized-dialog-description"
      >
        <AppBar className={classes.appBar}>
          <Toolbar style={{ minHeight: 45 }}>
            <Typography className="capitalize" variant="button">
              Progress Grade Help
            </Typography>
            <IconButton
              aria-label="close"
              className={classes.closeButton}
              onClick={handleClose}
            >
              <CloseIcon />
            </IconButton>
          </Toolbar>
        </AppBar>
        <DialogContent dividers style={{ minHeight: "450px" }}>
          <ul>
            <li>
              <Typography variant="body2" gutterBottom>
                Works for Posted grade for completed terms or current teacher
                gradebook averages
              </Typography>
            </li>
            <li>
              For current grades, choose <strong>Progress Grades</strong> in
              Term dropdown and <strong>Current Grades</strong> in Grading Task
              dropdown.
            </li>
          </ul>
        </DialogContent>
      </Dialog>
    </>
  );
}
