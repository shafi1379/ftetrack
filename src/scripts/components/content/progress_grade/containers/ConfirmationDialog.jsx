import React from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import { AppBar, Dialog, Toolbar, Typography, Button } from "@material-ui/core";
import MuiDialogContent from "@material-ui/core/DialogContent";

const useStyles = makeStyles((theme) => ({
  appBar: {
    position: "static",
  },
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(0.5),
    color: "#f9f9f9",
    padding: 10,
  },
}));

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

export default function ConfirmationDialog(props) {
  const classes = useStyles();
  const handleClose = () => props.handleClose();
  return (
    <>
      <Dialog
        open={props.open}
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        aria-describedby="customized-dialog-description"
      >
        <AppBar className={classes.appBar}>
          <Toolbar style={{ minHeight: 45 }}>
            <Typography className="capitalize" variant="button">
              Download
            </Typography>
          </Toolbar>
        </AppBar>
        <DialogContent dividers>
          <Typography className="capitalize" variant="body2">
            Failing Grades Report is Ready to Download !
          </Typography>
          <div style={{ float: "right", marginTop: "15px" }}>
            <Button
              style={{
                color: "#ffffff",
                background: "#3f51b5",
                textTransform: "none",
                padding: "0px 10px",
              }}
              onClick={props.onSuccess}
            >
              Ok
            </Button>
            <Button
              onClick={handleClose}
              style={{
                textTransform: "none",
                padding: "0px 10px",
                marginLeft: "10px",
              }}
            >
              Cancel
            </Button>
          </div>
        </DialogContent>
      </Dialog>
    </>
  );
}
