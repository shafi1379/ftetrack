import {
  LOAD_PG_GRADING_TASK,
  LOAD_PG_SCHOOL_LIST,
  LOAD_PG_TERM,
  setPgGradingTask,
  setPgActualData,
  setPgFilterData,
  setPgSchoolList,
  setPgTerm,
  setPgSelectedSchool,
  setPgSelectedTerm,
  setPgSelectedTask,
  LOAD_PG_STUDENT_LIST,
  setPgStudentList,
  LOAD_PG_PROGRESS_GRADE,
  setPgProgressGrade,
  setPgFailingGrade,
  LOAD_PG_FAILING_GRADE,
} from "./actions";

import { select, all, call, fork, put, takeLatest } from "redux-saga/effects";
import { setProgressingValue } from "../../../app/actions";
import * as Api from "../../../services/api";
import { API_URLS } from "../../../utilities/constants";

function* loadPgSchoolListRequested(request) {
  try {
    const { pgSchoolList } = (yield select((s) =>
      s.get("progress_grade"),
    )).toJS();
    if (!pgSchoolList.length) {
      yield put(setProgressingValue(true));
      const response = yield call(
        Api.doPostRequest,
        API_URLS.schools,
        request.req,
      );
      if (response) {
        const pgSchoolList = response.map((res) => ({
          ...res,
          label: res.schoolname,
          value: res.school,
        }));
        yield put(setPgSchoolList(pgSchoolList));
        yield put(setPgSelectedSchool(pgSchoolList[0]));
        yield put(setProgressingValue(false));
      }
    }
  } catch (error) {
    console.error(error);
  }
}

function* pgSchoolListListener() {
  yield takeLatest(LOAD_PG_SCHOOL_LIST, loadPgSchoolListRequested);
}

function* loadPgStudentListRequested(request) {
  try {
    yield put(setProgressingValue(true));
    const response = yield call(
      Api.doPostRequest,
      API_URLS.progressStudents,
      request.req,
    );
    if (response) {
      yield put(setPgStudentList(response));
      yield put(setPgActualData(response));
      yield put(setPgFilterData(response));
      yield put(setProgressingValue(false));
    }
  } catch (error) {
    console.error(error);
  }
}

function* pgStudentListListener() {
  yield takeLatest(LOAD_PG_STUDENT_LIST, loadPgStudentListRequested);
}

function* loadPgGradingTaskRequested(request) {
  try {
    yield put(setProgressingValue(true));
    const response = yield call(
      Api.doPostRequest,
      API_URLS.gradingTasks,
      request.req,
    );
    if (response) {
      const pgGradingTask = response.map((res) => ({
        ...res,
        label: res.taskName,
        value: res.taskID,
      }));
      yield put(setPgGradingTask(pgGradingTask));
      yield put(setPgSelectedTask(pgGradingTask[0]));
      yield put(setProgressingValue(false));
    }
  } catch (error) {
    console.error(error);
  }
}

function* pgGradingTaskListener() {
  yield takeLatest(LOAD_PG_GRADING_TASK, loadPgGradingTaskRequested);
}

function* loadPgTermRequested(request) {
  try {
    yield put(setProgressingValue(true));
    const response = yield call(
      Api.doPostRequest,
      API_URLS.gradingTerms,
      request.req,
    );
    if (response) {
      const pgTermList = response.map((res) => ({
        ...res,
        label: res.termName,
        value: res.termID,
      }));
      yield put(setPgTerm(pgTermList));
      yield put(setPgSelectedTerm(pgTermList[0]));
      yield put(setProgressingValue(false));
    }
  } catch (error) {
    console.error(error);
  }
}

function* pgPgTermListener() {
  yield takeLatest(LOAD_PG_TERM, loadPgTermRequested);
}

function* loadPgProgressGradeRequested(request) {
  try {
    yield put(setProgressingValue(true));
    const response = yield call(
      Api.doPostRequest,
      API_URLS.progressGrades,
      request.req,
    );
    if (response) {
      yield put(setPgProgressGrade(response));
      yield put(setProgressingValue(false));
    }
  } catch (error) {
    console.error(error);
  }
}

function* pgProgressGradeListener() {
  yield takeLatest(LOAD_PG_PROGRESS_GRADE, loadPgProgressGradeRequested);
}

function* loadPgFailingGradeRequested(request) {
  try {
    yield put(setProgressingValue(true));
    const response = yield call(
      Api.doPostRequest,
      API_URLS.failingGrades,
      request.req,
    );
    if (response) {
      yield put(setPgFailingGrade(response));
      yield put(setProgressingValue(false));
    }
  } catch (error) {
    console.error(error);
  }
}

function* pgFailingGradeListener() {
  yield takeLatest(LOAD_PG_FAILING_GRADE, loadPgFailingGradeRequested);
}

export default function* root() {
  yield all([
    fork(pgPgTermListener),
    fork(pgGradingTaskListener),
    fork(pgSchoolListListener),
    fork(pgStudentListListener),
    fork(pgProgressGradeListener),
    fork(pgFailingGradeListener),
  ]);
}
