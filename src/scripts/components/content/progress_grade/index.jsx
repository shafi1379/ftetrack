import React, { Component } from "react";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";
import * as selectors from "./selectors";
import * as loginSelectors from "../../../app/selectors";
import * as actions from "./actions";
import DataTable from "react-data-table-component";
import { CSVLink } from "react-csv";
import Select from "react-select";
import * as Utilities from "../../../utilities/utilities";
import HelpDialog from "./containers/helpDialog";
import ProgressGradeDialog from "./containers/ProgressGrade";
import TextField from "@material-ui/core/TextField";
import {
  Paper,
  AppBar,
  Toolbar,
  Grid,
  FormControlLabel,
  Checkbox,
  Tooltip,
  IconButton,
  Typography,
  Button,
} from "@material-ui/core";
import MaterialIcon from "../../generic/materialIcon";
import {
  customStyles,
  ProgressGradeStudentDetails,
} from "../../../utilities/tableConfig";
import { ProgressGradesColumns } from "../../../utilities/tableHeaderNamesList";
import ConfirmationDialog from "./containers/ConfirmationDialog";
import { selectCustomStyles } from "../../generic/reactSelectCustomization";

class ProgressGrade extends Component {
  csvLink = React.createRef();
  state = {
    ProgressGradeStudentDetailsColumns: [...ProgressGradeStudentDetails].map(
      (obj) => {
        if (obj.selector === "studentNumber") {
          obj.cell = (row) => (
            <span
              style={{
                color: "#683598",
                textDecoration: "underline",
                cursor: "pointer",
              }}
              onClick={this.handleCellClick.bind(this, row)}
            >
              {row.studentNumber}
            </span>
          );
        }
        return obj;
      },
    ),
    openHelpInfo: false,
    selectedRowData: null,
    openProgressGradeInfo: false,
    openConfirmation: false,
    csvExport: {
      data: [],
      fileName: "ews.csv",
    },
  };

  componentDidMount() {
    /* this.props.loadPgSchoolList({
      school: this.props.userData.loginData.school,
    }); */
    this.props.handleCallToRouter(null, "progress_grade");
    this.clearSearchField();
  }

  downloadCSV = (evt) => {
    const currentTime = `_${new Date().getTime()}`;
    const columns = ProgressGradesColumns;
    const data = Utilities.generateExcelData(
      [...this.props.pgFilterDataList] || [],
      columns || {},
    );
    const school = this.props.pgSelectedSchool;
    data.unshift([`${school && school.label}`]);
    this.setState({
      csvExport: {
        data,
        fileName: `Progress_Grades${currentTime}.csv`,
      },
    });
    setTimeout(() => {
      this.csvLink.current.link.click();
    });
  };

  handleDropDownChange = (obj, evt, type) => {
    this.props["setPgSelected" + type]({
      ...obj,
    });
    const { pgSelectedTerm, pgSelectedSchool, pgGradingTaskList, pgTermList } =
      this.props;
    if (type === "School") {
      this.props.loadPgTerm({
        school: obj.school,
        option: "1",
      });
      this.props.setPgSelectedTerm(pgTermList[0]);
      this.props.setPgGradingTask([]);
      this.props.setPgFilterData([]);
      this.props.setPgSelectedTask([]);
    }
    if (type === "Term") {
      this.props.loadPgGradingTask({
        school: obj.schoolID,
        termID: obj.termID,
      });
      this.props.setPgSelectedTask(pgGradingTaskList[0]);
      this.props.setPgGradingTask([]);
      this.props.setPgFilterData([]);
    }
    if (type === "Task") {
      this.props.loadPgStudentList({
        school: pgSelectedSchool.school,
        termID: pgSelectedTerm.termID,
        termName: pgSelectedTerm.termName,
        taskID: obj.taskID,
      });
    }
  };

  downloadFailingGrade = (evt) => {
    const currentTime = `_${new Date().getTime()}`;
    const columns = {
      personID: "personID",
      "School Name": "School Name",
      Term: "Term",
      "Grading Task": "Grading Task",
      "Student Number": "studentNumber",
      "Student Name": "studentName",
      "Grade Level": "Grade Level",
      "Race/Ethnicity": "race",
      SPED: "SPED",
      EL: "EL",
      Homeless: "homeless",
      "Section 504": "sec504",
      Course: "Course",
      Teacher: "Teacher",
      Score: "score",
      "EOC Course": "EOC Course",
      "Days Absent": "days_abs",
      "Discipline Referrals": "tdr",
      "ISS Days": "iss_days",
      "OSS Days": "oss_days",
      "Suspension?": "has_susp",
    };
    const data = Utilities.generateExcelData(
      [...this.props.pgFailingGradeList] || [],
      columns || {},
    );
    data.unshift([`Failing Grade Report`]);
    this.setState({
      csvExport: {
        data,
        fileName: `Failing_Grades${currentTime}.csv`,
      },
    });
    setTimeout(() => {
      // window.open("FailingGradesReport.csv","Download")
      this.csvLink.current.link.click();
    });
    this.setState({ openConfirmation: false });
  };

  applyFilter = (evt, type) => {
    this.props["setPg" + type](evt.target.checked);
    const { pgFilterDataList, spedOnly, elOnly, pgStudentList } = this.props;
    if (evt.target.checked) {
      if (type === "SpedOnly") {
        this.props.setPgFilterData(
          pgFilterDataList.filter((obj) => obj.sped === "Y"),
        );
        this.props.setPgSpedOnly(true);
      }
      if (type === "ElOnly") {
        this.props.setPgFilterData(
          pgFilterDataList.filter((obj) => obj.EL === "Y"),
        );
        this.props.setPgElOnly(true);
      }
    } else {
      console.log("ELOnly::" + elOnly + ",SPED Only ::" + spedOnly);
      if (type === "SpedOnly") {
        if (elOnly) {
          this.props.setPgFilterData(
            pgStudentList.filter((obj) => obj.EL === "Y"),
          );
          this.props.setPgElOnly(true);
        } else {
          this.props.setPgFilterData(pgStudentList);
          this.props.setPgElOnly(false);
        }
      }
      if (type === "ElOnly") {
        if (spedOnly) {
          this.props.setPgFilterData(
            pgStudentList.filter((obj) => obj.sped === "Y"),
          );
          this.props.setPgElOnly(true);
        } else {
          this.props.setPgFilterData(pgStudentList);
          this.props.setPgElOnly(false);
        }
      }
    }
  };

  handleRowClick = (row) => {
    this.props.setPgProgressGrade([]);
    const { pgSelectedSchool, pgSelectedTerm, pgSelectedTask } = this.props;
    this.props.loadPgProgressGrade({
      school: pgSelectedSchool.school,
      termID: pgSelectedTerm.termID,
      termName: pgSelectedTerm.termName,
      taskID: pgSelectedTask.taskID,
      personID: row.personID,
    });
    this.setState({
      openProgressGradeInfo: true,
      selectedRowData: row,
    });
  };

  handleCellClick = (row) => {
    this.handleRowClick(row);
  };

  failingGrade = (evt) => {
    const { pgSelectedSchool, pgSelectedTerm, pgSelectedTask } = this.props;
    this.props.loadPgFailingGrade({
      school: pgSelectedSchool.school,
      termID: pgSelectedTerm.termID,
      termName: pgSelectedTerm.termName,
      taskID: pgSelectedTask.taskID,
    });
    this.setState({ openConfirmation: true });
  };

  filterCheckboxData = (filteredDatas) => {
    const { eLOnly, spedOnly } = this.props;
    if (spedOnly) {
      filteredDatas = filteredDatas.filter((obj) => obj["sped"] === "Y");
    }
    if (eLOnly) {
      filteredDatas = filteredDatas.filter((obj) => obj["EL"] === "Y");
    }
    return filteredDatas;
  };

  filterSearchField = (e) => {
    let value = e.target.value;
    if (!value.charAt(0) === "-") {
      value = "-Name:" + value;
    }
    if (value.length && this.props.pgActualDataList.length) {
      this.props.setSearchText(value);
      const objKeys = Object.keys(this.props.pgActualDataList[0]);
      const actualKeys = this.state.ProgressGradeStudentDetailsColumns;
      let filteredDatas = this.props.pgActualDataList.filter((item) => {
        return Utilities.generateFilteredData(
          item,
          value ? value : this.props.searchText,
          objKeys,
          actualKeys,
        );
      });
      filteredDatas = this.filterCheckboxData(filteredDatas);
      this.props.setPgFilterData(filteredDatas);
    } else {
      this.props.setSearchText(null);
      this.props.setPgFilterData(
        this.filterCheckboxData(this.props.pgActualDataList),
      );
    }
  };

  clearSearchField = () => {
    const { pgSchoolList, pgTermList, pgGradingTaskList, pgFilterDataList } =
      this.props;
    if (
      pgSchoolList.length &&
      pgTermList.length &&
      pgGradingTaskList.length &&
      pgFilterDataList.length
    ) {
      this.props.setPgFilterData(
        this.filterCheckboxData(this.props.pgActualDataList),
      );
    } else {
      this.props.setPgFilterData([]);
    }
    this.props.setSearchText(null);
  };

  render() {
    const {
      openHelpInfo,
      openProgressGradeInfo,
      ProgressGradeStudentDetailsColumns,
      csvExport,
      selectedRowData,
      openConfirmation,
    } = this.state;
    const {
      pgSchoolList,
      pgFilterDataList,
      pgSelectedSchool,
      pgProgressGradeList,
      pgTermList,
      pgSelectedTerm,
      pgGradingTaskList,
      pgSelectedTask,
      spedOnly,
      elOnly,
      searchText,
      pgFailingGradeList,
    } = this.props;

    return (
      <div className="page-under-construction ews">
        <Paper style={{ padding: 2 }}>
          <AppBar style={{ position: "static", height: "32px" }}>
            <Toolbar
              style={{ minHeight: 30, float: "right", position: "relative" }}
            >
              <Grid item container sm={12} className="progress_grade_toolbar">
                <Grid item sm={3}>
                  <Typography>School</Typography>
                  <Select
                    styles={selectCustomStyles}
                    options={pgSchoolList}
                    className="ews-dropdown"
                    placeholder={"Select School"}
                    value={pgSelectedSchool}
                    onChange={(val, evt) =>
                      this.handleDropDownChange(val, evt, "School")
                    }
                  />
                </Grid>

                <Grid item sm={2}>
                  {pgTermList.length > 0 && (
                    <React.Fragment>
                      <Typography>Term</Typography>
                      <Select
                        styles={selectCustomStyles}
                        options={pgTermList}
                        className="ews-dropdown"
                        value={pgSelectedTerm}
                        onChange={(val, evt) =>
                          this.handleDropDownChange(val, evt, "Term")
                        }
                      />
                    </React.Fragment>
                  )}
                </Grid>

                <Grid item sm={3} style={{ marginLeft: "30px" }}>
                  {pgGradingTaskList.length > 0 && (
                    <React.Fragment>
                      <Typography>Grading&nbsp;Task</Typography>
                      <Select
                        styles={selectCustomStyles}
                        options={pgGradingTaskList}
                        className="ews-dropdown"
                        value={pgSelectedTask}
                        onChange={(val, evt) =>
                          this.handleDropDownChange(val, evt, "Task")
                        }
                      />
                    </React.Fragment>
                  )}
                </Grid>

                <Grid item sm={3}>
                  <FormControlLabel
                    control={
                      <Checkbox
                        onChange={(evt) => this.applyFilter(evt, "SpedOnly")}
                        name="sped"
                        checked={spedOnly}
                      />
                    }
                    className="checkbox-color"
                    label="SPED only"
                    labelPlacement="start"
                    value="start"
                    style={{ marginRight: "5px" }}
                  />

                  <FormControlLabel
                    control={
                      <Checkbox
                        onChange={(evt) => this.applyFilter(evt, "ElOnly")}
                        name="el"
                        checked={elOnly}
                      />
                    }
                    className="checkbox-color"
                    label="EL only"
                    labelPlacement="start"
                    value="start"
                    style={{ marginRight: "5px" }}
                  />
                </Grid>
                <Grid item sm={1}>
                  {JSON.stringify(pgSelectedTask) !== "{}" && (
                    <Button
                      variant="outlined"
                      color="primary"
                      className="button-progress-toolbar"
                      onClick={this.failingGrade}
                    >
                      Failing Grades
                    </Button>
                  )}
                  <Tooltip
                    title="Export to CSV"
                    aria-label="csv-export-tooltip"
                  >
                    <IconButton
                      onClick={(evt) => this.downloadCSV(evt)}
                      className="csv-download-btn"
                      aria-label="csv-export"
                      style={{
                        color: "#ffffff",
                        padding: 5,
                        position: "absolute",
                        right: "50px",
                        top: "-2px",
                        cursor: "pointer",
                      }}
                    >
                      <MaterialIcon icon="cloud_download" />
                    </IconButton>
                  </Tooltip>
                  <Tooltip title="Grades Help" aria-label="info-btn-tooltip">
                    <IconButton
                      onClick={(evt) =>
                        this.setState({
                          openHelpInfo: !this.state.openHelpInfo,
                        })
                      }
                      className="tools-btn"
                      aria-label="info-btn"
                      style={{
                        color: "#ffffff",
                        position: "absolute",
                        right: "0px",
                        top: "-9px",
                      }}
                    >
                      <MaterialIcon icon="help" />
                    </IconButton>
                  </Tooltip>
                </Grid>
              </Grid>
            </Toolbar>
          </AppBar>
          <DataTable
            noHeader={false}
            columns={[...ProgressGradeStudentDetailsColumns]}
            pointerOnHover={true}
            styles={customStyles}
            onRowClicked={this.handleRowClick}
            pagination={true}
            paginationPerPage={25}
            actions={
              <TextField
                onChange={this.filterSearchField}
                value={searchText}
                placeholder="Search here ..."
                type="text"
                style={{ width: 225 }}
              />
            }
            paginationRowsPerPageOptions={[25, 50, 100, 200]}
            fixedHeader={true}
            fixedHeaderScrollHeight={"65vh"}
            highlightOnHover={true}
            data={pgFilterDataList || [{}]}
          />
        </Paper>
        <CSVLink
          ref={this.csvLink}
          data={csvExport.data}
          className="hidden"
          filename={csvExport.fileName}
          target="_self"
        />
        <HelpDialog
          open={openHelpInfo}
          handleClose={() => this.setState({ openHelpInfo: false })}
        />
        <ProgressGradeDialog
          open={openProgressGradeInfo && pgProgressGradeList.length > 0}
          rowData={selectedRowData}
          handleClose={() =>
            this.setState({
              openProgressGradeInfo: false,
              pgProgressGradeList: [],
              selectedRowData: null,
            })
          }
          data={pgProgressGradeList || [{}]}
        />
        <ConfirmationDialog
          open={openConfirmation && pgFailingGradeList.length > 0}
          handleClose={() => this.setState({ openConfirmation: false })}
          onSuccess={this.downloadFailingGrade}
        />
      </div>
    );
  }
}

const mapStateToProps = () =>
  createStructuredSelector({
    searchText: selectors.getSearchText(),
    pgSchoolList: loginSelectors.getSchoolList(),
    pgStudentList: selectors.getPgStudentList(),
    pgGradingTaskList: selectors.getPgGradingTask(),
    pgSelectedSchool: selectors.getPgSelectedSchool(),
    pgTermList: selectors.getPgTerm(),
    pgActualDataList: selectors.getPgActualDataList(),
    pgFilterDataList: selectors.getPgFilterDataList(),
    pgSelectedTerm: selectors.getPgSelectedTerm(),
    pgSelectedTask: selectors.getPgSelectedTask(),
    pgProgressGradeList: selectors.getPgProgressGrade(),
    pgFailingGradeList: selectors.getPgFailingGrade(),
    spedOnly: selectors.getPgSpedOnly(),
    elOnly: selectors.getPgElOnly(),
    userData: loginSelectors.getLoginData(),
  });
const mapDispatchToProps = (dispatch) => ({
  loadPgSchoolList: (req) => dispatch(actions.loadPgSchoolList(req)),
  loadPgTerm: (req) => dispatch(actions.loadPgTerm(req)),
  loadPgGradingTask: (req) => dispatch(actions.loadPgGradingTask(req)),
  loadPgStudentList: (req) => dispatch(actions.loadPgStudentList(req)),
  loadPgProgressGrade: (req) => dispatch(actions.loadPgProgressGrade(req)),
  setPgProgressGrade: (req) => dispatch(actions.setPgProgressGrade(req)),
  setPgGradingTask: (val) => dispatch(actions.setPgGradingTask(val)),
  setPgSelectedSchool: (obj) => dispatch(actions.setPgSelectedSchool(obj)),
  setPgSelectedTerm: (obj) => dispatch(actions.setPgSelectedTerm(obj)),
  setPgFilterData: (data) => dispatch(actions.setPgFilterData(data)),
  setPgSelectedTask: (obj) => dispatch(actions.setPgSelectedTask(obj)),
  setPgSpedOnly: (val) => dispatch(actions.setPgSpedOnly(val)),
  setPgElOnly: (val) => dispatch(actions.setPgElOnly(val)),
  loadPgFailingGrade: (req) => dispatch(actions.loadPgFailingGrade(req)),
  setSearchText: (req) => dispatch(actions.setSearchText(req)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ProgressGrade);
