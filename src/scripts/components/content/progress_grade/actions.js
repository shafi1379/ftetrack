export const SET_PG_SCHOOL_LIST = "SET_PG_SCHOOL_LIST";
export const setPgSchoolList = (pgSchoolList) => {
  return { type: SET_PG_SCHOOL_LIST, pgSchoolList };
};

export const LOAD_PG_SCHOOL_LIST = "LOAD_PG_SCHOOL_LIST";
export const loadPgSchoolList = (req) => {
  return { type: LOAD_PG_SCHOOL_LIST, req };
};

export const SET_PG_TERM = "SET_PG_TERM";
export const setPgTerm = (pgTermList) => {
  return { type: SET_PG_TERM, pgTermList };
};

export const LOAD_PG_TERM = "LOAD_PG_TERM";
export const loadPgTerm = (req) => {
  return { type: LOAD_PG_TERM, req };
};

export const SET_PG_GRADING_TASK = "SET_PG_GRADING_TASK";
export const setPgGradingTask = (pgGradingTaskList) => {
  return { type: SET_PG_GRADING_TASK, pgGradingTaskList };
};

export const LOAD_PG_GRADING_TASK = "LOAD_PG_GRADING_TASK";
export const loadPgGradingTask = (req) => {
  return { type: LOAD_PG_GRADING_TASK, req };
};

export const SET_PG_STUDENT_LIST = "SET_PG_STUDENT_LIST";
export const setPgStudentList = (pgStudentList) => {
  return { type: SET_PG_STUDENT_LIST, pgStudentList };
};

export const LOAD_PG_STUDENT_LIST = "LOAD_PG_STUDENT_LIST";
export const loadPgStudentList = (req) => {
  return { type: LOAD_PG_STUDENT_LIST, req };
};

export const SET_PG_PROGRESS_GRADE = "SET_PG_PROGRESS_GRADE";
export const setPgProgressGrade = (pgProgressGradeList) => {
  return { type: SET_PG_PROGRESS_GRADE, pgProgressGradeList };
};

export const LOAD_PG_PROGRESS_GRADE = "LOAD_PG_PROGRESS_GRADE";
export const loadPgProgressGrade = (req) => {
  return { type: LOAD_PG_PROGRESS_GRADE, req };
};

export const LOAD_PG_FAILING_GRADE = "LOAD_PG_FAILING_GRADE";
export const loadPgFailingGrade = (req) => {
  return { type: LOAD_PG_FAILING_GRADE, req };
};

export const SET_PG_FAILING_GRADE = "SET_PG_FAILING_GRADE";
export const setPgFailingGrade = (pgFailingGradeList) => {
  return { type: SET_PG_FAILING_GRADE, pgFailingGradeList };
};

export const SET_PG_SPED_ONLY = "SET_PG_SPED_ONLY";
export const setPgSpedOnly = (spedOnly) => {
  return { type: SET_PG_SPED_ONLY, spedOnly };
};

export const SET_PG_EL_ONLY = "SET_PG_EL_ONLY";
export const setPgElOnly = (elOnly) => {
  return { type: SET_PG_EL_ONLY, elOnly };
};

export const SET_PG_ACTUAL_DATA = "SET_PG_ACTUAL_DATA";
export const setPgActualData = (pgActualDataList) => {
  return { type: SET_PG_ACTUAL_DATA, pgActualDataList };
};

export const SET_PG_FILTER_DATA = "SET_PG_FILTER_DATA";
export const setPgFilterData = (pgFilterDataList) => {
  return { type: SET_PG_FILTER_DATA, pgFilterDataList };
};

export const SET_PG_SELECTED_SCHOOL = "SET_PG_SELECTED_SCHOOL";
export const setPgSelectedSchool = (pgSelectedSchool) => {
  return { type: SET_PG_SELECTED_SCHOOL, pgSelectedSchool };
};

export const SET_PG_SELECTED_TERM = "SET_PG_SELECTED_TERM";
export const setPgSelectedTerm = (pgSelectedTerm) => {
  return { type: SET_PG_SELECTED_TERM, pgSelectedTerm };
};

export const SET_PG_SELECTED_TASK = "SET_PG_SELECTED_TASK";
export const setPgSelectedTask = (pgSelectedTask) => {
  return { type: SET_PG_SELECTED_TASK, pgSelectedTask };
};

export const SET_SEARCH_TEXT = "SET_SEARCH_TEXT";
export const setSearchText = (text) => {
  return { type: SET_SEARCH_TEXT, text };
};
