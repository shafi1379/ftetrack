import { createSelector } from "reselect";
import * as selectors from "../../../redux/selectors";

export const getPgSchoolList = () =>
  createSelector(selectors.progress_gradeState, (progress_gradeState) =>
    progress_gradeState.get("pgSchoolList").toJS(),
  );

export const getPgSelectedSchool = () =>
  createSelector(selectors.progress_gradeState, (progress_gradeState) =>
    progress_gradeState.get("pgSelectedSchool").toJS(),
  );

export const getPgGradingTask = () =>
  createSelector(selectors.progress_gradeState, (progress_gradeState) =>
    progress_gradeState.get("pgGradingTaskList").toJS(),
  );

export const getPgSelectedTask = () =>
  createSelector(selectors.progress_gradeState, (progress_gradeState) =>
    progress_gradeState.get("pgSelectedTask").toJS(),
  );

export const getPgTerm = () =>
  createSelector(selectors.progress_gradeState, (progress_gradeState) =>
    progress_gradeState.get("pgTermList").toJS(),
  );

export const getPgSelectedTerm = () =>
  createSelector(selectors.progress_gradeState, (progress_gradeState) =>
    progress_gradeState.get("pgSelectedTerm").toJS(),
  );

export const getPgStudentList = () =>
  createSelector(selectors.progress_gradeState, (progress_gradeState) =>
    progress_gradeState.get("pgStudentList").toJS(),
  );

export const getPgActualDataList = () =>
  createSelector(selectors.progress_gradeState, (progress_gradeState) =>
    progress_gradeState.get("pgActualDataList").toJS(),
  );

export const getPgFilterDataList = () =>
  createSelector(selectors.progress_gradeState, (progress_gradeState) =>
    progress_gradeState.get("pgFilterDataList").toJS(),
  );

export const getPgSpedOnly = () =>
  createSelector(selectors.progress_gradeState, (progress_gradeState) =>
    progress_gradeState.get("spedOnly"),
  );

export const getPgElOnly = () =>
  createSelector(selectors.progress_gradeState, (progress_gradeState) =>
    progress_gradeState.get("elOnly"),
  );

export const getPgProgressGrade = () =>
  createSelector(selectors.progress_gradeState, (progress_gradeState) =>
    progress_gradeState.get("pgProgressGradeList").toJS(),
  );

export const getPgFailingGrade = () =>
  createSelector(selectors.progress_gradeState, (progress_gradeState) =>
    progress_gradeState.get("pgFailingGradeList").toJS(),
  );

export const getSearchText = () =>
  createSelector(selectors.progress_gradeState, (progress_gradeState) =>
    progress_gradeState.get("searchText"),
  );
