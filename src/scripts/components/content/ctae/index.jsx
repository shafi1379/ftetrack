import React, { Component } from "react";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";
import * as loginSelectors from "../../../app/selectors";
import * as selectors from "./selectors";
import TextField from "@material-ui/core/TextField";
import {
  Box,
  Button,
  Paper,
  AppBar,
  Toolbar,
  Grid,
  Typography,
  Tooltip,
  IconButton,
} from "@material-ui/core";
import Select from "react-select";
import { CSVLink } from "react-csv";
import MaterialIcon from "../../generic/materialIcon";
import { selectCustomStyles } from "../../generic/reactSelectCustomization";
import * as actions from "./actions";
import DataTable from "react-data-table-component";
import {
  ctaeSummaryColumns,
  ctaePathWayColumns,
  ctaeDetailsColumns,
} from "../../../utilities/tableConfig";
import * as Utilities from "../../../utilities/utilities";
import InfoModal from "./containers/InfoModal";

const gradeFilterOptions = [
  { label: "All Grade", value: "" },
  { label: "12", value: "12" },
  { label: "11", value: "11" },
  { label: "10", value: "10" },
  { label: "09", value: "09" },
];

class CTAE extends Component {
  csvLink = React.createRef();

  state = {
    csvExport: {
      data: [],
      fileName: "ctae.csv",
    },
    showPathWay: false,
    openInfo: false,
    selectGradeFilter: { label: "All Grade", value: "" },
    selectedStudent: {},
    ctaeSummaryColumns: ctaeSummaryColumns.map((dash, index) => {
      if (dash.name === "# of Students") {
        dash.cell = (row) => (
          <span
            style={{
              color: "#683598",
              textDecoration: "underline",
              cursor: "pointer",
            }}
            onClick={this.handleAction.bind(this, row)}
          >
            {row.students}
          </span>
        );
      }
      return dash;
    }),
    ctaePathWayColumns: ctaePathWayColumns.map((dash, index) => {
      if (dash.name === "Student#") {
        dash.cell = (row) => (
          <span
            style={{
              color: "#683598",
              textDecoration: "underline",
              cursor: "pointer",
            }}
            onClick={this.handleAction.bind(this, row)}
          >
            {row.studentNumber}
          </span>
        );
      }
      return dash;
    }),
  };

  componentDidMount() {
    /* this.props.loadCtaeSchoolList({
      school: this.props.userData.loginData.school,
    }); */
    this.props.handleCallToRouter(null, "ctae");
    this.props.setSearchText(null);
    this.props.setCtaeFilterPathwayList([]);
  }

  downloadCSV = (evt) => {
    const currentTime = `_${new Date().getTime()}`;
    const school = this.props.selectedCtaeSchool;
    if (this.state.showPathWay) {
      const columnDetails = [...ctaePathWayColumns];
      const columns = {};
      columnDetails.forEach((obj) => {
        columns[obj["name"]] = obj["selector"];
      });
      const data = Utilities.generateExcelData(
        [...this.props.ctaeFilterPathwayList] || [],
        columns || {},
      );
      data.unshift([`School: ${school.label}`]);
      data.unshift([
        `CTAE Pathway Student Enrollment for: ${this.props.selectedCtaePathway["Pathway"]}`,
      ]);
      this.setState({
        csvExport: {
          data,
          fileName: `CTAE_Students${currentTime}.csv`,
        },
      });
    } else {
      const columnDetails = [...ctaeSummaryColumns];
      const columns = {};
      columnDetails.forEach((obj) => {
        columns[obj["name"]] = obj["selector"];
      });
      const data = Utilities.generateExcelData(
        [...this.props.ctaeSummaryList.summary] || [],
        columns || {},
      );
      data.unshift([`School: ${school.label}`]);
      data.unshift([`CTAE Pathway Enrollment Summary`]);
      this.setState({
        csvExport: {
          data,
          fileName: `CTAE_Summary${currentTime}.csv`,
        },
      });
    }
    setTimeout(() => {
      this.csvLink.current.link.click();
    });
  };

  handleDropDownChange = (val, evt, type) => {
    this.props["setSelectedCtae" + type]({ ...val });
    this.props.loadCtaeSummaryList({
      school: val.school + "",
    });
    this.setState({
      showPathWay: false,
    });
  };

  handleAction = (data, evt) => {
    const { showPathWay } = this.state;
    if (showPathWay) {
      this.props.loadCtaePathwayStudentList({
        personID: data.personID + "",
      });
      this.setState({ openInfo: true, selectedStudent: data });
    } else {
      this.props.loadCtaePathwayList({
        school: this.props.selectedCtaeSchool.school + "",
        pathway: data["Pathway_Code"],
      });
      this.props.setSelectedCtaePathway(data);
    }
    this.setState({
      showPathWay: true,
    });
  };

  handleBack = () => {
    this.setState({ showPathWay: false });
    this.props.setCtaePathwayList([]);
  };

  handleFilter = (val) => {
    if (val.value) {
      this.props.setCtaeFilterPathwayList(
        this.props.ctaePathwayList.filter((obj) => obj.grade === val.value),
      );
    } else {
      this.props.setCtaeFilterPathwayList(this.props.ctaePathwayList);
    }
    this.setState({
      selectGradeFilter: val,
    });
  };

  filterSearchField = (e) => {
    let value = e.target.value;
    if (!value.charAt(0) === "-") {
      value = "-Name:" + value;
    }
    if (value.length && this.props.ctaePathwayList.length) {
      this.props.setSearchText(value);
      const objKeys = Object.keys(this.props.ctaePathwayList[0]);
      const actualKeys = this.state.ctaePathWayColumns;
      this.props.setCtaeFilterPathwayList(
        this.props.ctaePathwayList.filter((item) => {
          return Utilities.generateFilteredData(
            item,
            value ? value : this.props.searchText,
            objKeys,
            actualKeys,
          );
        }),
      );
    } else {
      this.props.setSearchText(null);
      this.props.setCtaeFilterPathwayList(this.props.ctaePathwayList);
    }
  };

  downloadPathwayCompleters = () => {
    if (this.props.ctaeSummaryList?.details) {
      const currentTime = `_${new Date().getTime()}`;
      const school = this.props.selectedCtaeSchool;
      const columnDetails = [...ctaeDetailsColumns];
      const columns = {};
      columnDetails.forEach((obj) => {
        columns[obj["name"]] = obj["selector"];
      });
      const data = Utilities.generateExcelData(
        [...this.props.ctaeSummaryList.details] || [],
        columns || {},
      );
      data.unshift([`School: ${school.label}`]);
      data.unshift([`CTAE Pathway Completers`]);
      this.setState({
        csvExport: {
          data,
          fileName: `CTAE_pathway_completers${currentTime}.csv`,
        },
      });

      setTimeout(() => {
        this.csvLink.current.link.click();
      });
    }
  };

  render() {
    const {
      ctaeSchoolList,
      selectedCtaeSchool,
      ctaeSummaryList,
      ctaeFilterPathwayList,
      ctaePathwayStudentList,
      selectedCtaePathway,
    } = this.props;

    const {
      csvExport,
      showPathWay,
      openInfo,
      selectedStudent,
      selectGradeFilter,
      ctaeSummaryColumns,
      ctaePathWayColumns,
      searchText,
    } = this.state;

    return (
      <div className="page-under-construction pbis">
        <Paper style={{ padding: 2 }}>
          <AppBar style={{ position: "static", height: "32px" }}>
            <Toolbar
              style={{ minHeight: 30, float: "right", position: "relative" }}
            >
              <Grid item container sm={12} className="progress_grade_toolbar">
                <Grid item sm={3}>
                  <Typography>School</Typography>
                  <Select
                    styles={selectCustomStyles}
                    options={ctaeSchoolList}
                    className="pbis-dropdown"
                    placeholder={"Select School"}
                    value={selectedCtaeSchool}
                    onChange={(val, evt) =>
                      this.handleDropDownChange(val, evt, "School")
                    }
                  />
                </Grid>
                <Grid item sm={2}>
                  <Box>
                    <Button
                      size="small"
                      style={{
                        color: "#FFF",
                        fontSize: 12,
                        textTransform: "capitalize",
                        border: "1px solid #fff",
                        height: 22,
                        marginTop: 10,
                        position: "absolute",
                        right: 50,
                      }}
                      onClick={(evt) => {
                        this.downloadPathwayCompleters();
                      }}
                    >
                      Pathway Completers
                    </Button>
                  </Box>
                  <Tooltip
                    title="Export to CSV"
                    aria-label="csv-export-tooltip"
                    style={{ float: "right" }}
                  >
                    <IconButton
                      onClick={(evt) => this.downloadCSV(evt)}
                      className="csv-download-btn"
                      aria-label="csv-export"
                      style={{
                        color: "#ffffff",
                        padding: 5,
                        position: "absolute",
                        right: "5px",
                        top: "-2px",
                        cursor: "pointer",
                      }}
                    >
                      <MaterialIcon icon="cloud_download" />
                    </IconButton>
                  </Tooltip>
                </Grid>
              </Grid>
            </Toolbar>
          </AppBar>
          <CSVLink
            ref={this.csvLink}
            data={csvExport.data}
            className="hidden"
            filename={csvExport.fileName}
            target="_self"
          />
        </Paper>
        <Paper className="secondary-tab-paper" square>
          {!showPathWay && (
            <DataTable
              columns={ctaeSummaryColumns}
              onRowClicked={this.handleAction}
              noHeader={true}
              highlightOnHover={true}
              data={ctaeSummaryList.summary}
            />
          )}

          {showPathWay && (
            <div>
              <Grid
                style={{
                  padding: "10px 15px",
                  borderBottom: "1px solid lightgrey",
                  display: "inline-flex",
                  width: "100%",
                }}
              >
                <Grid item xs={1}>
                  <span
                    style={{
                      color: "#683598",
                      textDecoration: "underline",
                      cursor: "pointer",
                    }}
                    onClick={(evt) => this.handleBack()}
                  >
                    <strong>Back</strong>
                  </span>
                </Grid>
                <Grid item xs={4}>
                  <Typography style={{ fontSize: "14px" }}>
                    <strong>{"Students for Pathway: "}</strong>
                    {selectedCtaePathway["Pathway"]}
                  </Typography>
                </Grid>
                <Grid item xs={4}></Grid>
                <Grid item xs={3} className="ctae-filter">
                  <strong>Grade&nbsp;:&nbsp;</strong>
                  <Select
                    styles={selectCustomStyles}
                    options={gradeFilterOptions}
                    className="pbis-dropdown"
                    placeholder={"Select Grade"}
                    value={selectGradeFilter}
                    onChange={(val, evt) => this.handleFilter(val)}
                  />
                </Grid>
              </Grid>
              <DataTable
                columns={ctaePathWayColumns}
                onRowClicked={this.handleAction}
                noHeader={false}
                actions={
                  <TextField
                    onChange={this.filterSearchField}
                    placeholder="Search here ..."
                    value={searchText}
                    type="text"
                    style={{ width: 225 }}
                  />
                }
                highlightOnHover={true}
                pagination={true}
                paginationPerPage={25}
                paginationRowsPerPageOptions={[25, 50, 100, 200]}
                fixedHeader={true}
                fixedHeaderScrollHeight={"65vh"}
                data={ctaeFilterPathwayList}
              />
            </div>
          )}
        </Paper>
        {ctaePathwayStudentList.length > 0 && (
          <InfoModal
            open={openInfo && ctaePathwayStudentList.length > 0}
            data={ctaePathwayStudentList}
            rowData={selectedStudent}
            handleClose={(evt) => {
              this.props.setCtaePathwayStudentList([]);
              this.setState({ openInfo: !openInfo });
            }}
          />
        )}
      </div>
    );
  }
}

const mapStateToProps = () =>
  createStructuredSelector({
    searchText: selectors.getSearchText(),
    ctaeSchoolList: loginSelectors.getSchoolList(),
    ctaeSummaryList: selectors.getCtaeSummaryList(),
    ctaePathwayList: selectors.getCtaePathwayList(),
    ctaeFilterPathwayList: selectors.getCtaeFilterPathwayList(),
    ctaePathwayStudentList: selectors.getCtaePathwayStudentList(),
    selectedCtaeSchool: selectors.getSelectedCtaeSchool(),
    selectedCtaePathway: selectors.getSelectedCtaePathway(),
    userData: loginSelectors.getLoginData(),
  });
const mapDispatchToProps = (dispatch) => ({
  loadCtaeSchoolList: (req) => dispatch(actions.loadCtaeSchoolList(req)),
  loadCtaeSummaryList: (req) => dispatch(actions.loadCtaeSummaryList(req)),
  loadCtaePathwayList: (req) => dispatch(actions.loadCtaePathwayList(req)),
  setCtaePathwayList: (data) => dispatch(actions.setCtaePathwayList(data)),
  setCtaeFilterPathwayList: (data) =>
    dispatch(actions.setCtaeFilterPathwayList(data)),
  loadCtaePathwayStudentList: (req) =>
    dispatch(actions.loadCtaePathwayStudentList(req)),
  setCtaePathwayStudentList: (data) =>
    dispatch(actions.setCtaePathwayStudentList(data)),
  setSelectedCtaeSchool: (data) =>
    dispatch(actions.setSelectedCtaeSchool(data)),
  setSelectedCtaePathway: (data) =>
    dispatch(actions.setSelectedCtaePathway(data)),
  setSearchText: (data) => dispatch(actions.setSearchText(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CTAE);
