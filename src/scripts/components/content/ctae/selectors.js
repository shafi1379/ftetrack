import { createSelector } from "reselect";
import * as selectors from "../../../redux/selectors";

export const getCtaeSchoolList = () =>
  createSelector(selectors.ctaeState, (ctaeState) =>
    ctaeState.get("ctaeSchoolList").toJS(),
  );

export const getCtaeSummaryList = () =>
  createSelector(selectors.ctaeState, (ctaeState) =>
    ctaeState.get("ctaeSummaryList").toJS(),
  );

export const getSelectedCtaeSchool = () =>
  createSelector(selectors.ctaeState, (ctaeState) =>
    ctaeState.get("selectedCtaeSchool").toJS(),
  );

export const getCtaePathwayList = () =>
  createSelector(selectors.ctaeState, (ctaeState) =>
    ctaeState.get("ctaePathwayList").toJS(),
  );

export const getCtaePathwayStudentList = () =>
  createSelector(selectors.ctaeState, (ctaeState) =>
    ctaeState.get("ctaePathwayStudentList").toJS(),
  );

export const getSelectedCtaePathway = () =>
  createSelector(selectors.ctaeState, (ctaeState) =>
    ctaeState.get("selectedCtaePathway").toJS(),
  );

export const getCtaeFilterPathwayList = () =>
  createSelector(selectors.ctaeState, (ctaeState) =>
    ctaeState.get("ctaeFilterPathwayList").toJS(),
  );

export const getSearchText = () =>
  createSelector(selectors.ctaeState, (ctaeState) =>
    ctaeState.get("searchText"),
  );
