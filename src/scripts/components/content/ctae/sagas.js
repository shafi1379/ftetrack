import { select, all, call, fork, put, takeLatest } from "redux-saga/effects";
import { setProgressingValue } from "../../../app/actions";
import * as Api from "../../../services/api";
import { API_URLS } from "../../../utilities/constants";
import {
  setCtaeSchoolList,
  setCtaeSummaryList,
  setSelectedCtaeSchool,
  setCtaePathwayStudentList,
  setCtaePathwayList,
  setCtaeFilterPathwayList,
  LOAD_CTAE_SCHOOL_LIST,
  LOAD_CTAE_SUMMARY_LIST,
  LOAD_CTAE_PATHWAY_LIST,
  LOAD_CTAE_PATHWAY_STUDENT_LIST,
} from "./actions";

function* loadCtaeSchoolListRequested(request) {
  try {
    const { ctaeSchoolList } = (yield select((s) => s.get("ctae"))).toJS();
    if (!ctaeSchoolList.length) {
      yield put(setProgressingValue(true));
      const response = yield call(
        Api.doPostRequest,
        API_URLS.schools,
        request.req,
      );
      if (response) {
        const ctaeSchoolList = response.map((res) => ({
          ...res,
          label: res.schoolname,
          value: res.school,
        }));
        yield put(setCtaeSchoolList(ctaeSchoolList));
        yield put(setSelectedCtaeSchool(ctaeSchoolList[0]));
        yield put(setProgressingValue(false));
      }
    }
  } catch (error) {
    console.error(error);
  }
}

function* ctaeSchoolListListener() {
  yield takeLatest(LOAD_CTAE_SCHOOL_LIST, loadCtaeSchoolListRequested);
}

function* loadCtaeSummaryListRequested(request) {
  try {
    yield put(setProgressingValue(true));
    const response = yield call(
      Api.doPostRequest,
      API_URLS.ctaeSummary,
      request.req,
    );
    if (response) {
      yield put(setCtaeSummaryList(response));
      yield put(setProgressingValue(false));
    }
  } catch (error) {
    console.error(error);
  }
}

function* ctaeSummaryListListener() {
  yield takeLatest(LOAD_CTAE_SUMMARY_LIST, loadCtaeSummaryListRequested);
}

function* loadCtaePathwayListRequested(request) {
  try {
    yield put(setProgressingValue(true));
    const response = yield call(
      Api.doPostRequest,
      API_URLS.ctaeStudents,
      request.req,
    );
    if (response) {
      yield put(setCtaePathwayList(response));
      yield put(setCtaeFilterPathwayList(response));
      yield put(setProgressingValue(false));
    }
  } catch (error) {
    console.error(error);
  }
}

function* ctaePathwayListListener() {
  yield takeLatest(LOAD_CTAE_PATHWAY_LIST, loadCtaePathwayListRequested);
}

function* loadCtaePathwayStudentListRequested(request) {
  try {
    yield put(setProgressingValue(true));
    const response = yield call(
      Api.doPostRequest,
      API_URLS.ctaeStudentPathways,
      request.req,
    );
    if (response) {
      yield put(setCtaePathwayStudentList(response));
      yield put(setProgressingValue(false));
    }
  } catch (error) {
    console.error(error);
  }
}

function* ctaePathwayStudentListListener() {
  yield takeLatest(
    LOAD_CTAE_PATHWAY_STUDENT_LIST,
    loadCtaePathwayStudentListRequested,
  );
}

export default function* root() {
  yield all([
    fork(ctaeSchoolListListener),
    fork(ctaeSummaryListListener),
    fork(ctaePathwayListListener),
    fork(ctaePathwayStudentListListener),
  ]);
}
