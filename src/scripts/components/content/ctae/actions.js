export const SET_CTAE_SCHOOL_LIST = "SET_CTAE_SCHOOL_LIST";
export const setCtaeSchoolList = (ctaeSchoolList) => {
  return { type: SET_CTAE_SCHOOL_LIST, ctaeSchoolList };
};

export const LOAD_CTAE_SCHOOL_LIST = "LOAD_CTAE_SCHOOL_LIST";
export const loadCtaeSchoolList = (req) => {
  return { type: LOAD_CTAE_SCHOOL_LIST, req };
};

export const LOAD_CTAE_SUMMARY_LIST = "LOAD_CTAE_SUMMARY_LIST";
export const loadCtaeSummaryList = (req) => {
  return { type: LOAD_CTAE_SUMMARY_LIST, req };
};

export const SET_CTAE_SUMMARY_LIST = "SET_CTAE_SUMMARY_LIST";
export const setCtaeSummaryList = (ctaeSummaryList) => {
  return { type: SET_CTAE_SUMMARY_LIST, ctaeSummaryList };
};

export const LOAD_CTAE_PATHWAY_LIST = "LOAD_CTAE_PATHWAY_LIST";
export const loadCtaePathwayList = (req) => {
  return { type: LOAD_CTAE_PATHWAY_LIST, req };
};

export const SET_CTAE_PATHWAY_LIST = "SET_CTAE_PATHWAY_LIST";
export const setCtaePathwayList = (ctaePathwayList) => {
  return { type: SET_CTAE_PATHWAY_LIST, ctaePathwayList };
};

export const LOAD_CTAE_PATHWAY_STUDENT_LIST = "LOAD_CTAE_PATHWAY_STUDENT_LIST";
export const loadCtaePathwayStudentList = (req) => {
  return { type: LOAD_CTAE_PATHWAY_STUDENT_LIST, req };
};

export const SET_CTAE_PATHWAY_STUDENT_LIST = "SET_CTAE_PATHWAY_STUDENT_LIST";
export const setCtaePathwayStudentList = (ctaePathwayStudentList) => {
  return { type: SET_CTAE_PATHWAY_STUDENT_LIST, ctaePathwayStudentList };
};

export const SET_CTAE_FILTER_PATHWAY = "SET_CTAE_FILTER_PATHWAY";
export const setCtaeFilterPathwayList = (ctaeFilterPathwayList) => {
  return { type: SET_CTAE_FILTER_PATHWAY, ctaeFilterPathwayList };
};

export const SET_SELECTED_CTAE_SCHOOL = "SET_SELECTED_CTAE_SCHOOL";
export const setSelectedCtaeSchool = (selectedCtaeSchool) => {
  return { type: SET_SELECTED_CTAE_SCHOOL, selectedCtaeSchool };
};

export const SET_SELECTED_CTAE_PATHWAY = "SET_SELECTED_CTAE_PATHWAY";
export const setSelectedCtaePathway = (selectedCtaePathway) => {
  return { type: SET_SELECTED_CTAE_PATHWAY, selectedCtaePathway };
};

export const SET_SEARCH_TEXT = "SET_SEARCH_TEXT";
export const setSearchText = (text) => {
  return { type: SET_SEARCH_TEXT, text };
};
