import {
  SET_CTAE_SCHOOL_LIST,
  SET_SELECTED_CTAE_SCHOOL,
  SET_CTAE_SUMMARY_LIST,
  SET_CTAE_PATHWAY_LIST,
  SET_CTAE_PATHWAY_STUDENT_LIST,
  SET_SELECTED_CTAE_PATHWAY,
  SET_CTAE_FILTER_PATHWAY,
  SET_SEARCH_TEXT,
} from "./actions";
import { fromJS } from "immutable";

const initialState = fromJS({
  ctaeSchoolList: [],
  ctaeSummaryList: {},
  ctaePathwayList: [],
  ctaeFilterPathwayList: [],
  ctaePathwayStudentList: [],
  selectedCtaeSchool: { label: "Select a School ...", value: -1 },
  selectedCtaePathway: {},
  searchText: null,
});

const ctae = (state = initialState, action) => {
  switch (action.type) {
    case SET_CTAE_SCHOOL_LIST:
      return state.set("ctaeSchoolList", fromJS([...action.ctaeSchoolList]));
    case SET_CTAE_SUMMARY_LIST:
      return state.set(
        "ctaeSummaryList",
        fromJS({ ...action.ctaeSummaryList }),
      );
    case SET_CTAE_PATHWAY_LIST:
      return state.set("ctaePathwayList", fromJS([...action.ctaePathwayList]));
    case SET_CTAE_FILTER_PATHWAY:
      return state.set(
        "ctaeFilterPathwayList",
        fromJS([...action.ctaeFilterPathwayList]),
      );
    case SET_CTAE_PATHWAY_STUDENT_LIST:
      return state.set(
        "ctaePathwayStudentList",
        fromJS([...action.ctaePathwayStudentList]),
      );
    case SET_SELECTED_CTAE_SCHOOL:
      return state.set(
        "selectedCtaeSchool",
        fromJS({ ...action.selectedCtaeSchool }),
      );
    case SET_SELECTED_CTAE_PATHWAY:
      return state.set(
        "selectedCtaePathway",
        fromJS({ ...action.selectedCtaePathway }),
      );
    case SET_SEARCH_TEXT:
      return state.set("searchText", action.text);
    default:
      return state;
  }
};

export default ctae;
