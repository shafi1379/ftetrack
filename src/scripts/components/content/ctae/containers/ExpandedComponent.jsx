import React from "react";
import MaterialIcon from "../../../generic/materialIcon";

// eslint-disable-next-line react/prop-types
export default ({ data }) => (
  <>
    {data.children.map((obj, index) => (
      <div className="ctae-modal-expanded-style">
        <div>
          <MaterialIcon icon="insert_drive_file_outlined" />
          <span>{obj["Course"]}</span>
        </div>
        <div>{obj.grade}</div>
        <div>{obj.level}</div>
        <div
          style={{ color: parseFloat(obj.creditsEarned) > 0 ? "black" : "red" }}
        >
          {obj.endYear}
        </div>
        <div
          style={{ color: parseFloat(obj.creditsEarned) > 0 ? "black" : "red" }}
        >
          {obj.creditsEarned || "0.000"}
        </div>
        <div
          style={{ color: parseFloat(obj.creditsEarned) > 0 ? "black" : "red" }}
        >
          {obj.status}
        </div>
      </div>
    ))}
  </>
);
