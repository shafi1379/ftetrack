import React, { useState } from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Dialog from "@material-ui/core/Dialog";
import MuiDialogContent from "@material-ui/core/DialogContent";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Select from "react-select";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import { selectCustomStyles } from "../../../generic/reactSelectCustomization";
import { ctaeInfoColumns } from "../../../../utilities/tableConfig";
import DataTable from "react-data-table-component";
import ExpandedComponent from "./ExpandedComponent";
import MaterialIcon from "../../../generic/materialIcon";

const labelFilterOptions = [
  { label: "All Pathways", value: "" },
  { label: "Pathways with atleast a Level 2 course", value: "2" },
  { label: "Pathways with atleast a Level 3 course", value: "3" },
];

const useStyles = makeStyles((theme) => ({
  appBar: {
    position: "static",
  },
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(0.5),
    color: "#f9f9f9",
    padding: 10,
  },
}));

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

export default function InfoModalDialog(props) {
  const classes = useStyles();
  const [dropdownValue, setDropdownValue] = useState({
    label: "All Pathways",
    value: "",
  });
  const [filteredData, setFilteredData] = useState(props.data);
  const handleClose = () => {
    setDropdownValue({
      label: "All Pathways",
      value: "",
    });
    props.handleClose();
  };
  const handleFilter = (val) => {
    let data = props.data;
    if (val.value) {
      data = props.data.filter(
        (obj) => obj.maxLevel && Number(obj.maxLevel) >= Number(val.value),
      );
    }
    setFilteredData(data);
    setDropdownValue(val);
  };

  let expandedData = {};
  filteredData.forEach((obj) => {
    if (Object.keys(expandedData).includes(obj["Pathway"])) {
      expandedData[obj["Pathway"]]["children"].push({ ...obj });
    } else {
      expandedData[obj["Pathway"]] = {
        ...obj,
        children: [{ ...obj }],
      };
    }
  });
  let expandedArray = Object.keys(expandedData).map((key) => {
    return expandedData[key];
  });

  return (
    <>
      <Dialog
        open={props.open}
        onClose={handleClose}
        maxWidth={"lg"}
        aria-labelledby="customized-dialog-title"
        aria-describedby="customized-dialog-description"
      >
        <AppBar className={classes.appBar}>
          <Toolbar style={{ minHeight: 45 }}>
            <Typography className="capitalize" variant="button">
              {"Pathways for : " + props.rowData.studentName}
            </Typography>
            <IconButton
              aria-label="close"
              className={classes.closeButton}
              onClick={handleClose}
            >
              <CloseIcon />
            </IconButton>
          </Toolbar>
        </AppBar>
        <DialogContent dividers style={{ width: "930px", height: "550px" }}>
          <div className="ctae-modal-filter">
            <Select
              styles={selectCustomStyles}
              options={labelFilterOptions}
              placeholder={"Select Grade"}
              value={dropdownValue}
              onChange={(val, evt) => handleFilter(val)}
            />
          </div>
          <DataTable
            columns={ctaeInfoColumns}
            noHeader={true}
            expandableRows={true}
            expandableRowExpanded={(row) => true}
            expandableIcon={{
              collapsed: (
                <MaterialIcon icon="folder" style={{ color: "#3f51b5" }} />
              ),
              expanded: (
                <MaterialIcon icon="folder_open" style={{ color: "#3f51b5" }} />
              ),
            }}
            expandableRowsComponent={<ExpandedComponent />}
            data={expandedArray}
          />
        </DialogContent>
      </Dialog>
    </>
  );
}
