import {
  SET_HR_FILTER_DATA,
  SET_HR_GRADING_TASK,
  SET_HR_SCHOOL_LIST,
  SET_HR_TERM,
  SET_HR_SELECTED_TASK,
  SET_HR_SELECTED_SCHOOL,
  SET_HR_SELECTED_TERM,
  SET_HR_STUDENT_LIST,
  SET_HR_PROGRESS_GRADE,
  SET_HR_SELECTED_FILTER,
  SET_HR_ACTUAL_DATA,
  SET_SEARCH_TEXT,
} from "./actions";
import { fromJS } from "immutable";

const HRFilterForList = [
  {
    label: "All Students",
    value: ["-1"],
  },
  {
    label: "As Only",
    value: [1],
  },
  {
    label: "As & Bs Only",
    value: [2],
  },
  {
    label: "As Only (Core Courses)",
    value: [3],
  },
  {
    label: "As & Bs Only (Core Courses)",
    value: [4],
  },
];

const initialState = fromJS({
  hrSchoolList: [],
  hrFailingGradeList: [],
  hrSelectedSchool: { label: "Select a School ...", value: -1 },
  hrSelectedTerm: {},
  hrSelectedTask: [],
  hrActualDataList: [],
  hrFilterDataList: [],
  hrGradingTaskList: [],
  hrProgressGradeList: [],
  hrTermList: [],
  hrStudentList: [],
  hrFilterForList: HRFilterForList,
  hrSelectedFilter: HRFilterForList[0],
  searchText: null,
});

const progress_grade = (state = initialState, action) => {
  switch (action.type) {
    case SET_HR_SCHOOL_LIST:
      return state.set("hrSchoolList", fromJS([...action.hrSchoolList]));
    case SET_HR_STUDENT_LIST:
      return state.set("hrStudentList", fromJS([...action.hrStudentList]));
    case SET_HR_TERM:
      return state.set("hrTermList", fromJS([...action.hrTermList]));
    case SET_HR_ACTUAL_DATA:
      return state.set(
        "hrActualDataList",
        fromJS([...action.hrActualDataList]),
      );
    case SET_HR_FILTER_DATA:
      return state.set(
        "hrFilterDataList",
        fromJS([...action.hrFilterDataList]),
      );
    case SET_HR_GRADING_TASK:
      return state.set(
        "hrGradingTaskList",
        fromJS([...action.hrGradingTaskList]),
      );
    case SET_HR_SELECTED_TASK:
      return state.set("hrSelectedTask", fromJS([...action.hrSelectedTask]));
    case SET_HR_SELECTED_TERM:
      return state.set("hrSelectedTerm", fromJS({ ...action.hrSelectedTerm }));
    case SET_HR_SELECTED_SCHOOL:
      return state.set(
        "hrSelectedSchool",
        fromJS({ ...action.hrSelectedSchool }),
      );
    case SET_HR_PROGRESS_GRADE:
      return state.set(
        "hrProgressGradeList",
        fromJS([...action.hrProgressGradeList]),
      );
    case SET_HR_SELECTED_FILTER:
      return state.set(
        "hrSelectedFilter",
        fromJS({ ...action.hrSelectedFilter }),
      );
    case SET_SEARCH_TEXT:
      return state.set("searchText", action.text);
    default:
      return state;
  }
};

export default progress_grade;
