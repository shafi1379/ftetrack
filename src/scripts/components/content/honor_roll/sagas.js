import {
  LOAD_HR_GRADING_TASK,
  LOAD_HR_SCHOOL_LIST,
  LOAD_HR_TERM,
  setHrGradingTask,
  setHrFilterData,
  setHrActualData,
  setHrSchoolList,
  setHrTerm,
  setHrSelectedSchool,
  setHrSelectedTerm,
  LOAD_HR_STUDENT_LIST,
  setHrStudentList,
  LOAD_HR_PROGRESS_GRADE,
  setHrProgressGrade,
} from "./actions";

import { select, all, call, fork, put, takeLatest } from "redux-saga/effects";
import { setProgressingValue } from "../../../app/actions";
import * as Api from "../../../services/api";
import { API_URLS } from "../../../utilities/constants";

function* loadHrSchoolListRequested(request) {
  try {
    const { hrSchoolList } = (yield select((s) => s.get("honor_roll"))).toJS();
    if (!hrSchoolList.length) {
      yield put(setProgressingValue(true));
      const response = yield call(
        Api.doPostRequest,
        API_URLS.schools,
        request.req,
      );
      if (response) {
        const hrSchoolList = response.map((res) => ({
          ...res,
          label: res.schoolname,
          value: res.school,
        }));
        yield put(setHrSchoolList(hrSchoolList));
        yield put(setHrSelectedSchool(hrSchoolList[0]));
        yield put(setProgressingValue(false));
      }
    }
  } catch (error) {
    console.error(error);
  }
}

function* hrSchoolListListener() {
  yield takeLatest(LOAD_HR_SCHOOL_LIST, loadHrSchoolListRequested);
}

function* loadHrStudentListRequested(request) {
  try {
    yield put(setProgressingValue(true));
    const response = yield call(
      Api.doPostRequest,
      API_URLS.honorStudents,
      request.req,
    );
    if (response) {
      yield put(setHrStudentList(response));
      yield put(setHrActualData(response));
      yield put(setHrFilterData(response));
      yield put(setProgressingValue(false));
    }
  } catch (error) {
    console.error(error);
  }
}

function* hrStudentListListener() {
  yield takeLatest(LOAD_HR_STUDENT_LIST, loadHrStudentListRequested);
}

function* loadHrGradingTaskRequested(request) {
  try {
    yield put(setProgressingValue(true));
    const response = yield call(
      Api.doPostRequest,
      API_URLS.gradingTasks,
      request.req,
    );
    if (response) {
      const hrGradingTask = response.map((res) => ({
        ...res,
        label: res.taskName,
        value: res.taskID,
      }));
      yield put(setHrGradingTask(hrGradingTask));
      // yield put(setHrSelectedTask(hrGradingTask[0]));
      yield put(setProgressingValue(false));
    }
  } catch (error) {
    console.error(error);
  }
}

function* hrGradingTaskListener() {
  yield takeLatest(LOAD_HR_GRADING_TASK, loadHrGradingTaskRequested);
}

function* loadHrTermRequested(request) {
  try {
    yield put(setProgressingValue(true));
    const response = yield call(
      Api.doPostRequest,
      API_URLS.gradingTerms,
      request.req,
    );
    if (response) {
      const hrTermList = response.map((res) => ({
        ...res,
        label: res.termName,
        value: res.termID,
      }));
      yield put(setHrTerm(hrTermList));
      yield put(setHrSelectedTerm(hrTermList[0]));
      yield put(setProgressingValue(false));
    }
  } catch (error) {
    console.error(error);
  }
}

function* hrHrTermListener() {
  yield takeLatest(LOAD_HR_TERM, loadHrTermRequested);
}

function* loadHrProgressGradeRequested(request) {
  try {
    yield put(setProgressingValue(true));
    const response = yield call(
      Api.doPostRequest,
      API_URLS.progressGrades,
      request.req,
    );
    if (response) {
      yield put(setHrProgressGrade(response));
      yield put(setProgressingValue(false));
    }
  } catch (error) {
    console.error(error);
  }
}

function* hrProgressGradeListener() {
  yield takeLatest(LOAD_HR_PROGRESS_GRADE, loadHrProgressGradeRequested);
}

export default function* root() {
  yield all([
    fork(hrHrTermListener),
    fork(hrGradingTaskListener),
    fork(hrSchoolListListener),
    fork(hrStudentListListener),
    fork(hrProgressGradeListener),
  ]);
}
