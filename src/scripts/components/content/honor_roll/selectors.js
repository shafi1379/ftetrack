import { createSelector } from "reselect";
import * as selectors from "../../../redux/selectors";

export const getHrSchoolList = () =>
  createSelector(selectors.honor_rollState, (honor_rollState) =>
    honor_rollState.get("hrSchoolList").toJS(),
  );

export const getHrSelectedSchool = () =>
  createSelector(selectors.honor_rollState, (honor_rollState) =>
    honor_rollState.get("hrSelectedSchool").toJS(),
  );

export const getHrGradingTask = () =>
  createSelector(selectors.honor_rollState, (honor_rollState) =>
    honor_rollState.get("hrGradingTaskList").toJS(),
  );

export const getHrSelectedTask = () =>
  createSelector(selectors.honor_rollState, (honor_rollState) =>
    honor_rollState.get("hrSelectedTask").toJS(),
  );

export const getHrTerm = () =>
  createSelector(selectors.honor_rollState, (honor_rollState) =>
    honor_rollState.get("hrTermList").toJS(),
  );

export const getHrSelectedTerm = () =>
  createSelector(selectors.honor_rollState, (honor_rollState) =>
    honor_rollState.get("hrSelectedTerm").toJS(),
  );

export const getHrStudentList = () =>
  createSelector(selectors.honor_rollState, (honor_rollState) =>
    honor_rollState.get("hrStudentList").toJS(),
  );

export const getHrActualDataList = () =>
  createSelector(selectors.honor_rollState, (honor_rollState) =>
    honor_rollState.get("hrActualDataList").toJS(),
  );

export const getHrFilterDataList = () =>
  createSelector(selectors.honor_rollState, (honor_rollState) =>
    honor_rollState.get("hrFilterDataList").toJS(),
  );

export const getHrProgressGrade = () =>
  createSelector(selectors.honor_rollState, (honor_rollState) =>
    honor_rollState.get("hrProgressGradeList").toJS(),
  );

export const getHrFilterForList = () =>
  createSelector(selectors.honor_rollState, (honor_rollState) =>
    honor_rollState.get("hrFilterForList").toJS(),
  );

export const getHrSelectedFilter = () =>
  createSelector(selectors.honor_rollState, (honor_rollState) =>
    honor_rollState.get("hrSelectedFilter").toJS(),
  );

export const getSearchText = () =>
  createSelector(selectors.honor_rollState, (honor_rollState) =>
    honor_rollState.get("searchText"),
  );
