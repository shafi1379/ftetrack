export const SET_HR_SCHOOL_LIST = "SET_HR_SCHOOL_LIST";
export const setHrSchoolList = (hrSchoolList) => {
  return { type: SET_HR_SCHOOL_LIST, hrSchoolList };
};

export const LOAD_HR_SCHOOL_LIST = "LOAD_HR_SCHOOL_LIST";
export const loadHrSchoolList = (req) => {
  return { type: LOAD_HR_SCHOOL_LIST, req };
};

export const SET_HR_TERM = "SET_HR_TERM";
export const setHrTerm = (hrTermList) => {
  return { type: SET_HR_TERM, hrTermList };
};

export const LOAD_HR_TERM = "LOAD_HR_TERM";
export const loadHrTerm = (req) => {
  return { type: LOAD_HR_TERM, req };
};

export const SET_HR_GRADING_TASK = "SET_HR_GRADING_TASK";
export const setHrGradingTask = (hrGradingTaskList) => {
  return { type: SET_HR_GRADING_TASK, hrGradingTaskList };
};

export const LOAD_HR_GRADING_TASK = "LOAD_HR_GRADING_TASK";
export const loadHrGradingTask = (req) => {
  return { type: LOAD_HR_GRADING_TASK, req };
};

export const SET_HR_STUDENT_LIST = "SET_HR_STUDENT_LIST";
export const setHrStudentList = (hrStudentList) => {
  return { type: SET_HR_STUDENT_LIST, hrStudentList };
};

export const LOAD_HR_STUDENT_LIST = "LOAD_HR_STUDENT_LIST";
export const loadHrStudentList = (req) => {
  return { type: LOAD_HR_STUDENT_LIST, req };
};

export const SET_HR_PROGRESS_GRADE = "SET_HR_PROGRESS_GRADE";
export const setHrProgressGrade = (hrProgressGradeList) => {
  return { type: SET_HR_PROGRESS_GRADE, hrProgressGradeList };
};

export const LOAD_HR_PROGRESS_GRADE = "LOAD_HR_PROGRESS_GRADE";
export const loadHrProgressGrade = (req) => {
  return { type: LOAD_HR_PROGRESS_GRADE, req };
};

export const SET_HR_ACTUAL_DATA = "SET_HR_ACTUAL_DATA";
export const setHrActualData = (hrActualDataList) => {
  return { type: SET_HR_ACTUAL_DATA, hrActualDataList };
};

export const SET_HR_FILTER_DATA = "SET_HR_FILTER_DATA";
export const setHrFilterData = (hrFilterDataList) => {
  return { type: SET_HR_FILTER_DATA, hrFilterDataList };
};

export const SET_HR_SELECTED_SCHOOL = "SET_HR_SELECTED_SCHOOL";
export const setHrSelectedSchool = (hrSelectedSchool) => {
  return { type: SET_HR_SELECTED_SCHOOL, hrSelectedSchool };
};

export const SET_HR_SELECTED_TERM = "SET_HR_SELECTED_TERM";
export const setHrSelectedTerm = (hrSelectedTerm) => {
  return { type: SET_HR_SELECTED_TERM, hrSelectedTerm };
};

export const SET_HR_SELECTED_TASK = "SET_HR_SELECTED_TASK";
export const setHrSelectedTask = (hrSelectedTask) => {
  return { type: SET_HR_SELECTED_TASK, hrSelectedTask };
};

export const SET_HR_SELECTED_FILTER = "SET_HR_SELECTED_FILTER";
export const setHrSelectedFilter = (hrSelectedFilter) => {
  return { type: SET_HR_SELECTED_FILTER, hrSelectedFilter };
};

export const SET_SEARCH_TEXT = "SET_SEARCH_TEXT";
export const setSearchText = (text) => {
  return { type: SET_SEARCH_TEXT, text };
};
