import React, { Component } from "react";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";
import * as loginSelectors from "../../../app/selectors";
import * as selectors from "./selectors";
import * as actions from "./actions";
import DataTable from "react-data-table-component";
import { CSVLink } from "react-csv";
import Select, { components } from "react-select";
import * as Utilities from "../../../utilities/utilities";
import HonorRollDialog from "./containers/HonorRoll";
import TextField from "@material-ui/core/TextField";
import {
  Paper,
  AppBar,
  Toolbar,
  Grid,
  Tooltip,
  IconButton,
  Typography,
  Checkbox,
} from "@material-ui/core";
import MaterialIcon from "../../generic/materialIcon";
import {
  customStyles,
  HonorRollStudentDetails,
} from "../../../utilities/tableConfig";
import { HonorRollStudentDetailsHeaders } from "../../../utilities/tableHeaderNamesList";
import { selectCustomStyles } from "../../generic/reactSelectCustomization";

const MenuList = (props) => {
  const handleCheckBox = (evt, obj) => {
    if (evt.target.checked) {
      props.setValue([...props.selectProps.selectedValues, obj]);
    } else {
      props.setValue([
        ...props.selectProps.selectedValues.filter(
          (_obj) => _obj.label !== obj.label,
        ),
      ]);
    }
  };
  return (
    <components.MenuList {...props}>
      {props.options.map((obj) => {
        let values = props.selectProps.selectedValues;
        let selected = false;
        values.forEach((_obj) => {
          if (_obj.label === obj.label) {
            selected = true;
          }
        });
        return (
          <div className="checkbox-style">
            <Checkbox
              onChange={(evt) => handleCheckBox(evt, obj)}
              checked={selected}
            />
            {obj.label}
          </div>
        );
      })}
    </components.MenuList>
  );
};

class Honor_Roll extends Component {
  csvLink = React.createRef();
  state = {
    HonorRollStudentDetailsColumns: [...HonorRollStudentDetails].map((obj) => {
      if (obj.selector === "studentNumber") {
        obj.cell = (row) => (
          <span
            style={{
              color: "#683598",
              textDecoration: "underline",
              cursor: "pointer",
            }}
            onClick={this.handleCellClick.bind(this, row)}
          >
            {row.studentNumber}
          </span>
        );
      }
      return obj;
    }),
    selectedRowData: null,
    openProgressGradeInfo: false,
    csvExport: {
      data: [],
      fileName: "ews.csv",
    },
    selectedFilter: [-1],
    lastSelectedIds: "-1",
  };

  componentDidMount() {
    /* this.props.loadHrSchoolList({
      school: this.props.userData.loginData.school,
    }); */
    this.clearSearchField();
  }

  downloadCSV = (evt) => {
    const currentTime = `_${new Date().getTime()}`;
    const data = Utilities.generateExcelData(
      [...this.props.hrFilterDataList] || [],
      HonorRollStudentDetailsHeaders || {},
    );
    const { hrSelectedSchool, hrSelectedTerm, hrSelectedTask } = this.props;
    if (hrSelectedTask.length) {
      data.unshift([
        `Selected Task: ${hrSelectedTask.map((d) => d.label).join(",")}`,
      ]);
    }
    if (hrSelectedTerm) {
      data.unshift([`Selected Term: ${hrSelectedTerm["label"]}`]);
    }
    if (hrSelectedSchool) {
      data.unshift([`Selected School: ${hrSelectedSchool["label"]}`]);
    }
    this.setState({
      csvExport: {
        data,
        headers: data[3],
        fileName: `Honor_Roll_Students${currentTime}.csv`,
      },
    });
    setTimeout(() => {
      this.csvLink.current.link.click();
    });
  };

  handleDropDownChange = (obj, evt, type) => {
    if (type === "Task") {
      this.props["setHrSelected" + type]([...obj]);
    } else if (type !== "TaskCompleted") {
      this.props["setHrSelected" + type]({ ...obj });
    }
    const { hrSelectedTerm, hrSelectedSchool, hrSelectedTask, hrTermList } =
      this.props;
    if (type === "School") {
      this.props.loadHrTerm({
        school: obj.school,
        option: "2",
      });
      this.props.setHrSelectedTerm(hrTermList[0]);
      this.props.setHrSelectedTask([]);
      this.props.setHrFilterData([]);
      this.props.setHrGradingTask([]);
      this.setState({
        lastSelectedIds: "-1",
      });
    }
    if (type === "Term") {
      this.props.loadHrGradingTask({
        school: obj.schoolID,
        termID: obj.termID,
        option: "2",
      });
      this.props.setHrSelectedTask([]);
      this.props.setHrFilterData([]);
      this.props.setHrGradingTask([]);
      this.setState({
        lastSelectedIds: "-1",
      });
    }
    if (type === "TaskCompleted") {
      const taskIds = hrSelectedTask.map((val) => val.taskID).join(",");
      if (taskIds.length && this.state.lastSelectedIds !== taskIds) {
        this.props.loadHrStudentList({
          school: hrSelectedSchool.school,
          termID: hrSelectedTerm.termID,
          termName: hrSelectedTerm.termName,
          taskID: taskIds,
        });
        this.setState({
          lastSelectedIds: taskIds,
        });
      } else if (taskIds === "") {
        this.props.setHrFilterData([]);
      }
    }
    if (type === "Filter") {
      this.setState({ selectedFilter: obj.value });
      const studentList = this.selectionFilter(obj.value);
      if (studentList.length) {
        this.props.setHrFilterData(studentList);
      }
    }
  };

  selectionFilter = (filter) => {
    let filterArr = [];
    if (this.props.hrStudentList.length) {
      const filterVal = filter ? filter : this.state.selectedFilter;
      filterArr = this.props.hrActualDataList.filter((item) => {
        if (filterVal[0] === 1) {
          if (item.filterGroup === 1) {
            return true;
          } else {
            return false;
          }
        } else if (filterVal[0] === 2) {
          if (item.filterGroup === 2 || item.filterGroup === 1) {
            return true;
          } else {
            return false;
          }
        } else if (filterVal[0] === 3) {
          if (item.filterGroup === 1 || item.filterGroup === 3) {
            return true;
          } else {
            return false;
          }
        } else if (filterVal[0] === 4) {
          if (
            item.filterGroup === 1 ||
            item.filterGroup === 2 ||
            item.filterGroup === 3 ||
            item.filterGroup === 4
          ) {
            return true;
          } else {
            return false;
          }
        } else {
          return true;
        }
      });
    }
    return filterArr;
  };

  handleRowClick = (row) => {
    this.props.setHrProgressGrade([]);
    const { hrSelectedSchool, hrSelectedTerm, hrSelectedTask } = this.props;
    this.props.loadHrProgressGrade({
      school: hrSelectedSchool.school,
      termID: hrSelectedTerm.termID,
      termName: hrSelectedTerm.termName,
      //taskID: hrSelectedTask.taskID,
      taskID:
        hrSelectedTask && hrSelectedTask.length > 0
          ? hrSelectedTask.map((val) => val.taskID).join(",")
          : "-1",
      personID: row.personID,
    });
    this.setState({
      openProgressGradeInfo: true,
      selectedRowData: row,
    });
  };

  handleCellClick = (row) => {
    this.handleRowClick(row);
  };

  filterSearchField = (e) => {
    let value = e.target.value;
    if (!value.charAt(0) === "-") {
      value = "-Name:" + value;
    }
    if (value.length && this.props.hrActualDataList.length) {
      this.props.setSearchText(value);
      const objKeys = Object.keys(this.props.hrActualDataList[0]);
      const actualKeys = this.state.HonorRollStudentDetailsColumns;
      let filteredDatas = this.selectionFilter();
      filteredDatas = filteredDatas.filter((item) => {
        return Utilities.generateFilteredData(
          item,
          value ? value : this.props.searchText,
          objKeys,
          actualKeys,
        );
      });
      this.props.setHrFilterData(filteredDatas);
    } else {
      this.props.setSearchText(null);
      this.props.setHrFilterData(this.selectionFilter());
    }
  };

  clearSearchField = () => {
    const { hrSchoolList, hrTermList, hrGradingTaskList, hrSelectedTask } =
      this.props;
    if (
      hrSchoolList?.length &&
      hrTermList?.length &&
      hrGradingTaskList?.length &&
      hrSelectedTask?.length
    ) {
      this.props.setHrFilterData(this.selectionFilter());
    } else {
      this.props.setHrFilterData([]);
    }
    this.props.setSearchText(null);
  };

  render() {
    const {
      openProgressGradeInfo,
      HonorRollStudentDetailsColumns,
      csvExport,
      selectedRowData,
    } = this.state;
    const {
      hrSchoolList,
      hrFilterDataList,
      hrSelectedSchool,
      hrProgressGradeList,
      hrTermList,
      hrSelectedTerm,
      hrGradingTaskList,
      hrSelectedTask,
      hrFilterForList,
      hrSelectedFilter,
      searchText,
    } = this.props;

    return (
      <div className="page-under-construction ews">
        <Paper style={{ padding: 2 }}>
          <AppBar style={{ position: "static", height: "32px" }}>
            <Toolbar
              style={{ minHeight: 30, float: "right", position: "relative" }}
            >
              <Grid item container sm={12} className="progress_grade_toolbar">
                <Grid item sm={3}>
                  <Typography>School</Typography>
                  <Select
                    styles={selectCustomStyles}
                    options={hrSchoolList}
                    className="ews-dropdown"
                    placeholder={"Select School"}
                    value={hrSelectedSchool}
                    onChange={(val, evt) =>
                      this.handleDropDownChange(val, evt, "School")
                    }
                  />
                </Grid>

                <Grid item sm={2}>
                  {hrTermList.length > 0 && (
                    <React.Fragment>
                      <Typography>Term</Typography>
                      <Select
                        styles={selectCustomStyles}
                        options={hrTermList}
                        className="ews-dropdown"
                        value={hrSelectedTerm}
                        onChange={(val, evt) =>
                          this.handleDropDownChange(val, evt, "Term")
                        }
                      />
                    </React.Fragment>
                  )}
                </Grid>

                <Grid item sm={3} style={{ marginLeft: "30px" }}>
                  {hrGradingTaskList.length > 0 && (
                    <React.Fragment>
                      <Typography>Grading&nbsp;Task</Typography>
                      <Select
                        styles={selectCustomStyles}
                        isMulti={true}
                        closeMenuOnSelect={false}
                        inputValue={hrSelectedTask
                          .map((obj) => obj.label)
                          .join(" | ")}
                        components={{ MenuList }}
                        options={hrGradingTaskList}
                        onMenuClose={(val, evt) =>
                          this.handleDropDownChange(val, evt, "TaskCompleted")
                        }
                        placeholder={"Select Taks"}
                        className="ews-dropdown"
                        selectedValues={hrSelectedTask}
                        value={[]}
                        onChange={(val, evt) =>
                          this.handleDropDownChange(val, evt, "Task")
                        }
                      />
                    </React.Fragment>
                  )}
                </Grid>

                <Grid item sm={3}>
                  <Typography>Filter for:</Typography>
                  <Select
                    styles={selectCustomStyles}
                    options={hrFilterForList}
                    className="ews-dropdown"
                    value={hrSelectedFilter}
                    onChange={(val, evt) =>
                      this.handleDropDownChange(val, evt, "Filter")
                    }
                  />
                </Grid>
                <Grid item sm={1}>
                  <Tooltip
                    title="Export to CSV"
                    aria-label="csv-export-tooltip"
                    style={{ float: "right" }}
                  >
                    <IconButton
                      onClick={(evt) => this.downloadCSV(evt)}
                      className="csv-download-btn"
                      aria-label="csv-export"
                      style={{
                        color: "#ffffff",
                        padding: 5,
                        position: "absolute",
                        right: "5px",
                        top: "-2px",
                        cursor: "pointer",
                      }}
                    >
                      <MaterialIcon icon="cloud_download" />
                    </IconButton>
                  </Tooltip>
                </Grid>
              </Grid>
            </Toolbar>
          </AppBar>
          <DataTable
            noHeader={false}
            columns={[...HonorRollStudentDetailsColumns]}
            pointerOnHover={true}
            styles={customStyles}
            onRowClicked={this.handleRowClick}
            actions={
              <TextField
                onChange={this.filterSearchField}
                placeholder="Search here ..."
                type="text"
                value={searchText}
                style={{ width: 225 }}
              />
            }
            pagination={true}
            paginationPerPage={25}
            paginationRowsPerPageOptions={[25, 50, 100, 200]}
            fixedHeader={true}
            fixedHeaderScrollHeight={"65vh"}
            highlightOnHover={true}
            data={hrFilterDataList || [{}]}
          />
        </Paper>
        <CSVLink
          ref={this.csvLink}
          data={csvExport.data}
          className="hidden"
          filename={csvExport.fileName}
          target="_self"
        />
        <HonorRollDialog
          open={openProgressGradeInfo && hrProgressGradeList.length > 0}
          rowData={selectedRowData}
          handleClose={() =>
            this.setState({
              openProgressGradeInfo: false,
              hrProgressGradeList: [],
              selectedRowData: null,
            })
          }
          data={hrProgressGradeList || [{}]}
        />
      </div>
    );
  }
}

const mapStateToProps = () =>
  createStructuredSelector({
    searchText: selectors.getSearchText(),
    hrSchoolList: loginSelectors.getSchoolList(),
    hrStudentList: selectors.getHrStudentList(),
    hrGradingTaskList: selectors.getHrGradingTask(),
    hrSelectedSchool: selectors.getHrSelectedSchool(),
    hrTermList: selectors.getHrTerm(),
    hrActualDataList: selectors.getHrActualDataList(),
    hrFilterDataList: selectors.getHrFilterDataList(),
    hrSelectedTerm: selectors.getHrSelectedTerm(),
    hrSelectedTask: selectors.getHrSelectedTask(),
    hrProgressGradeList: selectors.getHrProgressGrade(),
    hrFilterForList: selectors.getHrFilterForList(),
    hrSelectedFilter: selectors.getHrSelectedFilter(),
    userData: loginSelectors.getLoginData(),
  });
const mapDispatchToProps = (dispatch) => ({
  loadHrSchoolList: (req) => dispatch(actions.loadHrSchoolList(req)),
  loadHrTerm: (req) => dispatch(actions.loadHrTerm(req)),
  loadHrGradingTask: (req) => dispatch(actions.loadHrGradingTask(req)),
  loadHrStudentList: (req) => dispatch(actions.loadHrStudentList(req)),
  loadHrProgressGrade: (req) => dispatch(actions.loadHrProgressGrade(req)),
  setHrProgressGrade: (req) => dispatch(actions.setHrProgressGrade(req)),
  setHrGradingTask: (val) => dispatch(actions.setHrGradingTask(val)),
  setHrSelectedSchool: (obj) => dispatch(actions.setHrSelectedSchool(obj)),
  setHrSelectedTerm: (obj) => dispatch(actions.setHrSelectedTerm(obj)),
  setHrFilterData: (data) => dispatch(actions.setHrFilterData(data)),
  setHrSelectedTask: (obj) => dispatch(actions.setHrSelectedTask(obj)),
  setHrSelectedFilter: (obj) => dispatch(actions.setHrSelectedFilter(obj)),
  setSearchText: (obj) => dispatch(actions.setSearchText(obj)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Honor_Roll);
