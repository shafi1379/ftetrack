import React, { Component } from "react";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";
import * as loginSelectors from "../../../app/selectors";
import * as selectors from "./selectors";
import * as actions from "./actions";
import * as mainActions from "../../../app/actions";
import { selectCustomStyles } from "../../generic/reactSelectCustomization";
import * as TableConfig from "../../../utilities/tableConfig";
import { StudentsAtRiskColumns } from "../../../utilities/tableHeaderNamesList";
import DataTable from "react-data-table-component";
import Select from "react-select";
import TextField from "@material-ui/core/TextField";
import {
  Paper,
  AppBar,
  Toolbar,
  FormControlLabel,
  Checkbox,
  IconButton,
  Tooltip,
  Grid,
} from "@material-ui/core";
import MaterialIcon from "../../generic/materialIcon";
import { CSVLink } from "react-csv";
import GradeHelpDialog from "./containers/helpDialog";
import StudentsDetailsDialog from "../../generic/details_dialog";
import * as Utilities from "../../../utilities/utilities";
import PieChart from "../../generic/pieChartDialog.jsx";

const FilterOptions = [
  {
    label: "All Students",
    value: "all",
  },
  {
    label: "Attendance Only",
    value: "ewsAttend",
  },
  {
    label: "Behavior only",
    value: "ewsBehavior",
  },
  {
    label: "Grades Only",
    value: "ewsCourse",
  },
  {
    label: "Retention Only",
    value: "ewsRetain",
  },
  {
    label: "Attandance, Behavior & Grades",
    value: ["ewsAttend", "ewsBehavior", "ewsCourse"],
  },
];

class Ews extends Component {
  csvLink = React.createRef();
  state = {
    openPieChartModal: false,
    studentsAtRiskColumns: [...TableConfig.studentsAtRiskColumns].map((obj) => {
      if (obj.selector === "studentNumber") {
        obj.cell = (row) => (
          <span
            style={{
              color: "#683598",
              textDecoration: "underline",
              cursor: "pointer",
            }}
            onClick={this.handleCellClick.bind(this, row)}
          >
            {row.studentNumber}
          </span>
        );
      }
      return obj;
    }),
    selectedFilter: {
      label: "All Students",
      value: "all",
    },
    SPED: false,
    EL: false,
    EIP: false,
    openDetailsinfo: false,
    csvExport: {
      data: [],
      fileName: "ews.csv",
    },
    openHelpInfo: false,
    selectedRow: {},
  };

  componentDidMount() {
    /* this.props.loadSchoolList({
      school: this.props.userData.loginData.school,
    }); */
    this.clearSearchField();
  }

  handleDropDownChange = (option, evt, type) => {
    if (type === "School") {
      this.props.loadStudentsAtRisk({
        school: "" + option.value,
      });
      this.props.setSelectedSchool(option);
    } else {
      this.props.setProgressingValue(true);
      this.setState({
        selectedFilter: option,
      });
      setTimeout(() => this.filterEWSStudentAtRiskDatas());
    }
  };

  handleCellClick = (data, evt) => {
    this.props.loadStudentsDetails({
      personID: "" + data.personID,
    });
    this.setState({
      openDetailsinfo: true,
      selectedRow: data,
    });
  };

  downloadCSV = (evt) => {
    const currentTime = `_${new Date().getTime()}`;
    const columns = StudentsAtRiskColumns;
    const data = Utilities.generateExcelData(
      [...this.props.studentsAtRiskFilterList] || [],
      columns || {},
    );
    const school = this.props.selectedSchool;
    data.unshift([`${school && school.label}`]);
    this.setState({
      csvExport: {
        data,
        fileName: `AtRisk_Students${currentTime}.csv`,
      },
    });
    setTimeout(() => {
      this.csvLink.current.link.click();
    });
  };

  openGradeInfo = () =>
    this.setState({ openHelpInfo: !this.state.openHelpInfo });

  openStudentsDetailsInfo = () =>
    this.setState({ openDetailsinfo: !this.state.openDetailsinfo });

  applyFilter = (evt, type) => {
    this.props.setProgressingValue(true);
    this.setState({
      [type]: evt.target.checked,
    });
    setTimeout(() => this.filterEWSStudentAtRiskDatas());
  };

  filterEWSStudentAtRiskDatas = () => {
    const option = this.state.selectedFilter;
    let filteredDatas = [];
    if (Array.isArray(option.value)) {
      filteredDatas = [...this.props.studentsAtRiskList].filter(
        (obj) =>
          obj[option.value[0]] && obj[option.value[1]] && obj[option.value[2]],
      );
    } else {
      if (option.value === "all") {
        filteredDatas = [...this.props.studentsAtRiskList];
      } else {
        filteredDatas = [...this.props.studentsAtRiskList].filter(
          (obj) => obj[option.value],
        );
      }
    }
    filteredDatas = this.filterCheckBoxData(filteredDatas);
    this.props.setEwsStudentsAtRiskFilter(filteredDatas);
    setTimeout(() => this.props.setProgressingValue(false));
  };

  clearSearchField = () => {
    this.props.setSearchText(null);
    this.props.setEwsStudentsAtRiskFilter(this.props.studentsAtRiskList);
  };

  filterCheckBoxData = (filteredDatas) => {
    const SPED = this.state.SPED;
    const EL = this.state.EL;
    const EIP = this.state.EIP;
    if (SPED) {
      filteredDatas = filteredDatas.filter((obj) => obj["sped"] === "Y");
    }
    if (EL) {
      filteredDatas = filteredDatas.filter((obj) => obj["EL"] === "Y");
    }
    if (EIP) {
      filteredDatas = filteredDatas.filter((obj) => obj["eip"] === "Y");
    }
    return filteredDatas;
  };

  filterSearchField = (e) => {
    let value = e.target.value;
    if (!value.charAt(0) === "-") {
      value = "-Name:" + value;
    }
    if (value.length && this.props.studentsAtRiskList.length) {
      this.props.setSearchText(value);
      const objKeys = Object.keys(this.props.studentsAtRiskList[0]);
      const actualKeys = this.state.studentsAtRiskColumns;
      let filteredDatas = this.props.studentsAtRiskList.filter((item) => {
        return Utilities.generateFilteredData(
          item,
          value ? value : this.props.searchText,
          objKeys,
          actualKeys,
        );
      });
      filteredDatas = this.filterCheckBoxData(filteredDatas);
      this.props.setEwsStudentsAtRiskFilter(filteredDatas);
    } else {
      this.props.setSearchText(null);
      this.props.setEwsStudentsAtRiskFilter(
        this.filterCheckBoxData(this.props.studentsAtRiskList),
      );
    }
  };

  render() {
    const {
      openPieChartModal,
      studentsAtRiskColumns,
      selectedFilter,
      SPED,
      EL,
      EIP,
      csvExport,
      openHelpInfo,
      openDetailsinfo,
      selectedRow,
    } = this.state;

    const {
      schoolList,
      studentsAtRiskFilterList,
      studentsDetailsList,
      selectedSchool,
      searchText,
      plotData,
    } = this.props;

    return (
      <div className="page-under-construction ews">
        <Paper style={{ padding: 2 }}>
          <AppBar style={{ position: "static", height: "32px" }}>
            <Toolbar
              style={{ minHeight: 30, float: "right", position: "relative" }}
            >
              <Grid item container sm={12}>
                <div style={{ paddingTop: "12px", fontSize: "12px" }}>
                  <span style={{ float: "right", marginRight: "5px" }}>
                    School
                  </span>
                </div>
                <Grid item sm={3}>
                  <Select
                    styles={selectCustomStyles}
                    options={schoolList}
                    className="ews-dropdown"
                    placeholder={"Select School"}
                    value={selectedSchool}
                    onChange={(val, evt) =>
                      this.handleDropDownChange(val, evt, "School")
                    }
                  />
                </Grid>
                <Grid item sm={1}>
                  <span
                    style={{
                      float: "right",
                      paddingTop: "12px",
                      fontSize: "12px",
                      marginRight: "5px",
                    }}
                  >
                    {"Filter for"}
                  </span>
                </Grid>
                <Grid item sm={2}>
                  <Select
                    options={FilterOptions}
                    className="ews-dropdown"
                    styles={selectCustomStyles}
                    value={selectedFilter}
                    onChange={(val, evt) =>
                      this.handleDropDownChange(val, evt, "Filter")
                    }
                  />
                </Grid>
                <Grid item sm={5}>
                  <FormControlLabel
                    control={
                      <Checkbox
                        onChange={(evt) => this.applyFilter(evt, "SPED")}
                        name="sped"
                        checked={SPED}
                      />
                    }
                    className="checkbox-color"
                    label="SPED only"
                    labelPlacement="start"
                    value="start"
                    style={{ marginRight: "20px" }}
                  />

                  <FormControlLabel
                    control={
                      <Checkbox
                        onChange={(evt) => this.applyFilter(evt, "EL")}
                        name="el"
                        checked={EL}
                      />
                    }
                    className="checkbox-color"
                    label="EL only"
                    labelPlacement="start"
                    value="start"
                    style={{ marginRight: "20px" }}
                  />

                  <FormControlLabel
                    control={
                      <Checkbox
                        onChange={(evt) => this.applyFilter(evt, "EIP")}
                        name="ed"
                        checked={EIP}
                      />
                    }
                    className="checkbox-color"
                    label="ED"
                    labelPlacement="start"
                    value="start"
                    style={{ marginRight: "20px" }}
                  />
                </Grid>
                <Grid item sm={3}>
                  <Tooltip title="PieChart" aria-label="pie-chart-tooltip">
                    <IconButton
                      onClick={(evt) =>
                        this.setState({ openPieChartModal: true })
                      }
                      className="pie-chart-btn"
                      aria-label="pie-chart"
                      style={{
                        color: "#ffffff",
                        padding: 5,
                        position: "absolute",
                        right: "50px",
                        top: "-2px",
                        cursor: "pointer",
                      }}
                    >
                      <MaterialIcon icon="pie_chart" />
                    </IconButton>
                  </Tooltip>
                  <Tooltip
                    title="Export to CSV"
                    aria-label="csv-export-tooltip"
                  >
                    <IconButton
                      onClick={(evt) => this.downloadCSV(evt)}
                      className="csv-download-btn"
                      aria-label="csv-export"
                      style={{
                        color: "#ffffff",
                        padding: 5,
                        position: "absolute",
                        right: "100px",
                        top: "-2px",
                        cursor: "pointer",
                      }}
                    >
                      <MaterialIcon icon="cloud_download" />
                    </IconButton>
                  </Tooltip>
                  <Tooltip title="EWS Help" aria-label="info-btn-tooltip">
                    <IconButton
                      onClick={(evt) =>
                        this.setState({
                          openHelpInfo: !this.state.openHelpInfo,
                        })
                      }
                      className="tools-btn"
                      aria-label="info-btn"
                      style={{
                        color: "#ffffff",
                        position: "absolute",
                        right: "0px",
                        top: "-9px",
                      }}
                    >
                      <MaterialIcon icon="help" />
                    </IconButton>
                  </Tooltip>
                </Grid>
              </Grid>
            </Toolbar>
          </AppBar>
          <DataTable
            noHeader={false}
            columns={[...studentsAtRiskColumns]}
            pointerOnHover={true}
            highlightOnHover={true}
            actions={
              <TextField
                onChange={this.filterSearchField}
                placeholder="Search here ..."
                type="text"
                value={searchText}
                style={{ width: 225 }}
              />
            }
            pagination={true}
            paginationPerPage={25}
            paginationRowsPerPageOptions={[25, 50, 100, 200]}
            fixedHeader={true}
            fixedHeaderScrollHeight={"65vh"}
            data={studentsAtRiskFilterList || [{}]}
          />
        </Paper>
        <CSVLink
          ref={this.csvLink}
          data={csvExport.data}
          className="hidden"
          filename={csvExport.fileName}
          target="_self"
        />
        <GradeHelpDialog
          open={openHelpInfo}
          openGradeInfo={this.openGradeInfo}
        />
        {Object.keys(this.props.studentsDetailsList).length > 0 && (
          <StudentsDetailsDialog
            open={openDetailsinfo}
            openStudentsDetailsInfo={this.openStudentsDetailsInfo}
            row={selectedRow}
            data={studentsDetailsList}
          />
        )}
        {openPieChartModal && (
          <PieChart
            title="EWS"
            open={openPieChartModal}
            onClose={() =>
              this.setState({
                openPieChartModal: !openPieChartModal,
              })
            }
            data={plotData}
          />
        )}
      </div>
    );
  }
}

const mapStateToProps = () =>
  createStructuredSelector({
    searchText: selectors.getSearchText(),
    studentsAtRiskList: selectors.getEwsStudentsAtRisk(),
    studentsAtRiskFilterList: selectors.getEwsStudentsAtRiskFilter(),
    studentsDetailsList: selectors.getEwsStudentsDetails(),
    schoolList: loginSelectors.getSchoolList(),
    selectedSchool: selectors.getEwsSelectedSchool(),
    userData: loginSelectors.getLoginData(),
    plotData: selectors.getPlotData(),
  });
const mapDispatchToProps = (dispatch) => ({
  loadSchoolList: (school) => dispatch(actions.loadEwsSchoolList(school)),
  setEwsStudentsAtRiskFilter: (payload) =>
    dispatch(actions.setEwsStudentsAtRiskFilter(payload)),
  setSelectedSchool: (school) => dispatch(actions.setEwsSelectedSchool(school)),
  loadStudentsAtRisk: (request) =>
    dispatch(actions.loadEwsStudentsAtRisk(request)),
  loadStudentsDetails: (request) =>
    dispatch(actions.loadEwsStudentsDetails(request)),
  setProgressingValue: (payload) =>
    dispatch(mainActions.setProgressingValue(payload)),
  setSearchText: (payload) => dispatch(actions.setSearchText(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Ews);
