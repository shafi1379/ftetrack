import { all, call, fork, put, select, takeLatest } from "redux-saga/effects";
import {
  LOAD_EWS_STUDENTS_AT_RISK,
  LOAD_EWS_STUDENTS_DETAILS,
  LOAD_EWS_SCHOOL_LIST,
  setEwsSchoolList,
  setEwsStudentsAtRisk,
  setEwsStudentsAtRiskFilter,
  setEwsStudentsDetails,
  setEwsSelectedSchool,
  setPlotData,
} from "./actions";
import { setProgressingValue } from "../../../app/actions";
import * as Api from "../../../services/api";
import { API_URLS } from "../../../utilities/constants";
import * as Utilities from "../../../utilities/utilities";

function* loadSchoolListRequested(request) {
  try {
    const { schoolList } = (yield select((s) => s.get("ews"))).toJS();
    if (!schoolList.length) {
      yield put(setProgressingValue(true));
      const response = yield call(
        Api.doPostRequest,
        API_URLS.schools,
        request.school,
      );
      if (response) {
        const schoolList = response.map((res) => ({
          ...res,
          label: res.schoolname,
          value: res.school,
        }));
        yield put(setEwsSchoolList(schoolList));
        yield put(setEwsSelectedSchool(schoolList[0]));
        yield put(setProgressingValue(false));
      }
    }
  } catch (error) {
    console.error(error);
  }
}

function* schoolListListener() {
  yield takeLatest(LOAD_EWS_SCHOOL_LIST, loadSchoolListRequested);
}

function* loadStudentsAtRisk({ request }) {
  try {
    yield put(setProgressingValue(true));
    console.log(
      "reuestURL " + API_URLS.studentsAtRisk + "   api" + Api.doPostRequest,
    );
    const response = yield call(
      Api.doPostRequest,
      API_URLS.studentsAtRisk,
      request,
    );
    if (response && response.students) {
      yield put(setEwsStudentsAtRisk(response.students));
      yield put(setEwsStudentsAtRiskFilter(response.students));
      const raceList = Utilities.getRaceList(response.students);
      yield put(setPlotData([raceList, response.enrollment[0]]));
      yield put(setProgressingValue(false));
    }
  } catch (error) {
    console.error(error);
  }
}

function* studentsAtRiskListener() {
  yield takeLatest(LOAD_EWS_STUDENTS_AT_RISK, loadStudentsAtRisk);
}

function* loadStudentsDetails({ request }) {
  try {
    yield put(setProgressingValue(true));
    const response = yield call(
      Api.doPostRequest,
      API_URLS.studentsDetails,
      request,
    );
    if (response) {
      yield put(setProgressingValue(false));
      yield put(setEwsStudentsDetails(response));
    }
  } catch (error) {
    console.error(error);
  }
}

function* studentsDetailsListener() {
  yield takeLatest(LOAD_EWS_STUDENTS_DETAILS, loadStudentsDetails);
}

export default function* root() {
  yield all([
    fork(studentsAtRiskListener),
    fork(studentsDetailsListener),
    fork(schoolListListener),
  ]);
}
