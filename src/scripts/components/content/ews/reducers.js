import { fromJS } from "immutable";
import {
  SET_EWS_STUDENTS_AT_RISK,
  SET_EWS_STUDENTS_AT_RISK_FILTER,
  SET_EWS_STUDENTS_DETAILS,
  SET_EWS_SCHOOL_LIST,
  SET_EWS_SELECTED_SCHOOL,
  SET_SEARCH_TEXT,
  SET_PLOT_DATA,
} from "./actions";

const initialState = fromJS({
  studentsAtRiskList: [],
  studentsAtRiskFilterList: [],
  studentsDetailsList: {},
  schoolList: [],
  selectedSchool: { label: "Select a School ...", value: -1 },
  searchText: null,
  plotData: [],
});

const ews = (state = initialState, action) => {
  switch (action.type) {
    case SET_EWS_SCHOOL_LIST:
      return state.set("schoolList", fromJS([...action.schoolList]));
    case SET_EWS_STUDENTS_AT_RISK:
      return state.set(
        "studentsAtRiskList",
        fromJS([...action.studentsAtRiskList]),
      );
    case SET_EWS_STUDENTS_DETAILS:
      return state.set(
        "studentsDetailsList",
        fromJS({ ...action.studentsDetailsList }),
      );
    case SET_EWS_SELECTED_SCHOOL:
      return state.set("selectedSchool", fromJS({ ...action.selectedSchool }));
    case SET_EWS_STUDENTS_AT_RISK_FILTER:
      return state.set(
        "studentsAtRiskFilterList",
        fromJS([...action.studentsAtRiskFilterList]),
      );
    case SET_SEARCH_TEXT:
      return state.set("searchText", action.text);
    case SET_PLOT_DATA:
      return state.set("plotData", fromJS([...action.plotData]));
    default:
      return state;
  }
};

export default ews;
