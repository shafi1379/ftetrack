import React from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Dialog from "@material-ui/core/Dialog";
import MuiDialogContent from "@material-ui/core/DialogContent";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles((theme) => ({
  appBar: {
    position: "static",
  },
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(0.5),
    color: "#f9f9f9",
    padding: 10,
  },
}));

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

export default function AttendanceHelpDialog(props) {
  const classes = useStyles();
  const handleClose = () => props.openGradeInfo();

  return (
    <>
      <Dialog
        open={props.open}
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        aria-describedby="customized-dialog-description"
      >
        <AppBar className={classes.appBar}>
          <Toolbar style={{ minHeight: 45 }}>
            <Typography className="capitalize" variant="button">
              Early Warning System (EWS) Help
            </Typography>
            <IconButton
              aria-label="close"
              className={classes.closeButton}
              onClick={handleClose}
            >
              <CloseIcon />
            </IconButton>
          </Toolbar>
        </AppBar>
        <DialogContent dividers>
          <Typography variant="body2" gutterBottom>
            <strong>
              Student must meet one (or more) of the following criteria :
            </strong>
          </Typography>
          <ul>
            <li>
              <Typography variant="body2" gutterBottom>
                Must be <strong>actively</strong> enrolled at the school.
              </Typography>
            </li>
            <li>
              <Typography variant="body2" gutterBottom>
                Absent at least 10% of school days (
                <strong>upto last completed school day so far</strong>)
              </Typography>
            </li>
            <li>
              <Typography variant="body2" gutterBottom>
                Failing in Language Arts, Math, Science or Social Studies course
              </Typography>
            </li>
            <li>
              <Typography variant="body2" gutterBottom>
                Has a discipline Incident with ISS, OSS, Expulsion, Suspended
                from Riding the Bus, Assigned To an Alternative School, Court Or
                Juvenile System Referral
              </Typography>
            </li>
            <li style={{ marginTop: "30px" }}>
              <Typography variant="body2" gutterBottom>
                Was retained at the End of <strong>Last</strong> school year.
              </Typography>
            </li>
            <li style={{ marginBottom: "200px" }}>
              <Typography variant="body2" gutterBottom>
                Performed below grade level proficiency in ELA or Math of
                Georgia Milestones (EOG) tests Last year
              </Typography>
            </li>
          </ul>
        </DialogContent>
      </Dialog>
    </>
  );
}
