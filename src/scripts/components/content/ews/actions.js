export const LOAD_EWS_SCHOOL_LIST = "LOAD_EWS_SCHOOL_LIST";
export const loadEwsSchoolList = (school) => ({
  type: LOAD_EWS_SCHOOL_LIST,
  school,
});

export const SET_EWS_SCHOOL_LIST = "SET_EWS_SCHOOL_LIST";
export const setEwsSchoolList = (schoolList) => ({
  type: SET_EWS_SCHOOL_LIST,
  schoolList,
});

export const SET_EWS_SELECTED_SCHOOL = "SET_EWS_SELECTED_SCHOOL";
export const setEwsSelectedSchool = (selectedSchool) => ({
  type: SET_EWS_SELECTED_SCHOOL,
  selectedSchool,
});

export const SET_EWS_STUDENTS_AT_RISK = "SET_EWS_STUDENTS_AT_RISK";
export const setEwsStudentsAtRisk = (studentsAtRiskList) => ({
  type: SET_EWS_STUDENTS_AT_RISK,
  studentsAtRiskList,
});

export const SET_EWS_STUDENTS_AT_RISK_FILTER =
  "SET_EWS_STUDENTS_AT_RISK_FILTER";
export const setEwsStudentsAtRiskFilter = (studentsAtRiskFilterList) => ({
  type: SET_EWS_STUDENTS_AT_RISK_FILTER,
  studentsAtRiskFilterList,
});

export const SET_EWS_STUDENTS_DETAILS = "SET_EWS_STUDENTS_DETAILS";
export const setEwsStudentsDetails = (studentsDetailsList) => ({
  type: SET_EWS_STUDENTS_DETAILS,
  studentsDetailsList,
});

export const LOAD_EWS_STUDENTS_AT_RISK = "LOAD_EWS_STUDENTS_AT_RISK";
export const loadEwsStudentsAtRisk = (request) => ({
  type: LOAD_EWS_STUDENTS_AT_RISK,
  request,
});

export const LOAD_EWS_STUDENTS_DETAILS = "LOAD_EWS_STUDENTS_DETAILS";
export const loadEwsStudentsDetails = (request) => ({
  type: LOAD_EWS_STUDENTS_DETAILS,
  request,
});

export const SET_SEARCH_TEXT = "SET_SEARCH_TEXT";
export const setSearchText = (text) => {
  return { type: SET_SEARCH_TEXT, text };
};

export const SET_PLOT_DATA = "SET_PLOT_DATA";
export const setPlotData = (plotData) => ({
  type: SET_PLOT_DATA,
  plotData,
});
