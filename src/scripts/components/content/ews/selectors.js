import { createSelector } from "reselect";
import * as selectors from "../../../redux/selectors";

export const getEwsSchoolList = () =>
  createSelector(selectors.ewsState, (ewsState) =>
    ewsState.get("schoolList").toJS(),
  );

export const getEwsSelectedSchool = () =>
  createSelector(selectors.ewsState, (ewsState) =>
    ewsState.get("selectedSchool").toJS(),
  );

export const getEwsStudentsAtRisk = () =>
  createSelector(selectors.ewsState, (ewsState) =>
    ewsState.get("studentsAtRiskList").toJS(),
  );

export const getEwsStudentsAtRiskFilter = () =>
  createSelector(selectors.ewsState, (ewsState) =>
    ewsState.get("studentsAtRiskFilterList").toJS(),
  );

export const getEwsStudentsDetails = () =>
  createSelector(selectors.ewsState, (ewsState) =>
    ewsState.get("studentsDetailsList").toJS(),
  );

export const getSearchText = () =>
  createSelector(selectors.ewsState, (ewsState) => ewsState.get("searchText"));

export const getPlotData = () =>
  createSelector(selectors.ewsState, (ewsState) =>
    ewsState.get("plotData").toJS(),
  );
