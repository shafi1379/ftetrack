export const LOAD_ATT_SCHOOL_LIST = "LOAD_ATT_SCHOOL_LIST";
export const loadAttSchoolList = (school) => ({
  type: LOAD_ATT_SCHOOL_LIST,
  school,
});

export const SET_ATT_SCHOOL_LIST = "SET_ATT_SCHOOL_LIST";
export const setAttSchoolList = (schoolList) => ({
  type: SET_ATT_SCHOOL_LIST,
  schoolList,
});

export const LOAD_ATT_DASHBOARD_STUDENTS_LIST =
  "LOAD_ATT_DASHBOARD_STUDENTS_LIST";
export const loadAttDashboardStudentsList = (option) => ({
  type: LOAD_ATT_DASHBOARD_STUDENTS_LIST,
  option,
});

export const SET_ATT_DASHBOARD_STUDENTS_LIST =
  "SET_ATT_DASHBOARD_STUDENTS_LIST";
export const setAttDashboardStudentsList = (dashboardStudentsList) => ({
  type: SET_ATT_DASHBOARD_STUDENTS_LIST,
  dashboardStudentsList,
});

export const SET_ATT_SELECTED_SCHOOL = "SET_ATT_SELECTED_SCHOOL";
export const setAttSelectedSchool = (selectedSchool) => ({
  type: SET_ATT_SELECTED_SCHOOL,
  selectedSchool,
});

export const SET_ATT_SELECTED_SCHOOL_ADHOC = "SET_ATT_SELECTED_SCHOOL_ADHOC";
export const setAttSelectedSchoolAdhoc = (selectedSchoolAdhoc) => ({
  type: SET_ATT_SELECTED_SCHOOL_ADHOC,
  selectedSchoolAdhoc,
});

export const SET_ATT_SELECTED_ADA_PERCENT = "SET_ATT_SELECTED_ADA_PERCENT";
export const setAttSelectedADAPercent = (selectedADAPercent) => ({
  type: SET_ATT_SELECTED_ADA_PERCENT,
  selectedADAPercent,
});

export const SET_ATT_ADA_DATE_LIST = "SET_ATT_ADA_DATE_LIST";
export const setAttAdaDateList = (adaDateList) => ({
  type: SET_ATT_ADA_DATE_LIST,
  adaDateList,
});

export const SET_ATT_SELECTED_SCHOOL_DATA = "SET_ATT_SELECTED_SCHOOL_DATA";
export const setAttSelectedSchoolData = (selectedSchoolData) => ({
  type: SET_ATT_SELECTED_SCHOOL_DATA,
  selectedSchoolData,
});

export const SET_ATT_CURRENT_FULL_SCREEN_DIALOG =
  "SET_ATT_CURRENT_FULL_SCREEN_DIALOG";
export const setAttCurrentFullScreenDialog = (dialog) => ({
  type: SET_ATT_CURRENT_FULL_SCREEN_DIALOG,
  dialog,
});

export const SET_ATT_SELECTED_DAY = "SET_ATT_SELECTED_DAY";
export const setAttSelectedDay = (selectedDay) => ({
  type: SET_ATT_SELECTED_DAY,
  selectedDay,
});

export const SET_ATT_DATE_RANGE_PERIOD = "SET_ATT_DATE_RANGE_PERIOD";
export const setAttDateRangePeriod = (dateRangePeriod) => ({
  type: SET_ATT_DATE_RANGE_PERIOD,
  dateRangePeriod,
});

export const RUN_ATT_ADHOC_QUERY = "RUN_ATT_ADHOC_QUERY";
export const runAttAdhocQuery = (params) => ({
  type: RUN_ATT_ADHOC_QUERY,
  params,
});

export const SET_ATT_ADHOC_QUERY_RESULT = "SET_ATT_ADHOC_QUERY_RESULT";
export const setAttAdhocQueryResult = (adhocQueryResult) => ({
  type: SET_ATT_ADHOC_QUERY_RESULT,
  adhocQueryResult,
});

export const LOAD_STUDENT_ABCENTS_BY_DATE = "LOAD_STUDENT_ABCENTS_BY_DATE";
export const loadStudentAbcentsByDate = (option) => ({
  type: LOAD_STUDENT_ABCENTS_BY_DATE,
  option,
});

export const SET_STUDENT_ABCENTS_LIST = "SET_STUDENT_ABCENTS_LIST";
export const setStudentAbcentsList = (studentAbcents) => ({
  type: SET_STUDENT_ABCENTS_LIST,
  studentAbcents,
});
