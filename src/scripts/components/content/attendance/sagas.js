import { all, call, fork, put, select, takeLatest } from "redux-saga/effects";
import {
  LOAD_ATT_SCHOOL_LIST,
  LOAD_ATT_DASHBOARD_STUDENTS_LIST,
  RUN_ATT_ADHOC_QUERY,
  setAttSchoolList,
  setAttSelectedSchool,
  setAttSelectedSchoolData,
  setAttDashboardStudentsList,
  setStudentAbcentsList,
  setAttAdaDateList,
  setAttAdhocQueryResult,
  LOAD_STUDENT_ABCENTS_BY_DATE,
} from "./actions";
import { setProgressingValue } from "../../../app/actions";
import * as Api from "../../../services/api";
import { API_URLS } from "../../../utilities/constants";

function* loadSchoolListRequested(params) {
  try {
    if (params.school.school === "9999") {
      const { schoolList, adaDateList } = (yield select((s) =>
        s.get("attendance"),
      )).toJS();
      if (!(schoolList.length && adaDateList.length)) {
        yield put(setProgressingValue(true));
        const response = yield call(
          Api.doPostRequest,
          API_URLS.schools,
          params.school,
        );
        if (response) {
          const schoolList = response.map((res) => ({
            ...res,
            label: res.schoolname,
            value: res.school,
          }));
          yield put(setAttSchoolList(schoolList));
          yield put(setAttSelectedSchool(schoolList[0]));
        }
        const dateResponse = yield call(
          Api.doPostRequest,
          API_URLS.adadates,
          params.school,
        );
        if (dateResponse) {
          yield put(setAttAdaDateList(dateResponse));
          yield put(setProgressingValue(false));
        }
      }
    } else {
      yield put(setProgressingValue(true));
      yield put(setAttSelectedSchoolData({}));
      const response = yield call(
        Api.doPostRequest,
        API_URLS.schoolada,
        params.school,
      );
      if (response) {
        yield put(setAttSelectedSchoolData(response));
      }
      const dateResponse = yield call(
        Api.doPostRequest,
        API_URLS.adadates,
        params.school,
      );
      if (dateResponse) {
        yield put(setAttAdaDateList(dateResponse));
        yield put(setProgressingValue(false));
      }
    }
  } catch (error) {
    console.error(error);
  }
}

function* schoolListListener() {
  yield takeLatest(LOAD_ATT_SCHOOL_LIST, loadSchoolListRequested);
}

function* dashboardStudentsRequested({ option }) {
  try {
    yield put(setProgressingValue(true));
    yield put(setAttDashboardStudentsList([]));
    const response = yield call(
      Api.doPostRequest,
      API_URLS.dashboardstudents,
      option,
    );
    if (response) {
      yield put(setProgressingValue(false));
      yield put(setAttDashboardStudentsList(response));
    }
  } catch (error) {
    console.error(error);
  }
}

function* dashboardStudentsListener() {
  yield takeLatest(
    LOAD_ATT_DASHBOARD_STUDENTS_LIST,
    dashboardStudentsRequested,
  );
}

function* loadStudentAbcentsByDateRequested({ option }) {
  try {
    yield put(setProgressingValue(true));
    yield put(setStudentAbcentsList([]));
    const response = yield call(
      Api.doPostRequest,
      API_URLS.studentsabsentbydate,
      option,
    );
    if (response) {
      yield put(setProgressingValue(false));
      yield put(setStudentAbcentsList(response));
    }
  } catch (error) {
    console.error(error);
  }
}

function* loadStudentsAbcentByDateListener() {
  yield takeLatest(
    LOAD_STUDENT_ABCENTS_BY_DATE,
    loadStudentAbcentsByDateRequested,
  );
}

function* adhocQueryRequested({ params }) {
  try {
    yield put(setProgressingValue(true));
    yield put(setAttAdhocQueryResult([]));
    const response = yield call(Api.doPostRequest, API_URLS.adareport, params);
    if (response) {
      yield put(setProgressingValue(false));
      yield put(setAttAdhocQueryResult(response));
    }
  } catch (error) {
    console.error(error);
  }
}

function* adhocQueryListener() {
  yield takeLatest(RUN_ATT_ADHOC_QUERY, adhocQueryRequested);
}

export default function* root() {
  yield all([
    fork(schoolListListener),
    fork(dashboardStudentsListener),
    fork(adhocQueryListener),
    fork(loadStudentsAbcentByDateListener),
  ]);
}
