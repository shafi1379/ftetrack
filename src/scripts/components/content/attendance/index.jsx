import React, { Component } from "react";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import Paper from "@material-ui/core/Paper";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Select from "react-select";
import Tooltip from "@material-ui/core/Tooltip";
import Typography from "@material-ui/core/Typography";
import { withRouter } from "react-router-dom";
import { BrowserRouter as Router, Switch, Link } from "react-router-dom";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";
import * as loginSelectors from "../../../app/selectors";
import * as selectors from "./selectors";
import * as actions from "./actions";
import * as mainSelectors from "../../../app/selectors";
import * as mainActions from "../../../app/actions";
import Slider from "../../generic/airbnbSlider";
import MaterialIcon from "../../generic/materialIcon";
import RouteWithSubRoutes from "../../generic/subRoutes";
import { selectCustomStyles } from "../../generic/reactSelectCustomization";
import FullScreenDialog from "../../generic/fullScreenDialog";
import * as Utilities from "../../../utilities/utilities";
import AttendanceHelpDialog from "./containers/helpDialog";
import { CSVLink } from "react-csv";

class Attendance extends Component {
  csvLink = React.createRef();
  state = {
    value: "ada",
    openHelpInfo: false,
    csvExport: {
      data: [],
      fileName: "attendance.csv",
    },
  };

  componentWillMount() {
    this.props.handleCallToRouter(null, "attendance");
    this.props.history.replace("/attendance/ada");
    /* this.props.loadSchoolList({
      school: this.props.userData.loginData.school,
    }); */
  }

  handleDropdownChange = (param, option) => {
    if (param === "SelectedSchool") {
      this.props.loadSchoolList({ school: option.school });
    }
    this.props[`set${param}`](option);
  };

  handleCallToRouter = (event, newValue) => this.setState({ value: newValue });

  adhocQuery = () => {
    this.props.setCurrentFullScreenDialog("ada-adhoc");
    this.props.toggleFullScreenDialog(!this.props.openFullScreenDialog);
  };

  openAttendanceInfo = () =>
    this.setState({ openHelpInfo: !this.state.openHelpInfo });

  downloadCSV = () => {
    const currentTime = `_${new Date().getTime()}`;
    const locationPath = window.location.href;
    const currentPage = locationPath.substring(
      locationPath.lastIndexOf("/") + 1,
    );
    const selectedSchoolData = this.props.selectedSchoolData;
    if (currentPage === "ada" || currentPage === "dashboard") {
      const columns = {
        ada: {
          "Calendar Date": "school_date",
          Enrollments: "enrollments",
          Absences: "absences",
          ADA: "ada",
        },
        dashboard: {
          Title: "title",
          "# of Students": "students",
          Percent: "percent",
        },
      };
      const data = Utilities.generateExcelData(
        selectedSchoolData[currentPage] || [],
        columns[currentPage] || {},
      );
      let excelPageName = currentPage + currentTime;
      if (currentPage === "ada") {
        excelPageName = "Avg_Daily_Att" + currentTime;
        data.unshift([
          `${this.props.selectedSchool && this.props.selectedSchool.label}`,
        ]);
      } else if (currentPage === "dashboard") {
        excelPageName = "Attendance_Dashboard" + currentTime;
        data.unshift([`Attendance Dashboard`]);
      }
      this.setState({ csvExport: { data, fileName: `${excelPageName}.csv` } });
      setTimeout(() => {
        this.csvLink.current.link.click();
      });
    }
  };

  render() {
    const { value, openHelpInfo, csvExport } = this.state;
    const {
      routes,
      selectedSchool,
      selectedDay,
      schoolList,
      dayList,
      currentFullScreenDialog,
      adaDateList,
    } = this.props;
    return (
      <div style={{ padding: 2 }} className="attendance">
        <Paper className="sub-toolbar toolbar-header-style" elevation={3}>
          <Grid item md={12} xs={12} container className="vertical-align">
            <Grid
              item
              md={3}
              sm={6}
              xs={12}
              container
              className="vertical-align"
            >
              <Grid
                item
                style={{ textAlign: "right" }}
                md={3}
                xs={5}
                className="vertical-align"
              >
                <Typography className="mr-rt10" variant="caption">
                  {" "}
                  School:{" "}
                </Typography>
              </Grid>
              <Grid item md={9} xs={7} className="vertical-align">
                <Select
                  styles={selectCustomStyles}
                  value={selectedSchool}
                  onChange={this.handleDropdownChange.bind(
                    this,
                    "SelectedSchool",
                  )}
                  options={schoolList}
                />
              </Grid>
            </Grid>
            <Grid
              item
              md={3}
              sm={6}
              xs={12}
              container
              className="vertical-align"
            >
              <Grid
                item
                style={{ textAlign: "right" }}
                md={4}
                xs={5}
                className="vertical-align"
              >
                <Typography variant="caption"> Select Period: </Typography>
              </Grid>
              <Grid item md={8} xs={7} className="pad-tp3 vertical-align">
                <Slider data={adaDateList} />
              </Grid>
            </Grid>
            <Grid
              item
              md={3}
              sm={6}
              xs={12}
              container
              className="vertical-align"
            >
              <Grid
                item
                style={{ textAlign: "right" }}
                md={6}
                xs={5}
                className="vertical-align"
              >
                <Typography className="mr-rt10" variant="caption">
                  {" "}
                  Show Only:{" "}
                </Typography>
              </Grid>
              <Grid item md={6} xs={7} className="vertical-align">
                <Select
                  styles={selectCustomStyles}
                  value={selectedDay}
                  onChange={this.handleDropdownChange.bind(this, "SelectedDay")}
                  options={dayList}
                />
              </Grid>
            </Grid>
            <Grid
              item
              md={3}
              sm={6}
              xs={12}
              className="other-tools vertical-align"
            >
              <div className="other-tools-container vertical-align">
                <Tooltip title="Attendance Help" aria-label="info-btn-tooltip">
                  <IconButton
                    onClick={this.openAttendanceInfo}
                    className="tools-btn"
                    aria-label="info-btn"
                  >
                    <MaterialIcon icon="help" />
                  </IconButton>
                </Tooltip>
                <Tooltip title="Export to CSV" aria-label="csv-export-tooltip">
                  <IconButton
                    onClick={this.downloadCSV}
                    className="tools-btn"
                    aria-label="csv-export"
                  >
                    <MaterialIcon icon="cloud_download" />
                  </IconButton>
                </Tooltip>
                <CSVLink
                  ref={this.csvLink}
                  data={csvExport.data}
                  className="hidden"
                  filename={csvExport.fileName}
                  target="_self"
                />
                <Button
                  onClick={this.adhocQuery}
                  size="small"
                  variant="outlined"
                  color="primary"
                  className="addhoc-button-style"
                >
                  ADHOC Query
                </Button>
              </div>
            </Grid>
          </Grid>
        </Paper>
        <Router basename="/abc">
          <Paper className="secondary-tab-paper" square>
            <Tabs
              value={value}
              indicatorColor="primary"
              textColor="primary"
              onChange={this.handleCallToRouter.bind(value)}
              centered
            >
              <Tab
                label="ADA"
                className="minWidth90"
                value="ada"
                to="ada"
                component={Link}
              />
              <Tab
                label="Dashboard"
                className="minWidth60"
                value="dashboard"
                to="dashboard"
                component={Link}
              />
            </Tabs>
          </Paper>
          <Switch>
            {routes.map((route, i) => (
              <RouteWithSubRoutes key={i} {...route} />
            ))}
          </Switch>
        </Router>
        {currentFullScreenDialog === "ada-adhoc" && (
          <FullScreenDialog title={"ADA ADHOC Query"} />
        )}
        <AttendanceHelpDialog
          open={openHelpInfo}
          openAttendanceInfo={this.openAttendanceInfo}
        />
      </div>
    );
  }
}

const mapStateToProps = () =>
  createStructuredSelector({
    selectedSchool: selectors.getAttSelectedSchool(),
    schoolList: loginSelectors.getSchoolList(),
    selectedDay: selectors.getAttSelectedDay(),
    dayList: selectors.getAttDayList(),
    openFullScreenDialog: mainSelectors.getFullScreenDialog(),
    currentFullScreenDialog: selectors.getAttCurrentFullScreenDialog(),
    adaDateList: selectors.getAttAdaDateList(),
    selectedSchoolData: selectors.getAttSelectedSchoolData(),
    userData: loginSelectors.getLoginData(),
  });
const mapDispatchToProps = (dispatch) => ({
  setSelectedSchool: (school) => dispatch(actions.setAttSelectedSchool(school)),
  loadSchoolList: (school) => dispatch(actions.loadAttSchoolList(school)),
  setSelectedDay: (day) => dispatch(actions.setAttSelectedDay(day)),
  toggleFullScreenDialog: (open) =>
    dispatch(mainActions.toggleFullScreenDialog(open)),
  setCurrentFullScreenDialog: (dialog) =>
    dispatch(actions.setAttCurrentFullScreenDialog(dialog)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withRouter(Attendance));
