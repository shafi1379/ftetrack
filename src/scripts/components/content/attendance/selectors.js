import { createSelector } from "reselect";
import * as selectors from "../../../redux/selectors";

export const getAttSchoolList = () =>
  createSelector(selectors.attendanceState, (attendanceState) =>
    attendanceState.get("schoolList").toJS(),
  );

export const getAttSelectedSchool = () =>
  createSelector(selectors.attendanceState, (attendanceState) =>
    attendanceState.get("selectedSchool").toJS(),
  );

export const getAttSelectedSchoolAdhoc = () =>
  createSelector(selectors.attendanceState, (attendanceState) =>
    attendanceState.get("selectedSchoolAdhoc").toJS(),
  );

export const getAttSelectedADAPercent = () =>
  createSelector(selectors.attendanceState, (attendanceState) =>
    attendanceState.get("selectedADAPercent").toJS(),
  );

export const getAttAdaDateList = () =>
  createSelector(selectors.attendanceState, (attendanceState) =>
    attendanceState.get("adaDateList").toJS(),
  );

export const getAttSelectedSchoolData = () =>
  createSelector(selectors.attendanceState, (attendanceState) =>
    attendanceState.get("selectedSchoolData").toJS(),
  );

export const getAttDayList = () =>
  createSelector(selectors.attendanceState, (attendanceState) =>
    attendanceState.get("dayList").toJS(),
  );

export const getAttSelectedDay = () =>
  createSelector(selectors.attendanceState, (attendanceState) =>
    attendanceState.get("selectedDay").toJS(),
  );

export const getAttCurrentFullScreenDialog = () =>
  createSelector(selectors.attendanceState, (attendanceState) =>
    attendanceState.get("currentFullScreenDialog"),
  );

export const getAttDashboardStudentsList = () =>
  createSelector(selectors.attendanceState, (attendanceState) =>
    attendanceState.get("dashboardStudentsList").toJS(),
  );

export const getAttDateRangePeriod = () =>
  createSelector(selectors.attendanceState, (attendanceState) =>
    attendanceState.get("dateRangePeriod").toJS(),
  );

export const getAttAdhocQueryResult = () =>
  createSelector(selectors.attendanceState, (attendanceState) =>
    attendanceState.get("adhocQueryResult").toJS(),
  );

export const getAttADAPercentList = () =>
  createSelector(selectors.attendanceState, (attendanceState) =>
    attendanceState.get("ADAPercentList").toJS(),
  );

export const getStudentAbcentsList = () =>
  createSelector(selectors.attendanceState, (attendanceState) =>
    attendanceState.get("studentAbcentsList").toJS(),
  );
