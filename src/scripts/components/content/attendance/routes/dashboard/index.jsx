import React, { Component } from "react";
import { Tooltip, IconButton } from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import Select from "react-select";
import { Chart } from "react-google-charts";
import { CSVLink } from "react-csv";
import * as selectors from "../../selectors";
import * as actions from "../../actions";
import * as mainSelectors from "../../../../../app/selectors";
import * as mainActions from "../../../../../app/actions";
import FullScreenDialog from "../../../../generic/fullScreenDialog";
import { selectCustomStyles } from "../../../../generic/reactSelectCustomization";
import { attendRaceDescription } from "../../../pbis/routes/summary/Constants/summaryConstants";
import * as Utilities from "../../../../../utilities/utilities";
import MaterialIcon from "../../../../generic/materialIcon";

const month_Abbr = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];

const orderArray = [
  "All",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
];

// Create a map from orderArray for quick lookup
const orderMap = new Map(orderArray.map((item, index) => [item, index]));

const colors = [
  "#5DA5DA",
  "#FAA43A",
  "#60BD68",
  "#F17CB0",
  "#B276B2",
  "#B2912F",
  "#F15854",
  "#FF00FF",
  "#CAB2D6",
  "#FB9A99",
  "#B2DF8A",
  "#A6CEE3",
  "#A52A2A",
];

class Dashboard extends Component {
  csvLink = React.createRef();
  state = {
    csvExport: {
      data: [],
      fileName: "summary.csv",
    },
    selectedTitle: null,
    selectedMonth: null,
  };

  handleAction = (row) => {
    this.props.loadDashboardStudentsList({
      school: row.schoolID,
      code: row.code,
    });
    this.props.setCurrentFullScreenDialog("ada-dashboard");
    this.setState({ selectedTitle: row.title });
    this.props.toggleFullScreenDialog(!this.props.openFullScreenDialog);
  };

  handleDropdownChange = (param, option) => {
    this.setState({ [param]: option });
  };

  downloadCSV = () => {
    const currentTime = `_${new Date().getTime()}`;
    const columns = {
      "School Id": "schoolID",
      "School Name": "schoolName",
      "Year-Month": "year-month",
      "Attendance Year": "attyr",
      "Attendance Month": "attmo",
      "Race/Ethinicity": "description",
      "Avg. Dialy Attendance (%)": "percent",
      "School Days (value)": "value",
    };
    const _summary = [...this.props.selectedSchoolData?.summary].map((s) => {
      return {
        ...s,
        schoolName: this.props.selectedSchool.label,
        attmo:
          parseInt(s.attmo) <= 12 ? month_Abbr[parseInt(s.attmo) - 1] : "All",
        "year-month": `${s.attyr}-${s.attmo}`,
      };
    });
    const data = Utilities.generateExcelData(_summary || [], columns || {});
    this.setState({
      csvExport: {
        data,
        fileName: `Summary${currentTime}.csv`,
      },
    });
    setTimeout(() => {
      this.csvLink?.current?.link?.click();
    });
  };

  chartEvents = [
    {
      eventName: "select",
      callback: ({ chartWrapper }) => {
        const chart = chartWrapper.getChart();
        const selection = chart.getSelection();
        if (selection.length === 1) {
          const [selectedItem] = selection;
          const { row } = selectedItem;
          const selectedRow = this.props.selectedSchoolData.dashboard[row];
          if (this.props.selectedSchool.value !== 9999) {
            this.handleAction(selectedRow);
          }
        }
      },
    },
  ];

  render() {
    const { csvExport, selectedTitle, selectedMonth } = this.state;
    const { selectedSchoolData, currentFullScreenDialog } = this.props;
    const dashboardList = [];
    const childDashList = [];
    const monthList = [];

    if (selectedSchoolData?.dashboard?.length) {
      dashboardList.push([
        "Title",
        "# of Students",
        { role: "tooltip", type: "string", p: { html: true } },
      ]);
      selectedSchoolData.dashboard.forEach((dash) => {
        dashboardList.push([
          dash.title,
          parseInt(dash.students),
          `${dash.students} (${dash.percent}%)`,
        ]);
      });
    }

    if (selectedSchoolData?.summary?.length) {
      const monthData = Object.groupBy(
        selectedSchoolData?.summary,
        ({ attmo }) => attmo,
      );
      const months = Object.keys(monthData).sort(function (a, b) {
        return b - a;
      });
      if (months.length) {
        const _months = [...months]?.map((m) => ({
          label: month_Abbr[parseInt(m) - 1]
            ? month_Abbr[parseInt(m) - 1]
            : "All",
          value: m,
        }));
        const sortedMonths = [..._months].sort((a, b) => {
          return orderMap.get(a.label) - orderMap.get(b.label);
        });
        monthList.push(...sortedMonths);
        childDashList.push([
          "Race",
          "Percentage",
          { role: "tooltip", type: "string", p: { html: true } },
          { role: "style" },
        ]);
        monthData[selectedMonth?.value || months[0]].forEach((dash, ind) => {
          if (dash.label.charAt(1) !== "Z") {
            childDashList.push([
              attendRaceDescription[dash.label],
              parseFloat(dash.percent),
              `${dash.percent}%`,
              colors[ind],
            ]);
          }          
        });
      }
    }

    return (
      <>
        <Paper style={{ mb: 15 }}>
          {dashboardList.length ? (
            <Chart
              chartType="BarChart"
              width="100%"
              height="350px"
              data={dashboardList}
              chartEvents={this.chartEvents}
              options={{
                legend: { position: "none" },
                titlePosition: "none",
                bar: { groupWidth: "80%" },
                vAxis: {
                  textStyle: {
                    fontSize: 12,
                  },
                },
                focusTarget: "category",
                tooltip: { isHtml: true },
              }}
            />
          ) : (
            <></>
          )}
          {currentFullScreenDialog === "ada-dashboard" && (
            <FullScreenDialog title={"Student List for: " + selectedTitle} />
          )}
        </Paper>
        <Box m={2} />
        <Paper style={{ mt: 15, position: "relative" }}>
          {selectedSchoolData?.summary?.length ? (
            <>
              <Box
                style={{
                  position: "absolute",
                  right: "45%",
                  top: 20,
                  height: 60,
                  zIndex: 100,
                }}
              >
                <Typography variant="subtitle1">
                  ADA by School, Month, Ethnicity, Homeless, EL & SPED
                </Typography>
              </Box>
              <Box
                style={{
                  display: "flex",
                  justifyItems: "center",
                  alignItems: "center",
                  paddingTop: 15,
                  paddingLeft: 30,
                  position: "absolute",
                  zIndex: 10,
                  right: 20,
                }}
              >
                <Typography className="mr-rt10" variant="caption">
                  Attendance Month:
                </Typography>
                <Box minWidth={200}>
                  <Select
                    style={selectCustomStyles}
                    value={selectedMonth || monthList[0]}
                    onChange={this.handleDropdownChange.bind(
                      this,
                      "selectedMonth",
                    )}
                    options={monthList}
                  />
                </Box>
                <Box ml={2}>
                  <Tooltip
                    title="Export to CSV"
                    aria-label="csv-export-tooltip"
                  >
                    <IconButton
                      style={{ color: "#3F50B5", padding: 5 }}
                      onClick={(evt) => this.downloadCSV()}
                      className="csv-download-btn"
                      aria-label="csv-export"
                    >
                      <MaterialIcon icon="cloud_download" />
                    </IconButton>
                  </Tooltip>
                  <CSVLink
                    ref={this.csvLink}
                    data={csvExport.data}
                    className="hidden"
                    filename={csvExport.fileName}
                    target="_self"
                  />
                </Box>
              </Box>
              <Chart
                chartType="BarChart"
                width="100%"
                height="450px"
                data={childDashList}
                style={{ paddingTop: 10 }}
                colors={["red", "green", "yellow"]}
                options={{
                  legend: { position: "none" },
                  titlePosition: "none",
                  bar: { groupWidth: "80%" },
                  is3D: true,
                  vAxis: {
                    textStyle: {
                      fontSize: 13,
                    },
                  },
                  focusTarget: "category",
                  tooltip: { isHtml: true },
                }}
              />
            </>
          ) : (
            <></>
          )}
          {!(
            selectedSchoolData?.summary?.length ||
            selectedSchoolData?.dashboard?.length
          ) && (
            <Box
              style={{ display: "flex", justifyContent: "center", padding: 30 }}
            >
              {" "}
              Please select a school to view the detailed visualization.
            </Box>
          )}
        </Paper>
      </>
    );
  }
}

const mapStateToProps = () =>
  createStructuredSelector({
    selectedSchool: selectors.getAttSelectedSchool(),
    selectedSchoolData: selectors.getAttSelectedSchoolData(),
    openFullScreenDialog: mainSelectors.getFullScreenDialog(),
    currentFullScreenDialog: selectors.getAttCurrentFullScreenDialog(),
  });
const mapDispatchToProps = (dispatch) => ({
  toggleFullScreenDialog: (open) =>
    dispatch(mainActions.toggleFullScreenDialog(open)),
  setCurrentFullScreenDialog: (dialog) =>
    dispatch(actions.setAttCurrentFullScreenDialog(dialog)),
  loadDashboardStudentsList: (option) =>
    dispatch(actions.loadAttDashboardStudentsList(option)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
