import React, { Component } from "react";
import ToggleButton from "@material-ui/lab/ToggleButton";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import Icon from "@material-ui/core/Icon";
import Paper from "@material-ui/core/Paper";
import Plot from "react-plotly.js";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";
import * as selectors from "../../selectors";
import * as Utilities from "../../../../../utilities/utilities";
import DataTable from "react-data-table-component";
import * as TableConfig from "../../../../../utilities/tableConfig";
import * as actions from "../../actions";
import * as mainActions from "../../../../../app/actions";
import FullScreenDialog from "../../../../generic/fullScreenDialog";
import * as mainSelectors from "../../../../../app/selectors";

class Ada extends Component {
  multiChartAda = React.createRef();
  lineChartAda = React.createRef();
  state = {
    adaView: "chart",
    plotlyChartW: 1250,
    plotlyChartH: 200,
    selectedTitle: null,
    adaAdaCols: TableConfig.adaAda.map((dash) => {
      if (dash?.selector === "absences") {
        dash.cell = (row) => {
          if (this.props.selectedSchool.value !== 9999 && row.absences > 0) {
            return (
              <span
                style={{
                  color: "#683598",
                  textDecoration: "underline",
                  cursor: "pointer",
                }}
                onClick={this.handleCellClick.bind(this, row)}
              >
                {row.absences}
              </span>
            );
          } else {
            return <span>{row.absences}</span>;
          }
        };
      }
      return dash;
    }),
  };

  componentDidMount() {
    this.setState({
      plotlyChartW: this.multiChartAda.current.clientWidth,
      plotlyChartH: this.multiChartAda.current.clientHeight,
    });
  }

  changeAdaView = (event, view) => {
    if (view !== null) {
      this.setState({ adaView: view });
    }
  };

  handleCellClick = (row) => {
    this.props.setCurrentFullScreenDialog("ada-students-list");
    const _date = row.school_date.toString();
    this.setState({
      //selectedTitle: `Absent on ${Utilities.formatDate( Utilities.formatDate( new Date(`${_date.substring(0, 4)}-${_date.substring(4,6)}-${_date.substring(6, 8)}T10:00:00`)) )}`,
      selectedTitle: `Absent on: ${_date.substring(0, 4)}-${_date.substring(4, 6)}-${_date.substring(6, 8)}`,
    });
    this.props.loadStudentAbcentsByDate({
      school: row.school,
      date: row.school_date,
    });
    this.props.toggleFullScreenDialog(!this.props.openFullScreenDialog);
    //this.props.toggleFullScreenDialog(!this.props.openFullScreenDialog);
  };

  render() {
    const { adaView, plotlyChartW, plotlyChartH, selectedTitle, adaAdaCols } =
      this.state;
    const {
      selectedSchoolDatas,
      dateRangePeriod,
      selectedDay,
      currentFullScreenDialog,
    } = this.props;
    let selectedSchoolData = selectedSchoolDatas;
    const avgAdaLine = parseFloat(
      selectedSchoolDatas.ada && selectedSchoolDatas.ada[0].avgada,
    );
    if (
      selectedSchoolDatas &&
      selectedSchoolDatas.ada &&
      dateRangePeriod.length
    ) {
      selectedSchoolData.ada = selectedSchoolDatas.ada.filter(
        (value) =>
          dateRangePeriod[0] <=
            new Date(Utilities.formatDateHyphen(value.school_date)).getTime() &&
          dateRangePeriod[1] >=
            new Date(Utilities.formatDateHyphen(value.school_date)).getTime(),
      );
    }
    if (
      selectedSchoolDatas.ada &&
      selectedSchoolDatas.ada.length &&
      selectedDay.value !== -1
    ) {
      selectedSchoolData.ada = selectedSchoolDatas.ada.filter(
        (value) => selectedDay.value === value.weekday,
        //new Date(Utilities.formatDateHyphen(value.school_date)).getDay()
      );
    }
    const school_date =
      selectedSchoolData.ada &&
      selectedSchoolData.ada.map((data) =>
        Utilities.formatDateHyphen(data.school_date),
      );
    const ada =
      selectedSchoolData.ada &&
      selectedSchoolData.ada.map((data) => parseFloat(data.ada));
    const enrollments =
      selectedSchoolData.ada &&
      selectedSchoolData.ada.map((data) => data.enrollments);
    const avgAdaY = Array(
      selectedSchoolData.ada && selectedSchoolData.ada.length,
    ).fill(avgAdaLine);
    return (
      <>
        <div
          className="attendance-ada"
          style={{
            height: "70vh",
            display: adaView === "chart" ? "block" : "none",
          }}
        >
          <Paper
            ref={this.multiChartAda}
            className="multi-chart-ada"
            style={{ height: "34vh", minHeight: 200 }}
          >
            <Plot
              data={[
                {
                  x: school_date,
                  y: ada,
                  name: "Daily ADA",
                  mode: "lines",
                },
                {
                  x: school_date,
                  y: avgAdaY,
                  name: "Avg ADA",
                  mode: "lines",
                },
              ]}
              layout={{
                showlegend: true,
                legend: {},
                width: plotlyChartW,
                height: plotlyChartH,
                margin: { l: 40, r: 40, b: 40, t: 40, pad: 15 },
                title: {
                  text: "Average Daily Attendance",
                  x: 0.01,
                  font: {
                    size: 13,
                    color: "#3F51B5",
                  },
                },
              }}
            />
          </Paper>
          <Paper
            ref={this.lineChartAda}
            className="line-chart-ada"
            style={{ height: "34vh", minHeight: 200 }}
          >
            <Plot
              data={[
                {
                  x: school_date,
                  y: enrollments,
                  name: "Avg Enroll",
                  mode: "lines",
                },
              ]}
              layout={{
                showlegend: true,
                margin: { l: 40, r: 40, b: 40, t: 40, pad: 4 },
                title: {
                  text: "Average Enrollments",
                  x: 0.01,
                  font: {
                    size: 13,
                    color: "#3F51B5",
                  },
                },
                width: plotlyChartW,
                height: plotlyChartH,
              }}
            />
          </Paper>
        </div>
        <div style={{ display: adaView === "grid" ? "block" : "none" }}>
          <DataTable
            noHeader={true}
            columns={adaAdaCols}
            pagination={true}
            data={selectedSchoolData.ada || [{}]}
            customStyles={TableConfig.customStyles}
            highlightOnHover={true}
          />
          {currentFullScreenDialog === "ada-students-list" && (
            <FullScreenDialog title={"Student List for: " + selectedTitle} />
          )}
        </div>
        <div style={{ textAlign: "center", padding: 10 }}>
          <ToggleButtonGroup
            value={adaView}
            exclusive
            aria-label="text alignment"
            onChange={this.changeAdaView}
          >
            <ToggleButton value="chart" aria-label="left aligned">
              <Icon>show_chart</Icon>
            </ToggleButton>
            <ToggleButton value="grid" aria-label="centered">
              <Icon>grid_on</Icon>
            </ToggleButton>
          </ToggleButtonGroup>
        </div>
      </>
    );
  }
}

const mapStateToProps = () =>
  createStructuredSelector({
    selectedSchool: selectors.getAttSelectedSchool(),
    selectedSchoolDatas: selectors.getAttSelectedSchoolData(),
    dateRangePeriod: selectors.getAttDateRangePeriod(),
    selectedDay: selectors.getAttSelectedDay(),
    currentFullScreenDialog: selectors.getAttCurrentFullScreenDialog(),
    openFullScreenDialog: mainSelectors.getFullScreenDialog(),
  });

const mapDispatchToProps = (dispatch) => ({
  toggleFullScreenDialog: (open) =>
    dispatch(mainActions.toggleFullScreenDialog(open)),
  setCurrentFullScreenDialog: (dialog) =>
    dispatch(actions.setAttCurrentFullScreenDialog(dialog)),
  loadDashboardStudentsList: (option) =>
    dispatch(actions.loadAttDashboardStudentsList(option)),
  loadStudentAbcentsByDate: (option) =>
    dispatch(actions.loadStudentAbcentsByDate(option)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Ada);
