import Ada from "./ada";
import Dashboard from "./dashboard";

const routes = [
  {
    path: "/attendance/ada",
    exact: true,
    component: Ada,
  },
  {
    path: "/attendance/dashboard",
    exact: true,
    component: Dashboard,
  },
];

export default routes;
