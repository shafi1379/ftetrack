import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Box from "@material-ui/core/Box";
import Toolbar from "@material-ui/core/Toolbar";
import CloseIcon from "@material-ui/icons/Close";
import DataTable from "react-data-table-component";
import Dialog from "@material-ui/core/Dialog";
import IconButton from "@material-ui/core/IconButton";
import Paper from "@material-ui/core/Paper";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import * as TableConfig from "../../../../utilities/tableConfig";

const useStyles = makeStyles((theme) => ({
  appBar: {
    position: "static",
  },
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(1),
    color: "#f9f9f9",
    padding: 10,
  },
}));

function TabPanel(props) {
  const { children, value, index, ...other } = props;
  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-prevent-tabpanel-${index}`}
      aria-labelledby={`scrollable-prevent-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

export default function MaxWidthDialog(props) {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const handleClose = () => props.handleStudentDetailsDialog();
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  return (
    <>
      <Dialog
        fullWidth={true}
        maxWidth={"md"}
        open={props.open}
        onClose={handleClose}
        aria-labelledby="max-width-dialog-title"
      >
        <AppBar className={classes.appBar}>
          <Toolbar style={{ minHeight: 45 }}>
            <Typography className="capitalize" variant="button">
              {(props.row && props.row.studentName) +
                " (" +
                (props.row && props.row.studentNumber) +
                ")"}
            </Typography>
            <IconButton
              aria-label="close"
              className={classes.closeButton}
              onClick={handleClose}
            >
              <CloseIcon />
            </IconButton>
          </Toolbar>
        </AppBar>
        <div>
          <Paper square>
            <Tabs
              value={value}
              indicatorColor="primary"
              textColor="primary"
              onChange={handleChange}
              centered
            >
              <Tab label="Attendance" />
              <Tab label="Behavior" />
              <Tab label="Course Grade (Current)" />
              <Tab label="EOG Milestones" />
              <Tab label="Grades (Posted)" />
            </Tabs>
          </Paper>
          <TabPanel value={value} index={0}>
            <DataTable
              className="dashboard-table"
              noHeader={true}
              fixedHeader={true}
              fixedHeaderScrollHeight={"82vh"}
              pagination={false}
              columns={TableConfig.studentDetailsColumns.attendance}
              data={[]}
              customStyles={TableConfig.customStyles}
            />
          </TabPanel>
          <TabPanel value={value} index={1}>
            <DataTable
              className="dashboard-table"
              noHeader={true}
              fixedHeader={true}
              fixedHeaderScrollHeight={"82vh"}
              pagination={false}
              columns={TableConfig.studentDetailsColumns.behavior}
              data={[]}
              customStyles={TableConfig.customStyles}
            />
          </TabPanel>
          <TabPanel value={value} index={2}>
            <DataTable
              className="dashboard-table"
              noHeader={true}
              fixedHeader={true}
              fixedHeaderScrollHeight={"82vh"}
              pagination={false}
              columns={TableConfig.studentDetailsColumns.courseGrade_Current}
              data={[]}
              customStyles={TableConfig.customStyles}
            />
          </TabPanel>
          <TabPanel value={value} index={3}>
            <DataTable
              className="dashboard-table"
              noHeader={true}
              fixedHeader={true}
              fixedHeaderScrollHeight={"82vh"}
              pagination={false}
              columns={TableConfig.studentDetailsColumns.eogMilestones}
              data={[]}
              customStyles={TableConfig.customStyles}
            />
          </TabPanel>
          <TabPanel value={value} index={4}>
            <DataTable
              className="dashboard-table"
              noHeader={true}
              fixedHeader={true}
              fixedHeaderScrollHeight={"82vh"}
              pagination={false}
              columns={TableConfig.studentDetailsColumns.grades_Posted}
              data={[]}
              customStyles={TableConfig.customStyles}
            />
          </TabPanel>
        </div>
      </Dialog>
    </>
  );
}
