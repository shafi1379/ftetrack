import { fromJS } from "immutable";
import {
  SET_ATT_SCHOOL_LIST,
  SET_ATT_SELECTED_SCHOOL,
  SET_ATT_SELECTED_SCHOOL_DATA,
  SET_ATT_CURRENT_FULL_SCREEN_DIALOG,
  SET_ATT_DASHBOARD_STUDENTS_LIST,
  SET_STUDENT_ABCENTS_LIST,
  SET_ATT_SELECTED_DAY,
  SET_ATT_ADA_DATE_LIST,
  SET_ATT_DATE_RANGE_PERIOD,
  SET_ATT_SELECTED_SCHOOL_ADHOC,
  SET_ATT_SELECTED_ADA_PERCENT,
  SET_ATT_ADHOC_QUERY_RESULT,
} from "./actions";

const initialState = fromJS({
  selectedSchool: { label: "Select a School ...", value: -1 },
  schoolList: [],
  selectedDay: { value: -1, label: "All Days" },
  dayList: [
    { value: -1, label: "All Days" },
    { value: 1, label: "Monday" },
    { value: 2, label: "Tuesday" },
    { value: 3, label: "Wednesday" },
    { value: 4, label: "Thursday" },
    { value: 5, label: "Friday" },
  ],
  adaDateList: [
    {
      school_date: 20190801,
    },
    {
      school_date: 20190802,
    },
  ],
  selectedSchoolData: {},
  currentFullScreenDialog: "ada-adhoc",
  dashboardStudentsList: [],
  studentAbcentsList: [],
  dateRangePeriod: [],
  selectedSchoolAdhoc: { label: "Select a School ...", value: -1 },
  ADAPercentList: [
    { label: "< 88%", value: 88 },
    { label: "< 89%", value: 89 },
    { label: "< 90%", value: 90 },
    { label: "< 91%", value: 91 },
    { label: "< 92%", value: 92 },
    { label: "< 93%", value: 93 },
    { label: "< 94%", value: 94 },
    { label: "< 95%", value: 95 },
  ],
  selectedADAPercent: { label: "< 88%", value: 88 },
  adhocQueryResult: [],
});

const attendance = (state = initialState, action) => {
  switch (action.type) {
    case SET_ATT_SCHOOL_LIST:
      return state.set("schoolList", fromJS([...action.schoolList]));
    case SET_ATT_SELECTED_SCHOOL:
      return state.set("selectedSchool", fromJS(action.selectedSchool));
    case SET_ATT_SELECTED_SCHOOL_DATA:
      return state.set(
        "selectedSchoolData",
        fromJS({ ...action.selectedSchoolData }),
      );
    case SET_ATT_SELECTED_DAY:
      return state.set("selectedDay", fromJS(action.selectedDay));
    case SET_ATT_CURRENT_FULL_SCREEN_DIALOG:
      return state.set("currentFullScreenDialog", fromJS(action.dialog));
    case SET_ATT_DASHBOARD_STUDENTS_LIST:
      return state.set(
        "dashboardStudentsList",
        fromJS([...action.dashboardStudentsList]),
      );
    case SET_STUDENT_ABCENTS_LIST:
      return state.set(
        "studentAbcentsList",
        fromJS([...action.studentAbcents]),
      );
    case SET_ATT_ADA_DATE_LIST:
      return state.set("adaDateList", fromJS([...action.adaDateList]));
    case SET_ATT_DATE_RANGE_PERIOD:
      return state.set("dateRangePeriod", fromJS([...action.dateRangePeriod]));
    case SET_ATT_SELECTED_SCHOOL_ADHOC:
      return state.set(
        "selectedSchoolAdhoc",
        fromJS({ ...action.selectedSchoolAdhoc }),
      );
    case SET_ATT_SELECTED_ADA_PERCENT:
      return state.set(
        "selectedADAPercent",
        fromJS({ ...action.selectedADAPercent }),
      );
    case SET_ATT_ADHOC_QUERY_RESULT:
      return state.set(
        "adhocQueryResult",
        fromJS([...action.adhocQueryResult]),
      );
    default:
      return state;
  }
};

export default attendance;
