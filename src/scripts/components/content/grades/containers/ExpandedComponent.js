import React from "react";
import MaterialIcon from "../../../generic/materialIcon";

// eslint-disable-next-line react/prop-types
export default ({ data }) => (
  <>
    {data.children.map((obj) => (
      <div className="grade-expanded-style">
        <div>
          <MaterialIcon icon="insert_drive_file_outlined" />
          <span>{obj.activityName}</span>
        </div>
        <div>{obj.dueDate}</div>
        <div>{obj.assignedDate}</div>
        <div>{obj.score}</div>
        <div>{obj.totalPoints}</div>
      </div>
    ))}
  </>
);
