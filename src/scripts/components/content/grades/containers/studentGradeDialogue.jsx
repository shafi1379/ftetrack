import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import CloseIcon from "@material-ui/icons/Close";
import DataTable from "react-data-table-component";
import Dialog from "@material-ui/core/Dialog";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import {
  StudentGradesColumns,
  customStyles,
} from "../../../../utilities/tableConfig";
import MaterialIcon from "../../../generic/materialIcon";
import ExpandedComponent from "./ExpandedComponent";

const useStyles = makeStyles((theme) => ({
  appBar: {
    position: "static",
  },
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(1),
    color: "#f9f9f9",
    padding: 10,
  },
}));

export default function StudentGradeDialog(props) {
  const classes = useStyles();
  let data = [...props.data];
  const handleClose = () => props.handleClose();
  let expandedData = {};
  data.map((obj) => {
    if (Object.keys(expandedData).includes(obj["groupID"] + "")) {
      expandedData[obj["groupID"]]["children"].push({ ...obj });
    } else {
      expandedData[obj["groupID"]] = {
        groupName: "Category : " + obj.groupName,
        children: [{ ...obj }],
      };
    }
    return null;
  });
  let expandedArray = Object.keys(expandedData).map((key) => {
    return expandedData[key];
  });
  return (
    <>
      <Dialog
        fullWidth={true}
        maxWidth={"md"}
        open={props.open}
        onClose={handleClose}
        aria-labelledby="max-width-dialog-title"
      >
        <AppBar className={classes.appBar}>
          <Toolbar style={{ minHeight: 45 }}>
            <Typography className="capitalize" variant="button">
              {(props.row && props.row.name) +
                " (" +
                (props.row && props.row.personID) +
                ")"}
            </Typography>
            <IconButton
              aria-label="close"
              className={classes.closeButton}
              onClick={handleClose}
            >
              <CloseIcon />
            </IconButton>
          </Toolbar>
        </AppBar>
        <div style={{ height: "800px" }}>
          <DataTable
            className="dashboard-table"
            noHeader={true}
            expandableRows={true}
            expandableRowExpanded={(row) => true}
            expandableIcon={{
              collapsed: (
                <MaterialIcon icon="folder" style={{ color: "#3f51b5" }} />
              ),
              expanded: (
                <MaterialIcon icon="folder_open" style={{ color: "#3f51b5" }} />
              ),
            }}
            expandableRowsComponent={<ExpandedComponent />}
            columns={StudentGradesColumns}
            data={expandedArray}
            customStyles={customStyles}
          />
        </div>
      </Dialog>
    </>
  );
}
