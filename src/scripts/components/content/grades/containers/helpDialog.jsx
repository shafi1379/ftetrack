import React from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Dialog from "@material-ui/core/Dialog";
import MuiDialogContent from "@material-ui/core/DialogContent";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles((theme) => ({
  appBar: {
    position: "static",
  },
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(0.5),
    color: "#f9f9f9",
    padding: 10,
  },
}));

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

export default function AttendanceHelpDialog(props) {
  const classes = useStyles();
  const handleClose = () => props.openGradeInfo();

  return (
    <>
      <Dialog
        open={props.open}
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        aria-describedby="customized-dialog-description"
      >
        <AppBar className={classes.appBar}>
          <Toolbar style={{ minHeight: 45 }}>
            <Typography className="capitalize" variant="button">
              Grades Help
            </Typography>
            <IconButton
              aria-label="close"
              className={classes.closeButton}
              onClick={handleClose}
            >
              <CloseIcon />
            </IconButton>
          </Toolbar>
        </AppBar>
        <DialogContent dividers>
          <ul>
            <li>
              <Typography variant="body2" gutterBottom>
                Analyze Course Grades by Teachers/Course numbers.
              </Typography>
            </li>
            <li>
              <Typography variant="body2" gutterBottom>
                Compare course performance in a section to Other students taking
                the same course with other teachers at the same school or across
                the district.
              </Typography>
            </li>
            <li style={{ marginTop: "50px" }}>
              <Typography variant="body2" gutterBottom>
                Grades are cumulative grades{" "}
                <strong>
                  calculated as per Gradebook Assignment scores taking into
                  account Category and Assignment weights.
                </strong>
              </Typography>
            </li>
          </ul>
        </DialogContent>
      </Dialog>
    </>
  );
}
