import React, { Component } from "react";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import Plot from "react-plotly.js";
import Tooltip from "@material-ui/core/Tooltip";
import MaterialIcon from "../../../../generic/materialIcon";
import DataTable from "react-data-table-component";
import { CSVLink } from "react-csv";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";
import * as selectors from "../../selectors";
import * as actions from "../../actions";
import * as TableConfig from "../../../../../utilities/tableConfig";
import * as Utilities from "../../../../../utilities/utilities";

class GradeSection extends Component {
  csvLink = React.createRef();
  state = {
    csvExport: {
      data: [],
      fileName: "section.csv",
    },
    selectedSection: {},
  };
  handleAnalyzer = (evt) => {
    this.props.toggleCourseAnalyser(evt.target.checked);
    this.props.setSelectedSchool({});
    this.props.setSelectedTeacher({});
    this.props.setSelectedCourse({});
    this.props.setGradeStudent([]);
    this.props.setSelectedSection({});
    this.props.history.push("/grades/system");
  };

  handleSingleRowClick = (data, evt) => {
    let _req = {
      school: data.school,
      teacherid: data.teacherid,
      course: data.course,
      baseCourse: data.baseCourse,
      classlink: data.classlink,
    };
    this.props.loadCountsInClass(_req);
    this.setState({ selectedSection: data });
    this.props.setSelectedSection(data);
  };

  handleDoubleRowClick = (data, evt) => {
    this.props.setSelectedSection(data);
    this.props.loadStudent({
      school: data.school,
      classlink: data.classlink,
    });
    this.props.setGradeStudent([]);
  };

  downloadCSV = () => {
    const currentTime = `_${new Date().getTime()}`;
    if (this.props.courseAnalyser) {
      const selectedData = this.props.gradeCourseClassesList;
      const course = this.props.selectedCourse;
      const columns = {
        Section: "name",
        Teacher: "tchname",
        Students: "students",
        As: "grade_a",
        Bs: "grade_b",
        Cs: "grade_c",
        Fs: "grade_f",
      };
      const data = Utilities.generateExcelData(selectedData, columns);
      data.unshift([`Sections for Course: ${course && course.name}`]);
      this.setState({
        csvExport: { data, fileName: `School_Grades${currentTime}.csv` },
      });
    } else {
      const selectedData = this.props.gradeTeacherClassesList;
      const teacher = this.props.selectedTeacher;
      const columns = {
        Section: "name",
        "No. of Students": "students",
        As: "grade_a",
        Bs: "grade_b",
        Cs: "grade_c",
        Fs: "grade_f",
      };
      const data = Utilities.generateExcelData(selectedData, columns);
      data.unshift([`Sections for Teacher: ${teacher && teacher.name}`]);
      this.setState({
        csvExport: { data, fileName: `School_Grades${currentTime}.csv` },
      });
    }
    setTimeout(() => {
      this.csvLink.current.link.click();
    });
  };

  loadPlotlyChartData = () => {
    return this.props.countsInClassList.map((obj) => {
      return {
        type: "bar",
        name: obj.name,
        x: ["As", "Bs", "Cs", "Fs"],
        y: [
          obj["grade_a_p"],
          obj["grade_b_p"],
          obj["grade_c_p"],
          obj["grade_f_p"],
        ],
      };
    });
  };

  render() {
    const { csvExport, selectedSection } = this.state;
    const {
      courseAnalyser,
      gradeTeacherClassesList,
      gradeCourseClassesList,
      plotlyChartWidth,
      selectedTeacher,
      selectedCourse,
    } = this.props;

    return (
      <>
        <Paper style={{ padding: 2 }}>
          <AppBar style={{ position: "static", height: "32px" }}>
            <Toolbar style={{ minHeight: 30 }}>
              <Typography
                style={{ flexGrow: 1 }}
                className="capitalize"
                variant="button"
              >
                {!courseAnalyser
                  ? `Classes for Teacher: ${selectedTeacher.name}`
                  : `Sections for Course: ${selectedCourse.name}`}
              </Typography>
              <FormControlLabel
                control={
                  <Checkbox
                    onChange={this.handleAnalyzer}
                    name="analyzer"
                    checked={courseAnalyser}
                  />
                }
                className="checkbox-color"
                label="Analyze by Courses"
                labelPlacement="start"
                value="start"
                style={{ marginRight: "20px" }}
              />
              <Tooltip title="Export to CSV" aria-label="csv-export-tooltip">
                <IconButton
                  style={{ color: "#ffffff", padding: 5 }}
                  onClick={(evt) => this.downloadCSV(evt)}
                  className="csv-download-btn"
                  aria-label="csv-export"
                >
                  <MaterialIcon icon="cloud_download" />
                </IconButton>
              </Tooltip>
            </Toolbar>
          </AppBar>
          <div>
            <DataTable
              noHeader={true}
              columns={
                courseAnalyser
                  ? TableConfig.courseClassesDetails
                  : TableConfig.teacherClassesDetails
              }
              pointerOnHover={true}
              highlightOnHover={true}
              data={
                (courseAnalyser
                  ? gradeCourseClassesList
                  : gradeTeacherClassesList) || [{}]
              }
              fixedHeader={true}
              fixedHeaderScrollHeight={selectedSection ? "27vh" : "70vh"}
              onRowClicked={this.handleSingleRowClick}
              //onRowDoubleClicked={this.handleDoubleRowClick}
              customStyles={TableConfig.customStyles}
            />
          </div>
        </Paper>
        <Paper
          style={{
            marginTop: "2px",
            padding: "2px",
            borderRight: "1px solid #C1C1C1",
          }}
        >
          <AppBar style={{ position: "static" }}>
            <Toolbar style={{ minHeight: 30 }}>
              <Typography
                style={{ flexGrow: 1 }}
                className="capitalize"
                variant="button"
              >
                {selectedSection.name}
              </Typography>
            </Toolbar>
          </AppBar>
          <Plot
            data={this.loadPlotlyChartData()}
            layout={{
              width: plotlyChartWidth - 5,
              height: 250,
              margin: { l: 40, r: 40, b: 40, t: 40, pad: 15 },
            }}
          />
        </Paper>
        <CSVLink
          ref={this.csvLink}
          data={csvExport.data}
          className="hidden"
          filename={csvExport.fileName}
          target="_self"
        />
      </>
    );
  }
}

const mapStateToProps = () =>
  createStructuredSelector({
    courseAnalyser: selectors.getCourseAnalyser(),
    gradeTeacherClassesList: selectors.getGradeTeacherClasses(),
    countsInClassList: selectors.getCountsInClass(),
    gradeCourseClassesList: selectors.getGradeCourseClasses(),
    plotlyChartData: selectors.getPlotlyChartData(),
    plotlyChartWidth: selectors.getPlotlyChartWidth(),
    selectedSchool: selectors.getSelectedSchool(),
    selectedTeacher: selectors.getSelectedTeacher(),
    selectedCourse: selectors.getSelectedCourse(),
  });
const mapDispatchToProps = (dispatch) => ({
  setSelectedSection: (selectedSection) =>
    dispatch(actions.setSelectedSection(selectedSection)),
  setGradeStudent: (gradeStudentList) =>
    dispatch(actions.setGradeStudent(gradeStudentList)),
  toggleCourseAnalyser: (courseAnalyser) =>
    dispatch(actions.toggleCourseAnalyser(courseAnalyser)),
  loadStudent: (request) => dispatch(actions.loadStudent(request)),
  loadCountsInClass: (request) => dispatch(actions.loadCountsInClass(request)),
  setSelectedSchool: (request) => dispatch(actions.setSelectedSchool(request)),
  setSelectedTeacher: (request) =>
    dispatch(actions.setSelectedTeacher(request)),
  setPlotlyChartData: (request) =>
    dispatch(actions.setPlotlyChartData(request)),
  setSelectedCourse: (request) => dispatch(actions.setSelectedCourse(request)),
});

export default connect(mapStateToProps, mapDispatchToProps)(GradeSection);
