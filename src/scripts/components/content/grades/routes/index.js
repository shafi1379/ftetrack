import System from "./system";
import Teacher from "./teacher";
import Section from "./section";
import Course from "./course";

const routes = [
  {
    path: "/grades/system",
    exact: true,
    component: System,
  },
  {
    path: "/grades/teacher",
    exact: true,
    component: Teacher,
  },
  {
    path: "/grades/section",
    exact: true,
    component: Section,
  },
  {
    path: "/grades/course",
    exact: true,
    component: Course,
  },
];

export default routes;
