import React, { Component } from "react";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import Plot from "react-plotly.js";
import Tooltip from "@material-ui/core/Tooltip";
import MaterialIcon from "../../../../generic/materialIcon";
import DataTable from "react-data-table-component";
import { CSVLink } from "react-csv";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";
import * as selectors from "../../selectors";
import * as actions from "../../actions";
import * as TableConfig from "../../../../../utilities/tableConfig";
import * as Utilities from "../../../../../utilities/utilities";

class GradeSystem extends Component {
  csvLink = React.createRef();
  state = {
    csvExport: {
      data: [],
      fileName: "system.csv",
    },
    selectedSchool: {},
  };

  componentDidMount() {
    this.props.setSelectedSchool({});
    this.props.setSelectedTeacher({});
    this.props.setSelectedCourse({});
  }

  handleAnalyzer = (evt) => {
    this.props.toggleCourseAnalyser(evt.target.checked);
    this.props.setSelectedSchool({});
    this.props.setSelectedTeacher({});
    this.props.setSelectedCourse({});
    this.props.setGradeStudent([]);
    this.props.setSelectedSection({});
  };

  handleSingleRowClick = (data, evt) => {
    const _plotlyChartData = [
      {
        type: "bar",
        name: "School - " + data.name,
        x: ["As", "Bs", "Cs", "Fs"],
        y: [
          data["grade_a_p"],
          data["grade_b_p"],
          data["grade_c_p"],
          data["grade_f_p"],
        ],
      },
    ];
    this.props.setPlotlyChartData(_plotlyChartData);
    this.setState({ selectedSchool: data });
  };

  handleDoubleRowClick = (data, evt) => {
    if (this.props.courseAnalyser) {
      this.props.loadAnalyzer({ school: data.school });
      this.props.history.push("/grades/course");
    } else {
      this.props.loadTeacher({ school: data.school });
      this.props.history.push("/grades/teacher");
    }
    this.props.setSelectedSchool(data);
    this.props.setSelectedTeacher({});
    this.props.setSelectedCourse({});
  };

  downloadCSV = () => {
    const currentTime = `_${new Date().getTime()}`;
    const columns = {
      School: "name",
      "No. of Classes": "classes",
      "No. of Students": "students",
      As: "grade_a",
      Bs: "grade_b",
      Cs: "grade_c",
      Fs: "grade_f",
    };
    const data = Utilities.generateExcelData(
      this.props.systemClassesList || [],
      columns || {},
    );
    data.unshift(["School Lists"]);
    this.setState({
      csvExport: { data, fileName: `School_Grades${currentTime}.csv` },
    });
    setTimeout(() => {
      this.csvLink.current.link.click();
    });
  };

  render() {
    const { csvExport, selectedSchool } = this.state;
    const {
      courseAnalyser,
      systemClassesList,
      plotlyChartData,
      plotlyChartWidth,
    } = this.props;
    return (
      <>
        <Paper style={{ padding: 2 }}>
          <AppBar style={{ position: "static", height: "32px" }}>
            <Toolbar style={{ minHeight: 30 }}>
              <Typography
                style={{ flexGrow: 1 }}
                className="capitalize"
                variant="button"
              >
                School
              </Typography>
              <FormControlLabel
                control={
                  <Checkbox
                    onChange={this.handleAnalyzer}
                    name="analyzer"
                    checked={courseAnalyser}
                  />
                }
                className="checkbox-color"
                label="Analyze by Courses"
                labelPlacement="start"
                value="start"
                style={{ marginRight: "20px" }}
              />
              <Tooltip title="Export to CSV" aria-label="csv-export-tooltip">
                <IconButton
                  style={{ color: "#ffffff", padding: 5 }}
                  onClick={(evt) => this.downloadCSV(evt)}
                  className="csv-download-btn"
                  aria-label="csv-export"
                >
                  <MaterialIcon icon="cloud_download" />
                </IconButton>
              </Tooltip>
            </Toolbar>
          </AppBar>
          <div>
            <DataTable
              noHeader={true}
              columns={TableConfig.systemClassesDetails}
              pointerOnHover={true}
              highlightOnHover={true}
              data={systemClassesList || [{}]}
              fixedHeader={true}
              fixedHeaderScrollHeight={
                plotlyChartData.length !== 0 ? "27vh" : "70vh"
              }
              onRowClicked={this.handleSingleRowClick}
              onRowDoubleClicked={this.handleDoubleRowClick}
              customStyles={TableConfig.customStyles}
            />
          </div>
        </Paper>
        {plotlyChartData.length !== 0 && (
          <Paper
            style={{
              marginTop: "2px",
              padding: "2px",
              borderRight: "1px solid #C1C1C1",
            }}
          >
            <AppBar style={{ position: "static" }}>
              <Toolbar style={{ minHeight: 30 }}>
                <Typography
                  style={{ flexGrow: 1 }}
                  className="capitalize"
                  variant="button"
                >
                  Grade Distribution for: {selectedSchool.name}
                </Typography>
              </Toolbar>
            </AppBar>
            <Plot
              data={plotlyChartData}
              layout={{
                width: plotlyChartWidth - 5,
                height: 250,
                margin: { l: 40, r: 40, b: 40, t: 40, pad: 15 },
              }}
            />
          </Paper>
        )}
        <CSVLink
          ref={this.csvLink}
          data={csvExport.data}
          className="hidden"
          filename={csvExport.fileName}
          target="_self"
        />
      </>
    );
  }
}

const mapStateToProps = () =>
  createStructuredSelector({
    courseAnalyser: selectors.getCourseAnalyser(),
    systemClassesList: selectors.getGradeSystemClasses(),
    plotlyChartData: selectors.getPlotlyChartData(),
    plotlyChartWidth: selectors.getPlotlyChartWidth(),
  });
const mapDispatchToProps = (dispatch) => ({
  setSelectedSection: (selectedSection) =>
    dispatch(actions.setSelectedSection(selectedSection)),
  setGradeStudent: (gradeStudentList) =>
    dispatch(actions.setGradeStudent(gradeStudentList)),
  toggleCourseAnalyser: (courseAnalyser) =>
    dispatch(actions.toggleCourseAnalyser(courseAnalyser)),
  loadTeacher: (request) => dispatch(actions.loadTeacher(request)),
  loadAnalyzer: (request) => dispatch(actions.loadAnalyzer(request)),
  setPlotlyChartData: (request) =>
    dispatch(actions.setPlotlyChartData(request)),
  setSelectedSchool: (request) => dispatch(actions.setSelectedSchool(request)),
  setSelectedTeacher: (request) =>
    dispatch(actions.setSelectedTeacher(request)),
  setSelectedCourse: (request) => dispatch(actions.setSelectedCourse(request)),
});

export default connect(mapStateToProps, mapDispatchToProps)(GradeSystem);
