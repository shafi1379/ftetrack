import React from "react";
import {
  Paper,
  AppBar,
  Toolbar,
  Typography,
  Tooltip,
  IconButton,
  Breadcrumbs,
} from "@material-ui/core";
import { BrowserRouter as Router, Switch, Link } from "react-router-dom";
import MaterialIcon from "../../generic/materialIcon";
import { withRouter } from "react-router-dom";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";
import DataTable from "react-data-table-component";
import * as loginSelectors from "../../../app/selectors";
import * as selectors from "./selectors";
import * as actions from "./actions";
import * as TableConfig from "../../../utilities/tableConfig";
import { CSVLink } from "react-csv";
import GradeHelpDialog from "./containers/helpDialog";
import StudentGradeDialog from "./containers/studentGradeDialogue";
import SplitPane from "react-split-pane";
import * as Utilities from "../../../utilities/utilities";
import RouteWithSubRoutes from "../../generic/subRoutes";

class Grades extends React.Component {
  csvLink = React.createRef();
  firstSplitPanePanel = React.createRef();
  state = {
    csvExport: {
      data: [],
      fileName: "grade.csv",
    },
    openHelpInfo: false,
    openGradeInfo: false,
    selectedStudent: {},
  };

  componentWillMount() {
    this.props.history.replace("/grades/system");
    this.props.loadSystemClasses({ school: this.props.userData.school });
  }

  componentDidMount() {
    this.props.setPlotlyChartWidth(
      this.firstSplitPanePanel.current.offsetWidth,
    );
  }

  handleSingleRowClick = (data, evt) => {
    this.props.loadStudentGrade({
      personid: data.personID + "",
      classlink: data.classlink + "",
    });
    this.setState({ openGradeInfo: true, selectedStudent: data });
  };

  downloadCSV = (evt, isStudentDownload) => {
    const currentTime = `_${new Date().getTime()}`;
    const columns = {
      Permnum: "permnum",
      Name: "name",
      "Letter Grade": "grade",
      Numeric: "score",
    };
    const data = Utilities.generateExcelData(
      this.props.gradeStudentList || [],
      columns || {},
    );
    const selectedSection = this.props.selectedSection;
    data.unshift([
      `Students In Section : ${selectedSection && selectedSection.name}`,
    ]);
    this.setState({
      csvExport: {
        data,
        fileName: `School_Grades${currentTime}.csv`,
      },
    });
    setTimeout(() => {
      this.csvLink.current.link.click();
    });
  };

  handleOpenGradeInfo = () =>
    this.setState({ openHelpInfo: !this.state.openHelpInfo });

  handleOpenStudentGradeInfo = () => {
    this.props.setGradeStudentGrade([]);
    this.setState({ openGradeInfo: !this.state.openGradeInfo });
  };

  onSplitPaneDragFinished = (size) => {
    this.props.setPlotlyChartWidth(
      this.firstSplitPanePanel.current.offsetWidth,
    );
  };

  resetStudentList = () => {
    this.props.setGradeStudent([]);
    this.props.setSelectedSection({});
  };

  render() {
    const { csvExport, openHelpInfo, openGradeInfo } = this.state;
    const {
      routes,
      gradeStudentList,
      courseAnalyser,
      selectedSchool,
      selectedTeacher,
      selectedCourse,
      selectedSection,
    } = this.props;
    return (
      <div className="grades">
        <Router basename="/abc">
          <Paper className="breadcrumbs">
            <strong style={{ marginRight: "10px" }}>{"viewing : "}</strong>
            <Breadcrumbs
              arial-label="Breadcrumb"
              style={{ fontSize: 13, fontWeight: 500 }}
            >
              <Link onClick={this.resetStudentList} to="/grades/system">
                System
              </Link>
              {selectedSchool.name && courseAnalyser && (
                <Link onClick={this.resetStudentList} to="/grades/course">
                  {selectedSchool.name}
                </Link>
              )}
              {selectedSchool.name && !courseAnalyser && (
                <Link onClick={this.resetStudentList} to="/grades/teacher">
                  {selectedSchool.name}
                </Link>
              )}
              {selectedTeacher.name && (
                <Link to="/grades/section">{selectedTeacher.name}</Link>
              )}
              {selectedCourse.name && (
                <Link to="/grades/section">{selectedCourse.name}</Link>
              )}
            </Breadcrumbs>
            <Tooltip title="Grades Help" aria-label="info-btn-tooltip">
              <IconButton
                onClick={this.handleOpenGradeInfo}
                className="tools-btn"
                aria-label="info-btn"
                style={{ position: "absolute", right: "0px", top: "70px" }}
              >
                <MaterialIcon icon="help" />
              </IconButton>
            </Tooltip>
          </Paper>
          <SplitPane
            split="vertical"
            allowResize={true}
            onChange={this.onSplitPaneDragFinished}
          >
            <div initialsize="60%" ref={this.firstSplitPanePanel}>
              <Switch>
                {routes.map((route, i) => (
                  <RouteWithSubRoutes key={i} {...route} />
                ))}
              </Switch>
            </div>
            <div>
              <Paper style={{ padding: 2 }}>
                <AppBar style={{ position: "static", height: "32px" }}>
                  <Toolbar style={{ minHeight: 30 }}>
                    <Typography
                      style={{ flexGrow: 1 }}
                      className="capitalize"
                      variant="button"
                    >
                      Students In Section : {selectedSection.name}
                    </Typography>
                    <Tooltip
                      title="Export to CSV"
                      aria-label="csv-export-tooltip"
                    >
                      <IconButton
                        style={{ color: "#ffffff", padding: 5 }}
                        onClick={(evt) => this.downloadCSV(evt, true)}
                        className="csv-download-btn"
                        aria-label="csv-export"
                      >
                        <MaterialIcon icon="cloud_download" />
                      </IconButton>
                    </Tooltip>
                  </Toolbar>
                </AppBar>
                <div>
                  <DataTable
                    noHeader={true}
                    columns={[...TableConfig.studentDetails]}
                    pointerOnHover={true}
                    highlightOnHover={true}
                    data={gradeStudentList}
                    fixedHeader={true}
                    fixedHeaderScrollHeight={"70vh"}
                    onRowClicked={this.handleSingleRowClick}
                    customStyles={TableConfig.customStyles}
                  />
                </div>
              </Paper>
            </div>
          </SplitPane>
        </Router>
        <CSVLink
          ref={this.csvLink}
          data={csvExport.data}
          className="hidden"
          filename={csvExport.fileName}
          target="_self"
        />
        <GradeHelpDialog
          open={openHelpInfo}
          openGradeInfo={this.handleOpenGradeInfo}
        />
        {this.props.gradeStudentGradeList.length > 0 && (
          <StudentGradeDialog
            open={openGradeInfo}
            data={this.props.gradeStudentGradeList}
            row={this.state.selectedStudent}
            handleClose={this.handleOpenStudentGradeInfo}
          />
        )}
      </div>
    );
  }
}

const mapStateToProps = () =>
  createStructuredSelector({
    courseAnalyser: selectors.getCourseAnalyser(),
    gradeStudentList: selectors.getGradeStudent(),
    gradeStudentGradeList: selectors.getGradeStudentGrade(),
    selectedSchool: selectors.getSelectedSchool(),
    selectedTeacher: selectors.getSelectedTeacher(),
    selectedCourse: selectors.getSelectedCourse(),
    selectedSection: selectors.getSelectedSection(),
    userData: loginSelectors.getLoginData(),
  });
const mapDispatchToProps = (dispatch) => ({
  setSelectedSection: (selectedSection) =>
    dispatch(actions.setSelectedSection(selectedSection)),
  setGradeStudent: (gradeStudentList) =>
    dispatch(actions.setGradeStudent(gradeStudentList)),
  loadSystemClasses: (request) => dispatch(actions.loadSystemClasses(request)),
  loadStudentGrade: (request) => dispatch(actions.loadStudentGrade(request)),
  setGradeStudentGrade: (val) => dispatch(actions.setGradeStudentGrade(val)),
  toggleCourseAnalyser: (courseAnalyser) =>
    dispatch(actions.toggleCourseAnalyser(courseAnalyser)),
  setPlotlyChartWidth: (width) => dispatch(actions.setPlotlyChartWidth(width)),
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Grades));
