import { all, call, fork, put, select, takeLatest } from "redux-saga/effects";
import {
  LOAD_GRADE_SYSTEM_CLASSES,
  LOAD_ANALYZER,
  LOAD_TEACHER,
  LOAD_COURSE_CLASSES,
  LOAD_STUDENT,
  LOAD_STUDENT_GRADE,
  LOAD_TEACHER_CLASSES,
  LOAD_COUNTS_IN_CLASS,
  setCountsInClass,
  setGradeSystemClasses,
  setGradeCourseClasses,
  setGradeTeacherClasses,
  setGradeStudentGrade,
  setGradeStudent,
  setGradeTeacher,
  setGradeAnanlyzer,
} from "./actions";
import { setProgressingValue } from "../../../app/actions";
import * as Api from "../../../services/api";
import { API_URLS } from "../../../utilities/constants";

function* loadSystemClassesRequested({ request }) {
  try {
    const { systemClassesList } = (yield select((s) => s.get("grades"))).toJS();
    if (!systemClassesList.length) {
      yield put(setProgressingValue(true));
      const response = yield call(
        Api.doPostRequest,
        API_URLS.systemClasses,
        request,
      );
      if (response) {
        yield put(setGradeSystemClasses(response));
        yield put(setProgressingValue(false));
      }
    }
  } catch (error) {
    console.error(error);
  }
}

function* systemClassesListener() {
  yield takeLatest(LOAD_GRADE_SYSTEM_CLASSES, loadSystemClassesRequested);
}

function* loadTeacherRequested({ request }) {
  try {
    yield put(setProgressingValue(true));
    const response = yield call(Api.doPostRequest, API_URLS.teachers, request);
    if (response) {
      yield put(setProgressingValue(false));
      yield put(setGradeTeacher(response));
    }
  } catch (error) {
    console.error(error);
  }
}

function* teacherListener() {
  yield takeLatest(LOAD_TEACHER, loadTeacherRequested);
}

function* loadAnalyzerRequested({ request }) {
  try {
    yield put(setProgressingValue(true));
    const response = yield call(Api.doPostRequest, API_URLS.courses, request);
    if (response) {
      yield put(setProgressingValue(false));
      yield put(setGradeAnanlyzer(response));
    }
  } catch (error) {
    console.error(error);
  }
}

function* analyzerListener() {
  yield takeLatest(LOAD_ANALYZER, loadAnalyzerRequested);
}

function* loadTeacherClassesRequested({ request }) {
  try {
    yield put(setProgressingValue(true));
    const response = yield call(
      Api.doPostRequest,
      API_URLS.teacherClasses,
      request,
    );
    if (response) {
      yield put(setProgressingValue(false));
      yield put(setGradeTeacherClasses(response));
    }
  } catch (error) {
    console.error(error);
  }
}

function* teacherClassesListener() {
  yield takeLatest(LOAD_TEACHER_CLASSES, loadTeacherClassesRequested);
}

function* loadCourseClassesRequested({ request }) {
  try {
    yield put(setProgressingValue(true));
    const response = yield call(
      Api.doPostRequest,
      API_URLS.courseClasses,
      request,
    );
    if (response) {
      yield put(setProgressingValue(false));
      yield put(setGradeCourseClasses(response));
    }
  } catch (error) {
    console.error(error);
  }
}

function* courseClassesListener() {
  yield takeLatest(LOAD_COURSE_CLASSES, loadCourseClassesRequested);
}

function* loadStudentRequested({ request }) {
  try {
    yield put(setProgressingValue(true));
    const response = yield call(Api.doPostRequest, API_URLS.student, request);
    if (response) {
      yield put(setProgressingValue(false));
      yield put(setGradeStudent(response));
    }
  } catch (error) {
    console.error(error);
  }
}

function* studentListener() {
  yield takeLatest(LOAD_STUDENT, loadStudentRequested);
}

function* loadStudentGradeRequested({ request }) {
  try {
    yield put(setProgressingValue(true));
    const response = yield call(
      Api.doPostRequest,
      API_URLS.studentGrade,
      request,
    );
    if (response) {
      yield put(setProgressingValue(false));
      yield put(setGradeStudentGrade(response));
    }
  } catch (error) {
    console.error(error);
  }
}

function* studentGradeListener() {
  yield takeLatest(LOAD_STUDENT_GRADE, loadStudentGradeRequested);
}

function* loadCountsInClassRequested({ request }) {
  try {
    yield put(setProgressingValue(true));
    const response = yield call(
      Api.doPostRequest,
      API_URLS.countsInClass,
      request,
    );
    if (response) {
      yield put(setProgressingValue(false));
      yield put(setCountsInClass(response.chart));
      yield put(setGradeStudent(response.students));
    }
  } catch (error) {
    console.error(error);
  }
}

function* countsInClassListenerListener() {
  yield takeLatest(LOAD_COUNTS_IN_CLASS, loadCountsInClassRequested);
}

export default function* root() {
  yield all([
    fork(systemClassesListener),
    fork(teacherListener),
    fork(analyzerListener),
    fork(teacherClassesListener),
    fork(courseClassesListener),
    fork(studentListener),
    fork(studentGradeListener),
    fork(countsInClassListenerListener),
  ]);
}
