export const SET_GRADE_SYSTEM_CLASSES = "SET_GRADE_SYSTEM_CLASSES";
export const setGradeSystemClasses = (systemClassesList) => ({
  type: SET_GRADE_SYSTEM_CLASSES,
  systemClassesList,
});

export const SET_GRADE_TEACHER = "SET_GRADE_TEACHER";
export const setGradeTeacher = (gradeTeacherList) => ({
  type: SET_GRADE_TEACHER,
  gradeTeacherList,
});

export const SET_GRADE_ANALYZER = "SET_GRADE_ANALYZER";
export const setGradeAnanlyzer = (gradeAnalyzerList) => ({
  type: SET_GRADE_ANALYZER,
  gradeAnalyzerList,
});

export const SET_GRADE_TEACHER_CLASSES = "SET_GRADE_TEACHER_CLASSES";
export const setGradeTeacherClasses = (gradeTeacherClassesList) => ({
  type: SET_GRADE_TEACHER_CLASSES,
  gradeTeacherClassesList,
});

export const SET_GRADE_COURSE_CLASSES = "SET_GRADE_COURSE_CLASSES";
export const setGradeCourseClasses = (gradeCourseClassesList) => ({
  type: SET_GRADE_COURSE_CLASSES,
  gradeCourseClassesList,
});

export const SET_GRADE_STUDENT = "SET_GRADE_STUDENT";
export const setGradeStudent = (gradeStudentList) => ({
  type: SET_GRADE_STUDENT,
  gradeStudentList,
});

export const SET_GRADE_STUDENT_GRADE = "SET_GRADE_STUDENT_GRADE";
export const setGradeStudentGrade = (gradeStudentGradeList) => ({
  type: SET_GRADE_STUDENT_GRADE,
  gradeStudentGradeList,
});

export const LOAD_GRADE_SYSTEM_CLASSES = "LOAD_GRADE_SYSTEM_CLASSES";
export const loadSystemClasses = (request) => ({
  type: LOAD_GRADE_SYSTEM_CLASSES,
  request,
});

export const LOAD_TEACHER = "LOAD_TEACHER";
export const loadTeacher = (request) => ({
  type: LOAD_TEACHER,
  request,
});

export const LOAD_ANALYZER = "LOAD_ANALYZER";
export const loadAnalyzer = (request) => ({
  type: LOAD_ANALYZER,
  request,
});

export const LOAD_TEACHER_CLASSES = "LOAD_TEACHER_CLASSES";
export const loadTeacherClasses = (request) => ({
  type: LOAD_TEACHER_CLASSES,
  request,
});

export const LOAD_COURSE_CLASSES = "LOAD_COURSE_CLASSES";
export const loadCourseClasses = (request) => ({
  type: LOAD_COURSE_CLASSES,
  request,
});

export const LOAD_STUDENT = "LOAD_STUDENT";
export const loadStudent = (request) => ({
  type: LOAD_STUDENT,
  request,
});

export const LOAD_STUDENT_GRADE = "LOAD_STUDENT_GRADE";
export const loadStudentGrade = (request) => ({
  type: LOAD_STUDENT_GRADE,
  request,
});

export const TOGGLE_COURSE_ANALYSER = "TOGGLE_COURSE_ANALYSER";
export const toggleCourseAnalyser = (courseAnalyser) => ({
  type: TOGGLE_COURSE_ANALYSER,
  courseAnalyser,
});

export const SET_SELECTED_SCHOOL = "SET_SELECTED_SCHOOL";
export const setSelectedSchool = (selectedSchool) => ({
  type: SET_SELECTED_SCHOOL,
  selectedSchool,
});

export const SET_SELECTED_TEACHER = "SET_SELECTED_TEACHER";
export const setSelectedTeacher = (selectedTeacher) => ({
  type: SET_SELECTED_TEACHER,
  selectedTeacher,
});

export const SET_SELECTED_SECTION = "SET_SELECTED_SECTION";
export const setSelectedSection = (selectedSection) => ({
  type: SET_SELECTED_SECTION,
  selectedSection,
});

export const SET_SELECTED_COURSE = "SET_SELECTED_COURSE";
export const setSelectedCourse = (selectedCourse) => ({
  type: SET_SELECTED_COURSE,
  selectedCourse,
});

export const SET_PLOTLY_CHART_DATA = "SET_PLOTLY_CHART_DATA";
export const setPlotlyChartData = (plotlyChartData) => ({
  type: SET_PLOTLY_CHART_DATA,
  plotlyChartData,
});

export const SET_PLOTLY_CHART_WIDTH = "SET_PLOTLY_CHART_WIDTH";
export const setPlotlyChartWidth = (plotlyChartWidth) => ({
  type: SET_PLOTLY_CHART_WIDTH,
  plotlyChartWidth,
});

export const SET_COUNTS_IN_CLASS = "SET_COUNTS_IN_CLASS";
export const setCountsInClass = (countsInClassList) => ({
  type: SET_COUNTS_IN_CLASS,
  countsInClassList,
});

export const LOAD_COUNTS_IN_CLASS = "LOAD_COUNTS_IN_CLASS";
export const loadCountsInClass = (request) => ({
  type: LOAD_COUNTS_IN_CLASS,
  request,
});
