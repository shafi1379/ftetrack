import { fromJS } from "immutable";
import {
  SET_GRADE_SYSTEM_CLASSES,
  SET_GRADE_TEACHER,
  SET_GRADE_ANALYZER,
  SET_COUNTS_IN_CLASS,
  SET_GRADE_TEACHER_CLASSES,
  SET_GRADE_STUDENT,
  SET_GRADE_STUDENT_GRADE,
  SET_GRADE_COURSE_CLASSES,
  TOGGLE_COURSE_ANALYSER,
  SET_SELECTED_SCHOOL,
  SET_SELECTED_TEACHER,
  SET_SELECTED_SECTION,
  SET_SELECTED_COURSE,
  SET_PLOTLY_CHART_DATA,
  SET_PLOTLY_CHART_WIDTH,
} from "./actions";

const initialState = fromJS({
  systemClassesList: [],
  gradeTeacherList: [],
  gradeAnalyzerList: [],
  countsInClassList: [],
  gradeTeacherClassesList: [],
  gradeCourseClassesList: [],
  gradeStudentList: [],
  gradeStudentGradeList: [],
  plotlyChartData: [],
  selectedSchool: { label: "Select a School ...", value: -1 },
  selectedTeacher: {},
  selectedSection: {},
  selectedCourse: {},
  courseAnalyser: false,
  plotlyChartWidth: 0,
});

const grades = (state = initialState, action) => {
  switch (action.type) {
    case SET_GRADE_SYSTEM_CLASSES:
      return state.set(
        "systemClassesList",
        fromJS([...action.systemClassesList]),
      );
    case SET_GRADE_TEACHER:
      return state.set(
        "gradeTeacherList",
        fromJS([...action.gradeTeacherList]),
      );
    case SET_GRADE_ANALYZER:
      return state.set(
        "gradeAnalyzerList",
        fromJS([...action.gradeAnalyzerList]),
      );
    case SET_GRADE_TEACHER_CLASSES:
      return state.set(
        "gradeTeacherClassesList",
        fromJS([...action.gradeTeacherClassesList]),
      );
    case SET_GRADE_COURSE_CLASSES:
      return state.set(
        "gradeCourseClassesList",
        fromJS([...action.gradeCourseClassesList]),
      );
    case SET_GRADE_STUDENT:
      return state.set(
        "gradeStudentList",
        fromJS([...action.gradeStudentList]),
      );
    case SET_GRADE_STUDENT_GRADE:
      return state.set(
        "gradeStudentGradeList",
        fromJS([...action.gradeStudentGradeList]),
      );
    case SET_PLOTLY_CHART_DATA:
      return state.set("plotlyChartData", fromJS([...action.plotlyChartData]));
    case SET_SELECTED_SCHOOL:
      return state.set("selectedSchool", fromJS({ ...action.selectedSchool }));
    case SET_SELECTED_TEACHER:
      return state.set(
        "selectedTeacher",
        fromJS({ ...action.selectedTeacher }),
      );
    case SET_SELECTED_SECTION:
      return state.set(
        "selectedSection",
        fromJS({ ...action.selectedSection }),
      );
    case SET_SELECTED_COURSE:
      return state.set("selectedCourse", fromJS({ ...action.selectedCourse }));
    case TOGGLE_COURSE_ANALYSER:
      return state.set("courseAnalyser", action.courseAnalyser);
    case SET_PLOTLY_CHART_WIDTH:
      return state.set("plotlyChartWidth", action.plotlyChartWidth);
    case SET_COUNTS_IN_CLASS:
      return state.set(
        "countsInClassList",
        fromJS([...action.countsInClassList]),
      );
    default:
      return state;
  }
};

export default grades;
