import { createSelector } from "reselect";
import * as selectors from "../../../redux/selectors";

export const getGradeSystemClasses = () =>
  createSelector(selectors.gradesState, (gradesState) =>
    gradesState.get("systemClassesList").toJS(),
  );

export const getGradeTeacher = () =>
  createSelector(selectors.gradesState, (gradesState) =>
    gradesState.get("gradeTeacherList").toJS(),
  );

export const getGradeAnanlyzer = () =>
  createSelector(selectors.gradesState, (gradesState) =>
    gradesState.get("gradeAnalyzerList").toJS(),
  );

export const getGradeTeacherClasses = () =>
  createSelector(selectors.gradesState, (gradesState) =>
    gradesState.get("gradeTeacherClassesList").toJS(),
  );

export const getGradeCourseClasses = () =>
  createSelector(selectors.gradesState, (gradesState) =>
    gradesState.get("gradeCourseClassesList").toJS(),
  );

export const getGradeStudent = () =>
  createSelector(selectors.gradesState, (gradesState) =>
    gradesState.get("gradeStudentList").toJS(),
  );

export const getGradeStudentGrade = () =>
  createSelector(selectors.gradesState, (gradesState) =>
    gradesState.get("gradeStudentGradeList").toJS(),
  );

export const getSelectedSchool = () =>
  createSelector(selectors.gradesState, (gradesState) =>
    gradesState.get("selectedSchool").toJS(),
  );

export const getSelectedTeacher = () =>
  createSelector(selectors.gradesState, (gradesState) =>
    gradesState.get("selectedTeacher").toJS(),
  );

export const getSelectedSection = () =>
  createSelector(selectors.gradesState, (gradesState) =>
    gradesState.get("selectedSection").toJS(),
  );

export const getSelectedCourse = () =>
  createSelector(selectors.gradesState, (gradesState) =>
    gradesState.get("selectedCourse").toJS(),
  );

export const getCourseAnalyser = () =>
  createSelector(selectors.gradesState, (gradesState) =>
    gradesState.get("courseAnalyser"),
  );

export const getPlotlyChartData = () =>
  createSelector(selectors.gradesState, (gradesState) =>
    gradesState.get("plotlyChartData").toJS(),
  );

export const getPlotlyChartWidth = () =>
  createSelector(selectors.gradesState, (gradesState) =>
    gradesState.get("plotlyChartWidth"),
  );

export const getCountsInClass = () =>
  createSelector(selectors.gradesState, (gradesState) =>
    gradesState.get("countsInClassList").toJS(),
  );
