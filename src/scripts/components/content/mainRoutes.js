import Summary from "./summary";
import Attendance from "./attendance";
import Behaviour from "./behaviour";
import CohortGraduation from "./cohort_graduation";
import Enrollment from "./enrollment";
import Ews from "./ews";
import GradeDistribution from "./grade_distribution";
import HonorRoll from "./honor_roll";
import PBIS from "./pbis";
import ProgressGrade from "./progress_grade";
import Grades from "./grades";
import CTAE from "./ctae";
import attendanceRoutes from "./attendance/routes";
import behaviourRoutes from "./behaviour/routes";
import gradesRoutes from "./grades/routes";
import pbisRoutes from "./pbis/routes";

export const routes = [
  {
    path: "/summary",
    exact: true,
    component: Summary,
  },
  {
    path: "/attendance",
    component: Attendance,
    routes: [...attendanceRoutes],
  },
  {
    path: "/grades",
    component: Grades,
    routes: [...gradesRoutes],
  },
  {
    path: "/behaviour",
    component: Behaviour,
    routes: [...behaviourRoutes],
  },
  {
    path: "/cohort_graduation",
    exact: true,
    component: CohortGraduation,
  },
  {
    path: "/enrollment",
    exact: true,
    component: Enrollment,
  },
  {
    path: "/ews",
    exact: true,
    component: Ews,
  },
  {
    path: "/grade_distribution",
    exact: true,
    component: GradeDistribution,
  },
  {
    path: "/honor_roll",
    exact: true,
    component: HonorRoll,
  },
  {
    path: "/pbis",
    component: PBIS,
    routes: [...pbisRoutes],
  },
  {
    path: "/progress_grade",
    exact: true,
    component: ProgressGrade,
  },
  {
    path: "/ctae",
    exact: true,
    component: CTAE,
  },
];
