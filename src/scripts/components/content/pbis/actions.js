export const SET_PBIS_SCHOOL_LIST = "SET_PBIS_SCHOOL_LIST";
export const setPbisSchoolList = (pbisSchoolList) => {
  return { type: SET_PBIS_SCHOOL_LIST, pbisSchoolList };
};

export const LOAD_PBIS_SCHOOL_LIST = "LOAD_PBIS_SCHOOL_LIST";
export const loadPbisSchoolList = (req) => {
  return { type: LOAD_PBIS_SCHOOL_LIST, req };
};

export const SET_PBIS_DATE_LIST = "SET_PBIS_DATE_LIST";
export const setPbisDateList = (pbisDateList) => {
  return { type: SET_PBIS_DATE_LIST, pbisDateList };
};

export const LOAD_PBIS_DATE_LIST = "LOAD_PBIS_DATE_LIST";
export const loadPbisDateList = (req) => {
  return { type: LOAD_PBIS_DATE_LIST, req };
};

export const SET_PBIS_DATA_LIST = "SET_PBIS_DATA_LIST";
export const setPbisDataList = (pbisDataList) => {
  return { type: SET_PBIS_DATA_LIST, pbisDataList };
};

export const LOAD_PBIS_DATA_LIST = "LOAD_PBIS_DATA_LIST";
export const loadPbisDataList = (req) => {
  return { type: LOAD_PBIS_DATA_LIST, req };
};

export const SET_SELECTED_PBIS_DATE = "SET_SELECTED_PBIS_DATE";
export const setSelectedPbisDate = (selectedPbisDate) => {
  return { type: SET_SELECTED_PBIS_DATE, selectedPbisDate };
};

export const SET_SELECTED_PBIS_SCHOOL = "SET_SELECTED_PBIS_SCHOOL";
export const setSelectedPbisSchool = (selectedPbisSchool) => {
  return { type: SET_SELECTED_PBIS_SCHOOL, selectedPbisSchool };
};

export const SET_PBIS_STUDENT_LIST = "SET_PBIS_STUDENT_LIST";
export const setPbisStudentList = (pbisStudentList) => {
  return { type: SET_PBIS_STUDENT_LIST, pbisStudentList };
};

export const LOAD_PBIS_STUDENT_LIST = "LOAD_PBIS_STUDENT_LIST";
export const loadPbisStudentList = (req) => {
  return { type: LOAD_PBIS_STUDENT_LIST, req };
};

export const SET_PBIS_INCIDENT_LIST = "SET_PBIS_INCIDENT_LIST";
export const setPbisIncidentList = (pbisIncidentList) => {
  return { type: SET_PBIS_INCIDENT_LIST, pbisIncidentList };
};

export const LOAD_PBIS_INCIDENT_LIST = "LOAD_PBIS_INCIDENT_LIST";
export const loadPbisIncidentList = (req) => {
  return { type: LOAD_PBIS_INCIDENT_LIST, req };
};

export const SET_PLOT_DATA = "SET_PLOT_DATA";
export const setPlotData = (plotData) => ({
  type: SET_PLOT_DATA,
  plotData,
});
