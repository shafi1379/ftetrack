import AverageReferrals from "./average_referrals";
import ReferralsByGrade from "./referrals_by_grade";
import ReferralsByLocation from "./referrals_by_location";
import ReferralsByProblemBehaviour from "./referrals_by_problem_behaviour";
import ReferralsByReport from "./referrals_by_report";
import ReferralsByTime from "./referrals_by_time";
import ReferralsByWeekday from "./referrals_by_weekday";
import Summary from "./summary";

const routes = [
  { path: "/pbis/avg", exact: true, component: AverageReferrals },
  { path: "/pbis/grade", exact: true, component: ReferralsByGrade },
  { path: "/pbis/location", exact: true, component: ReferralsByLocation },
  { path: "/pbis/type", exact: true, component: ReferralsByProblemBehaviour },
  { path: "/pbis/reporter", exact: true, component: ReferralsByReport },
  { path: "/pbis/time", exact: true, component: ReferralsByTime },
  { path: "/pbis/weekday", exact: true, component: ReferralsByWeekday },
  { path: "/pbis/summary", exact: true, component: Summary },
];

export default routes;
