import React from "react";
import DataTable from "react-data-table-component";
import { Paper } from "@material-ui/core";
import * as Selectors from "../../selectors";
import * as actions from "../../actions";
import { createStructuredSelector } from "reselect";
import DetailModal from "../../containers/TabsModal";
import { connect } from "react-redux";
import { pbisColumns } from "../../../../../utilities/tableConfig";

class ReferralsByReport extends React.Component {
  state = {
    dialogOpen: false,
    selectedLabel: "",
    selectedType: "Referrals by Report",
  };
  handleRowClick = (data, evt) => {
    if (Number(data.incidents) !== 0) {
      this.props.loadPbisIncidentList({
        school: this.props.selectedPbisSchool.school + "",
        tab: "7",
        parm1: data.label + "",
        startDate: this.props.selectedPbisDate.startDate,
        endDate: this.props.selectedPbisDate.endDate || new Date(),
      });
      this.setState({
        dialogOpen: true,
        selectedLabel: data.label,
      });
    }
  };
  render() {
    const { dialogOpen } = this.state;
    const { pbisDataList, pbisIncidentList } = this.props;
    return (
      <div className="pbis-average-referrals">
        <Paper>
          <div className="pbistabs-table">
            <DataTable
              columns={pbisColumns.reporter.map((dash, index) => {
                if (dash.name === "Reported BY") {
                  dash.cell = (row) =>
                    Number(row.incidents) !== 0 ? (
                      <span
                        style={{
                          color: "#683598",
                          textDecoration: "underline",
                          cursor: "pointer",
                        }}
                        onClick={this.handleRowClick.bind(this, row)}
                      >
                        {row.label}
                      </span>
                    ) : (
                      row.label
                    );
                }
                return dash;
              })}
              highlightOnHover={true}
              noHeader={true}
              onRowClicked={this.handleRowClick}
              data={pbisDataList.reporter || [{}]}
            />
          </div>
        </Paper>
        {this.props.pbisIncidentList.length > 0 && (
          <DetailModal
            open={dialogOpen}
            data={pbisIncidentList}
            selectedType={this.state.selectedType}
            selectedLabel={this.state.selectedLabel}
            selectedSchool={this.props.selectedPbisSchool}
            handleClose={(evt) => this.setState({ dialogOpen: !dialogOpen })}
          />
        )}
      </div>
    );
  }
}

const mapStateToProps = () =>
  createStructuredSelector({
    pbisDataList: Selectors.getPbisDataList(),
    pbisIncidentList: Selectors.getPbisIncidentList(),
    selectedPbisSchool: Selectors.getSelectedPbisSchool(),
    selectedPbisDate: Selectors.getSelectedPbisDate(),
  });

const mapDispatchToProps = (dispatch) => ({
  loadPbisIncidentList: (req) => dispatch(actions.loadPbisIncidentList(req)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ReferralsByReport);
