import React from "react";
import DataTable from "react-data-table-component";
import { Paper, Grid } from "@material-ui/core";
import * as Selectors from "../../selectors";
import * as actions from "../../actions";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";
import { pbisColumns } from "../../../../../utilities/tableConfig";
import StudentDetailsDialog from "../../containers/studentDetailsDialog";
import * as summaryConstant from "./Constants/summaryConstants";

class Summary extends React.Component {
  state = {
    openModal: false,
    modalTitle: null,
  };

  cellClick = (type, value, title) => {
    if (type === "ISS/OSS") {
      Object.keys(summaryConstant.issOssDescription).forEach((_key) => {
        if (summaryConstant.issOssDescription[_key] === value) {
          value = _key;
          type = _key.toUpperCase().replace(" ", "-");
        }
      });
    }
    if (type === "INCIDENTS") {
      Object.keys(summaryConstant.discipDescription).forEach((_key) => {
        if (summaryConstant.discipDescription[_key] === value) {
          value = _key;
        }
      });
    }
    this.props.loadPbisStudentList({
      school: this.props.selectedSchool.school + "",
      startDate: this.props.selectedDate.startDate,
      endDate: this.props.selectedDate.endDate,
      cat: type,
      val: value === "T" ? "Z" : value,
    });
    this.setState({
      openModal: true,
      modalTitle: title,
    });
  };

  handleModalClose = () => {
    this.props.setPbisStudentList([]);
    this.setState({ openModal: !this.state.openModal });
  };

  render() {
    const { pbisDataList, pbisStudentList } = this.props;
    const { openModal, modalTitle } = this.state;
    const dispColumns = [],
      issOssColumns = [];

    pbisColumns.summary &&
      pbisColumns.summary.discipColumns.forEach((obj, ind) => {
        if (obj.selector === "count")
          obj["cell"] = (row) => (
            <span
              style={{
                color: "#683598",
                textDecoration: "underline",
                cursor: "pointer",
              }}
              id={"incident-" + ind}
              onClick={(evt) => {
                this.cellClick(
                  row.detail.includes("incident") ? "INCIDENTS" : "ISS/OSS",
                  row.detail,
                  "School Discipline Data: " + row.detail,
                );
              }}
            >
              {row.count}
            </span>
          );
        dispColumns.push(obj);
      });

    pbisColumns.summary &&
      pbisColumns.summary.discipColumns.forEach((obj) => {
        if (obj.selector === "count") {
          obj["cell"] = (row) =>
            row.detail.includes("students") ? (
              <span
                style={{
                  color: "#683598",
                  textDecoration: "underline",
                  cursor: "pointer",
                }}
                onClick={(evt) =>
                  this.cellClick(
                    row.detail.includes("incident") ? "INCIDENTS" : "ISS/OSS",
                    row.detail,
                    "School Discipline Data: " + row.detail,
                  )
                }
              >
                {row.count}
              </span>
            ) : (
              row.count
            );
        }
        issOssColumns.push(obj);
      });

    return (
      <div
        className="pbis-average-referrals"
        id="pbis_summary"
        key="pbis_summary"
      >
        {pbisDataList.summary && (
          <Paper>
            <div className="pbis-summary-header ">Student Enrollments</div>
            <div className="pbis-summary-table-div">
              <DataTable
                columns={pbisColumns.summary.enrolledColumns}
                noHeader={true}
                data={[pbisDataList.summary.enrolledDataObj]}
              />
            </div>
            <div className="pbis-summary-header "> Active Students</div>
            <div className="pbis-summary-table-div">
              <DataTable
                columns={pbisColumns.summary.enrolledColumns}
                noHeader={true}
                data={[pbisDataList.summary.activeDataObj]}
              />
            </div>
            <div className="pbis-summary-header ">
              Student Enrollments By Race/Ethnicity
            </div>
            <div className="pbis-summary-table-div">
              <DataTable
                columns={pbisColumns.summary.raceColumns}
                noHeader={true}
                data={[pbisDataList.summary.raceDataObj]}
              />
            </div>
            <div className="pbis-summary-header "> Attendance&nbsp;Data</div>
            <div className="pbis-summary-table-div">
              <DataTable
                columns={pbisColumns.summary.attendanceColumns.map((obj) => {
                  if (
                    summaryConstant.attendanceCellClickColumns.includes(
                      obj.selector,
                    )
                  ) {
                    obj["cell"] = (row) => (
                      <span
                        style={{
                          color: "#683598",
                          textDecoration: "underline",
                          cursor: "pointer",
                        }}
                        onClick={(evt) => {
                          this.cellClick(
                            obj.selector,
                            row.race.includes("Indian")
                              ? "I"
                              : row.race.substr(0, 1),
                            "Attendance Data: " + row.race,
                          );
                        }}
                      >
                        {row["race"] === "Total Students" ? (
                          <strong>{row[obj.selector]}</strong>
                        ) : (
                          <div>{row[obj.selector]}</div>
                        )}
                      </span>
                    );
                  } else {
                    obj["cell"] = (row) =>
                      row.race === "Total Students" ? (
                        <strong>{row[obj.selector]}</strong>
                      ) : (
                        <div>{row[obj.selector]}</div>
                      );
                  }
                  return obj;
                })}
                noHeader={true}
                data={[...pbisDataList.summary.attendanceData]}
              />
            </div>
            <div className="pbis-summary-header ">
              Suspension&nbsp;Rate&nbsp;by&nbsp;Race/Ethnicity
            </div>
            <div className="pbis-summary-table-div">
              <DataTable
                columns={pbisColumns.summary.suspRateColumns.map((obj) => {
                  obj["cell"] = (row, index, column, id) => (
                    <span
                      style={{
                        color: "#683598",
                        textDecoration: "underline",
                        cursor: "pointer",
                      }}
                      onClick={(evt) =>
                        this.cellClick(
                          "SUSP-RATE",
                          obj.id,
                          "Suspension Rate by Race/Ethnicity",
                        )
                      }
                    >
                      {row[obj["selector"]]}
                    </span>
                  );
                  return obj;
                })}
                noHeader={true}
                data={
                  pbisDataList.summary.Total
                    ? [
                        {
                          Z:
                            pbisDataList.summary.TotalSusp +
                            " (" +
                            (
                              (pbisDataList.summary.TotalSusp * 100) /
                              Number(pbisDataList.summary.Total.students)
                            ).toFixed(1) +
                            "%)",
                          ...pbisDataList.summary.suspDataObj,
                        },
                      ]
                    : []
                }
              />
            </div>
            <Grid xs={12} style={{ display: "inline-flex", width: "100%" }}>
              <Grid item xs={6} style={{ borderRight: "2px solid lightgrey" }}>
                <div className="pbis-summary-header ">
                  School&nbsp;Discipline&nbsp;Data
                </div>
                <div className="pbis-summary-table-div">
                  <DataTable
                    columns={dispColumns}
                    noHeader={true}
                    noTableHead={true}
                    data={[...pbisDataList.summary.discipData]}
                  />
                </div>
              </Grid>
              <Grid item xs={6}>
                <div className="pbis-summary-header ">
                  Total&nbsp;ISS/OSS&nbsp;Days&nbsp;Data
                </div>
                <div className="pbis-summary-table-div">
                  <DataTable
                    columns={[...issOssColumns]}
                    noHeader={true}
                    noTableHead={true}
                    data={[...pbisDataList.summary.issOssData]}
                  />
                </div>
              </Grid>
            </Grid>
          </Paper>
        )}
        {pbisStudentList.length > 0 && (
          <StudentDetailsDialog
            open={openModal}
            handleClose={() => this.handleModalClose()}
            data={pbisStudentList}
            title={modalTitle}
          />
        )}
      </div>
    );
  }
}

const mapStateToProps = () =>
  createStructuredSelector({
    pbisDataList: Selectors.getPbisDataList(),
    pbisStudentList: Selectors.getPbisStudentList(),
    selectedSchool: Selectors.getSelectedPbisSchool(),
    selectedDate: Selectors.getSelectedPbisDate(),
  });

const mapDispatchToProps = (dispatch) => ({
  loadPbisStudentList: (req) => dispatch(actions.loadPbisStudentList(req)),
  setPbisStudentList: (pbisStudentList) =>
    dispatch(actions.setPbisStudentList(pbisStudentList)),
});
export default connect(mapStateToProps, mapDispatchToProps)(Summary);
