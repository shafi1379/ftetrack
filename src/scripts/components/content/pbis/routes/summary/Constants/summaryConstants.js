export const enrollmentStudentDataKeyword = ["Z", "SPED", "EL", "FRM"];
export const attendanceCellClickColumns = [
  "ATTD-LT5",
  "ATTD-LT15",
  "ATTD-MT15",
];
export const raceStudentDataKeyword = ["A", "B", "H", "I", "W", "M", "P"];
export const attendRaceDescription = {
  Z: "Total Students",
  A: "Asian",
  B: "Black",
  H: "Hispanic",
  I: "American Indian",
  W: "White",
  M: "Multi Racial",
  P: "Pacific Islander",
  ZE: "English Learner",
  ZH: "Homeless",
  ZS: "Special Ed",
};
export const discipDescription = {
  "0 Incident": "Total # of students with 0 discipline incidents",
  "1 Incident": "Total # of students with 1 discipline incidents",
  "01 Incident": "Total # of students with 0 or 1 discipline incidents",
  "2 Incident": "Total # of students with 2 discipline incidents",
  "3 Incident": "Total # of students with 3 discipline incidents",
  "4 Incident": "Total # of students with 4 dscipline incidents",
  "5 Incident": "Total # of students with 5 discipline incidents",
  "6 Incident": "Total # of students with 6-10 discipline incidents",
  "11 Incident": "Total # of students with 11 or more discipline incidents",
};
export const issOssDescription = {
  "ISS Days": "Total # of days ISS",
  "OSS Days": "Total # of days OSS",
  "ISS STUDENTS": "Total unduplicated students with ISS",
  "OSS STUDENTS": "Total unduplicated students with OSS",
  "99 Incident": "Total Incidents",
};
