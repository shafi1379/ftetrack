import React from "react";
import DataTable from "react-data-table-component";
import Plot from "react-plotly.js";
import { Paper } from "@material-ui/core";
import * as Selectors from "../../selectors";
import * as actions from "../../actions";
import { createStructuredSelector } from "reselect";
import DetailModal from "../../containers/TabsModal";
import { connect } from "react-redux";
import { pbisColumns } from "../../../../../utilities/tableConfig";

class ReferralsByGrade extends React.Component {
  state = {
    dialogOpen: false,
    selectedLabel: "",
    selectedType: "Referrals by Grade",
  };
  handleRowClick = (data, evt) => {
    if (Number(data.incidents) !== 0) {
      this.props.loadPbisIncidentList({
        school: this.props.selectedPbisSchool.school + "",
        tab: "6",
        parm1: data.label + "",
        startDate: this.props.selectedPbisDate.startDate,
        endDate: this.props.selectedPbisDate.endDate || new Date(),
      });
      this.setState({
        dialogOpen: true,
        selectedLabel: data.label,
      });
    }
  };
  render() {
    const { dialogOpen } = this.state;
    const { pbisDataList, pbisIncidentList } = this.props;
    var trace1 = {
      x: [],
      y: [],
      name: " of Incidents",
      yaxis: "y1",
      type: "bar",
    };
    pbisDataList.grade &&
      pbisDataList.grade.forEach((obj, ind) => {
        trace1.x.push(obj.label);
        trace1.y.push(obj.incidents);
      });
    return (
      <div className="pbis-average-referrals">
        <Paper>
          <Plot
            data={[trace1]}
            layout={{
              yaxis: { title: "Number of Referrals" },
              width: 1350,
              showlegend: false,
              autosize: true,
              margin: {
                l: 50,
                r: 70,
                b: 50,
                t: 50,
                pad: 2,
              },
            }}
          />
          <div className="pbistabs-table">
            <DataTable
              columns={pbisColumns.grade.map((dash, index) => {
                if (dash.name === "Grade") {
                  dash.cell = (row) =>
                    Number(row.incidents) !== 0 ? (
                      <span
                        style={{
                          color: "#683598",
                          textDecoration: "underline",
                          cursor: "pointer",
                        }}
                        onClick={this.handleRowClick.bind(this, row)}
                      >
                        {row.label}
                      </span>
                    ) : (
                      row.label
                    );
                }
                return dash;
              })}
              highlightOnHover={true}
              noHeader={true}
              onRowClicked={this.handleRowClick}
              data={pbisDataList.grade || [{}]}
            />
          </div>
        </Paper>
        {this.props.pbisIncidentList.length > 0 && (
          <DetailModal
            open={dialogOpen}
            data={pbisIncidentList}
            selectedType={this.state.selectedType}
            selectedLabel={this.state.selectedLabel}
            selectedSchool={this.props.selectedPbisSchool}
            handleClose={(evt) => this.setState({ dialogOpen: !dialogOpen })}
          />
        )}
      </div>
    );
  }
}

const mapStateToProps = () =>
  createStructuredSelector({
    pbisDataList: Selectors.getPbisDataList(),
    pbisIncidentList: Selectors.getPbisIncidentList(),
    selectedPbisSchool: Selectors.getSelectedPbisSchool(),
    selectedPbisDate: Selectors.getSelectedPbisDate(),
  });

const mapDispatchToProps = (dispatch) => ({
  loadPbisIncidentList: (req) => dispatch(actions.loadPbisIncidentList(req)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ReferralsByGrade);
