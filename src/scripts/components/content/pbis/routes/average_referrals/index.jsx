import React from "react";
import DataTable from "react-data-table-component";
import Plot from "react-plotly.js";
import { Paper } from "@material-ui/core";
import * as Selectors from "../../selectors";
import * as actions from "../../actions";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";
import { pbisColumns } from "../../../../../utilities/tableConfig";
import DetailModal from "../../containers/TabsModal";

class AverageReferrals extends React.Component {
  state = {
    dialogOpen: false,
    selectedLabel: "",
    selectedType: "Average Referrals by Month",
  };
  handleRowClick = (data, evt) => {
    if (Number(data.incidents) !== 0) {
      this.props.loadPbisIncidentList({
        school: this.props.selectedPbisSchool.school + "",
        tab: "1",
        parm1: data.month_num + "",
        startDate: this.props.selectedPbisDate.startDate,
        endDate: this.props.selectedPbisDate.endDate || new Date(),
      });
      this.setState({
        dialogOpen: true,
        selectedLabel: data.month,
      });
    }
  };
  render() {
    const { dialogOpen } = this.state;
    const { pbisDataList, pbisIncidentList } = this.props;
    var trace1 = {
      x: [],
      y: [],
      name: " of Incidents",
      yaxis: "y1",
      type: "bar",
    };

    var trace2 = {
      x: [],
      y: [],
      name: "Avg Referrals",
      yaxis: "y2",
      type: "scatter",
    };
    pbisDataList.avg &&
      pbisDataList.avg.forEach((obj, ind) => {
        if (ind < pbisDataList.avg.length - 1) {
          trace1.x.push(obj.month);
          trace2.x.push(obj.month);
          trace1.y.push(obj.incidents);
          trace2.y.push(obj.avgPerDay);
        }
      });
    return (
      <div className="pbis-average-referrals">
        <Paper>
          <Plot
            data={[trace1, trace2]}
            layout={{
              yaxis: { title: "Number of Referrals" },
              yaxis2: {
                title: "Average Referrals Per Day",
                titlefont: { color: "rgb(148, 103, 189)" },
                tickfont: { color: "rgb(148, 103, 189)" },
                overlaying: "y",
                side: "right",
              },
              width: 1350,
              showlegend: false,
              autosize: true,
              margin: {
                l: 50,
                r: 70,
                b: 50,
                t: 50,
                pad: 2,
              },
            }}
          />
          <div className="pbistabs-table">
            <DataTable
              columns={pbisColumns.avg.map((dash, index) => {
                if (dash.name === "Month") {
                  dash.cell = (row) =>
                    Number(row.incidents) !== 0 ? (
                      <span
                        style={{
                          color: "#683598",
                          textDecoration: "underline",
                          cursor: "pointer",
                        }}
                        onClick={this.handleRowClick.bind(this, row)}
                      >
                        {row.month}
                      </span>
                    ) : (
                      row.month
                    );
                }
                return dash;
              })}
              highlightOnHover={true}
              noHeader={true}
              onRowClicked={this.handleRowClick}
              data={pbisDataList.avg || [{}]}
            />
          </div>
        </Paper>
        {this.props.pbisIncidentList.length > 0 && (
          <DetailModal
            open={dialogOpen}
            data={pbisIncidentList}
            selectedType={this.state.selectedType}
            selectedLabel={this.state.selectedLabel}
            selectedSchool={this.props.selectedPbisSchool}
            handleClose={(evt) => this.setState({ dialogOpen: !dialogOpen })}
          />
        )}
      </div>
    );
  }
}

const mapStateToProps = () =>
  createStructuredSelector({
    pbisDataList: Selectors.getPbisDataList(),
    pbisIncidentList: Selectors.getPbisIncidentList(),
    selectedPbisSchool: Selectors.getSelectedPbisSchool(),
    selectedPbisDate: Selectors.getSelectedPbisDate(),
  });

const mapDispatchToProps = (dispatch) => ({
  loadPbisIncidentList: (req) => dispatch(actions.loadPbisIncidentList(req)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AverageReferrals);
