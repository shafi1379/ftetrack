import { createSelector } from "reselect";
import * as selectors from "../../../redux/selectors";

export const getPbisSchoolList = () =>
  createSelector(selectors.pbisState, (pbisState) =>
    pbisState.get("pbisSchoolList").toJS(),
  );

export const getPbisStudentList = () =>
  createSelector(selectors.pbisState, (pbisState) =>
    pbisState.get("pbisStudentList").toJS(),
  );

export const getPbisIncidentList = () =>
  createSelector(selectors.pbisState, (pbisState) =>
    pbisState.get("pbisIncidentList").toJS(),
  );

export const getPbisDateList = () =>
  createSelector(selectors.pbisState, (pbisState) =>
    pbisState.get("pbisDateList").toJS(),
  );

export const getPbisDataList = () =>
  createSelector(selectors.pbisState, (pbisState) =>
    pbisState.get("pbisDataList").toJS(),
  );

export const getSelectedPbisSchool = () =>
  createSelector(selectors.pbisState, (pbisState) =>
    pbisState.get("selectedPbisSchool").toJS(),
  );

export const getSelectedPbisDate = () =>
  createSelector(selectors.pbisState, (pbisState) =>
    pbisState.get("selectedPbisDate").toJS(),
  );

export const getPlotData = () =>
  createSelector(selectors.pbisState, (pbisState) =>
    pbisState.get("plotData").toJS(),
  );
