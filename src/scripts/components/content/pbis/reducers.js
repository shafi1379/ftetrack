import {
  SET_PBIS_DATA_LIST,
  SET_PBIS_DATE_LIST,
  SET_PBIS_SCHOOL_LIST,
  SET_SELECTED_PBIS_DATE,
  SET_SELECTED_PBIS_SCHOOL,
  SET_PBIS_STUDENT_LIST,
  SET_PBIS_INCIDENT_LIST,
  SET_PLOT_DATA,
} from "./actions";
import { fromJS } from "immutable";

const initialState = fromJS({
  pbisSchoolList: [],
  pbisStudentList: [],
  pbisIncidentList: [],
  pbisDateList: [],
  pbisDataList: {},
  selectedPbisDate: { startDate: "", endDate: new Date() },
  selectedPbisSchool: { label: "Select a School ...", value: -1 },
  plotData: [],
});

const pbis = (state = initialState, action) => {
  switch (action.type) {
    case SET_PBIS_SCHOOL_LIST:
      return state.set("pbisSchoolList", fromJS([...action.pbisSchoolList]));
    case SET_PBIS_DATE_LIST:
      return state.set("pbisDateList", fromJS([...action.pbisDateList]));
    case SET_PBIS_DATA_LIST:
      return state.set("pbisDataList", fromJS({ ...action.pbisDataList }));
    case SET_PBIS_INCIDENT_LIST:
      return state.set(
        "pbisIncidentList",
        fromJS([...action.pbisIncidentList]),
      );
    case SET_SELECTED_PBIS_SCHOOL:
      return state.set(
        "selectedPbisSchool",
        fromJS({ ...action.selectedPbisSchool }),
      );
    case SET_PBIS_STUDENT_LIST:
      return state.set("pbisStudentList", fromJS([...action.pbisStudentList]));
    case SET_SELECTED_PBIS_DATE:
      return state.set(
        "selectedPbisDate",
        fromJS({ ...action.selectedPbisDate }),
      );
    case SET_PLOT_DATA:
      return state.set("plotData", fromJS([...action.plotData]));
    default:
      return state;
  }
};

export default pbis;
