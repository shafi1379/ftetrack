import { select, all, call, fork, put, takeLatest } from "redux-saga/effects";
import { setProgressingValue } from "../../../app/actions";
import * as Api from "../../../services/api";
import { API_URLS } from "../../../utilities/constants";
import * as summaryConstant from "./routes/summary/Constants/summaryConstants";
import {
  setPbisDataList,
  setPbisDateList,
  setPbisSchoolList,
  setSelectedPbisSchool,
  setSelectedPbisDate,
  setPbisStudentList,
  setPbisIncidentList,
  setPlotData,
  LOAD_PBIS_DATA_LIST,
  LOAD_PBIS_DATE_LIST,
  LOAD_PBIS_SCHOOL_LIST,
  LOAD_PBIS_STUDENT_LIST,
  LOAD_PBIS_INCIDENT_LIST,
} from "./actions";
import * as Utilities from "../../../utilities/utilities";

function* loadPbisSchoolListRequested(request) {
  try {
    const { pbisSchoolList } = (yield select((s) => s.get("pbis"))).toJS();
    if (!pbisSchoolList.length) {
      yield put(setProgressingValue(true));
      const response = yield call(
        Api.doPostRequest,
        API_URLS.schools,
        request.req,
      );
      if (response) {
        const pbisSchoolList = response.map((res) => ({
          ...res,
          label: res.schoolname,
          value: res.school,
        }));
        yield put(setPbisSchoolList(pbisSchoolList));
        yield put(setSelectedPbisSchool(pbisSchoolList[0]));
        yield put(setProgressingValue(false));
      }
    }
  } catch (error) {
    console.error(error);
  }
}

function* pbisSchoolListListener() {
  yield takeLatest(LOAD_PBIS_SCHOOL_LIST, loadPbisSchoolListRequested);
}

function* loadPbisStudentListRequested(request) {
  try {
    yield put(setProgressingValue(true));
    const response = yield call(
      Api.doPostRequest,
      API_URLS.pbisStudents,
      request.req,
    );
    if (response && response.students) {
      yield put(setPbisStudentList(response.students));
      const raceList = Utilities.getRaceList(response.students, "raceFederal");
      yield put(setPlotData([raceList, response.enrollment[0]]));
      yield put(setProgressingValue(false));
    }
  } catch (error) {
    console.error(error);
  }
}

function* pbisStudentListListener() {
  yield takeLatest(LOAD_PBIS_STUDENT_LIST, loadPbisStudentListRequested);
}

function* loadPbisIncidentListRequested(request) {
  try {
    yield put(setPbisIncidentList([]));
    yield put(setProgressingValue(true));
    const response = yield call(
      Api.doPostRequest,
      API_URLS.pbisIncidents,
      request.req,
    );
    if (response && response.students) {
      yield put(setPbisIncidentList(response.students));
      yield put(setProgressingValue(false));
    }
  } catch (error) {
    console.error(error);
  }
}

function* pbisIncidentListListener() {
  yield takeLatest(LOAD_PBIS_INCIDENT_LIST, loadPbisIncidentListRequested);
}

function* loadPbisDateListRequested(request) {
  try {
    const { pbisDateList } = (yield select((s) => s.get("pbis"))).toJS();
    if (!pbisDateList.length) {
      yield put(setProgressingValue(true));
      const response = yield call(
        Api.doPostRequest,
        API_URLS.pbisDates,
        request.req,
      );
      if (response) {
        yield put(setPbisDateList(response));
        yield put(setSelectedPbisDate(response[0]));
        yield put(setProgressingValue(false));
      }
    }
  } catch (error) {
    console.error(error);
  }
}

function* pbisDateListListener() {
  yield takeLatest(LOAD_PBIS_DATE_LIST, loadPbisDateListRequested);
}

function* loadPbisDataListRequested(request) {
  try {
    yield put(setProgressingValue(true));

    const response = yield call(
      Api.doPostRequest,
      API_URLS.pbisData,
      request.req,
    );
    if (response) {
      var summaryData = {
        attendanceData: [],
        discipData: [],
        issOssData: [],
        enrolledDataObj: {},
        activeDataObj: {},
        raceDataObj: {},
        attendanceDataObj: {},
        suspDataObj: {},
        TotalSusp: 0,
        Total: 0,
      };
      response.summary &&
        response.summary.length &&
        summaryConstant.raceStudentDataKeyword.forEach((val) => {
          summaryData.attendanceDataObj[val] = {};
        });

      summaryData.Total =
        response.summary &&
        response.summary.filter(
          (obj) => obj.cat === "DEMO-Count" && obj.val === "Z",
        )[0];

      var TotalByRace = {};

      response.summary &&
        response.summary.forEach((obj) => {
          if (obj.cat.includes("ATTD-")) {
            TotalByRace[obj.val] = TotalByRace[obj.val]
              ? TotalByRace[obj.val] + Number(obj.students)
              : Number(obj.students);
          }
        });

      if (response.summary && response.summary.length) {
        summaryData.attendanceDataObj = {
          ...summaryData.attendanceDataObj,
          Z: {},
        };
      }

      response.summary &&
        response.summary.forEach((obj) => {
          obj.students = obj.students.replace(/\.00/g, "");
          if (obj.cat === "DEMO-Count") {
            if (
              summaryConstant.enrollmentStudentDataKeyword.includes(obj.val)
            ) {
              summaryData.enrolledDataObj[obj.val] = obj.students;
            }
            if (summaryConstant.raceStudentDataKeyword.includes(obj.val)) {
              summaryData.raceDataObj[obj.val] =
                obj.students +
                " (" +
                (
                  (Number(obj.students) * 100) /
                  Number(summaryData.Total.students)
                ).toFixed(1) +
                "%)";
            }
          }
          if (obj.cat.includes("ATTD-")) {
            summaryData.attendanceDataObj[obj.val] = {
              ...summaryData.attendanceDataObj[obj.val],
              Z: TotalByRace[obj.val],
              [obj.cat]:
                obj.students +
                " (" +
                (
                  (Number(obj.students) * 100) /
                  Number(TotalByRace[obj.val])
                ).toFixed(1) +
                "%)",
            };
          }
          if (obj.cat === "SUSP-RATE") {
            if (summaryConstant.raceStudentDataKeyword.includes(obj.val)) {
              summaryData.suspDataObj[obj.val] =
                obj.students +
                " (" +
                (
                  (Number(obj.students) * 100) /
                  Number(TotalByRace[obj.val])
                ).toFixed(1) +
                "%)";
              summaryData.TotalSusp += Number(obj.students);
            }
          }
          if (obj.cat === "INCIDENTS" && obj.val !== "99 Incident") {
            summaryData.discipData = [
              ...summaryData.discipData,
              {
                detail: summaryConstant.discipDescription[obj.val],
                count:
                  obj.students +
                  " (" +
                  (
                    (Number(obj.students) * 100) /
                    Number(summaryData.Total.students)
                  ).toFixed(1) +
                  "%)",
              },
            ];
          }
          if (obj.cat === "DEMO-Active") {
            if (
              summaryConstant.enrollmentStudentDataKeyword.includes(obj.val)
            ) {
              summaryData.activeDataObj[obj.val] = obj.students;
            }
          }
          if (
            obj.cat.includes("ISS") ||
            obj.cat.includes("OSS") ||
            obj.val === "99 Incident"
          ) {
            summaryData.issOssData = [
              ...summaryData.issOssData,
              {
                detail: summaryConstant.issOssDescription[obj.val],
                count: obj.students,
              },
            ];
          }
        });
      if (response.summary && response.summary.length) {
        summaryData.attendanceData = Object.keys(
          summaryData.attendanceDataObj,
        ).map((val) => {
          return {
            race: summaryConstant.attendRaceDescription[val],
            ...summaryData.attendanceDataObj[val],
          };
        });
        summaryData.attendanceData.unshift(summaryData.attendanceData.pop());
      }
      response["summary"] = { ...summaryData };
      yield put(setPbisDataList(response));
      yield put(setProgressingValue(false));
    }
  } catch (error) {
    console.error(error);
  }
}

function* pbisDataListListener() {
  yield takeLatest(LOAD_PBIS_DATA_LIST, loadPbisDataListRequested);
}

export default function* root() {
  yield all([
    fork(pbisSchoolListListener),
    fork(pbisDateListListener),
    fork(pbisDataListListener),
    fork(pbisStudentListListener),
    fork(pbisIncidentListListener),
  ]);
}
