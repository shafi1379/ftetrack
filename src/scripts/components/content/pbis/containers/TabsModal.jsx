import React, { useState } from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Dialog from "@material-ui/core/Dialog";
import MuiDialogContent from "@material-ui/core/DialogContent";
import IconButton from "@material-ui/core/IconButton";
import { Close, CloudDownload } from "@material-ui/icons";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import { pbisTabModalColumns } from "../../../../utilities/tableConfig";
import * as Utilities from "../../../../utilities/utilities";
import { CSVLink } from "react-csv";
import DataTable from "react-data-table-component";

const useStyles = makeStyles((theme) => ({
  appBar: {
    position: "static",
  },
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(0.5),
    color: "#f9f9f9",
    padding: 10,
  },
  downloadButton: {
    position: "absolute",
    right: "45px",
    color: "#f9f9f9",
    padding: 10,
  },
}));

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

export default function TabsModalDialog(props) {
  const classes = useStyles();
  const [csvExport, setCsvExport] = useState({
    data: [],
    fileName: "ctae.csv",
  });
  const csvLink = React.useRef();
  const handleClose = () => props.handleClose();
  const downloadCSV = () => {
    const columnDetails = [...pbisTabModalColumns];
    const columns = {};
    columnDetails.forEach((obj) => {
      columns[obj["name"]] = obj["selector"];
    });
    const data = Utilities.generateExcelData(props.data || [], columns || {});
    data.unshift(["School : " + props.selectedSchool.label]);
    data.unshift([
      "Incident List for " + props.selectedType + " : " + props.selectedLabel,
    ]);
    setCsvExport({
      data,
      fileName: `${props.selectedType + props.selectedLabel}.csv`,
    });
    setTimeout(() => {
      csvLink.current.link.click();
    });
  };
  return (
    <>
      <Dialog
        open={props.open}
        onClose={handleClose}
        maxWidth={"lg"}
        aria-labelledby="customized-dialog-title"
        aria-describedby="customized-dialog-description"
      >
        <AppBar className={classes.appBar}>
          <Toolbar style={{ minHeight: 45 }}>
            <Typography>
              {props.selectedType + " : " + props.selectedLabel}
            </Typography>
            <IconButton
              aria-label="close"
              className={classes.downloadButton}
              onClick={downloadCSV}
            >
              <CloudDownload />
            </IconButton>
            <IconButton
              aria-label="close"
              className={classes.closeButton}
              onClick={handleClose}
            >
              <Close />
            </IconButton>
          </Toolbar>
        </AppBar>
        <DialogContent dividers>
          <DataTable
            columns={pbisTabModalColumns}
            noHeader={true}
            data={props.data}
          />
        </DialogContent>
      </Dialog>
      <CSVLink
        ref={csvLink}
        data={csvExport.data || []}
        className="hidden"
        filename={csvExport.fileName}
        target="_self"
      />
    </>
  );
}
