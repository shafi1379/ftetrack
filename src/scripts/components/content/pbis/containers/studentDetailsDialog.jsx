import React from "react";
import { withRouter } from "react-router-dom";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import CloseIcon from "@material-ui/icons/Close";
import CloudDownload from "@material-ui/icons/CloudDownload";
import DataTable from "react-data-table-component";
import Dialog from "@material-ui/core/Dialog";
import IconButton from "@material-ui/core/IconButton";
import MaterialIcon from "../../../generic/materialIcon";
import Tooltip from "@material-ui/core/Tooltip";
import Typography from "@material-ui/core/Typography";
import * as selectors from "../selectors";
import * as ewsSelectors from "../../ews/selectors";
import * as ewsActions from "../../ews/actions";
import Paper from "@material-ui/core/Paper";
import Slide from "@material-ui/core/Slide";
import { CSVLink } from "react-csv";
import StudentDetailsDialog from "../../../generic/details_dialog";
import {
  pibStudentsColumns,
  customStyles,
} from "../../../../utilities/tableConfig";
import * as Utilities from "../../../../utilities/utilities";
import PieChart from "../../../generic/pieChartDialog.jsx";

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

class StudentsDialog extends React.Component {
  csvLink = React.createRef();
  state = {
    openPieChartModal: false,
    openStudentDetailsDialog: false,
    studentsRowData: {},
    pbisStudentsColumns: pibStudentsColumns.map((obj) => {
      if (obj.selector === "studentNumber") {
        obj["cell"] = (row) => (
          <span
            style={{
              color: "#683598",
              textDecoration: "underline",
              cursor: "pointer",
            }}
            onClick={(evt) => this.handleRowClick(row, evt)}
          >
            {row.studentNumber}
          </span>
        );
      }
      return obj;
    }),
    csvExport: {
      data: [],
      fileName: "pbis.csv",
    },
  };

  handleRowClick = (data, evt) => {
    this.props.loadStudentDetails({
      personID: data.personID,
    });
    this.setState({
      studentsRowData: data,
      openStudentDetailsDialog: true,
    });
  };

  downloadCSV = () => {
    const currentTime = `_${new Date().getTime()}`;
    const columns = {};
    const columnKeys = Object.keys(this.props.data[0]);
    // eslint-disable-next-line
    columnKeys.filter((col) => {
      const firstLetter = col.trim().charAt(0);
      let replaceLetter = null;
      if (firstLetter === "#") {
        replaceLetter = "No. of ";
      } else {
        replaceLetter = firstLetter.toUpperCase();
      }
      const rtn_word = replaceLetter + col.slice(1);
      const curatedCol = rtn_word
        .replace(/_/g, " ")
        .split(/\s/)
        .map((v) => v.charAt(0).toUpperCase() + v.slice(1))
        .join(" ");
      columns[curatedCol] = col;
    });
    const data = Utilities.generateExcelData(
      this.props.data || [],
      columns || {},
    );
    const excelPageName = this.props.title + currentTime;
    this.setState({ csvExport: { data, fileName: `${excelPageName}.csv` } });
    setTimeout(() => {
      this.csvLink.current.link.click();
    });
  };

  render() {
    const {
      openStudentDetailsDialog,
      studentsRowData,
      pbisStudentsColumns,
      csvExport,
      openPieChartModal,
    } = this.state;
    const { plotData } = this.props;
    return (
      <React.Fragment>
        <Dialog
          className="full-screen-dialog"
          fullScreen
          TransitionComponent={Transition}
          open={this.props.open}
          onClose={(evt) => this.props.handleClose()}
        >
          <AppBar style={{ position: "relative" }}>
            <Toolbar style={{ minHeight: 45 }}>
              <Typography className="capitalize" variant="button">
                {this.props.title}
              </Typography>
              <IconButton
                aria-label="close"
                style={{
                  position: "absolute",
                  right: "90px",
                  color: "#f9f9f9",
                  padding: 10,
                }}
                onClick={this.downloadCSV}
              >
                <CloudDownload />
              </IconButton>
              <Tooltip title="PieChart" aria-label="pie-chart-tooltip">
                <IconButton
                  style={{
                    position: "absolute",
                    right: "45px",
                    color: "#f9f9f9",
                    padding: 10,
                  }}
                  onClick={(evt) => this.setState({ openPieChartModal: true })}
                  className="pie-chart-btn"
                  aria-label="pie-chart"
                >
                  <MaterialIcon icon="pie_chart" />
                </IconButton>
              </Tooltip>
              <IconButton
                aria-label="close"
                onClick={this.props.handleClose}
                style={{ color: "#FFFFFF", position: "absolute", right: "0px" }}
              >
                <CloseIcon />
              </IconButton>
            </Toolbar>
          </AppBar>
          <div>
            <Paper square>
              <DataTable
                className="dashboard-table"
                noHeader={true}
                fixedHeader={true}
                onRowClicked={this.handleRowClick}
                columns={pbisStudentsColumns}
                data={this.props.data || []}
                customStyles={customStyles}
              />
            </Paper>
          </div>
        </Dialog>
        <CSVLink
          ref={this.csvLink}
          data={csvExport.data || []}
          className="hidden"
          filename={csvExport.fileName}
          target="_self"
        />
        {Object.keys(this.props.studentsDetailsList).length > 0 && (
          <StudentDetailsDialog
            open={openStudentDetailsDialog}
            openStudentsDetailsInfo={() =>
              this.setState({
                openStudentDetailsDialog: !openStudentDetailsDialog,
              })
            }
            row={studentsRowData}
            data={this.props.studentsDetailsList}
          />
        )}
        {openPieChartModal && (
          <PieChart
            title="PBIS"
            open={openPieChartModal}
            onClose={() =>
              this.setState({
                openPieChartModal: !openPieChartModal,
              })
            }
            data={plotData}
          />
        )}
      </React.Fragment>
    );
  }
}

const mapStateToProps = () =>
  createStructuredSelector({
    studentsDetailsList: ewsSelectors.getEwsStudentsDetails(),
    plotData: selectors.getPlotData(),
  });

const mapDispatchToProps = (dispatch) => ({
  loadStudentDetails: (request) =>
    dispatch(ewsActions.loadEwsStudentsDetails(request)),
});
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withRouter(StudentsDialog));
