import React, { Component } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Link,
  withRouter,
} from "react-router-dom";
import DatePicker from "react-datepicker";
import jsPDF from "jspdf";
import "jspdf-autotable";
import { createStructuredSelector } from "reselect";
import { formatDate } from "../../../utilities/utilities";
import { pbisColumns } from "../../../utilities/tableConfig";
import {
  Paper,
  AppBar,
  Toolbar,
  Grid,
  Typography,
  Tabs,
  Tab,
  Button,
  IconButton,
  Tooltip,
} from "@material-ui/core";
import Select from "react-select";
import { connect } from "react-redux";
import MaterialIcon from "../../generic/materialIcon";
import * as loginSelectors from "../../../app/selectors";
import * as selectors from "./selectors";
import * as actions from "./actions";
import RouteWithSubRoutes from "./../../generic/subRoutes";
import { selectCustomStyles } from "../../generic/reactSelectCustomization";
import "react-datepicker/dist/react-datepicker.css";
import * as Utilities from "../../../utilities/utilities";

export const PBISTabs = [
  {
    label: "Summary",
    value: "/pbis/summary",
  },
  {
    label: "Average Referrals",
    value: "/pbis/avg",
  },
  {
    label: "Referrals by Time",
    value: "/pbis/time",
  },
  {
    label: "Referrals by Weekday",
    value: "/pbis/weekday",
  },
  {
    label: "Referrals by Problem Behavior",
    value: "/pbis/type",
  },
  {
    label: "Referrals by Location",
    value: "/pbis/location",
  },
  {
    label: "Referrals by Grade",
    value: "/pbis/grade",
  },
  {
    label: "Referrals by Reporter",
    value: "/pbis/reporter",
  },
];

class PBIS extends Component {
  state = {
    value: "/pbis/summary",
  };

  componentDidMount() {
    this.props.setSelectedPbisDate({
      startDate: new Date(
        "07/01/" + this.props.start_year.school_year.split("-")[0],
      ),
      endDate: new Date(),
    });
    document.getElementById("Summary").click();
  }

  handleCallToRouter = (event, newValue) => {
    this.setState({ value: newValue });
  };

  dateChange = (date, type, key) => {
    const dates = this.props.selectedPbisDate;
    dates[type] = date;
    this.props.setSelectedPbisDate({
      ...dates,
    });
  };

  viewReport = (val, evt, type) => {
    const dates = this.props.selectedPbisDate;
    if (type === "School") {
      this.props["setSelectedPbis" + type]({ ...val });
    } else {
      document.getElementById("Summary").click();
      this.props.loadPbisDataList({
        school: this.props.selectedPbisSchool.school,
        startDate: dates.startDate,
        endDate: dates.endDate || new Date(),
      });
    }
  };

  pdfDownload = (evt) => {
    var doc = new jsPDF("p", "pt");
    const dates = this.props.selectedPbisDate;
    const autoCalculateMargin = { top: 105 };
    var lastTableOffset = 10;
    const mainHeader = "PBIS Report";
    const subHeader = this.props.userData.system_name;
    const currentDate = Utilities.formatDateSlash(
      formatDate(new Date(), "-"),
      "mm/dd/yyyy",
    );
    doc.setFontSize(10);
    const xOffsetH1 =
      doc.internal.pageSize.width / 2 -
      (doc.getStringUnitWidth(mainHeader) * doc.internal.getFontSize()) / 2;
    const subHeaderOffset =
      doc.internal.pageSize.width / 2 -
      (doc.getStringUnitWidth(subHeader) * doc.internal.getFontSize()) / 2;

    doc.setFontSize(14);
    doc.text(mainHeader, xOffsetH1 - (currentDate.length + 10), 30);
    doc.setFontSize(12);
    doc.text(subHeader, subHeaderOffset - (currentDate.length + 10), 45);
    doc.setFontSize(10);
    const xOffsetH2 =
      doc.internal.pageSize.width / 2 -
      (doc.getStringUnitWidth(this.props.selectedPbisSchool.label) *
        doc.internal.getFontSize()) /
        2;
    doc.text(
      this.props.selectedPbisSchool.label,
      xOffsetH2 - currentDate.length,
      60,
    );
    doc.text(
      currentDate,
      doc.internal.pageSize.width -
        doc.getStringUnitWidth(currentDate) * doc.internal.getFontSize() -
        50,
      60,
    );
    doc.text(
      `Report for: ${Utilities.formatDateSlash(
        Utilities.formatDate(dates["startDate"], "-"),
        "mm/dd/yyyy",
      )} - ${Utilities.formatDateSlash(
        Utilities.formatDate(dates["endDate"], "-"),
        "mm/dd/yyyy",
      )} `,
      xOffsetH2 - currentDate.length - 60,
      75,
    );
    doc.setFontSize(8);

    var colHeader = {};
    pbisColumns.summary.enrolledColumns.forEach((obj) => {
      colHeader[obj.selector] = obj.name;
    });

    doc.autoTable(
      pbisColumns.summary.enrolledColumns.map((obj) => {
        obj["dataKey"] = obj["selector"];
        obj["title"] = obj["name"];
        return obj;
      }),
      [colHeader, this.props.pbisDataList.summary.enrolledDataObj],
      {
        margin: autoCalculateMargin,
        headStyles: { fontSize: 7 },
        bodyStyles: { fontSize: 7 },
        head: [
          [
            {
              content: "Student Enrollments",
              colSpan: 5,
              styles: { halign: "center", top: -20, fillColor: [22, 73, 160] },
            },
          ],
        ],
        rowStyles: {
          rowHeight: 100,
        },
        styles: {
          halign: "center",
        },
        rowHeight: 10,

        startY: 90,
        didParseCell: function (data) {
          if (data.row.index === 0) {
            data.cell.styles.fontStyle = "bold";
          }
          data.cell.styles.hAlign = "center";
        },
        didDrawPage: function (data) {
          lastTableOffset = data.cursor.y;
        },
      },
    );

    doc.autoTable(
      pbisColumns.summary.enrolledColumns.map((obj) => {
        obj["dataKey"] = obj["selector"];
        obj["title"] = obj["name"];
        return obj;
      }),
      [colHeader, this.props.pbisDataList.summary.activeDataObj],
      {
        headStyles: { fontSize: 7 },
        bodyStyles: { fontSize: 7 },
        rowStyles: {
          rowHeight: 100,
        },
        styles: {
          halign: "center",
        },
        head: [
          [
            {
              content: "Active Enrollments",
              colSpan: 5,
              styles: { halign: "center", fillColor: [22, 73, 160] },
            },
          ],
        ],

        rowHeight: 10,

        startY: lastTableOffset + 10,
        didParseCell: function (data) {
          if (data.row.index === 0) {
            data.cell.styles.fontStyle = "bold";
          }
        },
        didDrawPage: function (data) {
          lastTableOffset = data.cursor.y;
        },
      },
    );
    colHeader = {};
    pbisColumns.summary.raceColumns.forEach((obj) => {
      colHeader[obj.selector] = obj.name;
    });

    doc.autoTable(
      pbisColumns.summary.raceColumns.map((obj) => {
        obj["dataKey"] = obj["selector"];
        obj["title"] = obj["name"];
        return obj;
      }),
      [colHeader, this.props.pbisDataList.summary.raceDataObj],
      {
        headStyles: { fontSize: 7 },
        bodyStyles: { fontSize: 7 },
        rowStyles: {
          rowHeight: 100,
        },
        styles: {
          halign: "center",
        },
        head: [
          [
            {
              content: "Student Enrollment by Race/Ethnicity",
              colSpan: 7,
              styles: { halign: "center", fillColor: [22, 73, 160] },
            },
          ],
        ],

        rowHeight: 10,

        startY: lastTableOffset + 10,
        didParseCell: function (data) {
          if (data.row.index === 0) {
            data.cell.styles.fontStyle = "bold";
          }
        },
        didDrawPage: function (data) {
          lastTableOffset = data.cursor.y;
        },
      },
    );
    colHeader = {};
    pbisColumns.summary.attendanceColumns.forEach((obj) => {
      colHeader[obj.selector] = obj.name;
    });

    doc.autoTable(
      pbisColumns.summary.attendanceColumns.map((obj) => {
        obj["dataKey"] = obj["selector"];
        obj["title"] = obj["name"];
        return obj;
      }),
      [colHeader, ...this.props.pbisDataList.summary.attendanceData],
      {
        headStyles: { fontSize: 7 },
        bodyStyles: { fontSize: 7 },
        rowStyles: {
          rowHeight: 100,
        },
        styles: {
          halign: "center",
        },
        head: [
          [
            {
              content: "Attendance Data",
              colSpan: 5,
              styles: { halign: "center", fillColor: [22, 73, 160] },
            },
          ],
        ],

        rowHeight: 10,

        startY: lastTableOffset + 10,
        didParseCell: function (data) {
          if (data.row.index === 0) {
            data.cell.styles.fontStyle = "bold";
          }
        },
        didDrawPage: function (data) {
          lastTableOffset = data.cursor.y;
        },
      },
    );
    colHeader = {};
    pbisColumns.summary.suspRateColumns.forEach((obj) => {
      colHeader[obj.selector] = obj.name;
    });

    doc.autoTable(
      pbisColumns.summary.suspRateColumns.map((obj) => {
        obj["dataKey"] = obj["selector"];
        obj["title"] = obj["name"];
        return obj;
      }),
      this.props.pbisDataList.summary.Total
        ? [
            colHeader,
            {
              Z:
                this.props.pbisDataList.summary.TotalSusp +
                " (" +
                (
                  (this.props.pbisDataList.summary.TotalSusp * 100) /
                  Number(this.props.pbisDataList.summary.Total.students)
                ).toFixed(1) +
                "%)",
              ...this.props.pbisDataList.summary.suspDataObj,
            },
          ]
        : [],
      {
        headStyles: { fontSize: 7 },
        bodyStyles: { fontSize: 7 },
        rowStyles: {
          rowHeight: 100,
        },
        styles: {
          halign: "center",
        },
        head: [
          [
            {
              content: "Suspension Rate by Race/Ethnicity",
              colSpan: 8,
              styles: { halign: "center", fillColor: [22, 73, 160] },
            },
          ],
        ],

        rowHeight: 10,

        startY: lastTableOffset + 10,
        didParseCell: function (data) {
          if (data.row.index === 0) {
            data.cell.styles.fontStyle = "bold";
          }
        },
        didDrawPage: function (data) {
          lastTableOffset = data.cursor.y;
        },
      },
    );

    doc.autoTable(
      pbisColumns.summary.discipColumns.map((obj) => {
        obj["dataKey"] = obj["selector"];
        obj["title"] = obj["name"];
        return obj;
      }),
      [...this.props.pbisDataList.summary.discipData],
      {
        margin: { right: 305 },
        headStyles: { fontSize: 7 },
        bodyStyles: { fontSize: 7 },
        head: [
          [
            {
              content: "School Discipline Data ",
              colSpan: 2,
              styles: { halign: "center", fillColor: [22, 73, 160] },
            },
          ],
        ],
        rowStyles: {
          rowHeight: 100,
        },

        rowHeight: 10,

        startY: lastTableOffset + 10,
        didParseCell: function (data) {
          if (data.row.index === 0) {
            data.cell.styles.fontStyle = "bold";
          }
        },
      },
    );

    doc.autoTable(
      pbisColumns.summary.discipColumns.map((obj) => {
        obj["dataKey"] = obj["selector"];
        obj["title"] = obj["name"];
        return obj;
      }),
      [...this.props.pbisDataList.summary.issOssData],
      {
        margin: { left: 305 },
        headStyles: { fontSize: 7 },
        bodyStyles: { fontSize: 7 },
        head: [
          [
            {
              content: "Total ISS/OSS Days Data",
              colSpan: 5,
              styles: { halign: "center", fillColor: [22, 73, 160] },
              top: -20,
            },
          ],
        ],
        rowStyles: {
          rowHeight: 100,
        },

        rowHeight: 10,

        startY: lastTableOffset + 10,
        didParseCell: function (data) {
          if (data.row.index === 0) {
            data.cell.styles.fontStyle = "bold";
          }
        },
        didDrawPage: function (data) {
          lastTableOffset = data.cursor.y;
        },
      },
    );
    doc.addPage();
    doc.setFontSize(14);
    doc.text(mainHeader, xOffsetH1 - (currentDate.length + 10), 30);
    doc.setFontSize(12);
    doc.text(subHeader, subHeaderOffset - (currentDate.length + 10), 45);
    doc.setFontSize(10);
    doc.text(
      this.props.selectedPbisSchool.label,
      xOffsetH2 - currentDate.length,
      60,
    );
    doc.text(
      currentDate,
      doc.internal.pageSize.width -
        doc.getStringUnitWidth(currentDate) * doc.internal.getFontSize() -
        50,
      60,
    );
    doc.setFontSize(8);
    colHeader = {};
    pbisColumns.avg.forEach((obj) => {
      colHeader[obj.selector] = obj.name;
    });

    doc.autoTable(
      pbisColumns.avg.map((obj) => {
        obj["dataKey"] = obj["selector"];
        obj["title"] = obj["name"];
        return obj;
      }),
      [colHeader, ...this.props.pbisDataList.avg],
      {
        margin: autoCalculateMargin,
        headStyles: { fontSize: 7 },
        bodyStyles: { fontSize: 7 },
        rowStyles: {
          rowHeight: 100,
        },
        head: [
          [
            {
              content: "Average Referrals",
              colSpan: 4,
              styles: { halign: "center", fillColor: [22, 73, 160] },
            },
          ],
        ],

        rowHeight: 10,

        startY: 90,
        didParseCell: function (data) {
          if (data.row.index === 0) {
            data.cell.styles.fontStyle = "bold";
          }
        },
        didDrawPage: function (data) {},
      },
    );
    doc.addPage();
    doc.setFontSize(14);
    doc.text(mainHeader, xOffsetH1 - (currentDate.length + 10), 30);
    doc.setFontSize(12);
    doc.text(subHeader, subHeaderOffset - (currentDate.length + 10), 45);
    doc.setFontSize(10);
    doc.text(
      this.props.selectedPbisSchool.label,
      xOffsetH2 - currentDate.length,
      60,
    );
    doc.text(
      currentDate,
      doc.internal.pageSize.width -
        doc.getStringUnitWidth(currentDate) * doc.internal.getFontSize() -
        50,
      60,
    );
    doc.setFontSize(8);
    colHeader = {};
    pbisColumns.time.forEach((obj) => {
      colHeader[obj.selector] = obj.name;
    });

    doc.autoTable(
      pbisColumns.time.map((obj) => {
        obj["dataKey"] = obj["selector"];
        obj["title"] = obj["name"];
        return obj;
      }),
      [colHeader, ...this.props.pbisDataList.time],
      {
        margin: autoCalculateMargin,
        headStyles: { fontSize: 7 },
        bodyStyles: { fontSize: 7 },
        rowStyles: {
          rowHeight: 100,
        },

        rowHeight: 10,

        startY: 90,
        head: [
          [
            {
              content: "Referrals by Time",
              colSpan: 3,
              styles: { halign: "center", fillColor: [22, 73, 160] },
            },
          ],
        ],
        didParseCell: function (data) {
          if (data.row.index === 0) {
            data.cell.styles.fontStyle = "bold";
          }
        },
        didDrawPage: function (data) {},
      },
    );
    doc.addPage();
    doc.setFontSize(14);
    doc.text(mainHeader, xOffsetH1 - (currentDate.length + 10), 30);
    doc.setFontSize(12);
    doc.text(subHeader, subHeaderOffset - (currentDate.length + 10), 45);
    doc.setFontSize(10);
    doc.text(
      this.props.selectedPbisSchool.label,
      xOffsetH2 - currentDate.length,
      60,
    );
    doc.text(
      currentDate,
      doc.internal.pageSize.width -
        doc.getStringUnitWidth(currentDate) * doc.internal.getFontSize() -
        50,
      60,
    );
    doc.setFontSize(8);
    colHeader = {};
    pbisColumns.weekday.forEach((obj) => {
      colHeader[obj.selector] = obj.name;
    });

    doc.autoTable(
      pbisColumns.weekday.map((obj) => {
        obj["dataKey"] = obj["selector"];
        obj["title"] = obj["name"];
        return obj;
      }),
      [colHeader, ...this.props.pbisDataList.weekday],
      {
        headStyles: { fontSize: 7 },
        bodyStyles: { fontSize: 7 },
        head: [
          [
            {
              content: "Referrals by Weekday",
              colSpan: 3,
              styles: { halign: "center", fillColor: [22, 73, 160] },
            },
          ],
        ],

        rowHeight: 10,

        startY: 90,
        didParseCell: function (data) {
          if (data.row.index === 0) {
            data.cell.styles.fontStyle = "bold";
          }
        },
        didDrawPage: function (data) {},
      },
    );
    doc.addPage();
    doc.setFontSize(14);
    doc.text(mainHeader, xOffsetH1 - (currentDate.length + 10), 30);
    doc.setFontSize(12);
    doc.text(subHeader, subHeaderOffset - (currentDate.length + 10), 45);
    doc.setFontSize(10);
    doc.text(
      this.props.selectedPbisSchool.label,
      xOffsetH2 - currentDate.length,
      60,
    );
    doc.text(
      currentDate,
      doc.internal.pageSize.width -
        doc.getStringUnitWidth(currentDate) * doc.internal.getFontSize() -
        50,
      60,
    );
    doc.setFontSize(8);
    colHeader = {};
    pbisColumns.type.forEach((obj) => {
      colHeader[obj.selector] = obj.name;
    });

    doc.autoTable(
      pbisColumns.type.map((obj) => {
        obj["dataKey"] = obj["selector"];
        obj["title"] = obj["name"];
        return obj;
      }),
      [colHeader, ...this.props.pbisDataList.type],
      {
        headStyles: { fontSize: 7 },
        bodyStyles: { fontSize: 7 },
        head: [
          [
            {
              content: "Referrals by Problem Behavior",
              colSpan: 3,
              styles: { halign: "center", fillColor: [22, 73, 160] },
            },
          ],
        ],
        rowHeight: 10,
        startY: 90,
        didParseCell: function (data) {
          if (data.row.index === 0) {
            data.cell.styles.fontStyle = "bold";
          }
        },
        didDrawPage: function (data) {},
      },
    );
    doc.addPage();
    doc.setFontSize(14);
    doc.text(mainHeader, xOffsetH1 - (currentDate.length + 10), 30);
    doc.setFontSize(12);
    doc.text(subHeader, subHeaderOffset - (currentDate.length + 10), 45);
    doc.setFontSize(10);
    doc.text(
      this.props.selectedPbisSchool.label,
      xOffsetH2 - currentDate.length,
      60,
    );
    doc.text(
      currentDate,
      doc.internal.pageSize.width -
        doc.getStringUnitWidth(currentDate) * doc.internal.getFontSize() -
        50,
      60,
    );
    doc.setFontSize(8);
    colHeader = {};
    pbisColumns.location.forEach((obj) => {
      colHeader[obj.selector] = obj.name;
    });

    doc.autoTable(
      pbisColumns.location.map((obj) => {
        obj["dataKey"] = obj["selector"];
        obj["title"] = obj["name"];
        return obj;
      }),
      [colHeader, ...this.props.pbisDataList.location],
      {
        margin: autoCalculateMargin,
        headStyles: { fontSize: 7 },
        bodyStyles: { fontSize: 7 },
        rowStyles: {
          rowHeight: 100,
        },
        head: [
          [
            {
              content: "Referrals by Location",
              colSpan: 3,
              styles: { halign: "center", fillColor: [22, 73, 160] },
            },
          ],
        ],

        rowHeight: 10,

        startY: 90,
        didParseCell: function (data) {
          if (data.row.index === 0) {
            data.cell.styles.fontStyle = "bold";
          }
        },
        didDrawPage: function (data) {},
      },
    );
    doc.addPage();
    doc.setFontSize(14);
    doc.text(mainHeader, xOffsetH1 - (currentDate.length + 10), 30);
    doc.setFontSize(12);
    doc.text(subHeader, subHeaderOffset - (currentDate.length + 10), 45);
    doc.setFontSize(10);
    doc.text(
      this.props.selectedPbisSchool.label,
      xOffsetH2 - currentDate.length,
      60,
    );
    doc.text(
      currentDate,
      doc.internal.pageSize.width -
        doc.getStringUnitWidth(currentDate) * doc.internal.getFontSize() -
        50,
      60,
    );
    doc.setFontSize(8);
    colHeader = {};
    pbisColumns.grade.forEach((obj) => {
      colHeader[obj.selector] = obj.name;
    });

    doc.autoTable(
      pbisColumns.grade.map((obj) => {
        obj["dataKey"] = obj["selector"];
        obj["title"] = obj["name"];
        return obj;
      }),
      [colHeader, ...this.props.pbisDataList.grade],
      {
        margin: autoCalculateMargin,
        headStyles: { fontSize: 7 },
        bodyStyles: { fontSize: 7 },
        rowStyles: {
          rowHeight: 100,
        },
        head: [
          [
            {
              content: "Referrals by Grade",
              colSpan: 3,
              styles: { halign: "center", fillColor: [22, 73, 160] },
            },
          ],
        ],

        rowHeight: 10,

        startY: 90,
        didParseCell: function (data) {
          if (data.row.index === 0) {
            data.cell.styles.fontStyle = "bold";
          }
        },
        didDrawPage: function (data) {},
      },
    );
    doc.addPage();
    doc.setFontSize(14);
    doc.text(mainHeader, xOffsetH1 - (currentDate.length + 10), 30);
    doc.setFontSize(12);
    doc.text(subHeader, subHeaderOffset - (currentDate.length + 10), 45);
    doc.setFontSize(10);
    doc.text(
      this.props.selectedPbisSchool.label,
      xOffsetH2 - currentDate.length,
      60,
    );
    doc.text(
      currentDate,
      doc.internal.pageSize.width -
        doc.getStringUnitWidth(currentDate) * doc.internal.getFontSize() -
        50,
      60,
    );
    doc.setFontSize(8);
    colHeader = {};
    pbisColumns.reporter.forEach((obj) => {
      colHeader[obj.selector] = obj.name;
    });

    doc.autoTable(
      pbisColumns.reporter.map((obj) => {
        obj["dataKey"] = obj["selector"];
        obj["title"] = obj["name"];
        return obj;
      }),
      [colHeader, ...this.props.pbisDataList.reporter],
      {
        margin: autoCalculateMargin,
        headStyles: { fontSize: 7 },
        bodyStyles: { fontSize: 7 },
        rowStyles: {
          rowHeight: 100,
        },
        head: [
          [
            {
              content: "Referrals by Reporter",
              colSpan: 3,
              styles: { halign: "center", fillColor: [22, 73, 160] },
            },
          ],
        ],

        rowHeight: 10,

        startY: 90,
        didParseCell: function (data) {
          if (data.row.index === 0) {
            data.cell.styles.fontStyle = "bold";
          }
        },
        didDrawPage: function (data) {},
      },
    );

    doc.save("pbis.pdf");
  };

  render() {
    const { value } = this.state;
    const { routes, pbisSchoolList, selectedPbisSchool, selectedPbisDate } =
      this.props;
    return (
      <div className="page-under-construction pbis">
        <Paper style={{ padding: 2 }}>
          <AppBar style={{ position: "static", height: "32px" }}>
            <Toolbar
              style={{ minHeight: 30, float: "right", position: "relative" }}
            >
              <Grid item container sm={12} className="progress_grade_toolbar">
                <Grid item sm={3}>
                  <Typography>School</Typography>
                  <Select
                    styles={selectCustomStyles}
                    options={pbisSchoolList}
                    className="pbis-dropdown"
                    placeholder={"Select School"}
                    value={selectedPbisSchool}
                    onChange={(val, evt) => this.viewReport(val, evt, "School")}
                  />
                </Grid>
                <Grid item sm={3}>
                  <Typography>Start Date :</Typography>
                  <DatePicker
                    selected={selectedPbisDate.startDate}
                    value={selectedPbisDate.startDate}
                    onChange={(date) =>
                      this.dateChange(date, "startDate", "Date")
                    }
                  />
                </Grid>
                <Grid item sm={3}>
                  <Typography>End Date :</Typography>
                  <DatePicker
                    selected={selectedPbisDate.endDate || new Date()}
                    onChange={(date) =>
                      this.dateChange(date, "endDate", "Date")
                    }
                    defaultValue={selectedPbisDate.endDate}
                  />
                </Grid>
                <Grid item sm={2}>
                  <Button
                    className="pbis-view-button"
                    disabled={
                      selectedPbisDate.startDate &&
                      this.props.selectedPbisSchool &&
                      this.props.selectedPbisSchool.school
                        ? false
                        : true
                    }
                    onClick={(evt) => {
                      this.viewReport();
                    }}
                  >
                    View Report
                  </Button>
                </Grid>

                <Grid item sm={1}>
                  {Object.keys(this.props.pbisDataList).length > 0 && (
                    <Tooltip
                      title="Export PDF"
                      aria-label="pdf-export-tooltip"
                      style={{ float: "right" }}
                    >
                      <IconButton
                        className="csv-download-btn"
                        aria-label="csv-export"
                        style={{
                          color: "#ffffff",
                          padding: 5,
                          position: "absolute",
                          right: "5px",
                          top: "-2px",
                          cursor: "pointer",
                        }}
                        onClick={(evt) => this.pdfDownload(evt)}
                      >
                        <MaterialIcon icon="cloud_download" />
                      </IconButton>
                    </Tooltip>
                  )}
                </Grid>
              </Grid>
            </Toolbar>
          </AppBar>
        </Paper>
        <Router basename="/abc">
          <Paper className="secondary-tab-paper" square>
            <Tabs
              value={value}
              indicatorColor="primary"
              textColor="primary"
              onChange={this.handleCallToRouter.bind(value)}
              centered
            >
              {PBISTabs.map((obj) => (
                <Tab
                  id={obj.label}
                  key={obj.label}
                  label={obj.label}
                  className="minWidth90"
                  value={obj.value}
                  to={obj.value}
                  style={{ padding: "0px 20px" }}
                  component={Link}
                />
              ))}
            </Tabs>
          </Paper>
          <Switch>
            {routes.map((route, i) => (
              <RouteWithSubRoutes key={i} {...route} />
            ))}
          </Switch>
        </Router>
      </div>
    );
  }
}

const mapStateToProps = () =>
  createStructuredSelector({
    pbisSchoolList: loginSelectors.getSchoolList(),
    pbisDateList: selectors.getPbisDateList(),
    pbisDataList: selectors.getPbisDataList(),
    start_year: loginSelectors.getLoginData(),
    selectedPbisSchool: selectors.getSelectedPbisSchool(),
    selectedPbisDate: selectors.getSelectedPbisDate(),
    userData: loginSelectors.getLoginData(),
  });
const mapDispatchToProps = (dispatch) => ({
  loadPbisSchoolList: (req) => dispatch(actions.loadPbisSchoolList(req)),
  loadPbisDateList: (req) => dispatch(actions.loadPbisDateList(req)),
  loadPbisDataList: (req) => dispatch(actions.loadPbisDataList(req)),
  setSelectedPbisSchool: (selectedPbisSchool) =>
    dispatch(actions.setSelectedPbisSchool(selectedPbisSchool)),
  setSelectedPbisDate: (selectedPbisDate) =>
    dispatch(actions.setSelectedPbisDate(selectedPbisDate)),
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(PBIS));
