import React from "react";
import { Paper, AppBar, Toolbar } from "@material-ui/core";
import DataTable from "react-data-table-component";
import {
  AbsencesColumns,
  customStyles,
} from "../../../../utilities/tableConfig";

export default function AbsenceTable(props) {
  return (
    <Paper className="mr-lt-2">
      <AppBar style={{ position: "static", height: "32px" }}>
        <Toolbar
          style={{ minHeight: 30, float: "right", position: "relative" }}
        >
          Absences for: {props?.rowData?.studentName}
        </Toolbar>
      </AppBar>
      <DataTable
        noHeader={true}
        columns={[...AbsencesColumns]}
        highlightOnHover={true}
        fixedHeader={true}
        fixedHeaderScrollHeight={"77vh"}
        data={props.data.length > 0 ? [...props.data] : []}
        customStyles={customStyles}
      />
    </Paper>
  );
}
