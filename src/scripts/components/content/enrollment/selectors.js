import { createSelector } from "reselect";
import * as selectors from "../../../redux/selectors";

export const getEntSchoolList = () =>
  createSelector(selectors.enrollmentState, (enrollmentState) =>
    enrollmentState.get("schoolList").toJS(),
  );

export const getEntSelectedSchool = () =>
  createSelector(selectors.enrollmentState, (enrollmentState) =>
    enrollmentState.get("selectedEntSchool").toJS(),
  );

export const getEntSelectedTerm = () =>
  createSelector(selectors.enrollmentState, (enrollmentState) =>
    enrollmentState.get("selectedTerm").toJS(),
  );

export const getPlotData = () =>
  createSelector(selectors.enrollmentState, (enrollmentState) =>
    enrollmentState.get("plotData").toJS(),
  );

export const getEntAttendanceTerms = () =>
  createSelector(selectors.enrollmentState, (enrollmentState) =>
    enrollmentState.get("attendanceTermsList").toJS(),
  );

export const getEntFilteredEnrollments = () =>
  createSelector(selectors.enrollmentState, (enrollmentState) =>
    enrollmentState.get("filteredEnrollmentsList").toJS(),
  );

export const getEntEnrollments = () =>
  createSelector(selectors.enrollmentState, (enrollmentState) =>
    enrollmentState.get("enrollmentsList").toJS(),
  );

export const getEntAbsences = () =>
  createSelector(selectors.enrollmentState, (enrollmentState) =>
    enrollmentState.get("absencesList").toJS(),
  );

export const getSearchText = () =>
  createSelector(selectors.enrollmentState, (enrollmentState) =>
    enrollmentState.get("searchText"),
  );
