import { fromJS } from "immutable";
import {
  SET_ENT_ABSENCES,
  SET_ENT_ATTENDANCE_TERMS,
  SET_ENT_ENROLLMENTS,
  SET_ENT_FILTERED_ENROLLMENTS,
  SET_ENT_SCHOOL_LIST,
  SET_ENT_SELECTED_SCHOOL,
  SET_ENT_SELECTED_TERM,
  SET_PLOT_DATA,
  SET_SEARCH_TEXT,
} from "./actions";

const initialState = fromJS({
  schoolList: [],
  selectedEntSchool: { label: "Select a School ...", value: -1 },
  selectedTerm: {},
  plotData: [],
  attendanceTermsList: [],
  filteredEnrollmentsList: [],
  enrollmentsList: [],
  absencesList: [],
  searchText: null,
});

const enrollment = (state = initialState, action) => {
  switch (action.type) {
    case SET_ENT_ATTENDANCE_TERMS:
      return state.set(
        "attendanceTermsList",
        fromJS([...action.attendanceTermsList]),
      );
    case SET_ENT_ENROLLMENTS:
      return state.set("enrollmentsList", fromJS([...action.enrollmentsList]));
    case SET_ENT_FILTERED_ENROLLMENTS:
      return state.set(
        "filteredEnrollmentsList",
        fromJS([...action.filteredEnrollmentsList]),
      );
    case SET_ENT_ABSENCES:
      return state.set("absencesList", fromJS([...action.absencesList]));
    case SET_ENT_SCHOOL_LIST:
      return state.set("schoolList", fromJS([...action.schoolList]));
    case SET_ENT_SELECTED_SCHOOL:
      return state.set(
        "selectedEntSchool",
        fromJS({ ...action.selectedEntSchool }),
      );
    case SET_ENT_SELECTED_TERM:
      return state.set("selectedTerm", fromJS({ ...action.selectedTerm }));
    case SET_PLOT_DATA:
      return state.set("plotData", fromJS([...action.plotData]));
    case SET_SEARCH_TEXT:
      return state.set("searchText", action.text);
    default:
      return state;
  }
};

export default enrollment;
