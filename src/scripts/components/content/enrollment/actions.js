export const LOAD_ENT_SCHOOL_LIST = "LOAD_ENT_SCHOOL_LIST";
export const loadEntSchoolList = (school) => ({
  type: LOAD_ENT_SCHOOL_LIST,
  school,
});

export const SET_ENT_SCHOOL_LIST = "SET_ENT_SCHOOL_LIST";
export const setEntSchoolList = (schoolList) => ({
  type: SET_ENT_SCHOOL_LIST,
  schoolList,
});

export const SET_ENT_SELECTED_SCHOOL = "SET_ENT_SELECTED_SCHOOL";
export const setEntSelectedSchool = (selectedEntSchool) => ({
  type: SET_ENT_SELECTED_SCHOOL,
  selectedEntSchool,
});

export const SET_ENT_SELECTED_TERM = "SET_ENT_SELECTED_TERM";
export const setEntSelectedTerm = (selectedTerm) => ({
  type: SET_ENT_SELECTED_TERM,
  selectedTerm,
});

export const SET_PLOT_DATA = "SET_PLOT_DATA";
export const setPlotData = (plotData) => ({
  type: SET_PLOT_DATA,
  plotData,
});

export const SET_ENT_ATTENDANCE_TERMS = "SET_ENT_ATTENDANCE_TERMS";
export const setEntAttendanceTerms = (attendanceTermsList) => ({
  type: SET_ENT_ATTENDANCE_TERMS,
  attendanceTermsList,
});

export const LOAD_ENT_ATTENDANCE_TERMS = "LOAD_ENT_ATTENDANCE_TERMS";
export const loadEntAttendanceTerms = (request) => ({
  type: LOAD_ENT_ATTENDANCE_TERMS,
  request,
});

export const SET_ENT_FILTERED_ENROLLMENTS = "SET_ENT_FILTERED_ENROLLMENTS";
export const setEntFilteredEnrollments = (filteredEnrollmentsList) => ({
  type: SET_ENT_FILTERED_ENROLLMENTS,
  filteredEnrollmentsList,
});

export const SET_ENT_ENROLLMENTS = "SET_ENT_ENROLLMENTS";
export const setEntEnrollments = (enrollmentsList) => ({
  type: SET_ENT_ENROLLMENTS,
  enrollmentsList,
});

export const LOAD_ENT_ENROLLMENTS = "LOAD_ENT_ENROLLMENTS";
export const loadEntEnrollments = (request) => ({
  type: LOAD_ENT_ENROLLMENTS,
  request,
});

export const SET_ENT_ABSENCES = "SET_ENT_ABSENCES";
export const setEntAbsences = (absencesList) => ({
  type: SET_ENT_ABSENCES,
  absencesList,
});

export const LOAD_ENT_ABSENCES = "LOAD_ENT_ABSENCES";
export const loadEntAbsences = (request) => ({
  type: LOAD_ENT_ABSENCES,
  request,
});

export const SET_SEARCH_TEXT = "SET_SEARCH_TEXT";
export const setSearchText = (text) => {
  return { type: SET_SEARCH_TEXT, text };
};
