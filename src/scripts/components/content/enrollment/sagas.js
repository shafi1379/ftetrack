import { all, call, fork, put, select, takeLatest } from "redux-saga/effects";
import {
  LOAD_ENT_ABSENCES,
  LOAD_ENT_ATTENDANCE_TERMS,
  LOAD_ENT_ENROLLMENTS,
  LOAD_ENT_SCHOOL_LIST,
  setEntAbsences,
  setEntAttendanceTerms,
  setEntEnrollments,
  setEntFilteredEnrollments,
  setEntSchoolList,
  setEntSelectedSchool,
  setEntSelectedTerm,
  setPlotData,
} from "./actions";
import { setProgressingValue } from "../../../app/actions";
import * as Api from "../../../services/api";
import { API_URLS } from "../../../utilities/constants";

function* loadSchoolListRequested(params) {
  try {
    const { schoolList } = (yield select((s) => s.get("enrollment"))).toJS();
    if (!schoolList.length) {
      yield put(setProgressingValue(true));
      const response = yield call(
        Api.doPostRequest,
        API_URLS.schools,
        params.school,
      );
      if (response) {
        const schoolList = response.map((res) => ({
          ...res,
          label: res.schoolname,
          value: res.school,
        }));
        yield put(setEntSchoolList(schoolList));
        yield put(setEntSelectedSchool(schoolList[0]));
        yield put(setProgressingValue(false));
      }
    }
  } catch (error) {
    console.error(error);
  }
}

function* schoolListListener() {
  yield takeLatest(LOAD_ENT_SCHOOL_LIST, loadSchoolListRequested);
}

function* loadAttendanceTerms({ request }) {
  try {
    const { attendanceTermsList } = (yield select((s) =>
      s.get("enrollment"),
    )).toJS();
    if (!attendanceTermsList.length) {
      yield put(setProgressingValue(true));
      const response = yield call(
        Api.doPostRequest,
        API_URLS.attendanceTerms,
        request,
      );
      if (response) {
        let terms = response.map((obj) => {
          obj["label"] = obj.name;
          obj["value"] = obj.name;
          return obj;
        });
        yield put(setEntAttendanceTerms(terms));
        yield put(setEntSelectedTerm(terms[0]));
        yield put(setProgressingValue(false));
      }
    }
  } catch (error) {
    console.error(error);
  }
}

function* attendanceTermsListener() {
  yield takeLatest(LOAD_ENT_ATTENDANCE_TERMS, loadAttendanceTerms);
}

function* loadEnrollments({ request }) {
  try {
    yield put(setProgressingValue(true));
    const response = yield call(
      Api.doPostRequest,
      API_URLS.enrollments,
      request,
    );
    if (response) {
      yield put(setEntFilteredEnrollments(response && response.enrollment));
      yield put(setEntEnrollments(response && response.enrollment));
      yield put(setPlotData([response.distribution[0]]));
      yield put(setProgressingValue(false));
    }
  } catch (error) {
    console.error(error);
  }
}

function* enrollmentsListener() {
  yield takeLatest(LOAD_ENT_ENROLLMENTS, loadEnrollments);
}

function* loadAbsences({ request }) {
  try {
    yield put(setProgressingValue(true));
    const response = yield call(Api.doPostRequest, API_URLS.absences, request);
    if (response) {
      yield put(setEntAbsences(response));
      yield put(setProgressingValue(false));
    }
  } catch (error) {
    console.error(error);
  }
}

function* absencesListener() {
  yield takeLatest(LOAD_ENT_ABSENCES, loadAbsences);
}

export default function* root() {
  yield all([
    fork(attendanceTermsListener),
    fork(enrollmentsListener),
    fork(schoolListListener),
    fork(absencesListener),
  ]);
}
