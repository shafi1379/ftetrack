import React, { Component } from "react";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";
import * as loginSelectors from "../../../app/selectors";
import * as loginActions from "../../../app/actions";
import * as selectors from "./selectors";
import * as actions from "./actions";
import DataTable from "react-data-table-component";
import * as Utilities from "../../../utilities/utilities";
import TextField from "@material-ui/core/TextField";
import {
  Paper,
  Toolbar,
  Grid,
  AppBar,
  IconButton,
  Tooltip,
} from "@material-ui/core";
import Select from "react-select";
import MaterialIcon from "../../generic/materialIcon";
import { selectCustomStyles } from "../../generic/reactSelectCustomization";
import * as TableConfig from "../../../utilities/tableConfig";
import { EnrollmentColumns } from "../../../utilities/tableHeaderNamesList";
import SplitPane from "react-split-pane";
import Absences from "./containers/Absences.jsx";
import PieChart from "../../generic/pieChartDialog.jsx";
import { CSVLink } from "react-csv";

class Enrollment extends Component {
  csvLink = React.createRef();
  firstSplitPanePanel = React.createRef();
  state = {
    csvExport: {
      data: [],
      fileName: "grades.csv",
    },
    openPieChartModal: false,
    columns: TableConfig.enrollmentColumns.map((dash, index) => {
      if (dash.selector === "studentNumber") {
        dash.cell = (row) => (
          <span
            style={{
              color: "#683598",
              textDecoration: "underline",
              cursor: "pointer",
            }}
            onClick={this.rowClick.bind(this, row)}
          >
            {row.studentNumber}
          </span>
        );
      }
      return dash;
    }),
    rowData: {}
  };

  componentWillMount() {
    this.props.handleCallToRouter(null, "enrollment");
  }

  componentDidMount() {
    /* this.props.loadSchoolList({
      school: this.props.userData.loginData.school,
    }); */
    this.props.setSearchText(null);
    this.clearSearchField();
    this.props.loadAttendanceTerms({
      school: this.props.userData.school,
    });
  }
  onSplitPaneDragFinished = (size) => {
    this.setState({
      plotlyChartWidth: this.firstSplitPanePanel.current.offsetWidth,
    });
  };

  handleDropDownChange = (val, evt, type) => {
    const { selectedEntSchool, selectedTerm } = this.props;
    if (type === "School") {
      this.props.setEntSelectedSchool(val);
      if (selectedTerm) {
        this.props.loadEnrollments({
          school: "" + val.school,
          startDate: selectedTerm.startDate,
          endDate: selectedTerm.endDate,
        });
      }
    } else {
      this.props.setEntSelectedTerm(val);
      this.props.loadEnrollments({
        school: "" + selectedEntSchool.school,
        startDate: val.startDate,
        endDate: val.endDate,
      });
    }
  };

  downloadCSV = (evt) => {
    const currentTime = `_${new Date().getTime()}`;
    const columns = EnrollmentColumns;
    let datas = [...this.props.enrollmentsList];
    const data = Utilities.generateExcelData(datas || [], columns || {});
    const school = this.props.selectedEntSchool;
    data.unshift([`${school && school.label}`]);
    this.setState({
      csvExport: {
        data,
        fileName: `School_Enrollments${currentTime}.csv`,
      },
    });
    setTimeout(() => {
      this.csvLink.current.link.click();
    });
  };

  rowClick = (rowData) => {
    this.setState({rowData: rowData});
    this.props.setProgressingValue(true);
    this.props.loadAbsences({
      personID: rowData.personID + "",
    });
  };

  filterSearchField = (e) => {
    let value = e.target.value;
    if (!value.charAt(0) === "-") {
      value = "-Name:" + value;
    }
    if (value.length && this.props.enrollmentsList.length) {
      this.props.setSearchText(value);
      const objKeys = Object.keys(this.props.enrollmentsList[0]);
      const actualKeys = TableConfig.enrollmentColumns;
      this.props.setEntFilteredEnrollments(
        this.props.enrollmentsList.filter((item) => {
          return Utilities.generateFilteredData(
            item,
            value ? value : this.props.searchText,
            objKeys,
            actualKeys,
          );
        }),
      );
    } else {
      this.clearSearchField();
    }
  };

  clearSearchField = () => {
    this.props.setSearchText(null);
    this.props.setEntFilteredEnrollments(this.props.enrollmentsList);
  };

  render() {
    const { csvExport, openPieChartModal, columns, rowData } = this.state;
    const { selectedEntSchool, selectedTerm, plotData, searchText } =
      this.props;
    return (
      <div
        id={"enrollment-split-panel"}
        key={"enrollment-split-panel"}
        className={"split-panel pad2"}
      >
        <SplitPane split="vertical" allowResize={true}>
          <div initialsize="65%" ref={this.firstSplitPanePanel}>
            <Paper className="mr-rt-2">
              <AppBar style={{ position: "static", height: "32px" }}>
                <Toolbar
                  style={{
                    minHeight: 30,
                    float: "right",
                    position: "relative",
                  }}
                >
                  <Grid container>
                    <div>
                      <span
                        style={{
                          float: "right",
                          marginRight: "20px",
                          paddingTop: "7px",
                          fontSize: "12px",
                        }}
                      >
                        School&nbsp;:
                      </span>
                    </div>
                    <Grid item sm={3}>
                      <Select
                        styles={selectCustomStyles}
                        options={this.props.schoolList}
                        className="enrollment-dropdown"
                        placeholder={"Select School"}
                        value={selectedEntSchool}
                        onChange={(val, evt) =>
                          this.handleDropDownChange(val, evt, "School")
                        }
                      />
                    </Grid>
                    <Grid item sm={3}>
                      <div>
                        <span
                          style={{
                            float: "left",
                            marginRight: "20px",
                            paddingTop: "7px",
                            fontSize: "12px",
                          }}
                        >
                          Term&nbsp;:
                        </span>
                      </div>
                      <Select
                        options={this.props.attendanceTermsList}
                        placeholder={"Select Term"}
                        className="enrollment-dropdown1"
                        styles={selectCustomStyles}
                        value={selectedTerm}
                        onChange={(val, evt) =>
                          this.handleDropDownChange(val, evt, "Terms")
                        }
                      />
                    </Grid>
                    <Grid item sm={3} style={{ marginTop: "7px" }}>
                      <div
                        onClick={(evt) =>
                          this.setState({ openPieChartModal: true })
                        }
                        className="link-div"
                      >
                        Active&nbsp;Enrollment&nbsp;By&nbsp;Race
                      </div>
                    </Grid>

                    <Grid item sm={2}>
                      <Tooltip
                        title="Export to CSV"
                        aria-label="csv-export-tooltip"
                      >
                        <IconButton
                          onClick={(evt) => this.downloadCSV(evt)}
                          className="csv-download-btn"
                          aria-label="csv-export"
                          style={{
                            color: "#ffffff",
                            padding: 5,
                            position: "absolute",
                            right: "10px",
                            top: "-2px",
                            cursor: "pointer",
                          }}
                        >
                          <MaterialIcon
                            icon="cloud_download"
                            style={{ color: "#FFFFFF" }}
                          />
                        </IconButton>
                      </Tooltip>
                    </Grid>
                  </Grid>
                </Toolbar>
              </AppBar>
              <DataTable
                noHeader={false}
                columns={columns}
                pagination={true}
                onRowClicked={this.rowClick}
                paginationPerPage={25}
                actions={
                  <TextField
                    onChange={this.filterSearchField}
                    placeholder="Search here ..."
                    value={searchText}
                    type="text"
                    style={{ width: 225 }}
                  />
                }
                paginationRowsPerPageOptions={[25, 50, 100, 200]}
                fixedHeader={true}
                fixedHeaderScrollHeight={"65vh"}
                data={
                  this.props.filteredEnrollmentsList.length > 0
                    ? [...this.props.filteredEnrollmentsList]
                    : []
                }
                customStyles={TableConfig.customStyles}
              />
            </Paper>
          </div>
          <div>
            <Absences rowData={rowData} data={this.props.absencesList} />
          </div>
        </SplitPane>
        {openPieChartModal && (
          <PieChart
            title="Active Enrollment"
            open={openPieChartModal}
            onClose={() =>
              this.setState({
                openPieChartModal: !openPieChartModal,
              })
            }
            data={plotData}
          />
        )}
        <CSVLink
          ref={this.csvLink}
          data={csvExport.data}
          className="hidden"
          filename={csvExport.fileName}
          target="_self"
        />
      </div>
    );
  }
}

const mapStateToProps = () =>
  createStructuredSelector({
    searchText: selectors.getSearchText(),
    attendanceTermsList: selectors.getEntAttendanceTerms(),
    enrollmentsList: selectors.getEntEnrollments(),
    filteredEnrollmentsList: selectors.getEntFilteredEnrollments(),
    schoolList: loginSelectors.getSchoolList(),
    plotData: selectors.getPlotData(),
    selectedEntSchool: selectors.getEntSelectedSchool(),
    selectedTerm: selectors.getEntSelectedTerm(),
    absencesList: selectors.getEntAbsences(),
    userData: loginSelectors.getLoginData(),
  });
const mapDispatchToProps = (dispatch) => ({
  loadSchoolList: (school) => dispatch(actions.loadEntSchoolList(school)),
  loadAttendanceTerms: (request) =>
    dispatch(actions.loadEntAttendanceTerms(request)),
  loadEnrollments: (request) => dispatch(actions.loadEntEnrollments(request)),
  setEntFilteredEnrollments: (request) =>
    dispatch(actions.setEntFilteredEnrollments(request)),
  loadAbsences: (request) => dispatch(actions.loadEntAbsences(request)),
  setEntSelectedSchool: (school) =>
    dispatch(actions.setEntSelectedSchool(school)),
  setEntSelectedTerm: (term) => dispatch(actions.setEntSelectedTerm(term)),
  setProgressingValue: (bool) =>
    dispatch(loginActions.setProgressingValue(bool)),
  setSearchText: (val) => dispatch(actions.setSearchText(val)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Enrollment);
