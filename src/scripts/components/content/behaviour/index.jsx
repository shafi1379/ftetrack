import React, { Component } from "react";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import Paper from "@material-ui/core/Paper";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Select from "react-select";
import Tooltip from "@material-ui/core/Tooltip";
import Typography from "@material-ui/core/Typography";
import { withRouter } from "react-router-dom";
import { BrowserRouter as Router, Switch, Link } from "react-router-dom";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";
import * as loginSelectors from "../../../app/selectors";
import * as selectors from "./selectors";
import * as actions from "./actions";
import MaterialIcon from "../../generic/materialIcon";
import RouteWithSubRoutes from "../../generic/subRoutes";
import { selectCustomStyles } from "../../generic/reactSelectCustomization";
import * as Utilities from "../../../utilities/utilities";
import DataTable from "react-data-table-component";
import * as TableConfig from "../../../utilities/tableConfig";
import { CSVLink } from "react-csv";
import SplitPane from "react-split-pane";
import PieChart from "../../generic/pieChartDialog";

class Behaviour extends Component {
  csvLink = React.createRef();
  state = {
    openPieChartModal: false,
    value: "incident_types",
    csvExport: {
      data: [],
      fileName: "behavior.csv",
    },
    selectedIncident: null,
  };

  componentWillMount() {
    /* this.props.loadSchoolList({
      school: this.props.userData.loginData.school,
    }); */
    this.props.handleCallToRouter(null, "behaviour");
    this.props.history.replace("/behaviour/incident_types");
  }

  handleDropdownChange = (option) => {
    const school = {
      school: option.value,
      sped: this.props.isSPEDOnlySelected ? "Y" : "N",
    };
    this.props.loadBehaviourData({
      apiCategory: "systemIncidents",
      request: school,
    });
    this.props.setSelectedSchool(option);
  };

  handleCallToRouter = (event, newValue) => this.setState({ value: newValue });

  downloadCSV = (type) => {
    const currentTime = `_${new Date().getTime()}`;
    if (type === "nested") {
      const school = this.props.selectedSchool;
      const columns = {
        "Schools By Incidents": { School: "name", "# of Incidents": "records" },
        "Students By Incidents": {
          "Student #": "studentNumber",
          "Student Name": "studentName",
          Race: "race",
          "Cohort Year": "cohortYear",
          "Incident Date": "incidentDate",
          "Incident Type": "incTitle",
          Resolution: "resTitle",
          "Susp Days": "duration",
          "Reported By": "submittedBy",
          Gender: "gender",
          Grade: "grade",
          SPED: "sped",
          EL: "EL",
          sec504: "sec504",
          SST: "sst",
          EIP: "eip",
          Homeless: "homeless",
        },
      };
      if (school.value === 9999) {
        const data = Utilities.generateExcelData(
          this.props.studentsForAllSchools || [],
          columns["Schools By Incidents"] || {},
        );
        data.unshift([`${school && school.label}`]);
        this.setState({
          csvExport: { data, fileName: `School_Behaviour${currentTime}.csv` },
        });
      } else {
        const data = Utilities.generateExcelData(
          this.props.studentsForSelectedSchools || [],
          columns["Students By Incidents"] || {},
        );
        data.unshift([`${school && school.label}`]);
        this.setState({
          csvExport: { data, fileName: `School_Behaviour${currentTime}.csv` },
        });
      }
    } else {
      const locationPath = window.location.href;
      const currentPage = locationPath.substring(
        locationPath.lastIndexOf("/") + 1,
      );
      if (currentPage === "incident_types") {
        const columns = {
          Code: "IncidentCode",
          "Incident Description": "IncidentDescription",
          "# of Students": "IncidentCount",
        };
        const data = Utilities.generateExcelData(
          this.props.systemIncidents || [],
          columns || {},
        );
        data.unshift([`All Schools`]);
        this.setState({
          csvExport: {
            data,
            fileName: `School_Behaviour_Incident${currentTime}.csv`,
          },
        });
      } else {
        const columns = {
          Code: "ActionCode",
          "Action Description": "ActionDescription",
          "# of Students": "ActionCount",
        };
        const data = Utilities.generateExcelData(
          this.props.systemActions || [],
          columns || {},
        );
        data.unshift([`All Schools`]);
        this.setState({
          csvExport: {
            data,
            fileName: `School_Behaviour_Actions${currentTime}.csv`,
          },
        });
      }
    }
    setTimeout(() => {
      this.csvLink.current.link.click();
    });
  };

  handleSPEDSelection = () => {
    const school = this.props.selectedSchool;
    if (school.value !== -1) {
      const request = {
        school: school.value,
        sped: !this.props.isSPEDOnlySelected ? "Y" : "N",
      };
      this.props.loadBehaviourData({
        apiCategory: "systemIncidents",
        request: request,
      });
    }
    this.props.setSPEDOnlySelected(!this.props.isSPEDOnlySelected);
  };

  render() {
    const { openPieChartModal, value, csvExport } = this.state;
    const {
      routes,
      schoolList,
      selectedSchool,
      studentsForAllSchools,
      studentsForSelectedSchools,
      incidentLabelForSchools,
      isSPEDOnlySelected,
      plotData,
    } = this.props;
    return (
      <div className="behaviour">
        <SplitPane split="vertical">
          <div initialSize="40%">
            <Paper className="mr-rt-2" square>
              <Grid
                className="sub-toolbar toolbar-header-style behavior-toolbar-style"
                item
                md={12}
                xs={12}
                container
              >
                <Grid
                  className="school-select"
                  item
                  md={6}
                  sm={6}
                  xs={12}
                  container
                >
                  <Grid className="select-label" item md={3} xs={5}>
                    <Typography className="mr-rt10" variant="caption">
                      {" "}
                      School:{" "}
                    </Typography>
                  </Grid>
                  <Grid item md={9} xs={7}>
                    <Select
                      styles={selectCustomStyles}
                      value={selectedSchool}
                      onChange={this.handleDropdownChange}
                      options={[...schoolList]}
                    />
                  </Grid>
                </Grid>
                <Grid item md={5} sm={5} xs={12} className="other-tools">
                  <FormControlLabel
                    className="checkbox-label"
                    control={
                      <Checkbox
                        className="checkbox"
                        style={{ padding: 2, marginLeft: 12 }}
                        checked={isSPEDOnlySelected}
                        onChange={this.handleSPEDSelection}
                        name="SPED Only"
                        color="primary"
                      />
                    }
                    label="SPED Only"
                    labelPlacement="start"
                  />
                  <Tooltip
                    title="Export to CSV"
                    aria-label="csv-export-tooltip"
                  >
                    <IconButton
                      onClick={() => this.downloadCSV("actual")}
                      className="tools-btn"
                      aria-label="csv-export"
                    >
                      <MaterialIcon icon="cloud_download" />
                    </IconButton>
                  </Tooltip>
                </Grid>
              </Grid>
            </Paper>
            <Router basename="/abc">
              <Paper className="secondary-tab-paper" square>
                <Tabs
                  value={value}
                  indicatorColor="primary"
                  textColor="primary"
                  onChange={this.handleCallToRouter.bind(value)}
                  centered
                >
                  <Tab
                    label="Incident Types"
                    className="minWidth90"
                    value="incident_types"
                    to="incident_types"
                    component={Link}
                  />
                  <Tab
                    label="Action Codes"
                    className="minWidth60"
                    value="action_codes"
                    to="action_codes"
                    component={Link}
                  />
                </Tabs>
              </Paper>
              <Switch>
                {routes.map((route, i) => (
                  <RouteWithSubRoutes key={i} {...route} />
                ))}
              </Switch>
            </Router>
          </div>
          <div initialSize="60%">
            <Paper className="mr-lt-2">
              <AppBar style={{ position: "static" }}>
                <Toolbar style={{ minHeight: 30 }}>
                  <Typography
                    style={{ flexGrow: 1 }}
                    className="capitalize"
                    variant="button"
                  >
                    {selectedSchool.value !== 9999 && (
                      <span>Students at: {selectedSchool.label}</span>
                    )}
                    {selectedSchool.value === 9999 && (
                      <span>School list for: {incidentLabelForSchools}</span>
                    )}
                  </Typography>
                  <Tooltip
                    title="Export to CSV"
                    aria-label="csv-export-tooltip"
                  >
                    <IconButton
                      style={{ color: "#ffffff", padding: 5, marginRight: 15 }}
                      onClick={() => this.downloadCSV("nested")}
                      className="csv-download-btn"
                      aria-label="csv-export"
                    >
                      <MaterialIcon icon="cloud_download" />
                    </IconButton>
                  </Tooltip>
                  <Tooltip title="PieChart" aria-label="pie-chart-tooltip">
                    <IconButton
                      style={{ color: "#ffffff", padding: 5 }}
                      onClick={(evt) =>
                        this.setState({ openPieChartModal: true })
                      }
                      className="pie-chart-btn"
                      aria-label="pie-chart"
                    >
                      <MaterialIcon icon="pie_chart" />
                    </IconButton>
                  </Tooltip>
                </Toolbar>
              </AppBar>
              {selectedSchool.value !== 9999 && (
                <DataTable
                  noHeader={true}
                  columns={TableConfig.behaviorStudents}
                  pagination={false}
                  fixedHeader={true}
                  fixedHeaderScrollHeight={"75vh"}
                  data={studentsForSelectedSchools}
                  customStyles={TableConfig.customStyles}
                />
              )}
              {selectedSchool.value === 9999 && (
                <DataTable
                  noHeader={true}
                  columns={TableConfig.behaviorSchools}
                  pagination={false}
                  fixedHeader={true}
                  fixedHeaderScrollHeight={"75vh"}
                  data={studentsForAllSchools}
                  customStyles={TableConfig.customStyles}
                />
              )}
            </Paper>
          </div>
        </SplitPane>
        <CSVLink
          ref={this.csvLink}
          data={csvExport.data}
          className="hidden"
          filename={csvExport.fileName}
          target="_self"
        />
        {openPieChartModal && (
          <PieChart
            title="Behavior"
            open={openPieChartModal}
            onClose={() =>
              this.setState({
                openPieChartModal: !openPieChartModal,
              })
            }
            data={plotData}
          />
        )}
      </div>
    );
  }
}

const mapStateToProps = () =>
  createStructuredSelector({
    selectedSchool: selectors.getBhrSelectedSchool(),
    schoolList: loginSelectors.getSchoolList(),
    studentsForAllSchools: selectors.getBhrStudentsForAllSchools(),
    studentsForSelectedSchools: selectors.getBhrStudentsForSelectedSchools(),
    systemIncidents: selectors.getBhrSystemIncidents(),
    systemActions: selectors.getBhrSystemActions(),
    incidentLabelForSchools: selectors.getBhrIncidentLabelForSchools(),
    isSPEDOnlySelected: selectors.getBhrSPEDOnlySelected(),
    userData: loginSelectors.getLoginData(),
    plotData: selectors.getPlotData(),
  });

const mapDispatchToProps = (dispatch) => ({
  setSelectedSchool: (school) => dispatch(actions.setBhrSelectedSchool(school)),
  loadSchoolList: (school) => dispatch(actions.loadBhrSchoolList(school)),
  loadBehaviourData: (payload) =>
    dispatch(actions.loadBhrBehaviourData(payload)),
  setSPEDOnlySelected: (payload) =>
    dispatch(actions.setBhrSPEDOnlySelected(payload)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withRouter(Behaviour));
