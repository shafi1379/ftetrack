import { fromJS } from "immutable";
import {
  SET_BHR_SCHOOL_LIST,
  SET_BHR_SELECTED_SCHOOL,
  SET_BHR_SYSTEM_INCIDENTS,
  SET_BHR_SYSTEM_ACTIONS,
  SET_BHR_STUDENTS_FOR_ALL_SCHOOLS,
  SET_BHR_STUDENTS_FOR_SELECTED_SCHOOLS,
  SET_BHR_SELECTED_INCIDENTS,
  SET_BHR_SELECTED_ACTIONS,
  SET_BHR_SPED_ONLY_SELECTED,
  SET_PLOT_DATA,
} from "./actions";

const initialState = fromJS({
  selectedSchool: { label: "Select a School ...", value: -1 },
  schoolList: [],
  systemIncidents: [{}],
  selectedIncidents: {},
  systemActions: [{}],
  selectedActions: {},
  studentsForAllSchools: [{}],
  studentsForSelectedSchools: [{}],
  incidentLabelForSchools: null,
  isSPEDOnlySelected: false,
  plotData: [],
});

const behaviour = (state = initialState, action) => {
  switch (action.type) {
    case SET_BHR_SCHOOL_LIST:
      return state.set("schoolList", fromJS([...action.schoolList]));
    case SET_BHR_SELECTED_SCHOOL:
      return state.set("selectedSchool", fromJS(action.selectedSchool));
    case SET_BHR_SYSTEM_INCIDENTS:
      return state.set("systemIncidents", fromJS(action.payload));
    case SET_BHR_SELECTED_INCIDENTS:
      return (
        state.set("selectedIncidents", fromJS(action.payload)) &&
        state.set("incidentLabelForSchools", action.payload.IncidentDescription)
      );
    case SET_BHR_SYSTEM_ACTIONS:
      return state.set("systemActions", fromJS(action.payload));
    case SET_BHR_SELECTED_ACTIONS:
      return (
        state.set("selectedActions", fromJS(action.payload)) &&
        state.set("incidentLabelForSchools", action.payload.ActionDescription)
      );
    case SET_BHR_STUDENTS_FOR_ALL_SCHOOLS:
      return state.set("studentsForAllSchools", fromJS(action.payload));
    case SET_BHR_STUDENTS_FOR_SELECTED_SCHOOLS:
      return state.set("studentsForSelectedSchools", fromJS(action.payload));
    case SET_BHR_SPED_ONLY_SELECTED:
      return state.set("isSPEDOnlySelected", action.payload);
    case SET_PLOT_DATA:
      return state.set("plotData", fromJS([...action.plotData]));
    default:
      return state;
  }
};

export default behaviour;
