export const LOAD_BHR_SCHOOL_LIST = "LOAD_BHR_SCHOOL_LIST";
export const loadBhrSchoolList = (school) => ({
  type: LOAD_BHR_SCHOOL_LIST,
  school,
});

export const SET_BHR_SCHOOL_LIST = "SET_BHR_SCHOOL_LIST";
export const setBhrSchoolList = (schoolList) => ({
  type: SET_BHR_SCHOOL_LIST,
  schoolList,
});

export const SET_BHR_SELECTED_SCHOOL = "SET_BHR_SELECTED_SCHOOL";
export const setBhrSelectedSchool = (selectedSchool) => ({
  type: SET_BHR_SELECTED_SCHOOL,
  selectedSchool,
});

export const SET_BHR_SPED_ONLY_SELECTED = "SET_BHR_SPED_ONLY_SELECTED";
export const setBhrSPEDOnlySelected = (payload) => ({
  type: SET_BHR_SPED_ONLY_SELECTED,
  payload,
});

export const LOAD_BHR_BEHAVIOUR_DATA = "LOAD_BHR_BEHAVIOUR_DATA";
export const loadBhrBehaviourData = (payload) => ({
  type: LOAD_BHR_BEHAVIOUR_DATA,
  payload,
});

export const SET_BHR_SYSTEM_INCIDENTS = "SET_BHR_SYSTEM_INCIDENTS";
export const setBhrSystemIncidents = (payload) => ({
  type: SET_BHR_SYSTEM_INCIDENTS,
  payload,
});

export const SET_BHR_SELECTED_INCIDENTS = "SET_BHR_SELECTED_INCIDENTS";
export const setBhrSelectedIncidents = (payload) => ({
  type: SET_BHR_SELECTED_INCIDENTS,
  payload,
});

export const SET_BHR_SYSTEM_ACTIONS = "SET_BHR_SYSTEM_ACTIONS";
export const setBhrSystemActions = (payload) => ({
  type: SET_BHR_SYSTEM_ACTIONS,
  payload,
});

export const SET_BHR_SELECTED_ACTIONS = "SET_BHR_SELECTED_ACTIONS";
export const setBhrSelectedActions = (payload) => ({
  type: SET_BHR_SELECTED_ACTIONS,
  payload,
});

export const SET_BHR_STUDENTS_FOR_ALL_SCHOOLS =
  "SET_BHR_STUDENTS_FOR_ALL_SCHOOLS";
export const setBhrStudentsForAllSchools = (payload) => ({
  type: SET_BHR_STUDENTS_FOR_ALL_SCHOOLS,
  payload,
});

export const SET_BHR_STUDENTS_FOR_SELECTED_SCHOOLS =
  "SET_BHR_STUDENTS_FOR_SELECTED_SCHOOLS";
export const setBhrStudentsForSelectedSchools = (payload) => ({
  type: SET_BHR_STUDENTS_FOR_SELECTED_SCHOOLS,
  payload,
});

export const SET_PLOT_DATA = "SET_PLOT_DATA";
export const setPlotData = (plotData) => ({
  type: SET_PLOT_DATA,
  plotData,
});
