import { createSelector } from "reselect";
import * as selectors from "../../../redux/selectors";

export const getBhrSchoolList = () =>
  createSelector(selectors.behaviourState, (behaviourState) =>
    behaviourState.get("schoolList").toJS(),
  );

export const getBhrSelectedSchool = () =>
  createSelector(selectors.behaviourState, (behaviourState) =>
    behaviourState.get("selectedSchool").toJS(),
  );

export const getBhrSPEDOnlySelected = () =>
  createSelector(selectors.behaviourState, (behaviourState) =>
    behaviourState.get("isSPEDOnlySelected"),
  );

export const getBhrSystemIncidents = () =>
  createSelector(selectors.behaviourState, (behaviourState) =>
    behaviourState.get("systemIncidents").toJS(),
  );

export const getBhrSelectedIncidents = () =>
  createSelector(selectors.behaviourState, (behaviourState) =>
    behaviourState.get("selectedIncidents").toJS(),
  );

export const getBhrSystemActions = () =>
  createSelector(selectors.behaviourState, (behaviourState) =>
    behaviourState.get("systemActions").toJS(),
  );

export const getBhrSelectedActions = () =>
  createSelector(selectors.behaviourState, (behaviourState) =>
    behaviourState.get("selectedActions").toJS(),
  );

export const getBhrStudentsForAllSchools = () =>
  createSelector(selectors.behaviourState, (behaviourState) =>
    behaviourState.get("studentsForAllSchools").toJS(),
  );

export const getBhrStudentsForSelectedSchools = () =>
  createSelector(selectors.behaviourState, (behaviourState) =>
    behaviourState.get("studentsForSelectedSchools").toJS(),
  );

export const getBhrIncidentLabelForSchools = () =>
  createSelector(selectors.behaviourState, (behaviourState) =>
    behaviourState.get("incidentLabelForSchools"),
  );

export const getPlotData = () =>
  createSelector(selectors.behaviourState, (behaviourState) =>
    behaviourState.get("plotData").toJS(),
  );
