import ActionCodes from "./action_codes";
import IncidentTypes from "./incident_types";

const routes = [
  {
    path: "/behaviour/action_codes",
    exact: true,
    component: ActionCodes,
  },
  {
    path: "/behaviour/incident_types",
    exact: true,
    component: IncidentTypes,
  },
];

export default routes;
