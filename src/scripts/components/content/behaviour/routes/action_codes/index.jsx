import React, { Component } from "react";
import DataTable from "react-data-table-component";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";
import * as selectors from "../../selectors";
import * as actions from "../../actions";
import * as TableConfig from "../../../../../utilities/tableConfig";

class ActionCodes extends Component {
  state = {
    columns: TableConfig.behaviourActions.map((dash, index) => {
      if (index === 2) {
        dash.cell = (row) =>
          row.ActionCount > 0 ? (
            <span
              style={{
                fontWeight: "bold",
                color: "#683598",
                textDecoration: "underline",
                cursor: "pointer",
              }}
              onClick={this.handleRowClick.bind(this, row)}
            >
              {row.ActionCount}
            </span>
          ) : (
            row.ActionCount
          );
      }
      return dash;
    }),
  };

  handleRowClick = (data, evt) => {
    const actions = {
      code: data.ActionCode,
      sped: this.props.isSPEDOnlySelected ? "Y" : "N",
    };
    if (this.props.selectedSchool.value === 9999) {
      this.props.loadBehaviourData({
        apiCategory: "schoolsbyaction",
        request: actions,
      });
    } else {
      actions.school = this.props.selectedSchool.value;
      this.props.loadBehaviourData({
        apiCategory: "studentsbyaction",
        request: actions,
      });
    }
    this.props.setSelectedActions(data);
  };

  render() {
    const { columns } = this.state;
    const { systemActions } = this.props;
    return (
      <DataTable
        noHeader={true}
        columns={columns}
        pagination={false}
        fixedHeader={true}
        style={{ minHeight: 300 }}
        fixedHeaderScrollHeight={"70vh"}
        data={systemActions}
        customStyles={TableConfig.customStyles}
        onRowClicked={this.handleRowClick}
      />
    );
  }
}

const mapStateToProps = () =>
  createStructuredSelector({
    isSPEDOnlySelected: selectors.getBhrSPEDOnlySelected(),
    selectedSchool: selectors.getBhrSelectedSchool(),
    systemActions: selectors.getBhrSystemActions(),
  });

const mapDispatchToProps = (dispatch) => ({
  setSelectedActions: (data) => dispatch(actions.setBhrSelectedActions(data)),
  loadBehaviourData: (payload) =>
    dispatch(actions.loadBhrBehaviourData(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ActionCodes);
