import React, { Component } from "react";
import DataTable from "react-data-table-component";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";
import * as selectors from "../../selectors";
import * as actions from "../../actions";
import * as TableConfig from "../../../../../utilities/tableConfig";

class IncidentTypes extends Component {
  state = {
    columns: TableConfig.behaviourIncidents.map((dash, index) => {
      if (index === 2) {
        dash.cell = (row) =>
          row.IncidentCount > 0 ? (
            <span
              style={{
                fontWeight: "bold",
                color: "#683598",
                textDecoration: "underline",
                cursor: "pointer",
              }}
              onClick={this.handleRowClick.bind(this, row)}
            >
              {row.IncidentCount}
            </span>
          ) : (
            row.IncidentCount
          );
      }
      return dash;
    }),
  };

  handleRowClick = (data, evt) => {
    const incidents = {
      code: data.IncidentCode,
      sped: this.props.isSPEDOnlySelected ? "Y" : "N",
    };
    if (this.props.selectedSchool.value === 9999) {
      this.props.loadBehaviourData({
        apiCategory: "schoolsbyincident",
        request: incidents,
      });
    } else {
      incidents.school = this.props.selectedSchool.value;
      this.props.loadBehaviourData({
        apiCategory: "studentsbyincident",
        request: incidents,
      });
    }
    this.props.setSelectedIncidents(data);
  };

  render() {
    const { columns } = this.state;
    const { systemIncidents } = this.props;
    return (
      <DataTable
        noHeader={true}
        columns={columns}
        pagination={false}
        fixedHeader={true}
        style={{ minHeight: 300 }}
        fixedHeaderScrollHeight={"70vh"}
        data={systemIncidents}
        customStyles={TableConfig.customStyles}
        onRowClicked={this.handleRowClick}
      />
    );
  }
}

const mapStateToProps = () =>
  createStructuredSelector({
    isSPEDOnlySelected: selectors.getBhrSPEDOnlySelected(),
    selectedSchool: selectors.getBhrSelectedSchool(),
    systemIncidents: selectors.getBhrSystemIncidents(),
  });

const mapDispatchToProps = (dispatch) => ({
  setSelectedIncidents: (data) =>
    dispatch(actions.setBhrSelectedIncidents(data)),
  loadBehaviourData: (payload) =>
    dispatch(actions.loadBhrBehaviourData(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(IncidentTypes);
