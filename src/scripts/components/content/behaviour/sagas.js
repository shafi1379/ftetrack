import { all, call, fork, put, select, takeLatest } from "redux-saga/effects";
import {
  LOAD_BHR_SCHOOL_LIST,
  setBhrSchoolList,
  setBhrSelectedSchool,
  LOAD_BHR_BEHAVIOUR_DATA,
  setBhrSystemIncidents,
  setBhrSystemActions,
  setBhrStudentsForAllSchools,
  setBhrStudentsForSelectedSchools,
  setPlotData,
} from "./actions";
import { setProgressingValue } from "../../../app/actions";
import * as Api from "../../../services/api";
import { API_URLS } from "../../../utilities/constants";
import * as Utilities from "../../../utilities/utilities";

function* loadSchoolListRequested(params) {
  try {
    const { schoolList } = (yield select((s) => s.get("behaviour"))).toJS();
    if (!schoolList.length) {
      yield put(setProgressingValue(true));
      const response = yield call(
        Api.doPostRequest,
        API_URLS.schools,
        params.school,
      );
      if (response) {
        const schoolList = response.map((res) => ({
          ...res,
          label: res.schoolname,
          value: res.school,
        }));
        yield put(setBhrSchoolList(schoolList));
        yield put(setBhrSelectedSchool(schoolList[0]));
        yield put(setProgressingValue(false));
      }
    }
  } catch (error) {
    console.error(error);
  }
}

function* schoolListListener() {
  yield takeLatest(LOAD_BHR_SCHOOL_LIST, loadSchoolListRequested);
}

function* behaviourDataRequested({ payload }) {
  try {
    yield put(setProgressingValue(true));
    switch (payload.apiCategory) {
      case "schoolsbyincident":
        const schoolsbyincident = yield call(
          Api.doPostRequest,
          API_URLS.schoolsbyincident,
          payload.request,
        );
        yield put(setBhrStudentsForAllSchools(schoolsbyincident));
        yield put(setProgressingValue(false));
        break;
      case "schoolsbyaction":
        const schoolsbyaction = yield call(
          Api.doPostRequest,
          API_URLS.schoolsbyaction,
          payload.request,
        );
        yield put(setBhrStudentsForAllSchools(schoolsbyaction));
        yield put(setProgressingValue(false));
        break;
      case "studentsbyincident":
        const studentsbyincident = yield call(
          Api.doPostRequest,
          API_URLS.studentsbyincident,
          payload.request,
        );
        yield put(
          setBhrStudentsForSelectedSchools(studentsbyincident.students),
        );
        const raceList1 = Utilities.getRaceList(studentsbyincident.students);
        yield put(setPlotData([raceList1, studentsbyincident.enrollment[0]]));
        yield put(setProgressingValue(false));
        break;
      case "studentsbyaction":
        const studentsbyaction = yield call(
          Api.doPostRequest,
          API_URLS.studentsbyaction,
          payload.request,
        );
        yield put(setBhrStudentsForSelectedSchools(studentsbyaction.students));
        const raceList2 = Utilities.getRaceList(studentsbyaction.students);
        yield put(setPlotData([raceList2, studentsbyaction.enrollment[0]]));
        yield put(setProgressingValue(false));
        break;
      default:
        const systemincidents = yield call(
          Api.doPostRequest,
          API_URLS.systemincidents,
          payload.request,
        );
        yield put(setBhrSystemIncidents(systemincidents.incidents));
        yield put(setBhrSystemActions(systemincidents.actions));
        yield put(setBhrStudentsForSelectedSchools([]));
        yield put(setProgressingValue(false));
    }
  } catch (error) {
    console.error(error);
  }
}

function* behaviourDataListener() {
  yield takeLatest(LOAD_BHR_BEHAVIOUR_DATA, behaviourDataRequested);
}

export default function* root() {
  yield all([fork(schoolListListener), fork(behaviourDataListener)]);
}
