import { createSelector } from "reselect";
import * as selectors from "../../../redux/selectors";

export const getSelectedSchool = () =>
  createSelector(selectors.summaryState, (summaryState) =>
    summaryState.get("selectedSchool").toJS(),
  );
