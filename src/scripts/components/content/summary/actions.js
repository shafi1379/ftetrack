export const SET_SELECTED_SCHOOL = "SET_SELECTED_SCHOOL";
export const setSelectedSchool = (selectedSchool) => ({
  type: SET_SELECTED_SCHOOL,
  selectedSchool,
});
