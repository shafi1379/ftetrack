import React, { Component } from "react";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import { withRouter } from "react-router-dom";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";
import { Chart } from "react-google-charts";
import WithStyles from "@material-ui/styles/withStyles";
import createStyles from "@material-ui/styles/createStyles";
import * as selectors from "../../../app/selectors";
import * as summarySelectors from "./selectors";
import { Typography } from "@material-ui/core";
import { ArrowForward } from "@material-ui/icons";
import Link from "@material-ui/core/Link";
import Select from "react-select";
import { selectCustomStyles } from "../../generic/reactSelectCustomization";
import * as loginSelectors from "../../../app/selectors";
import { API_URLS } from "../../../utilities/constants";
import * as actions from "../../../app/actions";
import * as summaryActions from "./actions";

const useStyles = createStyles((theme) => ({
  paper: {
    position: "relative",
    padding: theme.spacing(1),
    textAlign: "center",
    color: theme.palette.text.secondary,
  },
}));

const randColor = () => {
  return (
    "#" +
    Math.floor(Math.random() * 16777215)
      .toString(16)
      .padStart(6, "0")
      .toUpperCase()
  );
};

class Summary extends Component {
  handleDropdownChange = (option) => {
    this.props.setProgressingValue(true);
    this.props.setSelectedSchool(option);
    this.props.loadSummaryData({
      url: API_URLS.summary,
      params: { school: option.school },
    });
  };

  render() {
    const { classes, loginData, summaryList, schoolList, selectedSchool } =
      this.props;
    let ADA = {};
    let noOfCharts = 0;
    const attendanceList = [];
    const enrollmentList = [];
    const behaviorIncidentsList = [];
    const behaviorSummaryList = [];
    const gradesList = [];
    const ctaeList = [];
    const randomColors = [
      "#9C27B0",
      "#009688",
      "#03A9F4",
      "#3f51b5",
      "#673AB7",
      "#ff5722",
      "#4caf50",
      "#9e9e9e",
      "#ffc107",
      "#00bcd4",
      "#f44336",
      "#ffeb3b",
      "#2196f3",
      "#e91e63",
      "#cddc39",
      "#795548",
      "#607d8b",
      "#4a148c",
      "#00695c",
    ];

    if (summaryList.attendance.length) {
      ++noOfCharts;
      attendanceList.push([
        "Category",
        "Attendance",
        { role: "tooltip", type: "string", p: { html: true } },
      ]);
      summaryList.attendance.forEach((a) => {
        if (a.value.includes(" (")) {
          const s = a.value.split(" (");
          attendanceList.push([
            a.label,
            parseInt(s[0]),
            `${parseInt(s[0])} (${s[1]}`,
          ]);
        } else {
          ADA = a;
        }
      });
    }

    if (summaryList?.enrollment.length) {
      ++noOfCharts;
      enrollmentList.push(["Category", "Enrollment"]);
      for (const item in summaryList?.enrollment[0]) {
        enrollmentList.push([item, summaryList.enrollment[0][item]]);
      }
    }

    if (
      summaryList?.behavior?.incidents.length ||
      summaryList?.behavior?.summary.length
    ) {
      ++noOfCharts;
      behaviorIncidentsList.push(["Incidents", "Values"]);
      summaryList.behavior.incidents.forEach((i) =>
        behaviorIncidentsList.push([i.label, i.value]),
      );
      for (const item in summaryList?.behavior?.summary[0]) {
        behaviorSummaryList.push({
          label: item,
          value: summaryList.behavior?.summary[0][item],
        });
      }
    }

    if (summaryList?.grades?.length) {
      ++noOfCharts;
      gradesList.push([
        "Grades",
        "Passing",
        { role: "tooltip", type: "string", p: { html: true } },
        "Failing",
        { role: "tooltip", type: "string", p: { html: true } },
      ]);
      summaryList.grades.forEach((g) => {
        gradesList.push([
          g.label,
          g.passing,
          `${g.passing} (${g.passing_percent}%)`,
          g.failing,
          `${g.failing} (${g.failing_percent}%)`,
        ]);
      });
    }

    if (summaryList?.ctae?.length) {
      ++noOfCharts;
      ctaeList.push(["CTAE", "Values", { role: "style" }]);
      summaryList.ctae.forEach((i, ind) =>
        ctaeList.push([
          i.label,
          i.value,
          `color: ${randomColors[ind] ? randomColors[ind] : randColor()}`,
        ]),
      );
    }

    return (
      <Box style={{ padding: 10 }} className="summary">
        <Paper square style={{ marginBottom: 15, padding: 15 }}>
          <Box style={{ display: "flex" }}>
            <Typography>Schools: </Typography>
            <Box style={{ marginLeft: 15, width: 275 }}>
              <Select
                styles={selectCustomStyles}
                value={selectedSchool}
                onChange={this.handleDropdownChange.bind(this)}
                options={schoolList}
              />
            </Box>
          </Box>
        </Paper>
        <Grid container spacing={2}>
          {attendanceList?.length ? (
            <Grid item xs={12} sm={6}>
              <Paper className={classes.paper}>
                <Paper
                  variant="outlined"
                  square
                  style={{
                    position: "absolute",
                    right: 8,
                    zIndex: 100,
                    padding: 10,
                  }}
                >
                  <Box display={"flex"}>
                    <Typography
                      color="textSecondary"
                      variant="subtitle2"
                      style={{ marginRight: 10 }}
                    >
                      {ADA.label} (ADA)
                    </Typography>
                    <ArrowForward />
                    <Typography
                      color="textSecondary"
                      variant="subtitle2"
                      style={{ marginLeft: 10, fontWeight: "bold" }}
                    >
                      {ADA.value}
                    </Typography>
                  </Box>
                </Paper>
                <Chart
                  chartType="BarChart"
                  width="100%"
                  height="400px"
                  data={attendanceList}
                  options={{
                    legend: { position: "none" },
                    title: `Attendance ${loginData.school_year}`,
                    bar: { groupWidth: "80%" },
                    vAxis: {
                      textStyle: {
                        fontSize: 12,
                      },
                    },
                    focusTarget: "category",
                    tooltip: { isHtml: true },
                  }}
                />
                <Link
                  component="button"
                  variant="body2"
                  onClick={() => {
                    this.props.history.push("/attendance");
                  }}
                  style={{
                    display: "flex",
                    padding: "5px 15px",
                    marginTop: 5,
                    cursor: "pointer",
                  }}
                >
                  <Typography variant="subtitle2">Attendance view</Typography>
                  <ArrowForward />
                </Link>
              </Paper>
            </Grid>
          ) : (
            <></>
          )}
          {ctaeList?.length ? (
            <Grid item xs={12} sm={6}>
              <Paper className={classes.paper}>
                <Chart
                  chartType="ColumnChart"
                  width="100%"
                  height="400px"
                  data={ctaeList}
                  options={{
                    title: "CTAE course enrollment by Cluster",
                    legend: { position: "none" },
                    bars: "horizontal",
                  }}
                />
                <Link
                  component="button"
                  variant="body2"
                  onClick={() => {
                    this.props.history.push("/ctae");
                  }}
                  style={{
                    display: "flex",
                    padding: "5px 15px",
                    marginTop: 5,
                    cursor: "pointer",
                  }}
                >
                  <Typography variant="subtitle2">CTAE view</Typography>
                  <ArrowForward />
                </Link>
              </Paper>
            </Grid>
          ) : (
            <></>
          )}
          {enrollmentList?.length ? (
            <Grid item xs={12} sm={6}>
              <Paper className={classes.paper}>
                <Chart
                  chartType="PieChart"
                  data={enrollmentList}
                  options={{
                    title: `Students Enrollment ${loginData.school_year}`,
                    pieHole: 0.4,
                    is3D: false,
                  }}
                  width={"100%"}
                  height={"400px"}
                />
                <Link
                  component="button"
                  variant="body2"
                  onClick={() => {
                    this.props.history.push("/enrollment");
                  }}
                  style={{
                    display: "flex",
                    padding: "5px 15px",
                    marginTop: 5,
                    cursor: "pointer",
                  }}
                >
                  <Typography variant="subtitle2">Enrollment view</Typography>
                  <ArrowForward />
                </Link>
              </Paper>
            </Grid>
          ) : (
            <></>
          )}
          {gradesList?.length ? (
            <Grid item xs={12} sm={6}>
              <Paper className={classes.paper}>
                <Chart
                  chartType="ColumnChart"
                  width="100%"
                  height="380px"
                  data={gradesList}
                  options={{
                    title: `Students Grades ${loginData.school_year}`,
                    bars: "horizontal",
                    is3D: true,
                    isStacked: true,
                    focusTarget: "category",
                    tooltip: { isHtml: true },
                  }}
                />
                <Typography variant="body2">
                  <em style={{ paddingRight: 5 }}>Note:</em>Based on current
                  gradebook/progress grades in core courses
                </Typography>
                <Link
                  component="button"
                  variant="body2"
                  onClick={() => {
                    this.props.history.push("/progress_grade");
                  }}
                  style={{
                    display: "flex",
                    padding: "5px 15px",
                    marginTop: 5,
                    cursor: "pointer",
                  }}
                >
                  <Typography variant="subtitle2">
                    Progress Grade view
                  </Typography>
                  <ArrowForward />
                </Link>
              </Paper>
            </Grid>
          ) : (
            <></>
          )}
          {behaviorIncidentsList?.length ? (
            <Grid item xs={12} sm={noOfCharts % 2 === 1 ? 12 : 6}>
              <Paper className={classes.paper}>
                <Box
                  display={"flex"}
                  justifyContent={"center"}
                  mt={1}
                  m={"0px auto"}
                >
                  {behaviorSummaryList.map((s, ind) => (
                    <Paper
                      key={ind}
                      square
                      elevation={2}
                      style={{
                        display: "flex",
                        flexDirection: "column",
                        textAlign: "center",
                        padding: 5,
                        margin: 5,
                        minWidth: 160,
                        background: "#3d50b2",
                        color: "#fff",
                      }}
                    >
                      <Typography variant="caption">{s.label}</Typography>
                      <Typography
                        color="textSecondary"
                        style={{ fontWeight: "500", color: "#fff" }}
                      >
                        {s.value}
                      </Typography>
                    </Paper>
                  ))}
                </Box>
                <Chart
                  chartType="PieChart"
                  data={behaviorIncidentsList}
                  options={{
                    title: `Behavior Incidents ${loginData.school_year}`,
                    pieSliceText: "label",
                    slices: {
                      3: { offset: 0.1 },
                      5: { offset: 0.2 },
                      10: { offset: 0.2 },
                      14: { offset: 0.3 },
                      15: { offset: 0.2 },
                    },
                  }}
                  width={"100%"}
                  height={noOfCharts % 2 === 1 ? "380px" : "280px"}
                />
                <Typography variant="body2">
                  <em style={{ paddingRight: 5 }}>Note:</em>These numbers
                  exclude incidents where state code is 'Other'
                </Typography>
                <Link
                  component="button"
                  variant="body2"
                  onClick={() => {
                    this.props.history.push("/behaviour");
                  }}
                  style={{
                    display: "flex",
                    padding: "5px 15px",
                    marginTop: 5,
                    cursor: "pointer",
                  }}
                >
                  <Typography variant="subtitle2">Behavior view</Typography>
                  <ArrowForward />
                </Link>
              </Paper>
            </Grid>
          ) : (
            <></>
          )}
        </Grid>
      </Box>
    );
  }
}

const mapStateToProps = () =>
  createStructuredSelector({
    schoolList: loginSelectors.getSchoolList(),
    loginData: selectors.getLoginData(),
    summaryList: selectors.getSummaryList(),
    loadingSpinner: selectors.getProgressingValue(),
    selectedSchool: summarySelectors.getSelectedSchool(),
  });

const mapDispatchToProps = (dispatch) => ({
  loadSummaryData: (data) => dispatch(actions.loadSummaryDetailsData(data)),
  setProgressingValue: (data) => dispatch(actions.setProgressingValue(data)),
  setSelectedSchool: (data) => dispatch(summaryActions.setSelectedSchool(data)),
});

const WithStylesSummary = WithStyles(useStyles)(Summary);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withRouter(WithStylesSummary));
