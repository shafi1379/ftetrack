import { fromJS } from "immutable";
import { SET_SELECTED_SCHOOL } from "./actions";

const initialState = fromJS({
  selectedSchool: {
    school: 9999,
    schoolname: "All Schools",
    srlno: 1,
    label: "All Schools",
    value: 9999,
  },
});

const summary = (state = initialState, action) => {
  switch (action.type) {
    case SET_SELECTED_SCHOOL:
      return state.set("selectedSchool", fromJS(action.selectedSchool));
    default:
      return state;
  }
};

export default summary;
