import React, { Component } from "react";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";
import * as loginSelectors from "../../../app/selectors";
import * as selectors from "./selectors";
import * as actions from "./actions";
import DataTable from "react-data-table-component";
import { CSVLink } from "react-csv";
import Select, { components } from "react-select";
import * as Utilities from "../../../utilities/utilities";
import TextField from "@material-ui/core/TextField";
import {
  Paper,
  AppBar,
  Toolbar,
  Grid,
  Tooltip,
  IconButton,
  Typography,
  Checkbox,
} from "@material-ui/core";
import MaterialIcon from "../../generic/materialIcon";
import {
  customStyles,
  GradingDistributionColumnsDetails,
} from "../../../utilities/tableConfig";
import { selectCustomStyles } from "../../generic/reactSelectCustomization";

const MenuList = (props) => {
  const handleCheckBox = (evt, obj) => {
    if (evt.target.checked) {
      props.setValue([...props.selectProps.selectedValues, obj]);
    } else {
      props.setValue([
        ...props.selectProps.selectedValues.filter(
          (_obj) => _obj.label !== obj.label,
        ),
      ]);
    }
  };
  return (
    <components.MenuList {...props}>
      {props.options.map((obj) => {
        let values = props.selectProps.selectedValues;
        let selected = false;
        values.forEach((_obj) => {
          if (_obj.label === obj.label) {
            selected = true;
          }
        });
        return (
          <div className="checkbox-style">
            <Checkbox
              onChange={(evt) => handleCheckBox(evt, obj)}
              checked={selected}
            />
            {obj.label}
          </div>
        );
      })}
    </components.MenuList>
  );
};

class Honor_Roll extends Component {
  csvLink = React.createRef();
  state = {
    GradingDistributionColumnsDetails: [...GradingDistributionColumnsDetails],
    csvExport: {
      data: [],
      fileName: "ews.csv",
    },
  };

  componentDidMount() {
    /* this.props.loadGdSchoolList({
      school: this.props.userData.loginData.school,
    }); */
    this.clearSearchField();
  }

  downloadCSV = (evt) => {
    const currentTime = `_${new Date().getTime()}`;
    const columnDetails = [...GradingDistributionColumnsDetails];
    const columns = {};
    columnDetails.forEach((obj) => {
      columns[obj["name"]] = obj["selector"];
    });
    const { gdSelectedTask, gdSelectedTerm, gdSelectedSchool } = this.props;
    const data = Utilities.generateExcelData(
      [...this.props.gdFilterDataList] || [],
      columns || {},
    );
    if (gdSelectedTask.length) {
      data.unshift([
        `Selected Task: ${gdSelectedTask.map((d) => d.label).join(",")}`,
      ]);
    }
    if (gdSelectedTerm) {
      data.unshift([`Selected Term: ${gdSelectedTerm["label"]}`]);
    }
    if (gdSelectedSchool) {
      data.unshift([`Selected School: ${gdSelectedSchool["label"]}`]);
    }
    this.setState({
      csvExport: {
        data,
        fileName: `Grade_Dist${currentTime}.csv`,
      },
    });
    setTimeout(() => {
      this.csvLink.current.link.click();
    });
  };

  handleDropDownChange = (obj, evt, type) => {
    if (type === "Task") {
      this.props["setGdSelected" + type]([...obj]);
    } else if (type !== "TaskCompleted") {
      this.props["setGdSelected" + type]({ ...obj });
    }
    const { gdSelectedTerm, gdSelectedSchool, gdSelectedTask, gdTermList } =
      this.props;
    if (type === "School") {
      this.props.loadGdTerm({
        school: obj.school,
        option: "2",
      });
      this.props.setGdSelectedTerm(gdTermList[0]);
      this.props.setGdGradingTask([]);
    }
    if (type === "Term") {
      this.props.loadGdGradingTask({
        school: obj.schoolID,
        termID: obj.termID,
        option: "2",
      });
    }
    if (type === "TaskCompleted") {
      this.props.loadGdStudentList({
        school: gdSelectedSchool.school,
        termID: gdSelectedTerm.termID,
        termName: gdSelectedTerm.termName,
        taskID:
          gdSelectedTask && gdSelectedTask.length > 0
            ? gdSelectedTask.map((val) => val.taskID).join(",")
            : "-1",
      });
    }
  };

  handleCellClick = (row) => {
    this.handleRowClick(row);
  };

  filterSearchField = (e) => {
    let value = e.target.value;
    if (value.length && this.props.gdStudentList.length) {
      this.props.setSearchText(value);
      const objKeys = Object.keys(this.props.gdStudentList[0]);
      const actualKeys = this.state.GradingDistributionColumnsDetails;
      this.props.setGdFilterData(
        this.props.gdStudentList.filter((item) => {
          return Utilities.generateFilteredData(
            item,
            value ? value : this.props.searchText,
            objKeys,
            actualKeys,
          );
        }),
      );
    } else {
      this.props.setSearchText(null);
      this.props.setGdFilterData(this.props.gdStudentList);
    }
  };

  clearSearchField = () => {
    this.props.setSearchText(null);
    this.props.setGdFilterData(this.props.gdStudentList);
  };

  render() {
    const { GradingDistributionColumnsDetails, csvExport } = this.state;
    const {
      gdSchoolList,
      gdFilterDataList,
      gdSelectedSchool,
      gdTermList,
      gdSelectedTerm,
      gdGradingTaskList,
      gdSelectedTask,
      searchText,
    } = this.props;

    return (
      <div className="page-under-construction ews">
        <Paper style={{ padding: 2 }}>
          <AppBar style={{ position: "static", height: "32px" }}>
            <Toolbar
              style={{ minHeight: 30, float: "right", position: "relative" }}
            >
              <Grid item container sm={12} className="progress_grade_toolbar">
                <Grid item sm={3}>
                  <Typography>School</Typography>
                  <Select
                    styles={selectCustomStyles}
                    options={gdSchoolList}
                    className="ews-dropdown"
                    placeholder={"Select School"}
                    value={gdSelectedSchool}
                    onChange={(val, evt) =>
                      this.handleDropDownChange(val, evt, "School")
                    }
                  />
                </Grid>

                <Grid item sm={2}>
                  {gdTermList.length > 0 && (
                    <React.Fragment>
                      <Typography>Term</Typography>
                      <Select
                        styles={selectCustomStyles}
                        options={gdTermList}
                        className="ews-dropdown"
                        value={gdSelectedTerm}
                        onChange={(val, evt) =>
                          this.handleDropDownChange(val, evt, "Term")
                        }
                      />
                    </React.Fragment>
                  )}
                </Grid>

                <Grid item sm={3} style={{ marginLeft: "30px" }}>
                  {gdGradingTaskList.length > 0 && (
                    <React.Fragment>
                      <Typography>Grading&nbsp;Task</Typography>
                      <Select
                        styles={selectCustomStyles}
                        isMulti={true}
                        closeMenuOnSelect={false}
                        inputValue={gdSelectedTask
                          .map((obj) => obj.label)
                          .join(" | ")}
                        components={{ MenuList }}
                        options={gdGradingTaskList}
                        onMenuClose={(val, evt) =>
                          this.handleDropDownChange(val, evt, "TaskCompleted")
                        }
                        placeholder={"Select Taks"}
                        className="ews-dropdown"
                        selectedValues={gdSelectedTask}
                        value={[]}
                        onChange={(val, evt) =>
                          this.handleDropDownChange(val, evt, "Task")
                        }
                      />
                    </React.Fragment>
                  )}
                </Grid>

                <Grid item sm={1}>
                  <Tooltip
                    title="Export to CSV"
                    aria-label="csv-export-tooltip"
                    style={{ float: "right" }}
                  >
                    <IconButton
                      onClick={(evt) => this.downloadCSV(evt)}
                      className="csv-download-btn"
                      aria-label="csv-export"
                      style={{
                        color: "#ffffff",
                        padding: 5,
                        position: "absolute",
                        right: "5px",
                        top: "-2px",
                        cursor: "pointer",
                      }}
                    >
                      <MaterialIcon icon="cloud_download" />
                    </IconButton>
                  </Tooltip>
                </Grid>
              </Grid>
            </Toolbar>
          </AppBar>
          <DataTable
            noHeader={false}
            columns={[...GradingDistributionColumnsDetails]}
            pointerOnHover={true}
            styles={customStyles}
            highlightOnHover={true}
            pagination={true}
            actions={
              <TextField
                onChange={this.filterSearchField}
                placeholder="Search here ..."
                type="text"
                value={searchText}
                style={{ width: 225 }}
              />
            }
            paginationPerPage={25}
            paginationRowsPerPageOptions={[25, 50, 100, 200]}
            fixedHeader={true}
            fixedHeaderScrollHeight={"65vh"}
            data={gdFilterDataList || [{}]}
          />
        </Paper>
        <CSVLink
          ref={this.csvLink}
          data={csvExport.data}
          className="hidden"
          filename={csvExport.fileName}
          target="_self"
        />
      </div>
    );
  }
}

const mapStateToProps = () =>
  createStructuredSelector({
    searchText: selectors.getSearchText(),
    gdSchoolList: loginSelectors.getSchoolList(),
    gdStudentList: selectors.getGdStudentList(),
    gdGradingTaskList: selectors.getGdGradingTask(),
    gdSelectedSchool: selectors.getGdSelectedSchool(),
    gdTermList: selectors.getGdTerm(),
    gdFilterDataList: selectors.getGdFilterDataList(),
    gdSelectedTerm: selectors.getGdSelectedTerm(),
    gdSelectedTask: selectors.getGdSelectedTask(),
    userData: loginSelectors.getLoginData(),
  });
const mapDispatchToProps = (dispatch) => ({
  loadGdSchoolList: (req) => dispatch(actions.loadGdSchoolList(req)),
  loadGdTerm: (req) => dispatch(actions.loadGdTerm(req)),
  loadGdGradingTask: (req) => dispatch(actions.loadGdGradingTask(req)),
  loadGdStudentList: (req) => dispatch(actions.loadGdStudentList(req)),
  loadGdProgressGrade: (req) => dispatch(actions.loadGdProgressGrade(req)),
  setGdProgressGrade: (req) => dispatch(actions.setGdProgressGrade(req)),
  setGdGradingTask: (val) => dispatch(actions.setGdGradingTask(val)),
  setGdSelectedSchool: (obj) => dispatch(actions.setGdSelectedSchool(obj)),
  setGdSelectedTerm: (obj) => dispatch(actions.setGdSelectedTerm(obj)),
  setGdFilterData: (data) => dispatch(actions.setGdFilterData(data)),
  setGdSelectedTask: (obj) => dispatch(actions.setGdSelectedTask(obj)),
  setSearchText: (obj) => dispatch(actions.setSearchText(obj)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Honor_Roll);
