import { createSelector } from "reselect";
import * as selectors from "../../../redux/selectors";

export const getGdSchoolList = () =>
  createSelector(selectors.grade_distributionState, (grade_distributionState) =>
    grade_distributionState.get("gdSchoolList").toJS(),
  );

export const getGdSelectedSchool = () =>
  createSelector(selectors.grade_distributionState, (grade_distributionState) =>
    grade_distributionState.get("gdSelectedSchool").toJS(),
  );

export const getGdGradingTask = () =>
  createSelector(selectors.grade_distributionState, (grade_distributionState) =>
    grade_distributionState.get("gdGradingTaskList").toJS(),
  );

export const getGdSelectedTask = () =>
  createSelector(selectors.grade_distributionState, (grade_distributionState) =>
    grade_distributionState.get("gdSelectedTask").toJS(),
  );

export const getGdTerm = () =>
  createSelector(selectors.grade_distributionState, (grade_distributionState) =>
    grade_distributionState.get("gdTermList").toJS(),
  );

export const getGdSelectedTerm = () =>
  createSelector(selectors.grade_distributionState, (grade_distributionState) =>
    grade_distributionState.get("gdSelectedTerm").toJS(),
  );

export const getGdStudentList = () =>
  createSelector(selectors.grade_distributionState, (grade_distributionState) =>
    grade_distributionState.get("gdStudentList").toJS(),
  );

export const getGdFilterDataList = () =>
  createSelector(selectors.grade_distributionState, (grade_distributionState) =>
    grade_distributionState.get("gdFilterDataList").toJS(),
  );

export const getGdProgressGrade = () =>
  createSelector(selectors.grade_distributionState, (grade_distributionState) =>
    grade_distributionState.get("gdProgressGradeList").toJS(),
  );

export const getSearchText = () =>
  createSelector(selectors.grade_distributionState, (grade_distributionState) =>
    grade_distributionState.get("searchText"),
  );
