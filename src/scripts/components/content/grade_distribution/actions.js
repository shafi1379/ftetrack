export const SET_GD_SCHOOL_LIST = "SET_GD_SCHOOL_LIST";
export const setGdSchoolList = (gdSchoolList) => {
  return { type: SET_GD_SCHOOL_LIST, gdSchoolList };
};

export const LOAD_GD_SCHOOL_LIST = "LOAD_GD_SCHOOL_LIST";
export const loadGdSchoolList = (req) => {
  return { type: LOAD_GD_SCHOOL_LIST, req };
};

export const SET_GD_TERM = "SET_GD_TERM";
export const setGdTerm = (gdTermList) => {
  return { type: SET_GD_TERM, gdTermList };
};

export const LOAD_GD_TERM = "LOAD_GD_TERM";
export const loadGdTerm = (req) => {
  return { type: LOAD_GD_TERM, req };
};

export const SET_GD_GRADING_TASK = "SET_GD_GRADING_TASK";
export const setGdGradingTask = (gdGradingTaskList) => {
  return { type: SET_GD_GRADING_TASK, gdGradingTaskList };
};

export const LOAD_GD_GRADING_TASK = "LOAD_GD_GRADING_TASK";
export const loadGdGradingTask = (req) => {
  return { type: LOAD_GD_GRADING_TASK, req };
};

export const SET_GD_STUDENT_LIST = "SET_GD_STUDENT_LIST";
export const setGdStudentList = (gdStudentList) => {
  return { type: SET_GD_STUDENT_LIST, gdStudentList };
};

export const LOAD_GD_STUDENT_LIST = "LOAD_GD_STUDENT_LIST";
export const loadGdStudentList = (req) => {
  return { type: LOAD_GD_STUDENT_LIST, req };
};

export const SET_GD_PROGRESS_GRADE = "SET_GD_PROGRESS_GRADE";
export const setGdProgressGrade = (gdProgressGradeList) => {
  return { type: SET_GD_PROGRESS_GRADE, gdProgressGradeList };
};

export const LOAD_GD_PROGRESS_GRADE = "LOAD_GD_PROGRESS_GRADE";
export const loadGdProgressGrade = (req) => {
  return { type: LOAD_GD_PROGRESS_GRADE, req };
};

export const SET_GD_FILTER_DATA = "SET_GD_FILTER_DATA";
export const setGdFilterData = (gdFilterDataList) => {
  return { type: SET_GD_FILTER_DATA, gdFilterDataList };
};

export const SET_GD_SELECTED_SCHOOL = "SET_GD_SELECTED_SCHOOL";
export const setGdSelectedSchool = (gdSelectedSchool) => {
  return { type: SET_GD_SELECTED_SCHOOL, gdSelectedSchool };
};

export const SET_GD_SELECTED_TERM = "SET_GD_SELECTED_TERM";
export const setGdSelectedTerm = (gdSelectedTerm) => {
  return { type: SET_GD_SELECTED_TERM, gdSelectedTerm };
};

export const SET_GD_SELECTED_TASK = "SET_GD_SELECTED_TASK";
export const setGdSelectedTask = (gdSelectedTask) => {
  return { type: SET_GD_SELECTED_TASK, gdSelectedTask };
};

export const SET_SEARCH_TEXT = "SET_SEARCH_TEXT";
export const setSearchText = (text) => {
  return { type: SET_SEARCH_TEXT, text };
};
