import {
  SET_GD_FILTER_DATA,
  SET_GD_GRADING_TASK,
  SET_GD_SCHOOL_LIST,
  SET_GD_TERM,
  SET_GD_SELECTED_TASK,
  SET_GD_SELECTED_SCHOOL,
  SET_GD_SELECTED_TERM,
  SET_GD_STUDENT_LIST,
  SET_GD_PROGRESS_GRADE,
  SET_SEARCH_TEXT,
} from "./actions";
import { fromJS } from "immutable";

const initialState = fromJS({
  gdSchoolList: [],
  gdFailingGradeList: [],
  gdSelectedSchool: { label: "Select a School ...", value: -1 },
  gdSelectedTerm: {},
  gdSelectedTask: [],
  gdFilterDataList: [],
  gdGradingTaskList: [],
  gdProgressGradeList: [],
  gdTermList: [],
  gdStudentList: [],
  searchText: null,
});

const progress_grade = (state = initialState, action) => {
  switch (action.type) {
    case SET_GD_SCHOOL_LIST:
      return state.set("gdSchoolList", fromJS([...action.gdSchoolList]));
    case SET_GD_STUDENT_LIST:
      return state.set("gdStudentList", fromJS([...action.gdStudentList]));
    case SET_GD_TERM:
      return state.set("gdTermList", fromJS([...action.gdTermList]));
    case SET_GD_FILTER_DATA:
      return state.set(
        "gdFilterDataList",
        fromJS([...action.gdFilterDataList]),
      );
    case SET_GD_GRADING_TASK:
      return state.set(
        "gdGradingTaskList",
        fromJS([...action.gdGradingTaskList]),
      );
    case SET_GD_SELECTED_TASK:
      return state.set("gdSelectedTask", fromJS([...action.gdSelectedTask]));
    case SET_GD_SELECTED_TERM:
      return state.set("gdSelectedTerm", fromJS({ ...action.gdSelectedTerm }));
    case SET_GD_SELECTED_SCHOOL:
      return state.set(
        "gdSelectedSchool",
        fromJS({ ...action.gdSelectedSchool }),
      );
    case SET_GD_PROGRESS_GRADE:
      return state.set(
        "gdProgressGradeList",
        fromJS([...action.gdProgressGradeList]),
      );
    case SET_SEARCH_TEXT:
      return state.set("searchText", action.text);
    default:
      return state;
  }
};

export default progress_grade;
