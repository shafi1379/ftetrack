import {
  LOAD_GD_GRADING_TASK,
  LOAD_GD_SCHOOL_LIST,
  LOAD_GD_TERM,
  setGdGradingTask,
  setGdFilterData,
  setGdSchoolList,
  setGdTerm,
  setGdSelectedSchool,
  setGdSelectedTerm,
  LOAD_GD_STUDENT_LIST,
  setGdStudentList,
  LOAD_GD_PROGRESS_GRADE,
  setGdProgressGrade,
} from "./actions";

import { select, all, call, fork, put, takeLatest } from "redux-saga/effects";
import { setProgressingValue } from "../../../app/actions";
import * as Api from "../../../services/api";
import { API_URLS } from "../../../utilities/constants";

function* loadGdSchoolListRequested(request) {
  try {
    const { gdSchoolList } = (yield select((s) =>
      s.get("grade_distribution"),
    )).toJS();
    if (!gdSchoolList.length) {
      yield put(setProgressingValue(true));
      const response = yield call(
        Api.doPostRequest,
        API_URLS.schools,
        request.req,
      );
      if (response) {
        const gdSchoolList = response.map((res) => ({
          ...res,
          label: res.schoolname,
          value: res.school,
        }));
        yield put(setGdSchoolList(gdSchoolList));
        yield put(setGdSelectedSchool(gdSchoolList[0]));
        yield put(setProgressingValue(false));
      }
    }
  } catch (error) {
    console.error(error);
  }
}

function* gdSchoolListListener() {
  yield takeLatest(LOAD_GD_SCHOOL_LIST, loadGdSchoolListRequested);
}

function* loadGdStudentListRequested(request) {
  try {
    yield put(setProgressingValue(true));
    const response = yield call(
      Api.doPostRequest,
      API_URLS.gradingDistribution,
      request.req,
    );
    if (response) {
      yield put(setGdStudentList(response));
      yield put(setGdFilterData(response));
      yield put(setProgressingValue(false));
    }
  } catch (error) {
    console.error(error);
  }
}

function* gdStudentListListener() {
  yield takeLatest(LOAD_GD_STUDENT_LIST, loadGdStudentListRequested);
}

function* loadGdGradingTaskRequested(request) {
  try {
    yield put(setProgressingValue(true));
    const response = yield call(
      Api.doPostRequest,
      API_URLS.gradingTasks,
      request.req,
    );
    if (response) {
      const gdGradingTask = response.map((res) => ({
        ...res,
        label: res.taskName,
        value: res.taskID,
      }));
      yield put(setGdGradingTask(gdGradingTask));
      // yield put(setGdSelectedTask(gdGradingTask[0]));
      yield put(setProgressingValue(false));
    }
  } catch (error) {
    console.error(error);
  }
}

function* gdGradingTaskListener() {
  yield takeLatest(LOAD_GD_GRADING_TASK, loadGdGradingTaskRequested);
}

function* loadGdTermRequested(request) {
  try {
    yield put(setProgressingValue(true));
    const response = yield call(
      Api.doPostRequest,
      API_URLS.gradingTerms,
      request.req,
    );
    if (response) {
      const gdTermList = response.map((res) => ({
        ...res,
        label: res.termName,
        value: res.termID,
      }));
      yield put(setGdTerm(gdTermList));
      yield put(setGdSelectedTerm(gdTermList[0]));
      yield put(setProgressingValue(false));
    }
  } catch (error) {
    console.error(error);
  }
}

function* gdGdTermListener() {
  yield takeLatest(LOAD_GD_TERM, loadGdTermRequested);
}

function* loadGdProgressGradeRequested(request) {
  try {
    yield put(setProgressingValue(true));
    const response = yield call(
      Api.doPostRequest,
      API_URLS.progressGrades,
      request.req,
    );
    if (response) {
      yield put(setGdProgressGrade(response));
      yield put(setProgressingValue(false));
    }
  } catch (error) {
    console.error(error);
  }
}

function* gdProgressGradeListener() {
  yield takeLatest(LOAD_GD_PROGRESS_GRADE, loadGdProgressGradeRequested);
}

export default function* root() {
  yield all([
    fork(gdGdTermListener),
    fork(gdGradingTaskListener),
    fork(gdSchoolListListener),
    fork(gdStudentListListener),
    fork(gdProgressGradeListener),
  ]);
}
