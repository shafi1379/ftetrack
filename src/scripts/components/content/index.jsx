import React, { Component } from "react";
import Paper from "@material-ui/core/Paper";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import { withRouter } from "react-router-dom";
import { BrowserRouter as Router, Switch, Link } from "react-router-dom";
import RouteWithSubRoutes from "../generic/subRoutes";
import LoadingIndicator from "../generic/loadingIndicator";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";
import * as mainSelectors from "../../app/selectors";
import { routes } from "./mainRoutes";

class RouteConfig extends Component {
  state = { value: "summary" };

  componentWillMount() {
    /* if (window.location.host === "www.ftetrack.net") {
      window.location.href = "https://www.ftetrack.net/abc/index.html";
    } else { */
    this.props.history.replace("/abc/summary");
    /*  } */
  }

  handleCallToRouter = (event, newValue) => {
    this.setState({ value: newValue });
  };

  render() {
    const { value } = this.state;
    const { progressing } = this.props;
    return (
      <div className="app-content">
        {progressing && <LoadingIndicator />}
        <Router basename="/abc">
          <Paper className="primary-tab-paper" square>
            <Tabs
              value={value}
              indicatorColor="primary"
              textColor="primary"
              onChange={this.handleCallToRouter.bind(value)}
              centered
            >
              <Tab
                label="Summary"
                className="minWidth90"
                value="summary"
                to="/summary"
                component={Link}
              />
              <Tab
                label="Attendance"
                className="minWidth90"
                value="attendance"
                to="/attendance"
                component={Link}
              />
              <Tab
                label="Grades"
                className="minWidth90"
                value="grades"
                to="/grades"
                component={Link}
              />
              <Tab
                label="Behavior"
                className="minWidth90"
                value="behaviour"
                to="/behaviour"
                component={Link}
              />
              <Tab
                label="Cohort Graduation"
                className="minWidth90"
                value="cohort_graduation"
                to="/cohort_graduation"
                component={Link}
              />
              <Tab
                label="Enrollment"
                className="minWidth90"
                value="enrollment"
                to="/enrollment"
                component={Link}
              />
              <Tab
                label="EWS"
                className="minWidth60"
                value="ews"
                to="/ews"
                component={Link}
              />
              <Tab
                label="Progress Grade"
                className="minWidth90"
                value="progress_grade"
                to="/progress_grade"
                component={Link}
              />
              <Tab
                label="Honor Roll"
                className="minWidth90"
                value="honor_roll"
                to="/honor_roll"
                component={Link}
              />
              <Tab
                label="Grade Distribution"
                className="minWidth90"
                value="grade_distribution"
                to="/grade_distribution"
                component={Link}
              />
              <Tab
                label="PBIS"
                className="minWidth60"
                value="pbis"
                to="/pbis"
                component={Link}
              />
              <Tab
                label="CTAE"
                className="minWidth60"
                value="ctae"
                to="/ctae"
                component={Link}
              />
            </Tabs>
          </Paper>
          <Switch>
            {routes.map((route, i) => (
              <RouteWithSubRoutes
                key={i}
                {...route}
                handleCallToRouter={this.handleCallToRouter}
              />
            ))}
          </Switch>
        </Router>
      </div>
    );
  }
}

const mapStateToProps = () =>
  createStructuredSelector({
    progressing: mainSelectors.getProgressingValue(),
  });

export default connect(mapStateToProps, null)(withRouter(RouteConfig));
