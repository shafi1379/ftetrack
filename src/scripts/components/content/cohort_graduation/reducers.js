import { fromJS } from "immutable";
import {
  SET_RATE_PROJECTION,
  SET_STUDENT_PROJECTION,
  SET_STUDENT_DETAILS,
  SET_SELECTED_FILTERS,
  SET_FILTER_WITH_G,
  SET_GRADEPROGRESS_LIST,
  SET_COHORT_YEARS,
  SET_SELECTED_COHORT_YEARS,
  SET_YEARS_CREDITS_MAPPING,
} from "./actions";

const initialState = fromJS({
  rateProjectionList: [],
  studentProjectionList: [],
  studentDetailsList: [],
  selectedFilters: {},
  filterWithGList: [],
  gradeProgressList: [],
  cohortYearsList: [],
  selectedCohortYear: {},
  yearsCreditsMapping: {},
});

const attendance = (state = initialState, action) => {
  switch (action.type) {
    case SET_RATE_PROJECTION:
      return state.set(
        "rateProjectionList",
        fromJS([...action.projectionList]),
      );
    case SET_STUDENT_PROJECTION:
      return state.set(
        "studentProjectionList",
        fromJS([...action.studentProjectionList]),
      );
    case SET_STUDENT_DETAILS:
      return state.set(
        "studentDetailsList",
        fromJS([...action.studentDetailsList]),
      );
    case SET_SELECTED_FILTERS:
      return state.set("selectedFilters", fromJS({ ...action.filters }));
    case SET_FILTER_WITH_G:
      return state.set("filterWithGList", fromJS([...action.filterWithGList]));
    case SET_GRADEPROGRESS_LIST:
      return state.set("gradeProgressList", fromJS([...action.gradeProgress]));
    case SET_COHORT_YEARS:
      return state.set("cohortYearsList", fromJS([...action.cohortYears]));
    case SET_SELECTED_COHORT_YEARS:
      return state.set("selectedCohortYear", fromJS(action.cohortYear));
    case SET_YEARS_CREDITS_MAPPING:
      return state.set("yearsCreditsMapping", fromJS(action.mapping));
    default:
      return state;
  }
};

export default attendance;
