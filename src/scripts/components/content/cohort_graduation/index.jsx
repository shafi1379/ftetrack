import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";
import * as selectors from "./selectors";
import * as actions from "./actions";
import * as ewsSelectors from "../ews/selectors";
import * as ewsActions from "../ews/actions";
import AppBar from "@material-ui/core/AppBar";
import Button from "@material-ui/core/Button";
import Checkbox from "@material-ui/core/Checkbox";
import CohortGraduationToolbar from "./containers/toolbar";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Paper from "@material-ui/core/Paper";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import DataTable from "react-data-table-component";
import Icon from "@material-ui/core/Icon";
import * as TableConfig from "../../../utilities/tableConfig";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import MaterialIcon from "../../generic/materialIcon";
import { CSVLink } from "react-csv";
import * as Utilities from "../../../utilities/utilities";
import { SUBJECTS } from "../../../utilities/constants";
import ExpandedComponent from "./containers/ExpandedComponent";
import StudentDetailsDialog from "../../generic/details_dialog";

const subjectColumnWidths = {
  subject: "300px",
  schoolYear: "120px",
  term: "120px",
  grade: "120px",
  stateCode: "110px",
  courseName: "200px",
  score: "120px",
  creditsAttempted: "110px",
  creditsEarned: "120px",
};

const columnWidths = {
  schoolYear: "80px",
  term: "80px",
  grade: "80px",
  stateCode: "110px",
  courseName: "200px",
  score: "80px",
  creditsAttempted: "110px",
  creditsEarned: "80px",
  LA: "60px",
  MA: "60px",
  SC: "60px",
  SS: "60px",
  FL: "60px",
  HLPE: "60px",
  CTAE: "60px",
  ROTC: "60px",
  Rigor: "60px",
};

class CohortGraduation extends Component {
  csvLink = React.createRef();
  state = {
    openStudentDetailsDialog: false,
    rate_projection: TableConfig.cohortGraduation_rate_projection.map(
      (dash, index) => {
        if (
          dash.selector !== "schoolID" &&
          dash.selector !== "schoolName" &&
          dash.selector !== "Graduation Rate"
        ) {
          dash.cell = (row) => {
            let cellContent = row[dash.name];
            if (row["schoolName"] !== "System Total") {
              cellContent = (
                <span
                  style={{
                    color: "#683598",
                    textDecoration: "underline",
                    cursor: "pointer",
                  }}
                  onClick={this.handleAction.bind(this, row, dash.name)}
                >
                  {row[dash.name]}
                </span>
              );
            }
            return cellContent;
          };
        }
        return dash;
      },
    ),
    student_projection: TableConfig.cohortGraduationStudentProjection.map(
      (dash, index) => {
        if (dash.name === "Student#") {
          dash.cell = (row) => (
            <span
              style={{
                color: "#683598",
                textDecoration: "underline",
                cursor: "pointer",
              }}
              onClick={this.handleAction.bind(this, row, "StudentTable")}
            >
              {row.studentNumber}
            </span>
          );
        }
        return dash;
      },
    ),
    currentView: "initial_table_view",
    initRowData: {},
    studentsRowData: {},
    csvExport: {
      data: [],
      fileName: "cohort.csv",
    },
    showBySubject: false,
    gradeWithDiplomaG: false,
    schoolID: null,
    schoolName: null,
    personID: null,
  };

  handleAction = (row, cellName) => {
    if (cellName !== "StudentTable") {
      row.RecordType = cellName;
      this.setState({
        initRowData: row,
        currentView: "students_table_view",
        schoolID: row.schoolID,
        schoolName: row.schoolName,
        personID: null,
      });
      const selectedFilters = this.props.selectedFilters;
      this.props.loadRateProjection({
        action: "rowClick",
        school: row.schoolID,
        CohortYear: selectedFilters.year,
        TotalCredits: selectedFilters.total,
        LACredits: selectedFilters.la,
        MACredits: selectedFilters.ma,
        SCCredits: selectedFilters.sc,
        SSCredits: selectedFilters.ss,
        ElectiveCredits: selectedFilters.ctae,
        HealthCredits: selectedFilters.health_pe,
        RecordType: row.RecordType,
        SPEDOnly: selectedFilters.SPEDOnly
      });
    } else {
      this.setState({
        studentsRowData: row,
        currentView: "student_detail_view",
        personID: row.personID,
      });
      this.props.loadStudentDetails({ personID: row.personID });
    }
  };

  downloadCSV = () => {
    const currentTime = `_${new Date().getTime()}`;
    const currentView = this.state.currentView;
    const columns = {
      initial_table_view: {
        School: "schoolName",
        "On Track": "On Track",
        "At Risk": "At Risk",
        "Valid Withdrawals": "Valid Withdrawals",
        Dropout: "Dropout",
        Total: "Total",
        "Graduation Rate": "Graduation Rate",
      },
      students_table_view: {
        "Student Name": "studentName",
        "Student#": "studentNumber",
        Grade: "grade",
        "9th Grade Date": "grade9Date",
        SPED: "sped",
        EL: "EL",
        "With Code/Date": "withCodeDate",
        "Diploma Type": "diplomaType",
        Total: "Total",
        LA: "LA",
        MA: "MA",
        SC: "SC",
        SS: "SS",
        FL: "FL",
        "Hlth/PE": "HLPE",
        CTAE: "CTAE",
        Rigor: "Rigor",
        Reason: "Reason",
      },
      student_detail_view: {
        Year: "schoolYear",
        Term: "term",
        Grade: "grade",
        Title: "courseName",
        Score: "score",
        Attempted: "creditsAttempted",
        Earned: "creditsEarned",
        "Diploma Type": "diplomaType",
        Total: "Total",
        LA: "LA",
        MA: "MA",
        SC: "SC",
        SS: "SS",
        FL: "FL",
        HLPE: "HLPE",
        CTAE: "CTAE",
        ROTC: "ROTC",
        Rigor: "Rigor",
      },
    };
    const datas = {};
    if (currentView === "initial_table_view") {
      datas[currentView] = this.props.rateProjectionList;
    } else if (currentView === "students_table_view") {
      datas[currentView] = this.props.studentProjectionList;
    } else {
      datas[currentView] = this.props.studentDetailsList;
    }
    const data = Utilities.generateExcelData(
      datas[currentView] || [],
      columns[currentView] || {},
    );
    let excelFileName = "Cohort_Projections";
    if (currentView === "initial_table_view") {
      data.unshift([`Cohort Graduation Rate Projection`]);
    } else if (currentView === "students_table_view") {
      excelFileName = "Cohort_Analyzer";
      const initRowData = this.state.initRowData;
      data.unshift([
        `School: ${initRowData.schoolName}, Filter: ${initRowData.RecordType} Students`,
      ]);
    } else {
      data.unshift([`Cohort Graduation Rate Projection`]);
    }
    this.setState({
      csvExport: { data, fileName: `${excelFileName + currentTime}.csv` },
    });
    setTimeout(() => {
      this.csvLink.current.link.click();
    });
  };

  handleShowBySubject = () =>
    this.setState({ showBySubject: !this.state.showBySubject });

  handleGradeWithDiplomaG = () => {
    this.setState({ gradeWithDiplomaG: !this.state.gradeWithDiplomaG });
  };

  handleProgressReport = () => {
    const request = {
      school: this.state.schoolID,
      schoolName: this.state.schoolName,
      CohortYear: this.props.selectedFilters.year,
    };
    if (this.state.personID) {
      request["personID"] = this.state.personID;
    }
    this.props.loadGradeProgressList(request);
  };

  showCurrentGrade = () => {
    this.props.loadStudentGrade({
      personID: "" + this.state.studentsRowData.personID,
    });
    this.setState({
      openStudentDetailsDialog: !this.state.openStudentDetailsDialog,
    });
  };

  render() {
    const {
      rate_projection,
      student_projection,
      currentView,
      initRowData,
      studentsRowData,
      csvExport,
      showBySubject,
      gradeWithDiplomaG,
      openStudentDetailsDialog,
    } = this.state;

    const { rateProjectionList, studentDetailsList } = this.props;
    let subjectColumns = [...TableConfig.cohortGraduationStudentDetails].filter(
      (obj) => !Object.keys(SUBJECTS).includes(obj.selector),
    );
    subjectColumns = [
      { name: "Subjects", selector: "subject", width: "200px", sortable: true },
      ...subjectColumns,
    ];
    /* .map((obj) => {
      obj["width"] = "160px";
      if (obj.selector == "courseName") {
        //obj["width"] = "260px";
      }
      return obj;
    }); */

    let studentProjectionList = this.props.studentProjectionList;
    if (studentProjectionList.length > 0) {
      studentProjectionList = studentProjectionList.map((obj) => {
        obj["Score"] = (
          <div style={{ display: "inline-flex" }}>
            <div style={{ minWidth: "55px" }}>{obj["Total"]}</div>
            <div style={{ minWidth: "45px" }}>{obj["LA"]}</div>
            <div style={{ minWidth: "45px" }}>{obj["MA"]}</div>
            <div style={{ minWidth: "45px" }}>{obj["SC"]}</div>
            <div style={{ minWidth: "45px" }}>{obj["SS"]}</div>
            <div style={{ minWidth: "45px" }}>{obj["FL"]}</div>
            <div style={{ minWidth: "55px" }}>{obj["HLPE"]}</div>
            <div style={{ minWidth: "55px" }}>{obj["CTAE"]}</div>
            <div style={{ minWidth: "65px" }}>{obj["Rigor"]}</div>
          </div>
        );
        return obj;
      });
    }

    let expandedData = {};
    let oldYearTermGrade = "";
    if (this.state.showBySubject) {
      studentDetailsList.forEach((obj) => {
        let _obj = { ...obj };
        Object.keys(SUBJECTS).forEach((_key) => {
          delete _obj[_key];
        });
        if (Object.keys(expandedData).includes(_obj["subject"])) {
          expandedData[_obj["subject"]]["creditsAttempted"] += Number(
            _obj["creditsAttempted"],
          );
          expandedData[_obj["subject"]]["creditsEarned"] += Number(
            _obj["creditsEarned"],
          );
          expandedData[_obj["subject"]]["children"].push({ ..._obj });
        } else {
          expandedData[_obj["subject"]] = {
            subject: _obj["subject"],
            children: [{ ..._obj }],
            creditsAttempted: Number(_obj["creditsAttempted"]),
            creditsEarned: Number(_obj["creditsEarned"]),
          };
        }
      });
    } else {
      studentDetailsList.forEach((obj, index) => {
        let _obj = { ...obj };
        if (Object.keys(expandedData).includes(_obj["personID"] + "")) {
          if (
            oldYearTermGrade ===
            _obj["schoolYear"] + "-" + _obj["term"] + _obj["grade"]
          ) {
            _obj["schoolYear"] = "";
            _obj["term"] = "";
          } else {
            oldYearTermGrade =
              _obj["schoolYear"] + "-" + _obj["term"] + _obj["grade"];
          }
          expandedData[_obj["personID"]]["children"].push({ ..._obj });
        } else {
          oldYearTermGrade =
            _obj["schoolYear"] + "-" + _obj["term"] + _obj["grade"];
          expandedData[_obj["personID"]] = {
            schoolYear: _obj.personID,
            children: [{ ..._obj }],
          };
        }
      });
    }

    const expandedArray = Object.keys(expandedData).map((key) => {
      return expandedData[key];
    });

    return (
      <div className="cohort-graduation">
        <CohortGraduationToolbar
          onViewReport={() =>
            this.setState({ currentView: "initial_table_view" })
          }
        />
        <Paper style={{ padding: 2 }}>
          <AppBar style={{ position: "static" }}>
            <Toolbar style={{ minHeight: 30 }}>
              <Typography
                style={{ flexGrow: 1 }}
                className="capitalize"
                variant="button"
              >
                Cohort Graduation Rate Projection
              </Typography>
              <Tooltip title="Export to CSV" aria-label="csv-export-tooltip">
                <IconButton
                  style={{ color: "#ffffff", padding: 5 }}
                  onClick={this.downloadCSV}
                  className="csv-download-btn"
                  aria-label="csv-export"
                >
                  <MaterialIcon icon="cloud_download" />
                </IconButton>
              </Tooltip>
            </Toolbar>
          </AppBar>
          {currentView === "initial_table_view" && (
            <DataTable
              noHeader={true}
              columns={rate_projection}
              data={(rateProjectionList.length && rateProjectionList) || [{}]}
              customStyles={TableConfig.customStyles}
            />
          )}
          {currentView === "students_table_view" && (
            <>
              <div className="cohort-table-toolbar">
                <div className="table-title-section">
                  <span
                    style={{
                      fontWeight: "bold",
                      color: "#683598",
                      textDecoration: "underline",
                      cursor: "pointer",
                    }}
                    onClick={() =>
                      this.setState({
                        currentView: "initial_table_view",
                        personID: "",
                      })
                    }
                  >
                    Back
                  </span>
                  <Typography
                    style={{ marginLeft: 15, display: "inline-block" }}
                    variant="subtitle2"
                  >
                    School: {initRowData.schoolName}, Filter:{" "}
                    {initRowData.RecordType}
                  </Typography>
                </div>
                <div className="right-toolbar-section">
                  <FormControlLabel
                    className="cohort-checkbox"
                    control={
                      <Checkbox
                        checked={gradeWithDiplomaG}
                        onChange={this.handleGradeWithDiplomaG}
                        name="gradeWithDiplomaG"
                        color="primary"
                      />
                    }
                    label="Only Grades with Diploma G"
                  />
                  <Button
                    className="capitalize"
                    variant="contained"
                    size="small"
                    startIcon={<Icon>picture_as_pdf</Icon>}
                    onClick={this.handleProgressReport}
                  >
                    Progress Report
                  </Button>
                </div>
              </div>
              <hr />
              <DataTable
                noHeader={true}
                columns={student_projection}
                data={
                  (gradeWithDiplomaG
                    ? studentProjectionList.filter((obj) => obj.endDate !== "")
                    : studentProjectionList) || [{}]
                }
                customStyles={TableConfig.customStyles}
              />
            </>
          )}
          {currentView === "student_detail_view" && (
            <>
              <div className="cohort-table-toolbar">
                <div className="table-title-section">
                  <div>
                    <span
                      style={{
                        fontWeight: "bold",
                        color: "#683598",
                        textDecoration: "underline",
                        cursor: "pointer",
                      }}
                      onClick={() =>
                        this.setState({
                          currentView: "students_table_view",
                          personID: "",
                        })
                      }
                    >
                      Back
                    </span>
                    <Typography
                      style={{ marginLeft: 15, display: "inline-block" }}
                      variant="subtitle2"
                    >
                      Transcript Course for {studentsRowData.studentName},
                      School: {initRowData.schoolName}
                    </Typography>
                  </div>
                  {studentsRowData.Reason !== "" &&
                    studentsRowData.Reason !== null && (
                      <div style={{ paddingLeft: 50 }}>
                        <Typography variant="body2">
                          <strong>Reasons: </strong>
                          {studentsRowData.Reason}
                        </Typography>
                      </div>
                    )}
                </div>
                <div className="right-toolbar-section">
                  <FormControlLabel
                    className="cohort-checkbox"
                    control={
                      <Checkbox
                        checked={showBySubject}
                        onChange={this.handleShowBySubject}
                        name="showBySubject"
                        color="primary"
                      />
                    }
                    label="Show By Subject"
                  />
                  <Button
                    className="capitalize mr-rt10"
                    variant="contained"
                    size="small"
                    startIcon={<Icon>picture_as_pdf</Icon>}
                    onClick={this.handleProgressReport}
                  >
                    Progress Report
                  </Button>
                  <Button
                    className="capitalize"
                    variant="contained"
                    size="small"
                    onClick={this.showCurrentGrade}
                  >
                    Show Current Grades
                  </Button>
                </div>
              </div>
              <hr />
              <DataTable
                noHeader={true}
                striped={true}
                columns={
                  this.state.showBySubject
                    ? [...subjectColumns].map((obj) => {
                        obj["width"] = subjectColumnWidths[obj.selector];
                        // delete obj["width"];
                        return obj;
                      })
                    : [...TableConfig.cohortGraduationStudentDetails].map(
                        (obj) => {
                          obj["width"] = columnWidths[obj.selector];
                          return obj;
                        },
                      )
                }
                fixedHeader={true}
                fixedHeaderScrollHeight={"55vh"}
                expandableRows={true}
                expandableRowExpanded={(row) => true}
                expandableIcon={{
                  collapsed: (
                    <MaterialIcon icon="folder" style={{ color: "#3f51b5" }} />
                  ),
                  expanded: (
                    <MaterialIcon
                      icon="folder_open"
                      style={{ color: "#3f51b5" }}
                    />
                  ),
                }}
                keyField="cohort_graduation_0"
                expandableRowsComponent={<ExpandedComponent />}
                data={expandedArray || [{}]}
                customStyles={TableConfig.customStyles}
              />
            </>
          )}
        </Paper>
        <CSVLink
          ref={this.csvLink}
          data={csvExport.data}
          className="hidden"
          filename={csvExport.fileName}
          target="_self"
        />
        {Object.keys(this.props.studentsDetailsList).length > 0 && (
          <StudentDetailsDialog
            open={openStudentDetailsDialog}
            value={2}
            openStudentsDetailsInfo={() =>
              this.setState({
                openStudentDetailsDialog: !openStudentDetailsDialog,
              })
            }
            row={studentsRowData}
            data={this.props.studentsDetailsList}
          />
        )}
      </div>
    );
  }
}

const mapStateToProps = () =>
  createStructuredSelector({
    rateProjectionList: selectors.getRateProjection(),
    studentProjectionList: selectors.getStudentProjection(),
    studentDetailsList: selectors.getStudentDetails(),
    studentsDetailsList: ewsSelectors.getEwsStudentsDetails(),
    selectedFilters: selectors.getSelectedFilters(),
    filterWithGList: selectors.getFilterWithG(),
    gradeProgressList: selectors.getGradeProgressList(),
  });
const mapDispatchToProps = (dispatch) => ({
  loadRateProjection: (request) =>
    dispatch(actions.loadRateProjection(request)),
  loadStudentDetails: (request) =>
    dispatch(actions.loadStudentDetails(request)),
  setFilterWithG: (list) => dispatch(actions.setFilterWithG(list)),
  loadStudentGrade: (request) =>
    dispatch(ewsActions.loadEwsStudentsDetails(request)),
  loadGradeProgressList: (request) =>
    dispatch(actions.loadGradeProgressList(request)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withRouter(CohortGraduation));
