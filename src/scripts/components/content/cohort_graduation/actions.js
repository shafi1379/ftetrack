export const LOAD_RATE_PROJECTION = "LOAD_RATE_PROJECTION";
export const loadRateProjection = (request) => ({
  type: LOAD_RATE_PROJECTION,
  request,
});

export const LOAD_COHORT_YEAR = "LOAD_COHORT_YEAR";
export const loadCohortYear = (request) => ({
  type: LOAD_COHORT_YEAR,
  request,
});

export const SET_COHORT_YEARS = "SET_COHORT_YEARS";
export const setCohortYears = (cohortYears) => ({
  type: SET_COHORT_YEARS,
  cohortYears,
});

export const SET_YEARS_CREDITS_MAPPING = "SET_YEARS_CREDITS_MAPPING";
export const setYearsCreditsMapping = (mapping) => ({
  type: SET_YEARS_CREDITS_MAPPING,
  mapping,
});

export const SET_SELECTED_COHORT_YEARS = "SET_SELECTED_COHORT_YEARS";
export const setSelectedCohortYears = (cohortYear) => ({
  type: SET_SELECTED_COHORT_YEARS,
  cohortYear,
});

export const SET_RATE_PROJECTION = "SET_RATE_PROJECTION";
export const setRateProjection = (projectionList) => ({
  type: SET_RATE_PROJECTION,
  projectionList,
});

export const SET_FILTER_WITH_G = "SET_FILTER_WITH_G";
export const setFilterWithG = (filterWithGList) => ({
  type: SET_FILTER_WITH_G,
  filterWithGList,
});

export const SET_STUDENT_PROJECTION = "SET_STUDENT_PROJECTION";
export const setStudentProjection = (studentProjectionList) => ({
  type: SET_STUDENT_PROJECTION,
  studentProjectionList,
});

export const LOAD_STUDENT_DETAILS = "LOAD_STUDENT_DETAILS";
export const loadStudentDetails = (request) => ({
  type: LOAD_STUDENT_DETAILS,
  request,
});

export const SET_STUDENT_DETAILS = "SET_STUDENT_DETAILS";
export const setStudentDetails = (studentDetailsList) => ({
  type: SET_STUDENT_DETAILS,
  studentDetailsList,
});

export const SET_SELECTED_FILTERS = "SET_SELECTED_FILTERS";
export const setSelectedFilters = (filters) => ({
  type: SET_SELECTED_FILTERS,
  filters,
});

export const LOAD_GRADEPROGRESS_LIST = "LOAD_GRADEPROGRESS_LIST";
export const loadGradeProgressList = (request) => ({
  type: LOAD_GRADEPROGRESS_LIST,
  request,
});

export const SET_GRADEPROGRESS_LIST = "SET_GRADEPROGRESS_LIST";
export const setGradeProgressList = (gradeProgress) => ({
  type: SET_GRADEPROGRESS_LIST,
  gradeProgress,
});
