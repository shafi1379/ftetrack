import { createSelector } from "reselect";
import * as selectors from "../../../redux/selectors";

export const getRateProjection = () =>
  createSelector(selectors.cohort_graduationState, (cohort_graduationState) =>
    cohort_graduationState.get("rateProjectionList").toJS(),
  );

export const getStudentProjection = () =>
  createSelector(selectors.cohort_graduationState, (cohort_graduationState) =>
    cohort_graduationState.get("studentProjectionList").toJS(),
  );

export const getFilterWithG = () =>
  createSelector(selectors.cohort_graduationState, (cohort_graduationState) =>
    cohort_graduationState.get("filterWithGList").toJS(),
  );

export const getStudentDetails = () =>
  createSelector(selectors.cohort_graduationState, (cohort_graduationState) =>
    cohort_graduationState.get("studentDetailsList").toJS(),
  );

export const getSelectedFilters = () =>
  createSelector(selectors.cohort_graduationState, (cohort_graduationState) =>
    cohort_graduationState.get("selectedFilters").toJS(),
  );

export const getGradeProgressList = () =>
  createSelector(selectors.cohort_graduationState, (cohort_graduationState) =>
    cohort_graduationState.get("gradeProgressList").toJS(),
  );

export const getCohortYearsList = () =>
  createSelector(selectors.cohort_graduationState, (cohort_graduationState) =>
    cohort_graduationState.get("cohortYearsList").toJS(),
  );

export const getSelectedCohortYear = () =>
  createSelector(selectors.cohort_graduationState, (cohort_graduationState) =>
    cohort_graduationState.get("selectedCohortYear").toJS(),
  );

export const getYearsCreditsMapping = () =>
  createSelector(selectors.cohort_graduationState, (cohort_graduationState) =>
    cohort_graduationState.get("yearsCreditsMapping").toJS(),
  );
