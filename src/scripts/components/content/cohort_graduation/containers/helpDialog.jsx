import React from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Dialog from "@material-ui/core/Dialog";
import MuiDialogContent from "@material-ui/core/DialogContent";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles((theme) => ({
  appBar: {
    position: "static",
  },
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(0.5),
    color: "#f9f9f9",
    padding: 10,
  },
}));

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

export default function AttendanceHelpDialog(props) {
  const classes = useStyles();
  const handleClose = () => props.openAttendanceInfo();

  return (
    <>
      <Dialog
        open={props.open}
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        aria-describedby="customized-dialog-description"
      >
        <AppBar className={classes.appBar}>
          <Toolbar style={{ minHeight: 45 }}>
            <Typography className="capitalize" variant="button">
              Cohort Graduation Help
            </Typography>
            <IconButton
              aria-label="close"
              className={classes.closeButton}
              onClick={handleClose}
            >
              <CloseIcon />
            </IconButton>
          </Toolbar>
        </AppBar>
        <DialogContent dividers>
          <ul>
            <li>
              <Typography variant="body2" gutterBottom>
                Cohort year is determined based on{" "}
                <strong>9th Grade Entry</strong> date.
              </Typography>
            </li>
            <li>
              <Typography variant="body2" gutterBottom>
                HS Counselors/Administrators can run report on the fly by
                adjusting Credit Requirements in each of Course Subject Areas.
              </Typography>
            </li>
            <li>
              <Typography variant="body2" gutterBottom>
                Report can be used to adjust student schedules mid-year based on
                First Semester grades.
              </Typography>
            </li>
          </ul>
          <Typography variant="subtitle1" gutterBottom>
            Required Courses/Grades
          </Typography>
          <Typography variant="subtitle2" gutterBottom>
            9th Grade Lit and Comp (1.0)
          </Typography>
          <Typography variant="body2" gutterBottom>
            23.06100 - Ninth Grade Literature and Composition
          </Typography>
          <Typography variant="subtitle2" gutterBottom>
            Biology I (1.0)
          </Typography>
          <Typography variant="body2" gutterBottom>
            26.01200 - Biology I, 26.01400 - AP Biology, 26.01800 - IB Biology,
            Yr One
          </Typography>
          <Typography variant="subtitle2" gutterBottom>
            Math I/GPS Algebra (1.0)
          </Typography>
          <Typography variant="body2" gutterBottom>
            27.08100 - Math I-Alg/Geom/Stats, 27.09710 - GSE Coordinate Algebra,
            27.09750 - Accel GSE Coordinate Algebra/Analytic Geometry, 27.09900
            - GSE Algebra I, 27.09940 - GSE Accel Algebra I/ Geometry
          </Typography>
          <Typography variant="subtitle2" gutterBottom>
            Math II/GPS Geometry (1.0)
          </Typography>
          <Typography variant="body2" gutterBottom>
            27.08200 - Math II-Geom/Alge II/Stats, 27.09720 - GSE Analytic
            Geometry, 27.09760 - Accel GSE Analytic Geometry B/Adv Algebra,
            27.09910 - GSE Geometry, 27.09950 - GSE Accel Geometry B/Algebra II
          </Typography>
          <Typography variant="subtitle2" gutterBottom>
            American Government/Civic (0.5)
          </Typography>
          <Typography variant="body2" gutterBottom>
            45.05200 - AP Government/Politics: United States, 45.05700 -
            American Government/Civics
          </Typography>
          <Typography variant="subtitle2" gutterBottom>
            Economics (0.5)
          </Typography>
          <Typography variant="body2" gutterBottom>
            45.06100 - Economics/Business/Free Enterprise, 45.06200 - AP
            Macroeconomics, 45.06300 - AP Microeconomics, 45.06500 - IB
            Economics, Yr One
          </Typography>
          <Typography variant="subtitle2" gutterBottom>
            US History (1.0)
          </Typography>
          <Typography variant="body2" gutterBottom>
            45.08100 - United States History, 45.08200 - AP United States
            History, 45.08700 - IB History of the Americas, Yr One
          </Typography>
          <Typography variant="subtitle2" gutterBottom>
            World History (1.0)
          </Typography>
          <Typography variant="body2" gutterBottom>
            45.08300 - World History, 45.08110 - AP World History, 45.09800 - IB
            History of Europe/Middle East, Yr One
          </Typography>
          <Typography variant="subtitle2" gutterBottom>
            Physical Science/Physics (1.0)
          </Typography>
          <Typography variant="body2" gutterBottom>
            40.01100 - Physical Science, 40.08100 - Physics I, 40.08310 - AP
            Physics I
          </Typography>
        </DialogContent>
      </Dialog>
    </>
  );
}
