import React, { Component } from "react";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import * as loginSelectors from "../../../../app/selectors";
import * as selectors from "../selectors";
import * as actions from "../actions";
import Button from "@material-ui/core/Button";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Select from "react-select";
import { selectCustomStyles } from "../../../generic/reactSelectCustomization";
import Typography from "@material-ui/core/Typography";
import HelpDialog from "./helpDialog";
import { IconButton, Tooltip } from "@material-ui/core";
import MaterialIcon from "../../../generic/materialIcon";

const cohort_filter_value_list = [
  { label: "28.0", value: "28.0" },
  { label: "27.0", value: "27.0" },
  { label: "26.0", value: "26.0" },
  { label: "25.0", value: "25.0" },
  { label: "24.0", value: "24.0" },
  { label: "23.0", value: "23.0" },
  { label: "22.0", value: "22.0" },
  { label: "21.0", value: "21.0" },
  { label: "20.0", value: "20.0" },
  { label: "19.0", value: "19.0" },
  { label: "18.0", value: "18.0" },
  { label: "17.0", value: "17.0" },
  { label: "16.0", value: "16.0" },
  { label: "15.0", value: "15.0" },
  { label: "14.0", value: "14.0" },
  { label: "13.0", value: "13.0" },
  { label: "12.0", value: "12.0" },
  { label: "11.0", value: "11.0" },
  { label: "10.0", value: "10.0" },
  { label: "9.0", value: "9.0" },
  { label: "8.0", value: "8.0" },
  { label: "7.0", value: "7.0" },
  { label: "6.0", value: "6.0" },
  { label: "5.0", value: "5.0" },
  { label: "4.5", value: "4.5" },
  { label: "4.0", value: "4.0" },
  { label: "3.5", value: "3.5" },
  { label: "3.0", value: "3.0" },
  { label: "2.5", value: "2.5" },
  { label: "2.0", value: "2.0" },
  { label: "1.5", value: "1.5" },
  { label: "1.0", value: "1.0" },
  { label: "0.5", value: "0.5" },
  { label: "0.0", value: "0.0" },
];

const total_credits_list = cohort_filter_value_list.filter(
  (res) => res.value >= 3 || res.value === 0,
);
const credits_list = cohort_filter_value_list.filter((res) => res.value <= 4);
const health_credits_list = cohort_filter_value_list.filter(
  (res) => res.value <= 1,
);

class CohortGraduationToolbar extends Component {
  state = {
    selected_total_credits: total_credits_list[5],
    total_credits_list: total_credits_list,
    selected_LA_credits: credits_list[0],
    LA_credits_list: credits_list,
    selected_MA_credits: credits_list[0],
    MA_credits_list: credits_list,
    selected_SC_credits: credits_list[0],
    SC_credits_list: credits_list,
    selected_SS_credits: credits_list[2],
    SS_credits_list: credits_list,
    selected_CTAE_credits: credits_list[8],
    CTAE_credits_list: credits_list,
    selected_health_PE_credits: health_credits_list[0],
    health_PE_credits_list: health_credits_list,
    openHelpInfo: false,
    isHomeSchoolSelected: false,
  };

  componentDidMount() {
    this.props.loadCohortYear({
      school: this.props.userData.school,
    });
  }

  handleDropdownChange = (type, options) => {
    if (type === "cohort_year") {
      const years_credits_mapping = this.props.years_credits_mapping;
      if (years_credits_mapping[options.value]) {
        const selection = years_credits_mapping[options.value];
        this.setState({
          selected_total_credits: { label: selection[0], value: selection[0] },
          selected_LA_credits: { label: selection[1], value: selection[1] },
          selected_MA_credits: { label: selection[2], value: selection[2] },
          selected_SC_credits: { label: selection[3], value: selection[3] },
          selected_SS_credits: { label: selection[4], value: selection[4] },
          selected_CTAE_credits: { label: selection[5], value: selection[5] },
          selected_health_PE_credits: {
            label: selection[6],
            value: selection[6],
          },
        });
      }
      this.props.setSelectedCohortYears(options);
    } else {
      this.setState({ [`selected_${type}`]: options });
    }
  };

  handleViewReport = () => {
    this.props.loadRateProjection({
      action: "initLoad",
      school: this.props.userData.school,
      CohortYear: this.props.selected_cohort_year.value,
      TotalCredits: this.state.selected_total_credits.value,
      LACredits: this.state.selected_LA_credits.value,
      MACredits: this.state.selected_MA_credits.value,
      SCCredits: this.state.selected_SC_credits.value,
      SSCredits: this.state.selected_SS_credits.value,
      ElectiveCredits: this.state.selected_CTAE_credits.value,
      HealthCredits: this.state.selected_health_PE_credits.value,
      RecordType: "At Risk",
      SPEDOnly: this.state.isHomeSchoolSelected
    });
    this.props.setSelectedFilters({
      year: this.props.selected_cohort_year.value,
      total: this.state.selected_total_credits.value,
      la: this.state.selected_LA_credits.value,
      ma: this.state.selected_MA_credits.value,
      sc: this.state.selected_SC_credits.value,
      ss: this.state.selected_SS_credits.value,
      ctae: this.state.selected_CTAE_credits.value,
      health_pe: this.state.selected_health_PE_credits.value,
      SPEDOnly: this.state.isHomeSchoolSelected
    });
    this.props.onViewReport();
  };

  openAttendanceInfo = () =>
    this.setState({ openHelpInfo: !this.state.openHelpInfo });

  handleHomeSchoolSelection = () =>
    this.setState({ isHomeSchoolSelected: !this.state.isHomeSchoolSelected });

  render() {
    const {
      selected_total_credits,
      total_credits_list,
      selected_LA_credits,
      LA_credits_list,
      selected_MA_credits,
      MA_credits_list,
      selected_SC_credits,
      SC_credits_list,
      selected_SS_credits,
      SS_credits_list,
      selected_CTAE_credits,
      CTAE_credits_list,
      selected_health_PE_credits,
      health_PE_credits_list,
      openHelpInfo,
      isHomeSchoolSelected,
    } = this.state;
    const { cohort_year_list, selected_cohort_year } = this.props;
    return (
      <>
        <Paper className="toolbar" variant="outlined" square>
          <Grid className="main-container" container spacing={3}>
            <Grid item md={3} container sm={4} xs={6}>
              <Grid item style={{ textAlign: "right" }} md={5}>
                <Typography className="mr-rt10" variant="caption">
                  {" "}
                  Cohort Year:{" "}
                </Typography>
              </Grid>
              <Grid item md={4}>
                <Select
                  styles={selectCustomStyles}
                  value={selected_cohort_year}
                  onChange={this.handleDropdownChange.bind(this, "cohort_year")}
                  options={cohort_year_list}
                />
              </Grid>
            </Grid>
            <Grid item md={3} container sm={4} xs={6}>
              <Grid item style={{ textAlign: "right" }} md={5}>
                <Typography className="mr-rt10" variant="caption">
                  {" "}
                  Total Credits:{" "}
                </Typography>
              </Grid>
              <Grid item md={4}>
                <Select
                  styles={selectCustomStyles}
                  value={selected_total_credits}
                  onChange={this.handleDropdownChange.bind(
                    this,
                    "total_credits",
                  )}
                  options={total_credits_list}
                />
              </Grid>
            </Grid>
            <Grid item container md={3} sm={4} xs={6}>
              <Grid item style={{ textAlign: "right" }} md={5}></Grid>
              <Grid item md={7}>
                <Button
                  className="capitalize"
                  size="small"
                  color="primary"
                  variant="outlined"
                  onClick={this.handleViewReport}
                >
                  View Report
                </Button>
              </Grid>
            </Grid>
            <Grid item md={3} sm={4} xs={6}>
              <Tooltip
                title="Cohort Help/Required Courses and Credits?"
                aria-label="info-btn-tooltip"
              >
                <IconButton
                  onClick={this.openAttendanceInfo}
                  className="capitalize"
                  size="small"
                  color="primary"
                  style={{
                    position: "relative",
                    left: "87%",
                    cursor: "pointer",
                  }}
                >
                  <MaterialIcon icon="help" />
                </IconButton>
              </Tooltip>
            </Grid>
          </Grid>
        </Paper>
        <Paper className="toolbar" variant="outlined" square>
          <Grid className="main-container" container spacing={3}>
            <Grid item md={3} container sm={4} xs={6}>
              <Grid item style={{ textAlign: "right" }} md={5}>
                <Typography className="mr-rt10" variant="caption">
                  {" "}
                  LA Credits:{" "}
                </Typography>
              </Grid>
              <Grid item md={4}>
                <Select
                  styles={selectCustomStyles}
                  value={selected_LA_credits}
                  onChange={this.handleDropdownChange.bind(this, "LA_credits")}
                  options={LA_credits_list}
                />
              </Grid>
            </Grid>
            <Grid item md={3} container sm={4} xs={6}>
              <Grid item style={{ textAlign: "right" }} md={5}>
                <Typography className="mr-rt10" variant="caption">
                  {" "}
                  MA Credits:{" "}
                </Typography>
              </Grid>
              <Grid item md={4}>
                <Select
                  styles={selectCustomStyles}
                  value={selected_MA_credits}
                  onChange={this.handleDropdownChange.bind(this, "MA_credits")}
                  options={MA_credits_list}
                />
              </Grid>
            </Grid>
            <Grid item md={3} container sm={4} xs={6}>
              <Grid item style={{ textAlign: "right" }} md={5}>
                <Typography className="mr-rt10" variant="caption">
                  {" "}
                  SC Credits:{" "}
                </Typography>
              </Grid>
              <Grid item md={4}>
                <Select
                  styles={selectCustomStyles}
                  value={selected_SC_credits}
                  onChange={this.handleDropdownChange.bind(this, "SC_credits")}
                  options={SC_credits_list}
                />
              </Grid>
            </Grid>
            <Grid item sm={4} xs={6} md={3}>
              <FormControlLabel
                control={
                  <Checkbox
                    style={{ padding: 2, marginLeft: 12 }}
                    checked={isHomeSchoolSelected}
                    onChange={this.handleHomeSchoolSelection}
                    name="homeSchoolCheckBox"
                    color="primary"
                  />
                }
                label="SPED Only"
              />
            </Grid>
          </Grid>
        </Paper>
        <Paper className="toolbar" variant="outlined" square>
          <Grid className="main-container" container spacing={3}>
            <Grid item md={3} container sm={4} xs={6}>
              <Grid item style={{ textAlign: "right" }} md={5}>
                <Typography className="mr-rt10" variant="caption">
                  {" "}
                  SS Credits:{" "}
                </Typography>
              </Grid>
              <Grid item md={4}>
                <Select
                  styles={selectCustomStyles}
                  value={selected_SS_credits}
                  onChange={this.handleDropdownChange.bind(this, "SS_credits")}
                  options={SS_credits_list}
                />
              </Grid>
            </Grid>
            <Grid item md={3} container sm={4} xs={6}>
              <Grid item style={{ textAlign: "right" }} md={5}>
                <Typography className="mr-rt10" variant="caption">
                  {" "}
                  CTAE Credits:{" "}
                </Typography>
              </Grid>
              <Grid item md={4}>
                <Select
                  styles={selectCustomStyles}
                  value={selected_CTAE_credits}
                  onChange={this.handleDropdownChange.bind(
                    this,
                    "CTAE_credits",
                  )}
                  options={CTAE_credits_list}
                />
              </Grid>
            </Grid>
            <Grid item md={3} container sm={4} xs={6}>
              <Grid item style={{ textAlign: "right" }} md={5}>
                <Typography className="mr-rt10" variant="caption">
                  {" "}
                  Health/PE Credits:{" "}
                </Typography>
              </Grid>
              <Grid item md={4}>
                <Select
                  styles={selectCustomStyles}
                  value={selected_health_PE_credits}
                  onChange={this.handleDropdownChange.bind(
                    this,
                    "health_PE_credits",
                  )}
                  options={health_PE_credits_list}
                />
              </Grid>
            </Grid>
            <Grid item md={3} sm={4} xs={6}></Grid>
          </Grid>
        </Paper>
        <HelpDialog
          open={openHelpInfo}
          openAttendanceInfo={this.openAttendanceInfo}
        />
      </>
    );
  }
}

const mapStateToProps = () =>
  createStructuredSelector({
    userData: loginSelectors.getLoginData(),
    cohort_year_list: selectors.getCohortYearsList(),
    selected_cohort_year: selectors.getSelectedCohortYear(),
    years_credits_mapping: selectors.getYearsCreditsMapping(),
  });

const mapDispatchToProps = (dispatch) => ({
  loadRateProjection: (request) =>
    dispatch(actions.loadRateProjection(request)),
  setSelectedFilters: (filters) =>
    dispatch(actions.setSelectedFilters(filters)),
  setSelectedCohortYears: (options) =>
    dispatch(actions.setSelectedCohortYears(options)),
  loadCohortYear: (request) => dispatch(actions.loadCohortYear(request)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CohortGraduationToolbar);
