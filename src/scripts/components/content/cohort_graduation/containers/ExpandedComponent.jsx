import React from "react";

// eslint-disable-next-line react/prop-types
export default ({ data }) => (
  <div style={{ display: "grid" }}>
    {data.children.map((obj, index) => (
      <div
        key={index}
        className={
          Object.keys(obj).length < 20
            ? "cohort-subject-expanded-style"
            : "cohort-expanded-style"
        }
      >
        <div
          style={{
            borderBottom: obj.schoolYear ? "1px solid #888888" : "none",
            fontWeight: 600,
            padding: 5,
          }}
          className={Object.keys(obj).length < 20 ? "width100" : "width80 year"}
        >
          {obj.schoolYear}
        </div>
        <div
          style={{
            borderBottom: obj.term ? "1px solid #888888" : "none",
            fontWeight: 600,
            padding: 5,
          }}
          className={Object.keys(obj).length < 20 ? "width100" : "width80 term"}
        >
          {obj.term}
        </div>
        <div
          className={
            Object.keys(obj).length < 20 ? "width100" : "width80 grade"
          }
          style={{ fontWeight: 500, borderBottom: "1px solid #888888" }}
        >
          {obj.grade}
        </div>
        <div
          className="width110 stateCode"
          style={{ borderBottom: "1px solid #888888", padding: 5 }}
        >
          {obj.stateCode}
        </div>
        <div
          className="width200 courseName"
          style={{ borderBottom: "1px solid #888888", padding: 5 }}
        >
          {obj.courseName}
        </div>
        <div
          className={
            Object.keys(obj).length < 20 ? "width100" : "width80 score"
          }
          style={{ borderBottom: "1px solid #888888", padding: 5 }}
        >
          {obj.score}
        </div>
        <div
          className="width110 attempted"
          style={{ borderBottom: "1px solid #888888", padding: 5 }}
        >
          {isNaN(Number(obj.creditsAttempted).toFixed(3))
            ? null
            : Number(obj.creditsAttempted).toFixed(3)}
        </div>
        <div
          className={
            Object.keys(obj).length < 20 ? "width100" : "width80 earned"
          }
          style={{
            borderBottom: "1px solid #888888",
            padding: 5,
            color: parseFloat(obj.creditsEarned) > 0 ? "black" : "red",
          }}
        >
          {isNaN(Number(obj.creditsEarned).toFixed(3))
            ? null
            : Number(obj.creditsEarned).toFixed(3)}
        </div>
        {Object.keys(obj).length > 20 && (
          <>
            <div
              className="width60"
              style={{ borderBottom: "1px solid #888888", padding: 5 }}
            >
              {obj.LA}
            </div>
            <div
              className="width60"
              style={{ borderBottom: "1px solid #888888", padding: 5 }}
            >
              {obj.MA}
            </div>
            <div
              className="width60"
              style={{ borderBottom: "1px solid #888888", padding: 5 }}
            >
              {obj.SC}
            </div>
            <div
              className="width60"
              style={{ borderBottom: "1px solid #888888", padding: 5 }}
            >
              {obj.SS}
            </div>
            <div
              className="width60"
              style={{ borderBottom: "1px solid #888888", padding: 5 }}
            >
              {obj.FL}
            </div>
            <div
              className="width60"
              style={{ borderBottom: "1px solid #888888", padding: 5 }}
            >
              {obj.HLPE}
            </div>
            <div
              className="width60"
              style={{ borderBottom: "1px solid #888888", padding: 5 }}
            >
              {obj.CTAE}
            </div>
            <div
              className="width60"
              style={{ borderBottom: "1px solid #888888", padding: 5 }}
            >
              {obj.ROTC}
            </div>
            <div
              className="width60"
              style={{ borderBottom: "1px solid #888888", padding: 5 }}
            >
              {obj.Rigor}
            </div>
          </>
        )}
      </div>
    ))}
  </div>
);
