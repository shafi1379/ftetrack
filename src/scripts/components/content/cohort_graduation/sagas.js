import jsPDF from "jspdf";
import "jspdf-autotable";
import { all, call, fork, put, takeLatest } from "redux-saga/effects";
import {
  LOAD_COHORT_YEAR,
  LOAD_RATE_PROJECTION,
  LOAD_STUDENT_DETAILS,
  setRateProjection,
  setStudentProjection,
  setStudentDetails,
  LOAD_GRADEPROGRESS_LIST,
  setGradeProgressList,
  setCohortYears,
  setSelectedCohortYears,
  setYearsCreditsMapping,
} from "./actions";
import { setProgressingValue } from "../../../app/actions";
import * as Api from "../../../services/api";
import { API_URLS } from "../../../utilities/constants";
import { formatDate } from "../../../utilities/utilities";
import * as Utilities from "../../../utilities/utilities";

const cohort_filter_values = [
  ["23.0", "4.0", "4.0", "4.0", "3.0", "0.0", "1.0"],
  ["16.0", "3.0", "3.0", "3.0", "2.0", "0.0", "1.0"],
  ["11.0", "2.0", "2.0", "2.0", "1.0", "0.0", "1.0"],
  ["5.0", "1.0", "1.0", "1.0", "0.0", "0.0", "0.0"],
];

function* loadRateProjectionRequested({ request }) {
  try {
    yield put(setProgressingValue(true));
    if (request.action === "initLoad") {
      delete request.action;
      const response = yield call(
        Api.doPostRequest,
        API_URLS.cohortrates,
        request,
      );
      if (response) {
        yield put(setProgressingValue(false));
        yield put(setRateProjection(response));
      }
    } else {
      delete request.action;
      const response = yield call(
        Api.doPostRequest,
        API_URLS.cohortstudents,
        request,
      );
      if (response) {
        yield put(setProgressingValue(false));
        // response
        yield put(
          setStudentProjection(
            response.map((obj) => {
              obj["withCodeDate"] = obj.endDate
                ? obj.endStatus + " (" + obj.endDate + ")"
                : "";
              return obj;
            }),
          ),
        );
      }
    }
  } catch (error) {
    console.error(error);
  }
}

function* rateProjectionListener() {
  yield takeLatest(LOAD_RATE_PROJECTION, loadRateProjectionRequested);
}

function* studentDetailsRequested({ request }) {
  try {
    yield put(setProgressingValue(true));
    const response = yield call(
      Api.doPostRequest,
      API_URLS.transcript,
      request,
    );
    if (response) {
      yield put(setProgressingValue(false));
      yield put(setStudentDetails(response));
    }
  } catch (error) {
    console.error(error);
  }
}

function* studentDetailsListener() {
  yield takeLatest(LOAD_STUDENT_DETAILS, studentDetailsRequested);
}

function* cohortYearRequested({ request }) {
  try {
    yield put(setProgressingValue(true));
    const response = yield call(
      Api.doPostRequest,
      API_URLS.cohortyears,
      request,
    );
    if (response) {
      yield put(setProgressingValue(false));
      const cohortYears = response.map((res) => ({
        label: res.cohortYear,
        value: res.cohortYear,
      }));
      const yearCreditsMaps = {};
      cohort_filter_values.forEach((res, index) => {
        yearCreditsMaps[response[index].cohortYear] = res;
      });
      yield put(setCohortYears(cohortYears));
      yield put(setSelectedCohortYears(cohortYears[0]));
      yield put(setYearsCreditsMapping(yearCreditsMaps));
    }
  } catch (error) {
    console.error(error);
  }
}

function* cohortYearListener() {
  yield takeLatest(LOAD_COHORT_YEAR, cohortYearRequested);
}

function* gradeProgressRequested({ request }) {
  try {
    const schoolName = request.schoolName;
    delete request.schoolName;
    yield put(setProgressingValue(true));
    const response = yield call(
      Api.doPostRequest,
      API_URLS.gradprogress,
      request,
    );
    if (response) {
      yield put(setProgressingValue(false));
      yield put(setGradeProgressList(response));
      const doc = new jsPDF("p", "pt");
      const results = {};
      const mainHeader = "Graduation Progress";
      /*const students = response.reduce(function (r, a) {
        r[a.studentName] = r[a.studentName] || [];
        r[a.studentName].push(a);
        return r;
      }, Object.create(null));*/
      /*for (let obj in students) {*/
      /*const subjects = students[obj].reduce(function (r, a) {
          if (a.courseName) {
            r[a.subjectName] = r[a.subjectName] || [];
            r[a.subjectName].push(a);
          }
          return r;
        }, Object.create(null));
        results[obj] = subjects;*/
      /*}*/
      response.forEach(function (obj) {
        if (obj.courses.length) {
          const subjects = obj.courses.reduce(function (r, a) {
            const attr = { ...obj, ...a };
            r[a.subjectName] = r[a.subjectName] || [];
            r[a.subjectName].push(attr);
            return r;
          }, Object.create(null));
          results[obj.studentName] = subjects;
        }
        return obj;
      });
      let studentIndex = 1;
      for (let res in results) {
        let activateRight = false;
        let lastTableOffset = 105;
        let regorCreditEarned = 0;
        let totalCreditEarned = 0;
        const headerPlainTextInfo =
          results[res][Object.keys(results[res])[0]][0];
        if (studentIndex > 1) {
          doc.addPage();
        }
        const currentDate = Utilities.formatDateSlash(
          formatDate(new Date(), "-"),
          "mm/dd/yyyy",
        );
        doc.setFontSize(9);
        doc.text(
          currentDate,
          doc.internal.pageSize.width -
            doc.getStringUnitWidth(currentDate) * doc.internal.getFontSize() -
            50,
          45,
        );
        const xOffsetH1 =
          doc.internal.pageSize.width / 2 -
          (doc.getStringUnitWidth(mainHeader) * doc.internal.getFontSize()) / 2;
        doc.setFontSize(12);
        doc.text(mainHeader, xOffsetH1 - (currentDate.length + 10), 30);
        doc.setFontSize(10);
        const xOffsetH2 =
          doc.internal.pageSize.width / 2 -
          (doc.getStringUnitWidth(schoolName) * doc.internal.getFontSize()) / 2;
        doc.text(schoolName, xOffsetH2, 45);
        doc.setFontSize(8);

        doc.text("Student # ", 50, 70);
        doc.line(
          doc.getStringUnitWidth("Student # ") * doc.internal.getFontSize() +
            60,
          72,
          doc.internal.pageSize.width / 2 - 10,
          72,
        );
        doc.text(
          headerPlainTextInfo["studentNumber"],
          (doc.internal.pageSize.width / 2 - 50) / 2,
          70,
        );

        doc.text("Student's Name ", doc.internal.pageSize.width / 2, 70);
        doc.line(
          doc.internal.pageSize.width / 2 +
            doc.getStringUnitWidth("Student's Name ") *
              doc.internal.getFontSize() +
            10,
          72,
          doc.internal.pageSize.width - 50,
          72,
        );
        doc.text(
          headerPlainTextInfo["studentName"],
          doc.internal.pageSize.width / 2 +
            doc.getStringUnitWidth("Student's Name ") *
              doc.internal.getFontSize() +
            50,
          70,
        );

        doc.text("Class of ", 50, 90);
        doc.line(
          doc.getStringUnitWidth("Class of ") * doc.internal.getFontSize() + 60,
          92,
          doc.internal.pageSize.width / 3 - 20,
          92,
        );
        doc.text(
          headerPlainTextInfo["cohortYear"] + "",
          (doc.internal.pageSize.width / 2 - 50) / 2,
          90,
        );

        doc.text("Grade ", doc.internal.pageSize.width / 3, 90);
        doc.line(
          doc.internal.pageSize.width / 3 +
            doc.getStringUnitWidth("Grade ") * doc.internal.getFontSize() +
            10,
          92,
          doc.internal.pageSize.width / 2 - 10,
          92,
        );
        doc.text(
          headerPlainTextInfo["grade"] + "",
          doc.getStringUnitWidth("Grade ") * doc.internal.getFontSize() +
            doc.internal.pageSize.width / 3 +
            20,
          90,
        );

        doc.text("Counselor ", doc.internal.pageSize.width / 2, 90);
        doc.line(
          doc.internal.pageSize.width / 2 +
            doc.getStringUnitWidth("Counselor ") * doc.internal.getFontSize() +
            10,
          92,
          doc.internal.pageSize.width - 50,
          92,
        );
        doc.text(
          "",
          doc.getStringUnitWidth("Counselor ") * doc.internal.getFontSize() +
            doc.internal.pageSize.width / 2,
          90,
        );

        for (let sub in results[res]) {
          let total = 0;
          const columns = [
            { title: sub, dataKey: "stateCode" },
            { title: "", dataKey: "courseName" },
            { title: "", dataKey: "endYear" },
            { title: "", dataKey: "score" },
            { title: "", dataKey: "creditsEarned" },
          ];
          const autoCalculateMargin = { top: 105 };

          /* Calculating the total of each subject */
          results[res][sub].forEach((obj) => {
            total += Number(obj.creditsEarned);
          });
          const rows = [
            {
              stateCode: "Course",
              courseName: "Title",
              endYear: "Year",
              score: "Grade",
              creditsEarned: "Credit",
            },
            ...results[res][sub],
            {
              /* Added last line of Each Table */
              stateCode: "",
              courseName: "",
              endYear: "",
              score: "",
              creditsEarned: total.toFixed(3),
            },
          ];
          if (
            lastTableOffset + 18.05 * (results[res][sub].length + 5) + 10 >
            doc.internal.pageSize.height - 20
          ) {
            lastTableOffset = 105;
            if (!activateRight) {
              activateRight = !activateRight;
              autoCalculateMargin["left"] = 305;
            } else {
              activateRight = !activateRight;
              autoCalculateMargin["right"] = 305;
              doc.setPage(studentIndex);
              doc.addPage();
              studentIndex++;
            }
          }
          if (activateRight) {
            autoCalculateMargin["left"] = 305;
          } else {
            autoCalculateMargin["right"] = 305;
          }
          // eslint-disable-next-line
          results[res][sub].forEach((obj) => {
            if (obj["isRigor"] === "1") {
              regorCreditEarned += Number(obj["creditsEarned"]);
            }
            totalCreditEarned += Number(obj["creditsEarned"]);
          });

          doc.autoTable(columns, rows, {
            margin: autoCalculateMargin,
            headStyles: { fontSize: 7, valign: "middle" },
            bodyStyles: { fontSize: 7.5, cellPadding: 0.5 },
            columnStyles: {
              stateCode: {
                cellWidth: 45,
                cellPadding: 1,
                valign: "middle",
                overflow: "hidden",
              },
              courseName: {
                cellWidth: 128,
                cellPadding: 1,
                valign: "middle",
                overflow: "hidden",
              },
              endYear: { cellWidth: 22, cellPadding: 1, valign: "middle" },
              score: { cellWidth: 25, cellPadding: 1, valign: "middle" },
              creditEarned: { cellWidth: 20, cellPadding: 1, valign: "middle" },
            },
            rowStyles: {
              rowHeight: 100,
            },
            pageBreak: "avoid",
            rowHeight: 10,
            rowPageBreak: "avoid",
            head: [
              [
                {
                  content: sub,
                  colSpan: 5,
                  styles: {
                    halign: "left",
                    valign: "middle",
                    fillColor: [22, 73, 160],
                  },
                },
              ],
            ],
            startY: lastTableOffset + 10,
            didParseCell: function (data) {
              if (data.row.index === 0) {
                data.cell.styles.fontStyle = "bold";
              } else {
                if (data.row.raw.isRequired === "1") {
                  data.cell.styles.fontStyle = "bold";
                }
                if (data.row.raw.isRigor === "1" && isNaN(data.cell.raw)) {
                  data.cell.text = "*" + data.cell.raw;
                }
                if (data.row.raw.courseName === "") {
                  data.cell.styles.fontStyle = "bold";
                }
              }
            },
            // eslint-disable-next-line
            didDrawPage: function (data) {
              lastTableOffset = data.cursor.y;
            },
          });
        }
        doc.text(
          "Rigor(*) Credits Earned                       " +
            regorCreditEarned.toFixed(3),
          400,
          780,
        );
        doc.text(
          "Total Earned Credits                          " +
            totalCreditEarned.toFixed(3),
          400,
          790,
        );
        doc.line(50, 820, 170, 820);
        doc.text("Student's Signature", 50, 830);
        doc.text("Date", 150, 830);
        doc.line(250, 820, 370, 820);
        doc.text("Counselor's Signature", 250, 830);
        doc.text("Date", 350, 830);
        doc.line(450, 820, 560, 820);
        doc.text("Parent's Signature", 450, 830);
        doc.text("Date", 540, 830);
        studentIndex++;
      }
      doc.save(`Progress Report.pdf`);
    }
  } catch (error) {
    console.error(error);
  }
}

function* gradeProgressListener() {
  yield takeLatest(LOAD_GRADEPROGRESS_LIST, gradeProgressRequested);
}

export default function* root() {
  yield all([
    fork(rateProjectionListener),
    fork(studentDetailsListener),
    fork(gradeProgressListener),
    fork(cohortYearListener),
  ]);
}
