import React, { Component } from "react";
import AppBar from "@material-ui/core/AppBar";
import Hidden from "@material-ui/core/Hidden";
import IconButton from "@material-ui/core/IconButton";
import InputBase from "@material-ui/core/InputBase";
import SearchIcon from "@material-ui/icons/Search";
import Toolbar from "@material-ui/core/Toolbar";
import Tooltip from "@material-ui/core/Tooltip";
import Typography from "@material-ui/core/Typography";
import { alpha, createStyles, withStyles } from "@material-ui/core/styles";
import MaterialIcon from "../generic//materialIcon";
import { createStructuredSelector } from "reselect";
import { connect } from "react-redux";
import * as loginSelectors from "../../app/selectors";
import * as selectors from "../../app/selectors";
import * as actions from "../../app/actions";
import CircularProgress from "@material-ui/core/CircularProgress";
import StudentsDetailsDialog from "../generic/details_dialog";

const styles = (theme) =>
  createStyles({
    search: {
      position: "relative",
      borderRadius: theme.shape.borderRadius,
      backgroundColor: alpha(theme.palette.common.white, 0.15),
      "&:hover": {
        backgroundColor: alpha(theme.palette.common.white, 0.25),
      },
      marginLeft: 0,
      width: "100%",
      [theme.breakpoints.up("sm")]: {
        marginLeft: theme.spacing(1),
        width: "auto",
      },
    },
    searchIcon: {
      padding: theme.spacing(0, 2),
      height: "100%",
      position: "absolute",
      pointerEvents: "none",
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
    },
    inputRoot: {
      color: "inherit",
    },
    inputInput: {
      paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
      transition: theme.transitions.create("width"),
      width: "100%",
      [theme.breakpoints.up("sm")]: {
        width: "50ch",
        fontSize: "13px",
        "&:focus": {
          width: "58ch",
        },
      },
    },
  });

class AppHeader extends Component {
  state = {
    searchValue: "",
    selectedRow: null,
    openDetailsinfo: false,
  };

  componentDidMount() {
    window.addEventListener("click", this.handleOutSideListClick.bind(this));
  }

  handleSearch = (evt) => {
    const value = evt.target.value.trim();
    this.setState({ searchValue: value });
    if (value.length > 2) {
      this.props.setSearchProgressingValue(true);
      this.props.loadSearchList({
        school: this.props.userData.school,
        name: value,
      });
    }
  };

  handleSearchSelection = (item) => {
    this.setState({ selectedRow: item, openDetailsinfo: true });
    this.props.loadStudentDetails({ personID: item.personID });
    this.handleClearSearch();
  };

  handleClearSearch = () => {
    this.setState({ searchValue: "" });
    this.props.setSearchList([]);
  };

  handleModalClose = () => {
    this.setState({ openDetailsinfo: false });
    this.props.setStudentDetails({});
  };

  logout = () => {
    if (window.location.host === "www.ftetrack.net") {
      window.location.href = "https://www.ftetrack.net/index.php";
    } else {
      window.location.href = "../index.php";
    }
  };

  handleOutSideListClick = (e) => {
    const elmt = document.getElementById("searchMenuList");
    if (!(elmt && elmt.contains(e.target))) {
      this.handleClearSearch();
    }
  };

  render() {
    const { searchValue, selectedRow, openDetailsinfo } = this.state;
    const { classes, loginData, searchList, loadingSpinner, studentDetails } =
      this.props;
    return (
      <div className="app-header">
        <AppBar position="fixed">
          <Toolbar variant="dense">
            <Typography
              className="app-title capitalize"
              variant="button"
              color="inherit"
            >
              <span>A</span>ttendance
              <span>B</span>ehaviour
              <span>C</span>ohort Grades
            </Typography>
            <Hidden xsDown>
              <div className="relative">
                <div className={classes.search}>
                  <div className={classes.searchIcon}>
                    <SearchIcon />
                  </div>
                  <InputBase
                    placeholder="Search part of Lastname, part of Firstname ..."
                    onChange={this.handleSearch}
                    value={searchValue}
                    classes={{
                      root: classes.inputRoot,
                      input: classes.inputInput,
                    }}
                    inputProps={{ "aria-label": "search" }}
                  />
                  {loadingSpinner && (
                    <div
                      className=""
                      style={{ position: "absolute", right: 5, top: 5 }}
                    >
                      <CircularProgress
                        size={20}
                        style={{ color: "#FFFFFF" }}
                      />
                    </div>
                  )}
                </div>
                {searchList.length > 0 && (
                  <ul className="dd-list" id="searchMenuList">
                    {searchList.map((item, index) => (
                      <li
                        key={index}
                        className="dd-list-item"
                        onClick={() => this.handleSearchSelection(item)}
                      >
                        {item.studentName} ({item.studentNumber}) -{" "}
                        {item.schoolName}
                      </li>
                    ))}
                  </ul>
                )}
              </div>
            </Hidden>
            <Hidden smDown>
              <Typography
                className="last-updated"
                variant="caption"
                display="block"
                gutterBottom
              >
                Last Updated: {loginData.last_updated}
              </Typography>
              <Typography
                className="school-year"
                variant="caption"
                display="block"
                gutterBottom
              >
                School Year: {loginData.school_year}
              </Typography>
            </Hidden>
            <Tooltip title="Logout" aria-label="logout-tooltip">
              <IconButton
                onClick={this.logout}
                className="logout-btn"
                color="inherit"
                aria-label="logout"
              >
                <MaterialIcon icon="power_settings_new" />
              </IconButton>
            </Tooltip>
          </Toolbar>
        </AppBar>
        {Object.keys(studentDetails).length > 0 && (
          <StudentsDetailsDialog
            open={openDetailsinfo}
            openStudentsDetailsInfo={this.handleModalClose}
            row={selectedRow}
            data={studentDetails}
          />
        )}
      </div>
    );
  }
}

const mapStateToProps = () =>
  createStructuredSelector({
    loginData: selectors.getLoginData(),
    searchList: selectors.getSearchList(),
    loadingSpinner: selectors.getSearchProgressingValue(),
    studentDetails: selectors.getStudentDetails(),
    userData: loginSelectors.getLoginData(),
  });
const mapDispatchToProps = (dispatch) => ({
  logoutUser: () => dispatch(actions.logoutUser()),
  setSearchProgressingValue: (bool) =>
    dispatch(actions.setSearchProgressingValue(bool)),
  loadSearchList: (data) => dispatch(actions.loadSearchList(data)),
  setSearchList: (arrList) => dispatch(actions.setSearchList(arrList)),
  loadStudentDetails: (request) =>
    dispatch(actions.loadStudentDetails(request)),
  setStudentDetails: (details) => dispatch(actions.setStudentDetails(details)),
});

const styleAppHeader = withStyles(styles)(AppHeader);
export default connect(mapStateToProps, mapDispatchToProps)(styleAppHeader);
