export function sortArrObj(property) {
  return (a, b) => {
    if (a[property] > b[property]) {
      return 1;
    }
    if (a[property] < b[property]) {
      return -1;
    }
    return 0;
  };
}

export function isEmpty(obj) {
  for (var key in obj) {
    if (obj.hasOwnProperty(key)) {
      return false;
    }
  }
  return true;
}

export function formatDate(date, divider) {
  let formattedDate;
  const d = new Date(date),
    year = d.getFullYear();
  let month = "" + (d.getMonth() + 1),
    day = "" + d.getDate();
  if (month.length < 2) {
    month = "0" + month;
  }
  if (day.length < 2) {
    day = "0" + day;
  }
  if (divider) {
    formattedDate = [year, month, day].join(divider);
  } else {
    formattedDate = [year, month, day].join("-");
  }
  return formattedDate;
}

export function formatDateHyphen(date) {
  const year = date.toString().substring(0, 4);
  const month = date.toString().substring(4, 6);
  const day = date.toString().substring(6, 8);
  return [year, month, day].join("-");
}

export function formatDateSlash(date, format) {
  if (!date) {
    return false;
  }
  let formattedDate = date;
  if (!date.toString().includes("/") && !date.toString().includes("-")) {
    const year = date.toString().substring(0, 4);
    const month = date.toString().substring(4, 6);
    const day = date.toString().substring(6, 8);
    if (format && format.toLowerCase() === "mm/dd/yyyy") {
      formattedDate = [month, day, year].join("/");
    } else {
      formattedDate = [day, month, year].join("/");
    }
  } else {
    if (date.toString().includes("-")) {
      const dates = date.toString().split("-");
      formattedDate = [dates[1], dates[2], dates[0]].join("/");
    }
  }
  return formattedDate;
}

export function generateExcelData(datas, cols) {
  let dataList = [],
    columns = {};
  if (datas.length) {
    if (isEmpty(cols)) {
      Object.keys(datas[0]).filter((attr) => (columns[attr] = attr));
    } else {
      columns = cols;
    }
    dataList = datas.map((res) => {
      const arr = [];
      for (let col in columns) {
        if (columns[col] === "school_date") {
          const date = formatDateSlash(res[columns[col]], "mm/dd/yyyy");
          arr.push(date);
        } else {
          arr.push(res[columns[col]]);
        }
      }
      return arr;
    });
    dataList.unshift(Object.keys(columns));
  } else {
    dataList.unshift(Object.keys(cols));
  }
  return dataList;
}

export function capitalize(str) {
  return str.charAt(0).toUpperCase() + str.slice(1);
}

export function generateFilteredData(item, value, objKeys, actualKeys) {
  return objKeys.filter((key) => {
    if (
      value.charAt(0) === "-" &&
      value.length > 3 &&
      value.indexOf(":") > -1
    ) {
      const newKey = value.split(":")[0].substring(1);
      const newVal = value.split(":")[1].trim();
      const actualKey = actualKeys.filter((val) => val.name === newKey);
      if (!actualKey.length && !item[newKey]) {
        return false;
      }
      if (!actualKey[0].selector && !item[actualKey[0].selector] && !newVal) {
        return true;
      } else {
        return item[actualKey[0].selector]
          .toString()
          .toLowerCase()
          .indexOf(newVal.toLowerCase()) > -1
          ? true
          : false;
      }
    } else {
      if (value.charAt(0) !== "-") {
        if (!item[key]) {
          return false;
        }
        return item[key].toString().toLowerCase().indexOf(value.toLowerCase()) >
          -1
          ? true
          : false;
      } else {
        return true;
      }
    }
  }).length > 0
    ? true
    : false;
}

export function getRaceList(res, attr) {
  const raceList = { A: 0, B: 0, H: 0, I: 0, M: 0, P: 0, W: 0 };
  const raceItems = ["A", "B", "H", "I", "M", "P", "W"];
  res.forEach((v) => {
    if (raceItems[0] === v[attr || "race"]) {
      raceList.A += 1;
    } else if (raceItems[1] === v[attr || "race"]) {
      raceList.B += 1;
    } else if (raceItems[2] === v[attr || "race"]) {
      raceList.H += 1;
    } else if (raceItems[3] === v[attr || "race"]) {
      raceList.I += 1;
    } else if (raceItems[4] === v[attr || "race"]) {
      raceList.M += 1;
    } else if (raceItems[5] === v[attr || "race"]) {
      raceList.P += 1;
    } else if (raceItems[6] === v[attr || "race"]) {
      raceList.W += 1;
    }
  });
  return raceList;
}
