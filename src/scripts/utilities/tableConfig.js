import React from "react";
import * as Utilities from "./utilities";

export const customStyles = {
  rows: {
    style: {
      minHeight: "35px",
    },
  },
};

export const dashboardStudentsListColumns = [
  {
    name: "Student#",
    cell: null,
    sortable: true,
  },
  {
    name: "Name",
    selector: "studentName",
    sortable: true,
  },
  {
    name: "Grade",
    selector: "grade",
    sortable: true,
  },
  {
    name: "Race",
    selector: "race",
    sortable: true,
  },
  {
    name: "Gender",
    selector: "gender",
    sortable: true,
  },
  {
    name: "Birth Date",
    selector: "birthdate",
    sortable: true,
  },
  {
    name: "SPED",
    selector: "sped",
    sortable: true,
  },
  {
    name: "Days Enrolled",
    selector: "DaysEnrolled",
    sortable: true,
  },
  {
    name: "Days Absence",
    selector: "TotalAbsence",
    sortable: true,
  },
  {
    name: "% Absence",
    selector: "PercentAbsence",
    sortable: true,
    cell: (row) => <div>{parseFloat(row.PercentAbsence).toFixed(2)}</div>,
  },
];

export const studentAncentsColumns = [
  {
    name: "Person#",
    selector: "personID",
    sortable: true,
  },
  {
    name: "Student#",
    selector: "studentNumber",
    sortable: true,
  },
  {
    name: "Student Name",
    selector: "studentName",
    sortable: true,
  },
  {
    name: "Start Date",
    selector: "startDate",
    sortable: true,
  },
  {
    name: "Grade",
    selector: "grade",
    sortable: true,
  },
  {
    name: "SPED",
    selector: "sped",
    sortable: true,
  },
  {
    name: "SST",
    selector: "sst",
    sortable: true,
  },
  {
    name: "EIP",
    selector: "eip",
    sortable: true,
  },
  {
    name: "Homeless",
    selector: "homeless",
    omit: true,
  },
  {
    name: "Total Absence",
    selector: "TotalAbsence",
    sortable: true,
  },
  {
    name: "Days Enrolled",
    selector: "DaysEnrolled",
    sortable: true,
  },
  {
    name: "Absence %",
    selector: "PercentAbsence",
    sortable: true,
  },
  {
    name: "Race",
    selector: "race",
    sortable: true,
  },
  {
    name: "Gender",
    selector: "gender",
    sortable: true,
  },
  {
    name: "Birth Date",
    selector: "birthdate",
    sortable: true,
  },
  {
    name: "age",
    selector: "age",
    sortable: true,
  },
];

export const adaDashboard = [
  {
    name: "",
    selector: "title",
    sortable: true,
  },
  {
    cell: null,
    name: "# of Students",
    sortable: true,
  },
  {
    name: "Percent",
    selector: "percent",
    sortable: true,
    cell: (row) => <div>{parseFloat(row.percent).toFixed(2)}</div>,
  },
];

export const adaAda = [
  {
    name: "Calendar Date",
    cell: (row) => (
      <div>{Utilities.formatDateSlash(row.school_date, "mm/dd/yyyy")}</div>
    ),
    sortable: true,
  },
  {
    name: "Enrollments",
    selector: "enrollments",
    sortable: true,
  },
  {
    name: "Absences",
    selector: "absences",
    sortable: true,
  },
  {
    cell: (row) => (
      <div
        style={{
          color: parseFloat(row.ada) < 90 ? "red" : "",
          fontWeight: parseFloat(row.ada) < 90 ? "bold" : "",
        }}
      >
        {parseFloat(row.ada) || ""}
      </div>
    ),
    name: "ADA",
    sortable: true,
  },
];

export const adhocQueryResultColumns = [
  {
    name: "School",
    selector: "name",
    sortable: true,
  },
  {
    name: "Date",
    selector: "school_date",
    sortable: true,
  },
  {
    name: "Enrollments",
    selector: "enrollments",
    sortable: true,
  },
  {
    name: "Absences",
    selector: "absences",
    sortable: true,
  },
  {
    name: "ADA %",
    selector: "ada",
    sortable: true,
    cell: (row) => <div>{parseFloat(row.ada).toFixed(2)}</div>,
  },
];

export const studentDetailsColumns = {
  attendance: [
    {
      name: "",
    },
    {
      name: "Absence Date",
      selector: "school_date",
      sortable: true,
    },
    {
      name: "Absence Code",
      selector: "enrollments",
      sortable: true,
    },
    {
      name: "School",
      selector: "absences",
      sortable: true,
    },
  ],
  behavior: [
    {
      name: "Incident Date",
      selector: "absences",
      sortable: true,
    },
    {
      name: "Incident Type",
      selector: "absences",
      sortable: true,
    },
    {
      name: "Resolution",
      selector: "absences",
      sortable: true,
    },
    {
      name: "SuspDays",
      selector: "absences",
      sortable: true,
    },
  ],
  courseGrade_Current: [
    {
      name: "Section",
      selector: "absences",
      sortable: true,
    },
    {
      name: "Course Title",
      selector: "absences",
      sortable: true,
    },
    {
      name: "Teacher",
      selector: "absences",
      sortable: true,
    },
    {
      name: "Alpha Grade",
      selector: "absences",
      sortable: true,
    },
    {
      name: "Score",
      selector: "absences",
      sortable: true,
    },
  ],
  eogMileStones: [
    {
      name: "Test Date",
      selector: "absences",
      sortable: true,
    },
    {
      name: "Testing Grade",
      selector: "absences",
      sortable: true,
    },
    {
      name: "Subject",
      selector: "absences",
      sortable: true,
    },
    {
      name: "Scale Score",
      selector: "absences",
      sortable: true,
    },
    {
      name: "Lexile",
      selector: "absences",
      sortable: true,
    },
    {
      name: "Achievement Level",
      selector: "absences",
      sortable: true,
    },
  ],
  grades_Posted: [
    {
      name: "Course#",
      selector: "absences",
      sortable: true,
    },
    {
      name: "Course Title",
      selector: "absences",
      sortable: true,
    },
    {
      name: "Grade",
      selector: "absences",
      sortable: true,
    },
    {
      name: "Date",
      selector: "absences",
      sortable: true,
    },
  ],
};

export const cohortGraduation_rate_projection = [
  {
    name: "School Id",
    selector: "schoolID",
    omit: true,
  },
  {
    name: "School",
    selector: "schoolName",
    sortable: true,
    grow: 4,
  },
  {
    name: "On Track",
    cell: null,
    sortable: true,
    grow: 2,
  },
  {
    name: "At Risk",
    cell: null,
    sortable: true,
    grow: 2,
  },
  {
    name: "Valid Withdrawals",
    cell: null,
    sortable: true,
    grow: 2,
  },
  {
    name: "Dropout",
    cell: null,
    sortable: true,
    grow: 2,
  },
  {
    name: "Total",
    cell: null,
    sortable: true,
    grow: 2,
  },
  {
    name: "Graduation Rate",
    selector: "Graduation Rate",
    sortable: true,
    grow: 2,
  },
];

export const cohortGraduationStudentProjection = [
  {
    name: "Person Id",
    selector: "personID",
    omit: true,
  },
  {
    name: "Student Name",
    selector: "studentName",
    sortable: true,
    cell: (row) => (
      <div style={{ color: row.endDate ? "red" : "black" }}>
        {row.studentName}
      </div>
    ),
    width: "170px",
  },
  {
    name: "Student#",
    cell: null,
    sortable: true,
    width: "100px",
  },
  {
    name: "Grade",
    selector: "grade",
    sortable: true,
    width: "50px",
  },
  {
    name: "9th Grade Date",
    selector: "grade9Date",
    sortable: true,
    width: "105px",
  },
  {
    name: "SPED",
    selector: "sped",
    sortable: true,
    width: "50px",
  },
  {
    name: "EL",
    selector: "EL",
    omit: true,
  },
  {
    name: "With Code/Date",
    selector: "withCodeDate",
    sortable: true,
    width: "135px",
  },
  {
    name: "Diploma Type",
    selector: "diplomaType",
    sortable: true,
    width: "80px",
  },
  {
    name: (
      <>
        <div
          style={{
            marginLeft: "40%",
            paddingBottom: "10px",
            textDecoration: "underline",
          }}
        >
          Credits
        </div>
        <div style={{ display: "inline-flex" }}>
          <div style={{ minWidth: "55px" }}>{"Total"}</div>
          <div style={{ minWidth: "45px" }}>{"LA"}</div>
          <div style={{ minWidth: "45px" }}>{"MA"}</div>
          <div style={{ minWidth: "45px" }}>{"SC"}</div>
          <div style={{ minWidth: "45px" }}>{"SS"}</div>
          <div style={{ minWidth: "45px" }}>{"FL"}</div>
          <div style={{ minWidth: "55px" }}>{"Hlth/PE"}</div>
          <div style={{ minWidth: "55px" }}>{"CTAE"}</div>
          <div style={{ minWidth: "65px" }}>{"Rigor"}</div>
        </div>
      </>
    ),
    field: "Score",
    selector: "Score",
    sortable: true,
    width: "455px",
  },
  // {
  //   name: "SC",
  //   selector: "SC",
  //   sortable: true,
  // },
  // {
  //   name: "SS",
  //   selector: "SS",
  //   sortable: true,
  // },
  // {
  //   name: "FL",
  //   selector: "FL",
  //   sortable: true,
  // },
  // {
  //   name: "Hlth/PF",
  //   selector: "HLPE",
  //   sortable: true,
  // },
  // {
  //   name: "CTAE",
  //   selector: "CTAE",
  //   sortable: true,
  // },
  // {
  //   name: "Rigor",
  //   selector: "Rigor",
  //   sortable: true,
  // },
  {
    name: "Reason",
    selector: "Reason",
    sortable: true,
    width: "450px",
  },
];

export const cohortGraduationStudentDetails = [
  {
    name: "Year",
    selector: "schoolYear",
    sortable: true,
    width: "80px",
  },
  {
    name: "Term",
    selector: "term",
    sortable: true,
    width: "80px",
  },
  {
    name: "Grade",
    selector: "grade",
    sortable: true,
    width: "80px",
  },
  {
    name: "Course",
    selector: "stateCode",
    sortable: true,
    width: "110px",
  },
  {
    name: "Title",
    selector: "courseName",
    sortable: true,
    width: "200px",
  },
  {
    name: "Score",
    selector: "score",
    sortable: true,
    width: "80px",
  },
  {
    name: "Attempted",
    selector: "creditsAttempted",
    sortable: true,
    cell: (row) => (
      <div style={{ fontWeight: 600 }}>
        {isNaN(Number(row.creditsAttempted).toFixed(3))
          ? null
          : Number(row.creditsAttempted).toFixed(3)}
      </div>
    ),
    width: "110px",
  },
  {
    name: "Earned",
    selector: "creditsEarned",
    sortable: true,
    cell: (row) => (
      <div style={{ fontWeight: 600 }}>
        {isNaN(Number(row.creditsEarned).toFixed(3))
          ? null
          : Number(row.creditsEarned).toFixed(3)}
      </div>
    ),
    width: "80px",
  },
  {
    name: "LA",
    selector: "LA",
    sortable: true,
    width: "60px",
  },
  {
    name: "MA",
    selector: "MA",
    sortable: true,
    width: "60px",
  },
  {
    name: "SC",
    selector: "SC",
    sortable: true,
    width: "60px",
  },
  {
    name: "SS",
    selector: "SS",
    sortable: true,
    width: "60px",
  },
  {
    name: "FL",
    selector: "FL",
    sortable: true,
    width: "60px",
  },
  {
    name: "HLPE",
    selector: "HLPE",
    sortable: true,
    width: "60px",
  },
  {
    name: "CTAE",
    selector: "CTAE",
    sortable: true,
    width: "60px",
  },
  {
    name: "ROTC",
    selector: "ROTC",
    sortable: true,
    width: "60px",
  },
  {
    name: "Rigor",
    selector: "Rigor",
    sortable: true,
    width: "60px",
  },
];

export const systemClassesDetails = [
  {
    name: "School",
    selector: "name",
    sortable: true,
    grow: 4,
  },
  {
    name: "No. of Classes",
    selector: "classes",
    sortable: true,
    grow: 2,
  },
  {
    name: "No. of Students",
    selector: "students",
    sortable: true,
    grow: 2,
  },
  {
    name: "As",
    selector: "grade_a",
    sortable: true,
  },
  {
    name: "Bs",
    selector: "grade_b",
    sortable: true,
  },
  {
    name: "Cs",
    selector: "grade_c",
    sortable: true,
  },
  {
    name: "Fs",
    selector: "grade_f",
    sortable: true,
  },
];

export const teacherDetails = [
  {
    name: "Teacher",
    selector: "name",
    sortable: true,
    grow: 3,
  },
  {
    name: "No. of Classes",
    selector: "classes",
    sortable: true,
    grow: 2,
  },
  {
    name: "No. of Students",
    selector: "students",
    sortable: true,
    grow: 2,
  },
  {
    name: "As",
    selector: "grade_a",
    sortable: true,
  },
  {
    name: "Bs",
    selector: "grade_b",
    sortable: true,
  },
  {
    name: "Cs",
    selector: "grade_c",
    sortable: true,
  },
  {
    name: "Fs",
    selector: "grade_f",
    sortable: true,
  },
];

export const analyzerDetails = [
  {
    name: "Course",
    selector: "baseCourse",
    sortable: true,
    grow: 2,
  },
  {
    name: "Title",
    selector: "name",
    sortable: true,
    grow: 4,
  },
  {
    name: "Sections",
    selector: "classes",
    sortable: true,
  },
  {
    name: "Students",
    selector: "students",
    sortable: true,
  },
  {
    name: "As",
    selector: "grade_a",
    sortable: true,
  },
  {
    name: "Bs",
    selector: "grade_b",
    sortable: true,
  },
  {
    name: "Cs",
    selector: "grade_c",
    sortable: true,
  },
  {
    name: "Fs",
    selector: "grade_f",
    sortable: true,
  },
];

export const teacherClassesDetails = [
  {
    name: "Section",
    selector: "name",
    sortable: true,
    grow: 4,
  },
  {
    name: "No. Of Students",
    selector: "students",
    sortable: true,
    grow: 2,
  },
  {
    name: "As",
    selector: "grade_a",
    sortable: true,
    left: true,
  },
  {
    name: "Bs",
    selector: "grade_b",
    sortable: true,
    left: true,
  },
  {
    name: "Cs",
    selector: "grade_c",
    sortable: true,
    left: true,
  },
  {
    name: "Fs",
    selector: "grade_f",
    sortable: true,
    left: true,
  },
];

export const courseClassesDetails = [
  {
    name: "Section",
    selector: "name",
    sortable: true,
    grow: 3,
  },
  {
    name: "Teacher",
    selector: "tchname",
    sortable: true,
    grow: 2,
  },
  {
    name: "Students",
    selector: "students",
    sortable: true,
  },
  {
    name: "As",
    selector: "grade_a",
    sortable: true,
  },
  {
    name: "Bs",
    selector: "grade_b",
    sortable: true,
  },
  {
    name: "Cs",
    selector: "grade_c",
    sortable: true,
  },
  {
    name: "Fs",
    selector: "grade_f",
    sortable: true,
  },
];

export const studentDetails = [
  {
    name: "Permnum",
    selector: "permnum",
    sortable: true,
  },
  {
    name: "Name",
    selector: "name",
    sortable: true,
    grow: 2,
  },
  {
    name: "Letter Grade",
    selector: "grade",
    sortable: true,
  },
  {
    name: "Numeric",
    selector: "score",
    sortable: true,
  },
];

export const studentGradeDetails = [
  {
    name: "Section",
    selector: "name",
    sortable: true,
  },
  {
    name: "Teacher",
    selector: "tchname",
    sortable: true,
  },
  {
    name: "Students",
    selector: "students",
    sortable: true,
  },
  {
    name: "As",
    selector: "grade_a",
    sortable: true,
  },
  {
    name: "Bs",
    selector: "grade_b",
    sortable: true,
  },
  {
    name: "Cs",
    selector: "grade_c",
    sortable: true,
  },
  {
    name: "Fs",
    selector: "grade_f",
    sortable: true,
  },
];

export const behaviourIncidents = [
  {
    name: "Code",
    selector: "IncidentCode",
    sortable: true,
  },
  {
    name: "Incident Description",
    selector: "IncidentDescription",
    sortable: true,
    grow: 5,
  },
  {
    name: "# of Students",
    selector: "IncidentCount",
    sortable: true,
    grow: 2,
  },
];

export const behaviourActions = [
  {
    name: "Code",
    selector: "ActionCode",
    sortable: true,
  },
  {
    name: "Action Description",
    selector: "ActionDescription",
    sortable: true,
    grow: 5,
  },
  {
    name: "# of Students",
    selector: "ActionCount",
    sortable: true,
    grow: 2,
  },
];

export const behaviorStudents = [
  {
    name: "Student#",
    selector: "studentNumber",
    sortable: true,
    grow: 1,
  },
  {
    name: "Student Name",
    selector: "studentName",
    sortable: true,
    grow: 4,
  },
  {
    name: "Race",
    selector: "race",
    sortable: true,
    grow: 1,
  },
  {
    name: "Incident Date",
    selector: "incidentDate",
    sortable: true,
    grow: 3,
  },
  {
    name: "Incident Type",
    selector: "incTitle",
    sortable: true,
    grow: 3,
  },
  {
    name: "Resolution",
    selector: "resTitle",
    sortable: true,
    grow: 5,
  },
  {
    name: "SuspDays",
    selector: "duration",
    sortable: true,
    grow: 1,
  },
];

export const behaviorSchools = [
  {
    name: "School",
    selector: "name",
    sortable: true,
    grow: 2,
  },
  {
    name: "# of Incidents",
    selector: "records",
    sortable: true,
  },
];

export const studentsAtRiskColumns = [
  {
    name: "Srl",
    selector: "id",
    sortable: true,
    width: "60px",
  },
  {
    name: "Student#",
    selector: "studentNumber",
    cell: null,
    sortable: true,
    width: "110px",
  },
  {
    name: "Name",
    selector: "studentName",
    sortable: true,
    width: "220px",
  },
  {
    name: "Grade",
    selector: "grade",
    sortable: true,
    width: "50px",
  },
  {
    name: "Enroll Date",
    selector: "startDate",
    sortable: true,
    width: "120px",
  },
  {
    name: "Age",
    selector: "age",
    sortable: true,
    width: "50px",
  },
  {
    name: "SPED",
    selector: "sped",
    sortable: true,
    width: "50px",
  },
  {
    name: "EL",
    selector: "EL",
    sortable: true,
    width: "50px",
  },
  {
    name: "504 ",
    selector: "sec504",
    sortable: true,
    width: "50px",
  },
  {
    name: "SST",
    selector: "sst",
    sortable: true,
    width: "50px",
  },
  {
    name: "ED",
    selector: "mealStatus",
    sortable: true,
    width: "50px",
  },
  {
    name: "Absences",
    selector: "absences",
    sortable: true,
    cell: (row) => (
      <div style={{ color: Number(row.absences) > 15 ? "#e62e2d" : "#000000" }}>
        {row.absences}
      </div>
    ),
    width: "70px",
  },
  {
    name: "Unex Abs",
    selector: "unex",
    sortable: true,
    width: "50px",
  },
  {
    name: "Attendance",
    selector: "ewsAttend",
    cell: (row) => <div style={{ color: "#e62e2d" }}>{row.ewsAttend}</div>,
    width: "70px",
  },
  {
    name: "Course",
    selector: "ewsCourse",
    sortable: true,
    cell: (row) => <div style={{ color: "#e62e2d" }}>{row.ewsCourse}</div>,
    width: "50px",
  },
  {
    name: "Behavior",
    selector: "ewsBehavior",
    sortable: true,
    cell: (row) => <div style={{ color: "#e62e2d" }}>{row.ewsBehavior}</div>,
    width: "70px",
  },
  {
    name: "Retain",
    selector: "ewsRetain",
    sortable: true,
    cell: (row) => <div style={{ color: "#e62e2d" }}>{row.ewsRetain}</div>,
    width: "50px",
  },
  {
    name: "Milestones",
    selector: "ewsEOG",
    sortable: true,
    cell: (row) => <div style={{ color: "#e62e2d" }}>{row.ewsEOG}</div>,
    width: "100px",
  },
];

export const ewsStudentsDetailsColumns = {
  attendance: [
    {
      name: "Seq",
      selector: "seq",
      sortable: true,
      width: "80px",
    },
    {
      name: " Absence Date",
      selector: "absDate",
      cell: (row) => (
        <div>{Utilities.formatDateSlash(row.absDate, "mm/dd/yyyy")}</div>
      ),
      sortable: true,
      width: "180px",
    },
    {
      name: "Absence Code",
      selector: "absCode",
      sortable: true,
    },
    {
      name: "School",
      selector: "schname",
      sortable: true,
    },
  ],
  behavior: [
    /*{
      name: "Student#",
      selector: "studentNumber",
      sortable: true,
      grow: 2,
    },
    {
      name: "Student Name",
      selector: "studentName",
      sortable: true,
      grow: 2,
    },*/
    {
      name: "Incident Date",
      selector: "incidentDate",
      sortable: true,
      width: "160px",
    },
    {
      name: "Incident",
      selector: "incTitle",
      sortable: true,
      grow: 3,
    },
    {
      name: "Resolution",
      selector: "resTitle",
      sortable: true,
      grow: 4,
    },
    {
      name: "Duration (in days)",
      selector: "duration",
      sortable: true,
    },
  ],
  course: [
    {
      name: "Section",
      selector: "section",
      sortable: true,
      grow: 2,
      cell: (row) => (
        <text
          style={{
            fontWeight: row.isRequired === "1" ? 600 : 400,
          }}
        >
          {row.section}
        </text>
      ),
    },
    {
      name: "Title",
      selector: "title",
      sortable: true,
      grow: 5,
      cell: (row) => (
        <text
          style={{
            fontWeight: row.isRequired === "1" ? 600 : 400,
          }}
        >
          {row.title}
        </text>
      ),
    },
    {
      name: "Teacher",
      selector: "teacher",
      sortable: true,
      grow: 3,
    },
    {
      name: "Alpha Grade",
      selector: "grade",
      sortable: true,
    },
    {
      name: "Score",
      selector: "score",
      sortable: true,
      cell: (row) => (
        <text
          style={{
            color: row.failing === "X" ? "red" : "black",
            fontWeight: row.isRequired === "1" ? 600 : 400,
          }}
        >
          {row.score}
        </text>
      ),
    },
  ],
  grades: [
    {
      name: "Course",
      selector: "course",
      sortable: true,
      grow: 4,
    },
    {
      name: "Course Title",
      selector: "courseName",
      sortable: true,
      grow: 3,
    },
    {
      name: "Grade",
      selector: "score",
      sortable: true,
    },
    {
      name: "Date",
      selector: "date",
      sortable: true,
      grow: 2,
    },
  ],
  test: [
    {
      name: "Person#",
      selector: "personID",
      sortable: true,
    },
    {
      name: "Subject",
      selector: "subject",
      sortable: true,
    },
    {
      name: "Testing Grade",
      selector: "testingGrade",
      sortable: true,
    },
    {
      name: "Scale Score",
      selector: "scaleScore",
      sortable: true,
      cell: (row) => <div style={{ color: row.color }}>{row.scaleScore}</div>,
    },
    {
      name: "Lexile Score",
      selector: "lexileScore",
      sortable: true,
    },
  ],
};

export const enrollmentColumns = [
  {
    name: "Student#",
    selector: "studentNumber",
    cell: null,
    sortable: true,
    width: "110px",
  },
  {
    name: "Name",
    selector: "studentName",
    sortable: true,
    cell: (row) => (
      <div style={{ color: row.endDate ? "#e62e2d" : "#000000" }}>
        {row.studentName}
      </div>
    ),
    width: "220px",
  },
  {
    name: "Grade",
    selector: "grade",
    sortable: true,
    width: "50px",
  },
  {
    name: "Race",
    selector: "race",
    sortable: true,
    width: "50px",
  },
  {
    name: "504 ",
    selector: "sec504",
    sortable: true,
    width: "50px",
  },
  {
    name: "SPED",
    selector: "sped",
    sortable: true,
    width: "50px",
  },

  {
    name: "Enroll Date",
    selector: "startDate",
    sortable: true,
    width: "120px",
  },
  {
    name: "Leave Date",
    selector: "endDate",
    sortable: true,
    width: "120px",
  },
  {
    name: "Leave Code",
    selector: "endStatus",
    sortable: true,
    width: "60px",
  },
  {
    name: "Total Abs",
    selector: "absences",
    sortable: true,
    width: "60px",
  },
  {
    name: "Unex Abs",
    selector: "unexcused",
    sortable: true,
    width: "50px",
  },
  {
    name: "% Abs of Enrolled",
    selector: "absPercent",
    sortable: true,
    width: "100px",
  },
  {
    name: "HR Teacher",
    selector: "teacher",
    sortable: true,
    width: "200px",
  },
];

export const AbsencesColumns = [
  {
    name: "Absence Date",
    selector: "absDate",
    cell: (row) => (
      <div>{Utilities.formatDateSlash(row.absDate, "mm/dd/yyyy")}</div>
    ),
    sortable: true,
    grow: 2,
  },
  {
    name: "Absence Code",
    selector: "absCode",
    sortable: true,
  },
  {
    name: "School Name",
    selector: "schname",
    sortable: true,
    grow: 2,
  },
];

export const StudentGradesColumns = [
  {
    name: "Task Name",
    selector: "groupName",
    sortable: true,
    width: "400px",
  },
  {
    name: "Due Date",
    selector: "dueDate",
    cell: null,
    sortable: true,
    width: "120px",
  },
  {
    name: "Assigned Date",
    selector: "assignedDate",
    sortable: true,
    width: "150px",
  },
  {
    name: "Score",
    selector: "score",
    sortable: true,
    width: "100px",
  },
  {
    name: "Out of",
    selector: "totalPoints",
    sortable: true,
    width: "100px",
  },
];

export const ProgressGradeStudentDetails = [
  {
    name: "Srl",
    selector: "id",
    sortable: true,
    width: "50px",
  },
  {
    name: "Student #",
    selector: "studentNumber",
    width: "120px",
    sortable: true,
  },
  {
    name: "Student Name",
    selector: "studentName",
    width: "230px",
    sortable: true,
  },
  {
    name: "Gender",
    selector: "gender",
    width: "50px",
    sortable: true,
  },
  {
    name: "Grade",
    selector: "grade",
    //cell: (row) => Number(row.grade),
    width: "50px",
    sortable: true,
  },
  {
    name: "SPED",
    selector: "sped",
    width: "50px",
    sortable: true,
  },
  {
    name: "EL",
    selector: "EL",
    width: "50px",
    sortable: true,
  },
  {
    name: "Absences",
    selector: "absences",
    width: "70px",
    sortable: true,
  },
  {
    name: "Susp Evt",
    selector: "ewsBehavior",
    width: "85px",
    sortable: true,
  },
  {
    name: "Passing All",
    selector: "notFailingAny",
    width: "100px",
    cell: (row) => (
      <div style={{ color: row.notFailingAny === "N" ? "red" : "black" }}>
        {row.notFailingAny}
      </div>
    ),
    sortable: true,
  },
  {
    name: "Failing LA",
    selector: "failingLA",
    width: "50px",
    cell: (row) => (
      <div style={{ color: row.failingLA === "N" ? "black" : "red" }}>
        {row.failingLA}
      </div>
    ),
    sortable: true,
  },
  {
    name: "Failing MA",
    selector: "failingMA",
    width: "50px",
    cell: (row) => (
      <div style={{ color: row.failingMA === "N" ? "black" : "red" }}>
        {row.failingMA}
      </div>
    ),
    sortable: true,
  },
  {
    name: "Failing SC",
    selector: "failingSC",
    width: "50px",
    cell: (row) => (
      <div style={{ color: row.failingSC === "N" ? "black" : "red" }}>
        {row.failingSC}
      </div>
    ),
    sortable: true,
  },
  {
    name: "Failing SS",
    selector: "failingSS",
    width: "50px",
    cell: (row) => (
      <div style={{ color: row.failingSS === "N" ? "black" : "red" }}>
        {row.failingSS}
      </div>
    ),
    sortable: true,
  },
  {
    name: "Failing RD",
    selector: "failingRD",
    width: "50px",
    cell: (row) => (
      <div style={{ color: row.failingRD === "N" ? "black" : "red" }}>
        {row.failingRD}
      </div>
    ),
    sortable: true,
  },
  {
    name: "Failing FL",
    selector: "failingFL",
    width: "50px",
    cell: (row) => (
      <div style={{ color: row.failingFL === "N" ? "black" : "red" }}>
        {row.failingFL}
      </div>
    ),
    sortable: true,
  },
  {
    name: "# of Failing Grades",
    selector: "numFailing",
    width: "100px",
    sortable: true,
  },
  {
    name: "# of Total Grades",
    selector: "numGrades",
    width: "100px",
    sortable: true,
  },
];

export const HonorRollStudentDetails = [
  {
    name: "Student #",
    selector: "studentNumber",
    sortable: true,
    width: "120px",
  },
  {
    name: "Student Name",
    selector: "studentName",
    sortable: true,
    width: "230px",
  },
  {
    name: "Gender",
    selector: "gender",
    sortable: true,
    width: "60px",
  },
  {
    name: "Grade",
    selector: "grade",
    //cell: (row) => Number(row.grade),
    sortable: true,
    width: "60px",
  },
  {
    name: "SPED",
    selector: "sped",
    sortable: true,
    width: "60px",
  },
  {
    name: "EL",
    selector: "EL",
    sortable: true,
    width: "60px",
  },
  {
    name: "Lang Arts",
    selector: "LA",
    sortable: true,
    width: "120px",
  },
  {
    name: "Math",
    selector: "MA",
    sortable: true,
    width: "60px",
  },
  {
    name: "Science",
    selector: "SC",
    sortable: true,
    width: "60px",
  },
  {
    name: "Soc Studies",
    selector: "SS",
    sortable: true,
    width: "120px",
  },
  {
    name: "Other",
    selector: "OT",
    sortable: true,
    width: "60px",
  },
];

export const ProgressGradeDetails = [
  {
    name: "Section",
    selector: "section",
    width: "140px",
  },
  {
    name: "Course Title",
    selector: "title",
    width: "200px",
  },
  {
    name: "Teacher",
    selector: "teacher",
    width: "240px",
  },
  {
    name: "Score",
    selector: "score",
    width: "70px",
    cell: (row) => (
      <div style={{ color: row.score < 70 ? "red" : "black" }}>{row.score}</div>
    ),
  },
];

export const GradingDistributionColumnsDetails = [
  {
    name: "Teacher",
    selector: "teacherName",
    cell: (row) =>
      row.title === "All Courses" ? (
        <strong>{row.teacherName}</strong>
      ) : (
        <div></div>
      ),
    width: "250px",
  },
  {
    name: "Course Name",
    selector: "title",
    cell: (row) =>
      row.title === "All Courses" ? (
        <strong>{row.title}</strong>
      ) : (
        <div>{row.title}</div>
      ),
    width: "250px",
  },
  {
    name: "Course#",
    selector: "courseNumber",
    cell: (row) =>
      row.title === "All Courses" ? (
        <strong>{row.courseNumber}</strong>
      ) : (
        <div>{row.courseNumber}</div>
      ),
  },
  {
    name: "No Of A's",
    selector: "A",
    cell: (row) =>
      row.title === "All Courses" ? (
        <strong>{row.A}</strong>
      ) : (
        <div>{row.A}</div>
      ),
  },
  {
    name: "No Of B's",
    selector: "B",
    cell: (row) =>
      row.title === "All Courses" ? (
        <strong>{row.B}</strong>
      ) : (
        <div>{row.B}</div>
      ),
  },
  {
    name: "No Of C's",
    selector: "C",
    cell: (row) =>
      row.title === "All Courses" ? (
        <strong>{row.C}</strong>
      ) : (
        <div>{row.C}</div>
      ),
  },
  {
    name: "No Of F's",
    selector: "F",
    cell: (row) =>
      row.title === "All Courses" ? (
        <strong>{row.F}</strong>
      ) : (
        <div>{row.F}</div>
      ),
  },
  {
    name: "Total",
    selector: "Z",
    cell: (row) =>
      row.title === "All Courses" ? (
        <strong>{row.Z}</strong>
      ) : (
        <div>{row.Z}</div>
      ),
  },
];

export const pbisColumns = {
  summary: {
    enrolledColumns: [
      {
        name: "Total # of Students",
        selector: "Z",
      },
      {
        name: " # of EL Students",
        selector: "EL",
      },
      {
        name: "# of Free/Reduced Meal Eligible",
        selector: "FRM",
      },
      {
        name: "Total # Students with Disabilities",
        selector: "SPED",
      },
    ],
    suspRateColumns: [
      {
        name: "Total Students",
        selector: "Z",
        id: "Z",
      },
      {
        name: "Am. Indian",
        selector: "I",
        id: "I",
      },
      {
        name: "Asian",
        selector: "A",
        id: "A",
      },
      {
        name: "Black",
        selector: "B",
        id: "B",
      },
      {
        name: "Hispanic",
        selector: "H",
        id: "H",
      },
      {
        name: "White",
        selector: "W",
        id: "W",
      },
      {
        name: "Multi-Racial",
        selector: "M",
        id: "M",
      },
      {
        name: "Pacific Islander",
        selector: "P",
        id: "P",
      },
    ],
    raceColumns: [
      {
        name: "Am. Indian",
        selector: "I",
      },
      {
        name: "Asian",
        selector: "A",
      },
      {
        name: "Black",
        selector: "B",
      },
      {
        name: "Hispanic",
        selector: "H",
      },
      {
        name: "White",
        selector: "W",
      },
      {
        name: "Multi-Racial",
        selector: "M",
      },
      {
        name: "Pacific Islander",
        selector: "P",
      },
    ],
    attendanceColumns: [
      {
        name: "",
        selector: "race",
      },
      {
        name: "# of Students",
        selector: "Z",
      },
      {
        name: "Absent 5 or fewer days",
        selector: "ATTD-LT5",
      },
      {
        name: "Absent 6 to 15 days",
        selector: "ATTD-LT15",
      },
      {
        name: "Absent more than 15 days",
        selector: "ATTD-MT15",
      },
    ],
    discipColumns: [
      {
        name: "",
        selector: "detail",
      },
      {
        name: "",
        selector: "count",
        //cell: (row) => <a href="#">{row.count}</a>,
        sortable: true,
      },
    ],
  },
  avg: [
    {
      name: "Month",
      selector: "month",
    },
    {
      name: "Days Count",
      selector: "schoolDays",
    },
    {
      name: "Referral Counts",
      selector: "incidents",
    },
    {
      name: "ODR/School Day",
      selector: "avgPerDay",
    },
  ],
  time: [
    {
      name: "Time",
      selector: "label",
    },
    {
      name: "Frequency",
      selector: "incidents",
    },
    {
      name: "Proportions",
      selector: "proportion",
    },
  ],
  weekday: [
    {
      name: "Day",
      selector: "label",
    },
    {
      name: "Frequency",
      selector: "incidents",
    },
    {
      name: "Proportion (%)",
      selector: "proportion",
    },
  ],
  type: [
    {
      name: "Problem Behavior",
      selector: "label",
    },
    {
      name: "Frequency",
      selector: "incidents",
    },
    {
      name: "Proportion (%)",
      selector: "proportion",
    },
  ],
  location: [
    {
      name: "Location",
      selector: "label",
    },
    {
      name: "Frequency",
      selector: "incidents",
    },
    {
      name: "Proportion (%)",
      selector: "proportion",
    },
  ],
  grade: [
    {
      name: "Grade",
      selector: "label",
    },
    {
      name: "Frequency",
      selector: "incidents",
    },
    {
      name: "Proportion (%)",
      selector: "proportion",
    },
  ],
  reporter: [
    {
      name: "Reported BY",
      selector: "label",
    },
    {
      name: "Frequency",
      selector: "incidents",
    },
    {
      name: "Proportion (%)",
      selector: "proportion",
    },
  ],
};

export const pibStudentsColumns = [
  {
    name: "Student #",
    selector: "studentNumber",
    width: "100px",
    // cell: (row) => <a href="#">{row.studentNumber}</a>,
  },
  {
    name: "Name",
    selector: "studentName",
    width: "220px",
  },
  {
    name: "Grade",
    selector: "grade",
    width: "60px",
  },
  {
    name: "Race",
    selector: "raceFederal",
    width: "60px",
  },
  {
    name: "Gender",
    selector: "gender",
    width: "60px",
  },
  {
    name: "Birth Date",
    selector: "birthdate",
    width: "110px",
  },
  {
    name: "EL",
    selector: "EL",
    width: "40px",
  },
  {
    name: "SPED",
    selector: "sped",
    width: "40px",
  },
  {
    name: "Absences",
    selector: "abs",
    width: "80px",
  },
  {
    name: "Meal Type",
    selector: "mealStatus",
    width: "60px",
  },
  {
    name: "# of Incidents",
    selector: "#incidents",
    width: "100px",
  },
  {
    name: "# ISS Incidents",
    selector: "#iss",
    width: "100px",
  },
  {
    name: "# of OSS Incidents",
    selector: "#oss",
    width: "100px",
  },

  {
    name: "ISS Days",
    selector: "days_iss",
    width: "70px",
  },
  {
    name: "OSS Days",
    selector: "days_oss",
    width: "70px",
  },
];

export const pbisTabModalColumns = [
  {
    name: "Student #",
    selector: "studentNumber",
    width: "100px",
  },
  {
    name: "Name",
    selector: "studentName",
    width: "250px",
  },
  {
    name: "Grade",
    selector: "grade",
    width: "60px",
  },
  {
    name: "Inc #",
    selector: "incidentID",
    width: "80px",
  },
  {
    name: "Inc Date",
    selector: "incidentDate",
    width: "150px",
  },
  {
    name: "Inc Title",
    selector: "incTitle",
  },
  {
    name: "Resolution",
    selector: "resTitle",
    width: "200px",
  },
  {
    name: "Location",
    selector: "location",
    width: "100px",
  },
  {
    name: "Susp Days",
    selector: "duration",
    width: "90px",
  },
];

export const ctaeSummaryColumns = [
  {
    name: "Cluster",
    selector: "ClusterName",
    grow: 3,
  },
  {
    name: "Pathway",
    selector: "Pathway",
    grow: 4,
  },
  {
    name: "Courses",
    selector: "Courses",
    grow: 3,
  },
  {
    name: "# of Students",
    selector: "students",
    //cell: (row) => <a href="#">{row.students}</a>,
  },
];

export const ctaeDetailsColumns = [
  {
    name: "Cluster",
    selector: "ClusterName",
    grow: 3,
  },
  {
    name: "GTID",
    selector: "GTID",
    grow: 4,
  },
  {
    name: "Grade",
    selector: "Grade",
    grow: 3,
  },
  {
    name: "Graduation Date",
    selector: "Graduation Date",
  },
  {
    name: "Pathway_Code",
    selector: "Pathway_Code",
    grow: 2,
  },
  {
    name: "Pathway",
    selector: "Pathway",
    grow: 4,
  },
  {
    name: "School Name",
    selector: "School Name",
    grow: 3,
  },
  {
    name: "Year Completed",
    selector: "Year Completed",
  },
  {
    name: "Name",
    selector: "name",
    grow: 4,
  },
  {
    name: "Birth Date",
    selector: "Birth Date",
  },
  {
    name: "SPED",
    selector: "sped",
  },
  {
    name: "Race",
    selector: "race",
  },
  {
    name: "Cohort Year",
    selector: "cohortYear",
  },
  {
    name: "school ID",
    selector: "schoolID",
    grow: 3,
  },
  {
    name: "Student#",
    selector: "studentNumber",
  },
  {
    name: "Term Ending",
    selector: "termName",
  },
  {
    name: "EOPA Exam",
    selector: "examCode",
  },
  {
    name: "EOPA Pass/Fail",
    selector: "examResult",
  },
  {
    name: "EOPA Exam Date",
    selector: "examDate",
  },
];

export const ctaePathWayColumns = [
  {
    name: "Student#",
    selector: "studentNumber",
    //cell: (row) => <a href="#">{row.studentNumber}</a>,
    width: "150px",
  },
  {
    name: "Name",
    selector: "studentName",
  },
  {
    name: "Grade",
    selector: "grade",
    width: "80px",
  },
  {
    name: "Course/Section",
    selector: "Course",
  },
  {
    name: "Level",
    selector: "level",
    width: "80px",
  },
  {
    name: "Title",
    selector: "title",
  },
  {
    name: "Teacher",
    selector: "teacher",
  },
];

export const ctaeInfoColumns = [
  {
    name: "Course",
    selector: "Pathway",
    cell: (row) => <div>{row["Pathway"] + "(" + row["Courses"] + ")"}</div>,
    width: "470px",
  },
  {
    name: "Grade",
    selector: "grade",
    width: "60px",
    cell: (row) => <div style={{ visibility: "hidden" }}>{row.grade}</div>,
  },
  {
    name: "Level",
    selector: "level",
    width: "60px",
    cell: (row) => <div style={{ visibility: "hidden" }}>{row.level}</div>,
  },
  {
    name: "Year",
    selector: "endYear",
    width: "70px",
    cell: (row) => <div style={{ visibility: "hidden" }}>{row.endYear}</div>,
  },
  {
    name: "Credit",
    selector: "creditsEarned",
    width: "60px",
    cell: (row) => (
      <div style={{ visibility: "hidden" }}>{row.creditsEarned}</div>
    ),
  },
  {
    name: "Status",
    selector: "status",
    cell: (row) => <div style={{ visibility: "hidden" }}>{row.status}</div>,
  },
];
